﻿using APP.Core.Utilities;
using Project.Template.Web.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

public static class MenuUtility
{
    /// <summary>
    /// 获取当前菜单列表（面包屑列表）
    /// </summary>
    /// <param name="menus"></param>
    /// <returns></returns>
    public static IEnumerable<MenuInfo> GetCurrentMenuList(IEnumerable<MenuInfo> menus)
    {
        if (menus == null || !menus.Any())
            return new MenuInfo[0];

        var requestPath = ClientRequestUtility.RequestRealPath;
        var currentMenus = new List<MenuInfo>();

        Func<IEnumerable<MenuInfo>, bool> _handle = null;
        _handle = new Func<IEnumerable<MenuInfo>, bool>(ms =>
        {
            return ms.Any(m =>
            {
                // 当前菜单项是否对应请求路径
                if (m.Privilege.HasValue && m.Url == requestPath)
                {
                    currentMenus.Insert(0, m);
                    return true;
                }
                // 是否有子菜单项对应请求路径
                else if (m.SubMenuList != null && m.SubMenuList.Any())
                {
                    var r = _handle(m.SubMenuList);
                    if (r) currentMenus.Insert(0, m);
                    return r;
                }

                return false;
            });
        });
        _handle(menus);

        return currentMenus;
    }

    /// <summary>
    /// 生成菜单列表
    /// </summary>
    /// <param name="htmlHelper"></param>
    /// <param name="userMenus">用户菜单列表</param>
    /// <param name="currentMenus">当前菜单列表（面包屑列表）</param>
    /// <returns></returns>
    public static MvcHtmlString MenuListFor(this HtmlHelper htmlHelper, IEnumerable<MenuInfo> userMenus, IEnumerable<MenuInfo> currentMenus)
    {
        if (userMenus == null || !userMenus.Any(m => m.IsVisible))
            return MvcHtmlString.Empty;

        var html = new StringBuilder();
        foreach (var menu in userMenus.Where(m => m.IsVisible))
        {
            var hasSubMenus = menu.SubMenuList != null && menu.SubMenuList.Any(sm => sm.IsVisible);
            if (!menu.Privilege.HasValue && !hasSubMenus)
                continue;

            var isCurrent = currentMenus.Any(m => m.MenuId == menu.MenuId);
            if (hasSubMenus)
            {
                html.AppendFormat(@"
<li{0}>
    <a href=""{1}"">
        {2}
        <span class=""title"">{3}</span>
        <span class=""icon-arrow""></span>
    </a>
    <ul class=""sub-menu"">
        {4}
    </ul>
</li>",
                    isCurrent ? @" class=""active open""" : string.Empty,
                    CommonSetting.VALUE_HTML_VOIDLINK,
                    menu.Icon.HasValue() ? string.Format(@"<i class=""{0}""></i>", menu.Icon) : string.Empty,
                    menu.Name,
                    MenuListFor(htmlHelper, menu.SubMenuList, currentMenus).ToString()
                );
            }
            else
            {
                html.AppendFormat(@"
<li{0}>
    <a href=""{1}"">
        {2}
        <span class=""title"">{3}</span>
    </a>
</li>",
                    isCurrent ? @" class=""active""" : string.Empty,
                    menu.Url ?? CommonSetting.VALUE_HTML_VOIDLINK,
                    menu.Icon.HasValue() ? string.Format(@"<i class=""{0}""></i>", menu.Icon) : string.Empty,
                    menu.Name
                );
            }
        }

        return MvcHtmlString.Create(html.ToString());
    }
}