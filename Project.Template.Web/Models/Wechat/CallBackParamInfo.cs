﻿using Senparc.Weixin.MP;

namespace Project.Template.Web.Models.Wechat
{
    public class CallBackParamInfo
    {
        public OAuthScope Scope { get; set; }

        public string RedirectUrl { get; set; }

        public string Transfer { get; set; }
    }
}