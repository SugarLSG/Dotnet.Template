﻿using APP.Core.Models;
using System.ComponentModel;

namespace Framework.Models.Role
{
    public class SearchCondition : PagingCondition
    {
        /// <summary>
        /// 名称
        /// </summary>
        [DisplayName("名称")]
        public string Name { get; set; }
    }
}
