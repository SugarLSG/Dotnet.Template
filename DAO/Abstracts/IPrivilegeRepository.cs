﻿using Framework.Privilege;
using System.Collections.Generic;

namespace DAO.Abstracts
{
    public interface IPrivilegeRepository
    {
        /// <summary>
        /// 获取所有权限Code
        /// </summary>
        IEnumerable<string> GetAllCodes();

        /// <summary>
        /// 删除权限
        /// </summary>
        /// <param name="code"></param>
        void Delete(string code);

        /// <summary>
        /// 根据用户Id，获取用户权限Code
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<PrivilegeCode> GetUserPrivilegeCodes(int userId);
    }
}
