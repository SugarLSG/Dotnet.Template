﻿using System.Collections.Generic;

namespace Project.Template.Web.Models.Shared
{
    public class MenuInfo : Framework.Models.Menu.MenuInfo
    {
        /// <summary>
        /// 权限对应路径
        /// </summary>
        public string Url { get { return Privilege.HasValue ? WebsiteUtility.PrivilegeUrlConfigs[Privilege.Value] : string.Empty; } }


        /// <summary>
        /// 子菜单列表
        /// </summary>
        public new IEnumerable<MenuInfo> SubMenuList { get; set; }
    }
}