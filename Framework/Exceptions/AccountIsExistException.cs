﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("账号已存在")]
    public sealed class AccountIsExistException : APPBaseException
    {
        /// <summary>
        /// message: (2001) 账号已存在
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public AccountIsExistException(params object[] args)
        {
            Code = "2001";
            Args = args;
        }
    }
}
