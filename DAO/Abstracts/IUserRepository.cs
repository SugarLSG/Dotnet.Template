﻿using APP.Core.Models;
using Framework.Models.User;
using Framework.Role;
using System.Collections.Generic;

namespace DAO.Abstracts
{
    public interface IUserRepository
    {
        /// <summary>
        /// 搜索用户列表
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        PagingResult<SearchResult> Search(SearchCondition condition);

        /// <summary>
        /// 更新用户所有角色
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roles"></param>
        /// <returns></returns>
        int UpdateUserRoles(int userId, IEnumerable<RoleCode> roles);
    }
}
