﻿using APP.Core.Utilities;

public static class AppSetting
{
    public static readonly string WebsiteHost = ConfigurationUtility.GetString(CommonSetting.APPSETTINGS_WEBSITE_HOST);
}
