﻿using APP.Core.Utilities;
using Framework.Models.API;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Framework.Utilities
{
    public static class APIRequestUtility
    {
        private static readonly string API_HOST = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_APISETTINGS_SECTION, CommonSetting.APPSETTINGS_APISETTINGS_HOST);
        private static readonly string API_SIGNEDKEY = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_APISETTINGS_SECTION, CommonSetting.APPSETTINGS_APISETTINGS_SIGNEDKEY);
        private static readonly string API_SIGNEDKEY_HEX = SecurityUtility.HEXEncrypt(API_SIGNEDKEY);
        private static readonly string API_APPID = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_APISETTINGS_SECTION, CommonSetting.APPSETTINGS_APISETTINGS_APPID);
        private static readonly string API_APPSECRET = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_APISETTINGS_SECTION, CommonSetting.APPSETTINGS_APISETTINGS_APPSECRET);


        private static APIResponseInfo<T> Post<T>(string path, IDictionary<string, object> postParams, int timeout, string authId = null)
        {
            // Post 参数加密
            var newPostParams = new Dictionary<string, string>();
            if (postParams != null)
            {
                foreach (var item in postParams)
                {
                    var value = JsonUtility.Serialize(item.Value);
                    var valueHEX = SecurityUtility.HEXEncrypt(value).ToLower();
                    var valueXOR = SecurityUtility.XOREncrypt(valueHEX, API_SIGNEDKEY_HEX);
                    var valueCrypto = SecurityUtility.BASE64Encrypt(valueXOR);

                    newPostParams.Add(item.Key.ToLower(), valueCrypto);
                }
            }

            // 计算 Sign
            var sign = APIUtility.APISign(newPostParams);

            // 计算 Token
            var token = APIUtility.APIToken(API_APPID, API_APPSECRET, sign);

            // 请求数据
            try
            {
                var responseData = HttpRequestUtility.PostURL(
                    string.Format("{0}{1}?sign={2}&appid={3}&token={4}", API_HOST, path, sign, API_APPID, token),
                    new Dictionary<string, object> { { "authid", authId } }.Merge(
                        newPostParams.Select(i => new KeyValuePair<string, object>(i.Key, i.Value))
                    ),
                    null,
                    timeout: timeout
                );
                LogUtility.LogWarn(responseData);

                var tempData = JsonUtility.Deserialize<APIResponseInfo<dynamic>>(responseData);
                if (tempData.Result && tempData.Sign.HasValue())
                {
                    var data = JsonUtility.Deserialize<APIResponseInfo<string>>(responseData);
                    if (tempData.Sign == APIUtility.ResponseSign(data.Data, sign, data.Timestamp))
                        return new APIResponseInfo<T>
                        {
                            Result = data.Result,
                            Code = data.Code,
                            Message = data.Message,
                            Data = JsonUtility.Deserialize<T>(SecurityUtility.BASE64Decrypt(data.Data)),
                            Timestamp = data.Timestamp,
                            Sign = data.Sign
                        };

                    return null;
                }
                else return JsonUtility.Deserialize<APIResponseInfo<T>>(responseData);
            }
            catch (Exception ex)
            {
                LogUtility.LogError(JsonUtility.Serialize(new
                {
                    Path = path,
                    Params = newPostParams
                }), ex);
                throw ex;
            }
        }

        /// <summary>
        /// Post 请求（不使用验证）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path">请勿携带参数</param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static APIResponseInfo<T> PostNone<T>(string path, IDictionary<string, object> postParams, int timeout = 10000)
        {
            try
            {
                var responseData = HttpRequestUtility.PostURL<APIResponseInfo<T>>(API_HOST + path, postParams, null, timeout: timeout);
                LogUtility.LogWarn(JsonUtility.Serialize(responseData));

                return responseData;
            }
            catch (Exception ex)
            {
                LogUtility.LogError(JsonUtility.Serialize(new
                {
                    Path = path,
                    Params = postParams
                }), ex);
                throw ex;
            }
        }

        /// <summary>
        /// Post 请求（验证签名）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path">请勿携带参数</param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static APIResponseInfo<T> PostSign<T>(string path, IDictionary<string, object> postParams, int timeout = 10000)
        {
            return Post<T>(path, postParams, timeout);
        }

        /// <summary>
        /// Post 请求（验证令牌）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path">请勿携带参数</param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static APIResponseInfo<T> PostToken<T>(string path, IDictionary<string, object> postParams, int timeout = 10000)
        {
            return Post<T>(path, postParams, timeout);
        }

        /// <summary>
        /// Post 请求（验证用户登录）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path">请勿携带参数</param>
        /// <param name="authId"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static APIResponseInfo<T> PostAuth<T>(string path, string authId, IDictionary<string, object> postParams, int timeout = 10000)
        {
            return Post<T>(path, postParams, timeout, authId);
        }
    }
}
