﻿using APP.Core.Models;
using Framework.Models.Role;
using Framework.Privilege;
using Framework.Role;
using System.Collections.Generic;

namespace DAO.Abstracts
{
    public interface IRoleRepository
    {
        /// <summary>
        /// 搜索权限列表
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        PagingResult<SearchResult> Search(SearchCondition condition);

        /// <summary>
        /// 更新角色所有权限
        /// </summary>
        /// <param name="code"></param>
        /// <param name="privileges"></param>
        /// <returns></returns>
        int UpdateRolePrivileges(RoleCode code, IEnumerable<PrivilegeCode> privileges);
    }
}
