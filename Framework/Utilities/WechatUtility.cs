﻿using APP.Core.Utilities;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.Containers;
using System;
using System.Collections.Generic;

namespace Framework.Utilities
{
    public static class WechatUtility
    {
        private const string PRIMARY_COLOR = "#3388ff";
        private const string BLACK_COLOR = "#333333";
        private const string GRAY_COLOR = "#9a9a9a";
        private const string RED_COLOR = "#ee2222";


        /// <summary>
        /// 发送推送消息
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="appSecret"></param>
        /// <param name="openIds"></param>
        /// <param name="templateId"></param>
        /// <param name="url"></param>
        /// <param name="data"></param>
        private static void SendMessage(string appId, string appSecret, IEnumerable<string> openIds, string templateId, string url, object data)
        {
            foreach (var openId in openIds)
            {
                try
                {
                    //根据检查是否已经注册
                    if (!AccessTokenContainer.CheckRegistered(appId))
                    {
                        //如果没有注册则进行注册
                        AccessTokenContainer.Register(appId, appSecret);
                    }

                    TemplateApi.SendTemplateMessage(appId, openId, templateId, url, data);
                }
                catch (Exception ex) { LogUtility.LogErrorToFile("wechatfile", ex); }
            }
        }
    }
}
