﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Framework.Models.Auth
{
    public class LoginInfo
    {
        /// <summary>
        /// 账号
        /// </summary>
        [DisplayName("账号")]
        [Required]
        public string Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [DisplayName("密码")]
        [Password(".+")]
        [Required]
        public string Password { get; set; }


        /// <summary>
        /// 返回Url
        /// </summary>
        public string ReturnUrl { get; set; }
    }
}
