﻿using System.ComponentModel;

namespace Framework.Enums
{
    public enum UserType
    {
        /// <summary>
        /// 系统用户
        /// </summary>
        [Description("系统用户")]
        SystemUser = 0
    }
}
