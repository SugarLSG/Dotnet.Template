﻿using Quartz;
using Spring.Scheduling.Quartz;
using System;

namespace Project.Template.Task.Jobs
{
    public class TestTask : QuartzJobObject
    {
        protected override void ExecuteInternal(IJobExecutionContext context)
        {
            TaskUtility.Run(() =>
            {
                Console.WriteLine("Test Task executing.");
            });
        }
    }
}
