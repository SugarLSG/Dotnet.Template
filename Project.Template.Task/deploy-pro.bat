@echo off

set PROJECT_NAME=Project.Template.Task
set PROJECT_NAME_LOWERCASE=project.template.task

set SOURCE_DRIVE=D:
set SOURCE_FOLDER=D:\Project\Template\%PROJECT_NAME%\bin\Debug\
set TARGET_DRIVE=D:
set TARGET_FOLDER=D:\Publish\%PROJECT_NAME%\
set PACKAGE_DRIVE=D:
set PACKAGE_FOLDER=D:\Publish\
set PACKAGE_NAME=%PROJECT_NAME_LOWERCASE%
set ZIP_DRIVE=D:
set ZIP_FOLDER=D:\Software\7-Zip\


echo start deploy %PROJECT_NAME% ...


echo copy files -

rd /s/q %TARGET_FOLDER%

%SOURCE_DRIVE%
cd %SOURCE_FOLDER%

xcopy /s/y *.dll %TARGET_FOLDER%
xcopy /s/y *.pdb %TARGET_FOLDER%
xcopy /s/y *.exe %TARGET_FOLDER%
xcopy /s/y Configs\*.* %TARGET_FOLDER%Configs\
del /q %TARGET_FOLDER%Configs\log4net.xml
del /q %TARGET_FOLDER%Configs\log4net-sit.xml
del /q %TARGET_FOLDER%Configs\log4net-uat.xml
ren %TARGET_FOLDER%Configs\log4net-pro.xml log4net.xml
del /q %TARGET_FOLDER%Configs\task.config.xml
del /q %TARGET_FOLDER%Configs\task.config-sit.xml
del /q %TARGET_FOLDER%Configs\task.config-uat.xml
ren %TARGET_FOLDER%Configs\task.config-pro.xml task.config.xml

copy /y ..\..\App-pro.config %TARGET_FOLDER%%PROJECT_NAME%.exe.config


echo package files -

%ZIP_DRIVE%
cd %ZIP_FOLDER%

del /q %PACKAGE_FOLDER%%PACKAGE_NAME%.7z
7z a %PACKAGE_FOLDER%%PACKAGE_NAME%.7z %TARGET_FOLDER%


echo delete files -

rd /s/q %TARGET_FOLDER%


echo finish deploy %PROJECT_NAME% ...