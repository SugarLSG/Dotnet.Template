﻿using APP.Core.Utilities;
using Framework.Models.Auth;
using Framework.Privilege;
using Project.Template.Web.Models.Shared;
using Service.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project.Template.Web
{
    public class AuthenticationUtility : APP.Core.Utilities.AuthenticationUtility
    {
        private static IAuthService AuthService { get; set; }
        private static IPrivilegeService PrivilegeService { get; set; }
        private static IMenuService MenuService { get; set; }


        /// <summary>
        /// 记录登录信息（Cookie，使用默认配置）
        /// </summary>
        /// <param name="userInfo"></param>
        public static void SignIn(UserAuthorizedInfo userInfo)
        {
            APP.Core.Utilities.AuthenticationUtility.SignIn(userInfo);

            // 重新获取用户权限
            PrivilegeService.GetUserPrivileges(userInfo.Id, false);
            // 重新获取用户菜单
            GetAuthorizedUserMenuList(false);
        }

        /// <summary>
        /// 记录登录信息（Cookie，使用默认配置）
        /// </summary>
        /// <param name="userId"></param>
        public static void SignIn(int userId)
        {
            var userInfo = AuthService.AuthenticatUser(userId);
            APP.Core.Utilities.AuthenticationUtility.SignIn(userInfo);
        }

        /// <summary>
        /// 记录登录信息（Cookie，使用默认配置）
        /// </summary>
        /// <param name="authId"></param>
        public static void SignIn(string authId)
        {
            SignIn(AuthSetting.AuthCookieKey, authId);
        }


        /// <summary>
        /// 获取API用户凭证
        /// </summary>
        /// <param name="appId"></param>
        /// <returns></returns>
        public static UserAPIAuthorizedInfo APIAuthenticatUser(string appId)
        {
            return AuthService.APIAuthenticatUser(appId);
        }


        /// <summary>
        /// 当前登录用户信息
        /// </summary>
        public static UserAuthorizedInfo AuthorizedUserInfo
        {
            get
            {
                return GetAuthorizedUserInfo<UserAuthorizedInfo>();
            }
        }

        /// <summary>
        /// 当前登录用户首个菜单信息
        /// </summary>
        /// <returns></returns>
        public static MenuInfo AuthorizedUserFirstMenu
        {
            get
            {
                Func<IEnumerable<MenuInfo>, MenuInfo> _getFirstMenu = null;
                _getFirstMenu = new Func<IEnumerable<MenuInfo>, MenuInfo>(ml =>
                {
                    // 无菜单项
                    if (ml == null || !ml.Any())
                        return null;

                    // 本级别第一个菜单项
                    var first = ml.FirstOrDefault(m => m.Privilege.HasValue && m.IsVisible);
                    if (first != null)
                        return first;

                    // 子菜单中，第一个菜单项
                    foreach (var m in ml)
                    {
                        var firstSub = _getFirstMenu(m.SubMenuList);
                        if (firstSub != null)
                            return firstSub;
                    }

                    return null;
                });

                return _getFirstMenu(GetAuthorizedUserMenuList());
            }
        }

        /// <summary>
        /// 当前登录用户权限列表
        /// </summary>
        public static IEnumerable<PrivilegeCode> AuthorizedUserPrivilegeList
        {
            get
            {
                var userId = AuthorizedUserId;
                if (!userId.HasValue)
                    return null;

                return PrivilegeService.GetUserPrivileges(userId.Value);
            }
        }

        /// <summary>
        /// 获取当前登录用户菜单信息列表
        /// </summary>
        /// <param name="getCache">是否获取缓存数据</param>
        /// <returns></returns>
        public static IEnumerable<MenuInfo> GetAuthorizedUserMenuList(bool getCache = true)
        {
            var userId = AuthorizedUserId;
            if (!userId.HasValue)
                return null;

            return CacheUtility.GetOrSetFromCache(
                string.Format(CommonSetting.CACHEKEY_USER_MENU_FORMAT, userId.Value),
                () =>
                {
                    // 用户权限
                    var privilegeList = AuthorizedUserPrivilegeList;
                    if (privilegeList == null || !privilegeList.Any())
                        return null;

                    // 删除用户无权限访问的菜单
                    var menus = MenuService.GetAllMenu();
                    Func<IEnumerable<Framework.Models.Menu.MenuInfo>, IEnumerable<MenuInfo>> handle = null;
                    handle = new Func<IEnumerable<Framework.Models.Menu.MenuInfo>, IEnumerable<MenuInfo>>(items =>
                    {
                        if (items == null || !items.Any())
                            return new MenuInfo[0];

                        return items.Select(i =>
                        {
                            if (i.Privilege.HasValue && !privilegeList.Contains(i.Privilege.Value))
                                return null;

                            var subMenuList = handle(i.SubMenuList).ToArray();
                            if (!i.Privilege.HasValue && (subMenuList == null || !subMenuList.Any()))
                                return null;

                            return new MenuInfo
                            {
                                MenuId = i.MenuId,
                                Privilege = i.Privilege,
                                Name = i.Name,
                                Icon = i.Icon,
                                Description = i.Description,
                                Sorted = i.Sorted,
                                IsVisible = i.IsVisible,
                                SubMenuList = subMenuList
                            };
                        })
                        .Where(i => i != null);
                    });
                    return handle(menus).ToArray();
                },
                getCache
            );
        }

        /// <summary>
        /// 判断当前登录用户是否有某个权限
        /// </summary>
        /// <param name="privilege"></param>
        /// <returns></returns>
        public static bool HasPrivilege(PrivilegeCode privilege)
        {
            return AuthorizedUserId.HasValue ? PrivilegeService.HasPrivilege(AuthorizedUserId.Value, privilege) : false;
        }
    }
}
