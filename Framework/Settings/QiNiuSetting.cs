﻿using APP.Core.Utilities;

public static class QiNiuSetting
{
    public static readonly string Host = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_QINIUSETTINGS_SECTION, CommonSetting.APPSETTINGS_QINIUSETTINGS_HOST);
    public static readonly string AccessKey = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_QINIUSETTINGS_SECTION, CommonSetting.APPSETTINGS_QINIUSETTINGS_ACCESSKEY);
    public static readonly string SecretKey = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_QINIUSETTINGS_SECTION, CommonSetting.APPSETTINGS_QINIUSETTINGS_SECRETKEY);
    public static readonly string Zone = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_QINIUSETTINGS_SECTION, CommonSetting.APPSETTINGS_QINIUSETTINGS_ZONE);
    public static readonly string Bucket = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_QINIUSETTINGS_SECTION, CommonSetting.APPSETTINGS_QINIUSETTINGS_BUCKET);

    public static readonly string UserHeadIMGPrefix = "user/headimg/";
}
