﻿using APP.Core.AOP.Attributes;
using APP.Core.Models;
using APP.Core.Repositories;
using DAO.Abstracts;
using Framework.DBEntity;
using Framework.Enums;
using Framework.Models.Role;
using Framework.Role;
using NHibernate.Criterion;
using Service.Abstracts;
using Service.AOP.Cache;
using System.Linq;

namespace Service.Concretes
{
    public class RoleService : IRoleService
    {
        private IBaseRepository BaseRepository { get; set; }
        private IRoleRepository RoleRepository { get; set; }


        public PagingResult<SearchResult> Search(SearchCondition condition)
        {
            return RoleRepository.Search(condition);
        }


        [Notify(typeof(ClearUserCacheNotify))]
        public void UpdateStatus(RoleCode roleCode, SysRoleStatus status)
        {
            var role = BaseRepository.GetEntityById<SysRoleEntity>(roleCode);
            role.Status = status;
            BaseRepository.UpdateEntity(role);
        }

        public RoleInfo GetInfo(RoleCode roleCode)
        {
            var role = BaseRepository.GetEntityById<SysRoleEntity>(roleCode);

            return new RoleInfo
            {
                RoleCode = role.RoleCode.Value,
                Name = role.Name,
                Description = role.Description,
                IsEnabled = role.Status == SysRoleStatus.Enabled,
                PrivilegeList = BaseRepository.GetEntityList<SysRolePrivilegeEntity>(new ICriterion[]
                {
                    Restrictions.Eq(SysRolePrivilegeEntity.COL_ROLECODE, roleCode)
                }).Select(rp => rp.PrivilegeCode)
            };
        }

        [Notify(typeof(ClearUserCacheNotify))]
        public RoleCode SaveInfo(RoleInfo info)
        {
            var role = BaseRepository.GetEntityById<SysRoleEntity>(info.RoleCode.Value);
            role.Name = info.Name;
            role.Description = info.Description;
            role.Status = info.IsEnabled ? SysRoleStatus.Enabled : SysRoleStatus.Disabled;
            BaseRepository.UpdateEntity(role);

            RoleRepository.UpdateRolePrivileges(role.RoleCode.Value, info.PrivilegeList);

            return role.RoleCode.Value;
        }
    }
}
