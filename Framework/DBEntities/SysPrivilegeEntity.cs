﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Attributes;
using APP.Core.Extensions.Nhibernate;
using Newtonsoft.Json;
using Framework.Enums;
using NHibernate.Mapping.Attributes;
using NHibernate.Type;
using System;
using System.ComponentModel;

namespace Framework.DBEntity
{
    /// <summary>
    /// 权限
    /// </summary>
    [Class(0, Table = "sys_privilege")]
    [Cache(1, Usage = CacheUsage.ReadWrite)]
    public class SysPrivilegeEntity
    {
        public const string TABLE_NAME = "sys_privilege";
        public const string COL_PRIVILEGECODE = "PrivilegeCode";
        public const string COL_NAME = "Name";
        public const string COL_DESCRIPTION = "Description";
        public const string COL_STATUS = "Status";
        public const string COL_CREATEDBY = "CreatedBy";
        public const string COL_CREATEDTIME = "CreatedTime";
        public const string COL_MODIFIEDBY = "ModifiedBy";
        public const string COL_MODIFIEDTIME = "ModifiedTime";

        /// <summary>
        /// 权限Code
        /// ENUM[string,Privilege.PrivilegeCode]
        /// </summary>
        [Description("权限Code")]
        [Id(0, Name = "PrivilegeCode", Column = "PrivilegeCode", Length = 128, TypeType = typeof(EnumStringType<Privilege.PrivilegeCode>))]
        [Key(1)]
        [Generator(2, Class = "assigned")]
        public virtual Privilege.PrivilegeCode? PrivilegeCode { get; set; }

        /// <summary>
        /// 权限名称
        /// </summary>
        [Description("权限名称")]
        [Property(Column = "Name", NotNull = true, Length = 128)]
        public virtual string Name { get; set; }

        /// <summary>
        /// 权限说明
        /// </summary>
        [Description("权限说明")]
        [Property(Column = "Description", Length = 256)]
        public virtual string Description { get; set; }

        /// <summary>
        /// 状态
        /// ENUM[int]
        /// -1-Disabled-已禁用
        /// 1-Enabled-已启用
        /// </summary>
        [Description("状态")]
        [Property(Column = "Status", NotNull = true)]
        public virtual SysPrivilegeStatus Status { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        [Description("创建者")]
        [Property(Column = "CreatedBy", NotNull = true)]
        public virtual int CreatedBy { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Property(Column = "CreatedTime", NotNull = true)]
        public virtual DateTime CreatedTime { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        [Description("修改者")]
        [Property(Column = "ModifiedBy", NotNull = true)]
        public virtual int ModifiedBy { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        [Property(Column = "ModifiedTime", NotNull = true)]
        public virtual DateTime ModifiedTime { get; set; }
    }
}
