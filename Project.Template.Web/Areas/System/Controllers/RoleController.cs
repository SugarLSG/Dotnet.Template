﻿using APP.Core.Utilities;
using Framework.Enums;
using Framework.Models.Role;
using Framework.Role;
using Service.Abstracts;
using System.ComponentModel;
using System.Web.Mvc;

namespace Project.Template.Web.Areas.System.Controllers
{
    public class RoleController : Controller
    {
        private IRoleService RoleService { get; set; }


        [Description("角色列表")]
        public ActionResult List()
        {
            return View(new SearchCondition());
        }

        [HttpPost]
        [Description("角色列表")]
        public string List(SearchCondition condition)
        {
            return JsonUtility.Serialize(RoleService.Search(condition));
        }

        [HttpPost]
        [Description("更新角色状态")]
        public string UpdateStatus(RoleCode roleCode, SysRoleStatus status)
        {
            RoleService.UpdateStatus(roleCode, status);
            return JsonUtility.SerializeAjaxSuccessJSON();
        }


        [Description("编辑角色")]
        public ActionResult Edit(RoleCode roleCode)
        {
            return View(RoleService.GetInfo(roleCode));
        }

        [HttpPost]
        [Description("保存角色信息")]
        public string Save(RoleInfo model)
        {
            var role = RoleService.SaveInfo(model);
            return JsonUtility.SerializeAjaxSuccessJSON(data: role);
        }
    }
}