﻿using APP.Core.Utilities;
using Senparc.Weixin.Context;
using Senparc.Weixin.MP.Entities;
using Senparc.Weixin.MP.Entities.Request;
using Senparc.Weixin.MP.MessageHandlers;
using System.Web;

namespace Project.Template.Web.Core.Wechat
{
    public class WechatMessageHandler : MessageHandler<MessageContext<IRequestMessageBase, IResponseMessageBase>>
    {
        public WechatMessageHandler(PostModel postModel) : base(HttpContext.Current.Request.InputStream, postModel)
        {
            // 微信消息去重
            OmitRepeatedMessage = true;

            LogUtility.LogInfo(RequestDocument);
            LogUtility.LogInfo(UsingEcryptMessage);
            LogUtility.LogInfo(EcryptRequestDocument);
            LogUtility.LogInfo(RequestMessage);

            Execute();

            LogUtility.LogInfo(ResponseDocument);
            LogUtility.LogInfo(FinalResponseDocument);
        }

        public override IResponseMessageBase DefaultResponseMessage(IRequestMessageBase requestMessage)
        {
            var responseMessage = CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "感谢您的支持。";
            return responseMessage;
        }
    }
}