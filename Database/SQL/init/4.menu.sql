update sys_menu set ParentMenuId = null;

delete from sys_menu;

alter table sys_menu auto_increment = 1;


INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES (NULL, NULL, '系统管理', NULL, '系统管理', 0, 1);			-- 1

/* 系统管理 */
INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES (1, 'URL_SYSTEM_MENU_INDEX', '配置菜单', NULL, '配置菜单', 0, 1);
INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES (1, 'URL_SYSTEM_MENU_PARENTMENU', '添加父菜单', NULL, '添加父菜单', 1, 0);
INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES (1, 'URL_SYSTEM_MENU_SUBMENU', '添加子菜单', NULL, '添加子菜单', 2, 0);
INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES (1, 'URL_SYSTEM_MENU_EDIT', '编辑菜单', NULL, '编辑菜单', 3, 0);
INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES (1, 'URL_SYSTEM_ROLE_LIST', '角色列表', NULL, '角色列表', 4, 1);
INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES (1, 'URL_SYSTEM_ROLE_EDIT', '编辑角色', NULL, '编辑角色', 5, 0);
INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES (1, 'URL_SYSTEM_USER_INFO', '个人信息', NULL, '个人信息', 6, 0);
INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES (1, 'URL_SYSTEM_USER_LIST', '用户列表', NULL, '用户列表', 7, 1);
INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES (1, 'URL_SYSTEM_USER_ADD', '新增用户', NULL, '新增用户', 8, 0);
INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES (1, 'URL_SYSTEM_USER_EDIT', '编辑用户', NULL, '编辑用户', 9, 0);
INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES (1, 'URL_SYSTEM_CACHE_INDEX', '系统缓存', NULL, '系统缓存', 10, 1);
INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES (1, 'URL_API_INTERNAL_INDEX', 'API测试', NULL, 'API测试', 11, 1);
