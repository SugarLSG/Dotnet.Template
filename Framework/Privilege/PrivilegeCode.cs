﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using System.ComponentModel;

namespace Framework.Privilege
{
    public enum PrivilegeCode
    {
        [Description("API测试")]
        URL_API_INTERNAL_INDEX,
        [Description("POST-清除缓存")]
        URL_SYSTEM_CACHE_CLEAR,
        [Description("系统缓存")]
        URL_SYSTEM_CACHE_INDEX,
        [Description("编辑菜单")]
        URL_SYSTEM_MENU_EDIT,
        [Description("配置菜单")]
        URL_SYSTEM_MENU_INDEX,
        [Description("添加父菜单")]
        URL_SYSTEM_MENU_PARENTMENU,
        [Description("添加子菜单")]
        URL_SYSTEM_MENU_SUBMENU,
        [Description("POST-更新菜单")]
        URL_SYSTEM_MENU_UPDATE,
        [Description("编辑角色")]
        URL_SYSTEM_ROLE_EDIT,
        [Description("角色列表")]
        URL_SYSTEM_ROLE_LIST,
        [Description("POST-保存角色信息")]
        URL_SYSTEM_ROLE_SAVE,
        [Description("POST-更新角色状态")]
        URL_SYSTEM_ROLE_UPDATESTATUS,
        [Description("新增用户")]
        URL_SYSTEM_USER_ADD,
        [Description("编辑用户")]
        URL_SYSTEM_USER_EDIT,
        [Description("个人信息")]
        URL_SYSTEM_USER_INFO,
        [Description("用户列表")]
        URL_SYSTEM_USER_LIST,
        [Description("POST-更新用户状态")]
        URL_SYSTEM_USER_UPDATESTATUS,
    }
}
