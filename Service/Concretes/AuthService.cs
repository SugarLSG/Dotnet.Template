﻿using APP.Core.Repositories;
using APP.Core.Utilities;
using Framework.DBEntity;
using Framework.Enums;
using Framework.Exceptions;
using Framework.Models.Auth;
using NHibernate.Criterion;
using Service.Abstracts;
using System.Linq;

namespace Service.Concretes
{
    public class AuthService : IAuthService
    {
        private IBaseRepository BaseRepository { get; set; }


        public string PasswordEncrypt(string originalPassword)
        {
            return
                (
                    SecurityUtility.MD5Encrypt(SecurityUtility.SHA1Encrypt(originalPassword).ToLower()) +
                    SecurityUtility.MD5Encrypt(SecurityUtility.XOREncrypt(originalPassword).ToLower())
                ).ToUpper();
        }


        public UserAuthorizedInfo AuthenticatUser(int userId)
        {
            // 获取用户信息
            var user = BaseRepository.GetEntityById<SysUserEntity>(userId);
            if (user == null || user.Status == SysUserStatus.Disabled)
                throw new AccountIsNotExistException();

            return new UserAuthorizedInfo
            {
                Id = user.UserId.Value,
                Account = user.Account,
                Name = user.Name,
                HeadImageUrl = user.HeadImageId.HasValue ? user.HeadImage.Path.ToAttachmentUrl() : UrlExtension.DefaultUserHeadImgUrl,
                NeedUpdatePassword = !user.UpdatedPassword,
                Sex = user.Sex,
                Type = user.Type,
                RoleList = BaseRepository.GetEntityList<SysUserRoleEntity>(new ICriterion[]
                {
                    Restrictions.Eq(SysUserRoleEntity.COL_USERID, user.UserId.Value)
                }).Select(s => s.RoleCode)
            };
        }

        public UserAuthorizedInfo AuthenticatUser(LoginInfo model)
        {
            // 获取用户信息
            var user = BaseRepository.GetEntity<SysUserEntity>(new ICriterion[]
            {
                Restrictions.Eq(SysUserEntity.COL_ACCOUNT, model.Account)
            });
            if (user == null || user.Status == SysUserStatus.Disabled)
                throw new AccountIsNotExistException();
            if (PasswordEncrypt(model.Password) != user.Password)
                throw new WrongLoginPasswordException();

            return new UserAuthorizedInfo
            {
                Id = user.UserId.Value,
                Account = user.Account,
                Name = user.Name,
                HeadImageUrl = user.HeadImageId.HasValue ? user.HeadImage.Path.ToAttachmentUrl() : UrlExtension.DefaultUserHeadImgUrl,
                NeedUpdatePassword = !user.UpdatedPassword,
                Sex = user.Sex,
                Type = user.Type,
                RoleList = BaseRepository.GetEntityList<SysUserRoleEntity>(new ICriterion[]
                {
                    Restrictions.Eq(SysUserRoleEntity.COL_USERID, user.UserId.Value)
                }).Select(s => s.RoleCode)
            };
        }


        public UserAPIAuthorizedInfo APIAuthenticatUser(string appId)
        {
            return CacheUtility.GetOrSetFromCache(
                string.Format(CommonSetting.CACHEKEY_USER_API_AUTHORIZED_FORMAT, appId),
                () =>
                {
                    // 获取用户信息
                    var user = BaseRepository.GetEntity<SysUserEntity>(new ICriterion[]
                    {
                        Restrictions.Eq(SysUserEntity.COL_APPID, appId),
                    });
                    if (user == null || user.Status == SysUserStatus.Disabled)
                        throw new InvalidAppIdException();

                    return new UserAPIAuthorizedInfo
                    {
                        UserId = user.UserId.Value,
                        AppId = user.AppId,
                        AppSecret = user.AppSecret
                    };
                }
            );
        }
    }
}
