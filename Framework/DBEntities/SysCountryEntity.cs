﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Attributes;
using APP.Core.Extensions.Nhibernate;
using Newtonsoft.Json;
using Framework.Enums;
using NHibernate.Mapping.Attributes;
using NHibernate.Type;
using System;
using System.ComponentModel;

namespace Framework.DBEntity
{
    /// <summary>
    /// 国家
    /// </summary>
    [Class(0, Table = "sys_country")]
    [Cache(1, Usage = CacheUsage.ReadWrite)]
    public class SysCountryEntity
    {
        public const string TABLE_NAME = "sys_country";
        public const string COL_COUNTRYID = "CountryId";
        public const string COL_CONTINENTCODE = "ContinentCode";
        public const string COL_ISOCODE2 = "ISOCode2";
        public const string COL_ISOCODE3 = "ISOCode3";
        public const string COL_ISOCODENUMBER = "ISOCodeNumber";
        public const string COL_NAMEEN = "NameEN";
        public const string COL_NAMECN = "NameCN";
        public const string COL_CREATEDTIME = "CreatedTime";
        public const string COL_MODIFIEDTIME = "ModifiedTime";

        /// <summary>
        /// 国家Id
        /// </summary>
        [Description("国家Id")]
        [Id(0, Name = "CountryId", Column = "CountryId")]
        [Key(1)]
        [Generator(2, Class = "native")]
        public virtual int? CountryId { get; set; }

        /// <summary>
        /// 洲际码
        /// ENUM[string]
        /// AS-AS-亚洲
        /// EU-EU-欧洲
        /// AM-AM-美洲
        /// OA-OA-大洋洲
        /// AF-AF-非洲
        /// OT-OT-其他
        /// </summary>
        [Description("洲际码")]
        [Property(Column = "ContinentCode", NotNull = true, Length = 2, TypeType = typeof(EnumStringType<SysCountryContinentCode>))]
        public virtual SysCountryContinentCode ContinentCode { get; set; }

        /// <summary>
        /// ISO代码（2位字母）
        /// </summary>
        [Description("ISO代码（2位字母）")]
        [Property(Column = "ISOCode2", NotNull = true, Length = 2)]
        public virtual string ISOCode2 { get; set; }

        /// <summary>
        /// ISO代码（3位字母）
        /// </summary>
        [Description("ISO代码（3位字母）")]
        [Property(Column = "ISOCode3", NotNull = true, Length = 3)]
        public virtual string ISOCode3 { get; set; }

        /// <summary>
        /// ISO代码（3位数字）
        /// </summary>
        [Description("ISO代码（3位数字）")]
        [Property(Column = "ISOCodeNumber", NotNull = true, Length = 3)]
        public virtual string ISOCodeNumber { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        [Description("英文名")]
        [Property(Column = "NameEN", NotNull = true, Length = 256)]
        public virtual string NameEN { get; set; }

        /// <summary>
        /// 中文名
        /// </summary>
        [Description("中文名")]
        [Property(Column = "NameCN", NotNull = true, Length = 256)]
        public virtual string NameCN { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Property(Column = "CreatedTime", NotNull = true)]
        public virtual DateTime CreatedTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        [Property(Column = "ModifiedTime", NotNull = true)]
        public virtual DateTime ModifiedTime { get; set; }
    }
}
