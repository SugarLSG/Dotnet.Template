﻿using APP.Core.Builders;
using APP.Core.Enums;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web;

namespace Framework.Extensions
{
    public static class ConfigExtension
    {
        /// <summary>
        /// 生成 Web Spring 配置文件
        /// </summary>
        public static void GenerateWebSpringConfig()
        {
            var projectDirectory = HttpContext.Current.Server.MapPath("/");
            var dllDirectory = projectDirectory + @"bin\";
            var saveDirectory = projectDirectory + @"Configs\";
            var configs = new Dictionary<string, IEnumerable<string>>
            {
                { "DAO.dll", new string[] { "repository" } },
                { "Service.dll", new string[] { "service", "notify", "factory" } },
                { CommonSetting.PROJECT_WEB_CODE + ".dll", new string[] { "controller", "websiteutility", "authenticationutility" } }
            };

            BuildProvider.GenerateSpringConfig(dllDirectory, saveDirectory, configs);
        }

        /// <summary>
        /// 生成 Task Spring 配置文件
        /// </summary>
        public static void GenerateTaskSpringConfig()
        {
            var location = Assembly.GetExecutingAssembly().Location;
            var projectDirectory = location.Substring(0, location.LastIndexOf(@"\") + 1);
            var dllDirectory = projectDirectory;
            var saveDirectory = projectDirectory + @"Configs\";
            var configs = new Dictionary<string, IEnumerable<string>>
            {
                { "DAO.dll", new string[] { "repository" } },
                { "Service.dll", new string[] { "service", "notify" } },
                { CommonSetting.PROJECT_TASK_CODE + ".exe", new string[] { "task" } }
            };

            BuildProvider.GenerateSpringConfig(dllDirectory, saveDirectory, configs);
        }

        /// <summary>
        /// 生成 Enum 配置文件
        /// </summary>
        public static void GenerateEnumConfig()
        {
            var projectDirectory = HttpContext.Current.Server.MapPath("/");
            var dllDirectory = projectDirectory + @"bin\";
            var savePath = string.Format(@"{0}Content\Scripts\{1}.enum.mapping.js", projectDirectory, CommonSetting.SOLUTION_CODE.ToLower());
            var configs = new string[] { "APP.Core", "Framework" };
            var excludes = new string[]
            {
                "CacheExpirationType", "DBType", "Sorted",
                "PrivilegeCode", "AttachmentServer"
            };

            BuildProvider.GenerateEnumConfig(dllDirectory, savePath, configs, excludes);
        }


        /// <summary>
        /// 复制 Less 文件
        /// </summary>
        public static void CopyLess()
        {
            if (CoreConfig.SystemEnvironment != SystemEnvironment.DEV)
                return;

            var projectDirectory = HttpContext.Current.Server.MapPath("/");
            var temp = projectDirectory.Substring(0, projectDirectory.Length - 1);
            var sourceDirectory = temp.Substring(0, temp.LastIndexOf(@"\")) + @"\APP.Core\Extensions\Web\";
            var destDirectory = projectDirectory + @"Content\";
            var filePrefix = CommonSetting.SOLUTION_CODE.ToLower();

            var configs = new Dictionary<string, IEnumerable<string>>
            {
                { "Styles", new string[] { "admin.less", "mobile.less" } }
            };
            foreach (var item in configs)
            {
                foreach (var i in item.Value)
                    File.Copy(sourceDirectory + i, string.Format(@"{0}{1}\{2}.{3}", destDirectory, item.Key, filePrefix, i), true);
            }
        }

        /// <summary>
        /// 复制 Script 文件
        /// </summary>
        public static void CopyScript()
        {
            if (CoreConfig.SystemEnvironment != SystemEnvironment.DEV)
                return;

            var projectDirectory = HttpContext.Current.Server.MapPath("/");
            var temp = projectDirectory.Substring(0, projectDirectory.Length - 1);
            var sourceDirectory = temp.Substring(0, temp.LastIndexOf(@"\")) + @"\APP.Core\Extensions\Web\";
            var destDirectory = projectDirectory + @"Content\";
            var filePrefix = CommonSetting.SOLUTION_CODE.ToLower();

            var configs = new Dictionary<string, IEnumerable<string>>
            {
                { "Scripts", new string[] { "form.js", "jquery.validate.unobtrusive.js" } }
            };
            foreach (var item in configs)
            {
                foreach (var i in item.Value)
                    File.Copy(sourceDirectory + i, string.Format(@"{0}{1}\{2}.{3}", destDirectory, item.Key, filePrefix, i), true);
            }
        }
    }
}
