﻿using APP.Core.Repositories;
using DAO.Abstracts;
using Framework.Models.Menu;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAO.Concretes
{
    public class MenuRepository : IMenuRepository
    {
        private IBaseRepository BaseRepository { get; set; }


        public int Update(IEnumerable<MenuInfo> model)
        {
            var id = 0;
            var listSqls = new List<string>();
            Action<IEnumerable<MenuInfo>, int?> handle = null;
            handle = new Action<IEnumerable<MenuInfo>, int?>((menus, parentId) =>
            {
                if (menus == null || menus.Count() == 0)
                    return;

                foreach (var menu in menus)
                {
                    listSqls.Add(string.Format(
                        "({0}, {1}, '{2}', {3}, '{4}', {5}, {6})",
                        parentId.HasValue ? parentId.Value.ToString() : "NULL",
                        menu.Privilege.HasValue ? string.Format("'{0}'", menu.Privilege.Value.ToString()) : "NULL",
                        menu.Name,
                        menu.Icon.HasValue() ? string.Format("'{0}'", menu.Icon) : "NULL",
                        menu.Description.HasValue() ? menu.Description : menu.Name,
                        menu.Sorted,
                        menu.IsVisible
                    ));
                    ++id;

                    handle(menu.SubMenuList, id);
                }
            });
            handle(model, null);

            return BaseRepository.ExecuteUpdate(string.Format(@"
UPDATE sys_menu SET ParentMenuId = NULL;
DELETE FROM sys_menu;
ALTER TABLE sys_menu AUTO_INCREMENT = 1;
INSERT INTO sys_menu (ParentMenuId, PrivilegeCode, Name, Icon, Description, Sorted, IsVisible) VALUES {0};", string.Join(", ", listSqls)));
        }
    }
}
