﻿using APP.Core.Enums;
using APP.Core.Models;
using APP.Core.Repositories;
using DAO.Abstracts;
using Framework.Models.Role;
using Framework.Privilege;
using Framework.Role;
using NHibernate.Util;
using System.Collections.Generic;
using System.Text;

namespace DAO.Concretes
{
    public class RoleRepository : IRoleRepository
    {
        private IBaseRepository BaseRepository { get; set; }


        public PagingResult<SearchResult> Search(SearchCondition condition)
        {
            var sql = new StringBuilder(@"
SELECT RoleCode,
       Name,
       Description,
       Status,
       CreatedTime
  FROM sys_role
 WHERE 1 = 1");

            var parameters = new Dictionary<string, object>();
            if (condition.Name.HasValue())
            {
                sql.Append(" AND INSTR(Name, :name) > 0");
                parameters.Add("name", condition.Name);
            }

            return BaseRepository.ExecutePagingList<SearchResult>(
                sql.ToString(),
                new Dictionary<string, Sorted> { { "CreatedTime", Sorted.Desc } },
                parameters,
                condition.FirstIndex,
                condition.Size
            );
        }

        public int UpdateRolePrivileges(RoleCode code, IEnumerable<PrivilegeCode> privileges)
        {
            if (privileges == null || !privileges.Any())
                return 0;

            var listSqls = new List<string>();
            foreach (var privilege in privileges)
                listSqls.Add(string.Format("(:rolecode, '{0}')", privilege.ToString()));

            return BaseRepository.ExecuteUpdate(string.Format(@"
DELETE FROM sys_role_privilege WHERE RoleCode = :rolecode;
INSERT INTO sys_role_privilege(RoleCode, PrivilegeCode) VALUES {0};", string.Join(", ", listSqls)),
                new Dictionary<string, object> { { "rolecode", code.ToString() } }
            );
        }
    }
}
