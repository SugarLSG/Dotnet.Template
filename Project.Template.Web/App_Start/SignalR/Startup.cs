﻿using Microsoft.Owin;
using Owin;
using Project.Template.Web.App_Start.SignalR;

[assembly: OwinStartup(typeof(Startup))]

namespace Project.Template.Web.App_Start.SignalR
{
    // Microsoft.AspNet.SignalR
    // Microsoft.AspNet.SignalR.Core
    // Microsoft.AspNet.SignalR.JS
    // Microsoft.AspNet.SignalR.SystemWeb
    // Microsoft.Owin
    // Microsoft.Owin.Host.SystemWeb
    // Microsoft.Owin.Security
    // Newtonsoft.Json
    // Owin
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // 注册 signalr/hubs
            app.MapSignalR();
        }
    }
}
