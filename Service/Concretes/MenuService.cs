﻿using APP.Core.AOP.Attributes;
using APP.Core.Repositories;
using DAO.Abstracts;
using Framework.DBEntity;
using Framework.Models.Menu;
using Framework.Privilege;
using Service.Abstracts;
using Service.AOP.Cache;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Service.Concretes
{
    public class MenuService : IMenuService
    {
        private IBaseRepository BaseRepository { get; set; }
        private IMenuRepository MenuRepository { get; set; }


        public IEnumerable<MenuInfo> GetAllMenu()
        {
            var menus = BaseRepository.GetEntityList<SysMenuEntity>()
                .OrderBy(m => m.Name)
                .OrderBy(m => m.Sorted);

            Func<int?, IEnumerable<MenuInfo>> handle = null;
            handle = new Func<int?, IEnumerable<MenuInfo>>(parentId =>
                menus.Where(m => m.ParentMenuId == parentId)
                    .Select(m =>
                    {
                        // 获取子菜单
                        var subMenuList = handle(m.MenuId.Value).ToArray();
                        var hasSubMenu = subMenuList != null && subMenuList.Any();

                        // 无配置权限，且无子菜单
                        // 则该菜单项作废
                        if (!m.PrivilegeCode.HasValue() && !hasSubMenu)
                            return null;

                        // 有子菜单
                        // 则该菜单项不对应界面
                        var privilege = m.PrivilegeCode.ToEnumNullable<PrivilegeCode>();
                        if (hasSubMenu)
                            privilege = null;

                        // 封装结果
                        return new MenuInfo
                        {
                            MenuId = m.MenuId.Value,
                            Privilege = privilege,
                            Name = m.Name,
                            Icon = m.Icon,
                            Description = m.Description,
                            Sorted = m.Sorted,
                            IsVisible = m.IsVisible,
                            SubMenuList = hasSubMenu ? subMenuList : null
                        };
                    })
                    .Where(m => m != null)
            );

            return handle(null).ToArray();
        }

        [Notify(typeof(ClearUserCacheNotify))]
        public void Update(IEnumerable<MenuInfo> model)
        {
            var count = MenuRepository.Update(model);
        }
    }
}
