﻿using APP.Core.Utilities;
using Framework.Enums;
using Framework.Models.User;
using Framework.Role;
using Framework.Utilities;
using Service.Abstracts;
using System.ComponentModel;
using System.Web.Mvc;

namespace Project.Template.Web.Areas.System.Controllers
{
    public class UserController : Controller
    {
        private IUserService UserService { get; set; }


        [Description("用户列表")]
        public ActionResult List()
        {
            return View(new SearchCondition
            {
                Status = SysUserStatus.Enabled
            });
        }

        [HttpPost]
        [Description("用户列表")]
        public string List(SearchCondition condition)
        {
            return JsonUtility.Serialize(UserService.Search(condition));
        }

        [HttpPost]
        [Description("更新用户状态")]
        public string UpdateStatus(int userId, SysUserStatus status)
        {
            UserService.UpdateStatus(userId, status);
            return JsonUtility.SerializeAjaxSuccessJSON();
        }


        [Description("个人信息")]
        public ActionResult Info()
        {
            var info = UserService.GetInfo(AuthenticationUtility.AuthorizedUserId.Value);

            return View(new UserInfo
            {
                Account = info.Account,
                NeedUpdatePassword = info.NeedUpdatePassword,
                IsUpdatePassword = info.IsUpdatePassword,
                Name = info.Name,
                Sex = info.Sex,
                Phone = info.Phone,
                Email = info.Email,
                QQ = info.QQ
            });
        }

        [HttpPost]
        [Description("保存个人信息")]
        public string Info(UserInfo model)
        {
            var userId = AuthenticationUtility.AuthorizedUserId.Value;

            // 修改密码，检查旧密码
            if (model.IsUpdatePassword)
                UserService.CheckOldPassword(userId, model.OldPassword);

            var info = UserService.GetInfo(userId);
            info.NeedUpdatePassword = model.NeedUpdatePassword;
            info.IsUpdatePassword = model.IsUpdatePassword;
            info.NewPassword = model.NewPassword;
            info.Name = model.Name;
            info.Sex = model.Sex;
            info.Phone = model.Phone;
            info.Email = model.Email;
            info.Wechat = model.Wechat;
            info.QQ = model.QQ;
            UserService.SaveInfo(info);

            if (model.NeedUpdatePassword)
            {
                AuthenticationUtility.SignOut();
                return JsonUtility.SerializeAjaxSuccessJSON("操作成功，请重新登录");
            }
            else return JsonUtility.SerializeAjaxSuccessJSON();
        }


        [Description("新增用户")]
        public ActionResult Add()
        {
            return View(new AddUserInfo
            {
                RoleList = new RoleCode[]
                {
                    RoleCode.GeRenXinXi_0F283DDD9F12BA75
                }
            });
        }

        [HttpPost]
        [Description("新增用户")]
        public string Add(AddUserInfo model)
        {
            var userId = UserService.SaveInfo(new EditUserInfo
            {
                Account = model.Account,
                IsUpdatePassword = true,
                NewPassword = model.NewPassword,
                Name = model.Name,
                Type = model.Type,
                Sex = model.Sex,
                Phone = model.Phone,
                Email = model.Email,
                Wechat = model.Wechat,
                QQ = model.QQ,
                AppId = APIUtility.AppId(),
                IsEnabled = true,
                RoleList = model.RoleList
            });

            return JsonUtility.SerializeAjaxSuccessJSON(data: userId);
        }


        [Description("编辑用户")]
        public ActionResult Edit(int id)
        {
            return View(UserService.GetInfo(id));
        }

        [HttpPost]
        [Description("编辑用户")]
        public string Edit(EditUserInfo model)
        {
            if (!model.UserId.HasValue)
            {
                model.AppId = APIUtility.AppId();
            }

            var userId = UserService.SaveInfo(model);
            return JsonUtility.SerializeAjaxSuccessJSON(data: userId);
        }
    }
}