﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("验证码不正确")]
    public sealed class WrongCaptchaCodeException : APPBaseException
    {
        /// <summary>
        /// message: (0006) 验证码不正确
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public WrongCaptchaCodeException(params object[] args)
        {
            Code = "0006";
            Args = args;
        }
    }
}
