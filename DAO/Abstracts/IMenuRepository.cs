﻿using Framework.Models.Menu;
using System.Collections.Generic;

namespace DAO.Abstracts
{
    public interface IMenuRepository
    {
        /// <summary>
        /// 更新所有菜单
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int Update(IEnumerable<MenuInfo> model);
    }
}
