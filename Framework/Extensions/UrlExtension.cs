﻿using Framework.Enums;

public static class UrlExtension
{
    /// <summary>
    /// 将相对路径，转为附件绝对路径（七牛）
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static string ToAttachmentUrl(this string path)
    {
        return path.ToAttachmentUrl(AttachmentServer.QiNiu);
    }

    /// <summary>
    /// 将相对路径，转为附件绝对路径
    /// </summary>
    /// <param name="path"></param>
    /// <param name="attachmentServer">附件服务器</param>
    /// <returns></returns>
    public static string ToAttachmentUrl(this string path, AttachmentServer attachmentServer)
    {
        return path.HasValue() ? (QiNiuSetting.Host + path.TrimStart('/')) : null;
    }


    /// <summary>
    /// 用户头像默认图
    /// </summary>
    public static string DefaultUserHeadImgUrl
    {
        get
        {
            return (QiNiuSetting.UserHeadIMGPrefix + "default.png").ToAttachmentUrl();
        }
    }
}
