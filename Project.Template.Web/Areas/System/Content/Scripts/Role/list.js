﻿var edit = function (code, name) {
    loadUrl(
        '编辑角色「' + name + '」',
        '/system/role/edit/' + encodeParameters(code),
        undefined,
        function (result, modalObject) {
            // submit
            var $form = modalObject.find('form').appSubmit({
                onSuccess: function (responseData) {
                    if (responseData.result) {
                        modalObject.modal('hide');
                    }
                    handleAjaxResult(responseData, function () {
                        $('#datalist').appDatagridReload();
                    });
                }
            });


            // title
            $form.find('input[type="checkbox"][data-title]').each(function (i, v) {
                $this = $(v);
                $this.parent().attr({ title: $this.data('title') });
            });

            // group
            var groups = [];
            $form.find('input[type="checkbox"][data-group]').each(function (i, v) {
                var group = $(v).data('group');
                if (!groups.contains(group)) {
                    groups.push(group);
                }
            });
            $.each(groups, function (i, group) {
                var $items = $form.find('input[type="checkbox"][data-group="' + group + '"]').parent();

                var $container = $('<div></div>').insertBefore($items.first());
                $items.remove();
                $container.append($items).toggle();
                $items.find('input[type="checkbox"]').on({
                    click: function () {
                        $groupCount.html($items.find('input[type="checkbox"]:checked').length + '/' + $items.length);
                    }
                });

                var $groupTitle = $('<h5 class="mt10 mb5" style="cursor: pointer;">' + group + '</h5>').insertBefore($container).on({
                    click: function () {
                        $container.toggle(100);
                        if ($groupCaret.hasClass('fa-caret-down')) {
                            $groupCaret.removeClass('fa-caret-down').addClass('fa-caret-up');
                        } else {
                            $groupCaret.removeClass('fa-caret-up').addClass('fa-caret-down');
                        }
                    }
                });
                var $groupCount = $('<small class="ml10 text-gray">' + $items.find('input[type="checkbox"]:checked').length + '/' + $items.length + '</small>');
                var $groupCaret = $('<span class="fa fa-caret-down ml5 text-gray"></span>')
                $groupTitle.append($groupCount);
                $groupTitle.append($groupCaret);
            });
        }
    );
};

var updateStatus = function (code, name, status) {
    confirm('确定' + (status === SysRoleStatus.Enabled ? '启用' : '禁用') + '角色「' + name + '」吗？', function (result) {
        if (result) {
            $.appAjax({
                url: '/system/role/updatestatus',
                data: { roleCode: code, status: status },
                onSuccess: function (responseData) {
                    handleAjaxResult(responseData, function () {
                        $('#datalist').appDatagridReload();
                    });
                }
            });
        }
    });
};

$(function () {
    // 搜索
    $('form').appAutoSubmit().on({
        submit: function (event) {
            event.preventDefault();

            $('#datalist').appDatagrid({
                url: $('form').attr('action'),
                data: $('form').appSerialize(),
                columns: [
                    { field: 'roleCode', title: 'Code', width: 5 },
                    { field: 'name', title: '名称', width: 4 },
                    { field: 'description', title: '描述', width: 4 },
                    { field: 'status', title: '状态', width: 4, align: 'center' },
                    { field: 'createdTime', title: '创建时间', width: 4 },
                    { field: 'operate', title: '操作', width: 3, align: 'center' }
                ],
                onBeforeRenderRow: function (rowData, rowIndex) {
                    var original = rowData.status;
                    rowData.status = '<label class="label ' + (original === SysRoleStatus.Enabled ? 'label-success' : 'label-default') + '">' + getEnumMessage('SysRoleStatus', original) + '</label>';

                    var operates = [];
                    if (hasPrivilege('URL_SYSTEM_ROLE_EDIT')) {
                        operates.push('<a href="javascript: edit(\'' + rowData.roleCode + '\', \'' + rowData.name + '\');">编辑</a>');
                    }
                    if (hasPrivilege('URL_SYSTEM_ROLE_UPDATESTATUS')) {
                        if (original === SysRoleStatus.Enabled) {
                            operates.push('<a href="javascript: updateStatus(\'' + rowData.roleCode + '\', \'' + rowData.name + '\', ' + SysRoleStatus.Disabled + ');" class="ml10">禁用</a>');
                        } else if (original === SysRoleStatus.Disabled) {
                            operates.push('<a href="javascript: updateStatus(\'' + rowData.roleCode + '\', \'' + rowData.name + '\', ' + SysRoleStatus.Enabled + ');" class="ml10">启用</a>');
                        }
                    }
                    if (operates.length) {
                        rowData.operate = operates.join('');
                    }
                }
            });
        }
    }).submit();
});
