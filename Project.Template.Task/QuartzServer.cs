﻿using APP.Core.Utilities;
using Spring.Context.Support;
using Topshelf;

namespace Project.Template.Task
{
    public class QuartzServer : ServiceControl, ServiceSuspend
    {
        public bool Start(HostControl hostControl)
        {
            // spring
            ContextRegistry.GetContext();

            LogUtility.LogInfo("Task Start...");
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            // spring
            ContextRegistry.GetContext().Dispose();

            LogUtility.LogInfo("Task Stop...");
            return true;
        }

        public bool Pause(HostControl hostControl)
        {
            LogUtility.LogInfo("Task Pause...");
            return true;
        }

        public bool Continue(HostControl hostControl)
        {
            LogUtility.LogInfo("Task Continue...");
            return true;
        }
    }
}
