﻿using APP.Core.Utilities;

public static class AuthSetting
{
    public static string AuthCookieKey = ConfigurationUtility.GetString(CommonSetting.APPSETTINGS_AUTH_COOKIE_KEY);
    public static int AuthCookieVersion = ConfigurationUtility.GetInt(CommonSetting.APPSETTINGS_AUTH_COOKIE_VERSION);
}
