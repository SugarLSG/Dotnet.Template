﻿using APP.Core.Attributes;
using APP.Core.Caches;
using APP.Core.Enums;
using APP.Core.Extensions.Web;
using APP.Core.Utilities;
using Framework.Exceptions;
using Framework.Extensions;
using Framework.Resources;
using Framework.Utilities;
using Project.Template.Web.Core.Attributes;
using Senparc.CO2NET;
using Senparc.CO2NET.RegisterServices;
using Senparc.Weixin;
using Senparc.Weixin.Context;
using Senparc.Weixin.MP.Containers;
using Senparc.Weixin.MP.Entities;
using Senparc.Weixin.MP.MessageHandlers;
using Spring.Web.Mvc;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace Project.Template.Web
{
    public class MvcApplication : SpringMvcApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            // 检查 APP.Core 版本
            CoreConfig.CheckCoreVersion(CoreConfig.CoreVersion_1_10_94);

            // 生成配置
            ConfigExtension.GenerateWebSpringConfig();
            ConfigExtension.GenerateEnumConfig();
            ConfigExtension.CopyLess();
            ConfigExtension.CopyScript();

            // 权限路径配置
            WebsiteUtility.InitPrivilegeUrlConfigs();

            // routes
            RouteTable.Routes.LowercaseUrls = true;
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            // engines
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngineExtension());

            // model metadata
            ModelMetadataProviders.Current = new DataAnnotationsModelMetadataProviderExtension(typeof(MessageSetting));

            // wechat
            var senparcSetting = SenparcSetting.BuildFromWebConfig(CoreConfig.SystemEnvironment != SystemEnvironment.PRO);
            var senparcWeixinSetting = Senparc.Weixin.Entities.SenparcWeixinSetting.BuildFromWebConfig(CoreConfig.SystemEnvironment != SystemEnvironment.PRO);
            RegisterService.Start(senparcSetting)
                .UseSenparcGlobal()
                .UseSenparcWeixin(senparcWeixinSetting, senparcSetting);
            AccessTokenContainer.Register(WechatSetting.AppId, WechatSetting.AppSecret);
            MessageHandler<MessageContext<IRequestMessageBase, IResponseMessageBase>>.GlobalWeixinContext.ExpireMinutes = 3;
            MessageHandler<MessageContext<IRequestMessageBase, IResponseMessageBase>>.GlobalWeixinContext.MaxRecordCount = 10;

            // filter attribute
            // action 执行前处理
            GlobalFilters.Filters.Add(new HandleRequestSyncPrivilegeAttribute());
            GlobalFilters.Filters.Add(new HandleRequestAdminAuthenticationAttribute());
            GlobalFilters.Filters.Add(new HandleRequestNeedUpdatePasswordAttribute());
            GlobalFilters.Filters.Add(new HandleRequestDecodeParamsAttribute(new InvalidParameterException()));
            GlobalFilters.Filters.Add(new HandleRequestAutoVerifyModelAttribute());
            // action 执行后处理
            GlobalFilters.Filters.Add(new HandleResponseAuthExpiredAttribute());
            GlobalFilters.Filters.Add(new HandleResponseCompressHtmlAttribute());
            GlobalFilters.Filters.Add(new HandleResponseContentTypeAttribute());

            // shared cache
            SharedCacheUtility.Configure(ConfigurationUtility.GetString(
                CommonSetting.APPSETTINGS_SHARED_CACHE_ENGINE).ToEnumNullable<CacheEngine>() ?? CacheEngine.MemoryCache,
                errorHandle: new Func<bool>(() =>
                {
                    // 发送邮件间隔1小时（以上）
                    if ((DateTime.Now - SharedCacheUtility.LastErrorHandleTime).TotalHours > 1)
                    {
                        Task.Run(() =>
                        {
                            try
                            {
                                BusinessMailUtility.SendSystemMailToManager(
                                    "Redis异常通知",
                                    string.Format(
                                        "{0} Redis连接异常（{1}），已切换至内存缓存，请尽快处理",
                                        CommonSetting.PROJECT_WEB_CODE,
                                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                                    )
                                );
                            }
                            catch { }
                        });

                        return true;
                    }

                    return false;
                })
            );

            // auth cookie
            AuthenticationUtility.Configure(
                key: AuthSetting.AuthCookieKey,
                version: AuthSetting.AuthCookieVersion
            );

            // log
            LogUtility.Configure(Server.MapPath("~/Configs/log4net.xml"));

            // qiniu
            QiNiuUtility.Configure(QiNiuSetting.AccessKey, QiNiuSetting.SecretKey, QiNiuSetting.Zone, QiNiuSetting.Bucket);
        }

        protected void Application_Error()
        {
            ApplicationErrorExtension.Handle();
        }
    }
}
