﻿using APP.Core.Utilities;
using System;
using System.Diagnostics;

namespace Project.Template.Task
{
    public static class TaskUtility
    {
        public static void Run(Action action)
        {
            var baseMethod = new StackTrace().GetFrame(1).GetMethod();
            var taskName = baseMethod.ReflectedType.Name;

            var start = DateTime.Now;
            var startMessage = string.Format("{0} Start: {1}.", taskName, start.ToString("yyyy-MM-dd HH:mm:ss,fff"));
            LogUtility.LogInfo(startMessage);
            Console.WriteLine("\r\n" + startMessage);

            try { action(); }
            catch (Exception ex) { LogUtility.LogError(ex); }

            var end = DateTime.Now;
            var duration = (end - start).TotalSeconds;
            var endMessage = string.Format("{0} End: {1}, Duration: {2}s.", taskName, end.ToString("yyyy-MM-dd HH:mm:ss,fff"), duration);
            LogUtility.LogInfo(endMessage);
            Console.WriteLine(endMessage);
        }
    }
}
