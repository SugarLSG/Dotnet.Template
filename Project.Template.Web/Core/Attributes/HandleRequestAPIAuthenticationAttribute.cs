﻿using APP.Core.Utilities;
using Framework.Exceptions;
using Framework.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Mvc;
using static APP.Core.Extensions.Json.ContractResolverExtension;

namespace Project.Template.Web.Core.Attributes
{
    public class HandleRequestAPIAuthenticationAttribute : ActionFilterAttribute
    {
        public enum VerifyLevel
        {
            /// <summary>
            /// 不验证（不建议使用）
            /// </summary>
            None = 0,

            /// <summary>
            /// 验证签名（以下其他所有级别，均会先验证签名）
            /// </summary>
            Sign = 1,
            /// <summary>
            /// 验证令牌
            /// </summary>
            Token = 2,
            /// <summary>
            /// 验证用户凭证（登录）
            /// </summary>
            Auth = 3
        }

        public enum ResponseSecurity
        {
            /// <summary>
            /// 不加密
            /// </summary>
            Unencrypted,
            /// <summary>
            /// 加密
            /// </summary>
            Encrypted
        }


        public VerifyLevel Level { get; }
        public ResponseSecurity Security { get; }
        public bool IsOnlyPost { get; }

        private const string APPID_KEY = "appid";
        private const string TOKEN_KEY = "token";
        private const string AUTHID_KEY = "authid";
        private const string SIGN_KEY = "sign";


        /// <summary>
        /// （请求）API 功能授权验证（Order: 10）
        /// </summary>
        /// <param name="level">验证级别</param>
        /// <param name="security">响应安全级别</param>
        /// <param name="isOnlyPost">是否只允许 Post 请求</param>
        public HandleRequestAPIAuthenticationAttribute(VerifyLevel level = VerifyLevel.Token, ResponseSecurity security = ResponseSecurity.Encrypted, bool isOnlyPost = true)
        {
            Order = 10;
            Level = level;
            Security = security;
            IsOnlyPost = isOnlyPost;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            var response = filterContext.HttpContext.Response;

            // 设置 Response
            response.ContentEncoding = Encoding.UTF8;
            response.ContentType = CoreConfig.CONTENTTYPE_APPLICATION_JSON;

            // 验证 Post 请求
            if (IsOnlyPost && !request.HttpMethod.Equals(HttpMethod.Post.ToString(), StringComparison.CurrentCultureIgnoreCase))
                throw new OnlySupportsPOSTRequestException();

            #region 不验证

            if (Level == VerifyLevel.None)
                return;

            #endregion

            #region 验证签名

            // 拼接参数
            // QueryString & Form
            string appId = null, token = null, authId = null, sign = null;
            var requestParams = new Dictionary<string, string>();
            foreach (var key in request.QueryString.AllKeys.Concat(request.Form.AllKeys))
            {
                var keyLower = key.ToLower();
                if (keyLower == APPID_KEY)
                    appId = request.Params[key];
                else if (keyLower == TOKEN_KEY)
                    token = request.Params[key];
                else if (keyLower == AUTHID_KEY)
                    authId = request.Params[key];
                else if (keyLower == SIGN_KEY)
                    sign = request.Params[key];
                else if (!requestParams.ContainsKey(keyLower))
                    requestParams.Add(keyLower, request.Params[key]);
            }

            // 验证签名
            if (sign != APIUtility.APISign(requestParams))
                throw new SignVerifyFailureException();

            // 恢复参数值
            var getKeys = request.QueryString.AllKeys.Select(k => k.ToLower());
            foreach (var actionParam in filterContext.ActionDescriptor.GetParameters())
            {
                var type = actionParam.ParameterType;
                var name = actionParam.ParameterName;
                var nameLower = name.ToLower();
                if (requestParams.ContainsKey(nameLower))
                {
                    object value;
                    try
                    {
                        value = getKeys.Contains(nameLower) ?
                            requestParams[nameLower] :
                            JsonUtility.Deserialize(APIUtility.DecryptParam(requestParams[nameLower]), type);
                    }
                    catch { value = requestParams[nameLower]; }

                    if (!value.IsValid())
                        throw new InvalidParameterException();
                    filterContext.Controller.ViewData.ModelState[name].Errors.Clear();
                    filterContext.ActionParameters[name] = value;
                }
                else
                {
                    var value = filterContext.ActionParameters[name];
                    if (!value.IsValid())
                        throw new InvalidParameterException();
                }
            }

            #endregion

            #region 验证令牌

            if (Level == VerifyLevel.Token)
            {
                if (!token.HasValue())
                    throw new InvalidAppTokenException();

                var userInfo = AuthenticationUtility.APIAuthenticatUser(appId);
                if (token != APIUtility.APIToken(userInfo.AppId, userInfo.AppSecret, sign))
                    throw new InvalidAppTokenException();

                AuthenticationUtility.SignIn(userInfo.UserId);
            }

            #endregion

            #region 验证用户凭证（登录）

            if (Level == VerifyLevel.Auth)
            {
                if (authId.HasValue())
                    AuthenticationUtility.SignIn(authId);

                // 是否已登录
                if (!AuthenticationUtility.IsAuthenticated())
                    throw new UserIsNotLoggedInException();
            }

            #endregion

            // 判断是否有权限访问
            var privilege = WebsiteUtility.CurrentPrivilege;
            if (privilege.HasValue && !AuthenticationUtility.HasPrivilege(privilege.Value))
                throw new HaveNoPrivilegeToAccessAPIException();
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            // 异常统一交由 Application_Error 处理
            if (filterContext.Exception != null)
                return;

            if (filterContext.Result is ContentResult || filterContext.Result is JsonResult || filterContext.Result is EmptyResult)
            {
                object data = null;
                if (filterContext.Result is ContentResult)
                {
                    var content = (filterContext.Result as ContentResult).Content;
                    try { data = JsonUtility.Deserialize(content); }
                    catch { data = content; }
                }
                else if (filterContext.Result is JsonResult)
                {
                    data = (filterContext.Result as JsonResult).Data;
                    if (data is string)
                    {
                        try { data = JsonUtility.Deserialize(data.ToString()); }
                        catch { }
                    }
                }

                // 封装 Response
                var response = filterContext.HttpContext.Response;
                response.Clear();
                response.ContentEncoding = Encoding.UTF8;
                response.ContentType = CoreConfig.CONTENTTYPE_APPLICATION_JSON;
                if (Level == VerifyLevel.None || Security == ResponseSecurity.Unencrypted || data == null)
                    response.Write(JsonUtility.SerializeAjaxSuccessJSON(data: data));
                else
                {
                    // 获取 Sign
                    var request = filterContext.HttpContext.Request;
                    string sign = null;
                    foreach (var key in request.Params.AllKeys)
                    {
                        if (key.ToLower() == SIGN_KEY)
                        {
                            sign = request.Params[key];
                            break;
                        }
                    }

                    // 封装响应内容
                    var crypto = SecurityUtility.BASE64Encrypt(JsonUtility.Serialize(data));
                    var timestamp = DateTime.Now.ToTimestamp();
                    var signed = APIUtility.ResponseSign(crypto, sign, timestamp);
                    response.Write(JsonUtility.Serialize(new
                    {
                        result = true,
                        message = CoreConfig.MESSAGE_OPERATE_SUCCESS,
                        data = crypto,
                        timestamp = timestamp,
                        sign = signed
                    }, DateTimeExtension.DATETIME_DEFAULTFORMAT, true, false, PropertyNameType.CamelCase));
                }
            }
        }
    }
}
