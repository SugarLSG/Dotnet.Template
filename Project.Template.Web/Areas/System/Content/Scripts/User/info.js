﻿var showUpdatePassword = function (speed) {
    $('#btnShowUpdatePassword').parent().remove();
    $('#IsUpdatePassword').val('True');
    $('#divPassword').css({ opacity: 0 }).show().animate({ opacity: 1 }, speed)
        .find('#OldPassword').focus();
    changePasswordVisible(1);
};

var changePasswordVisible = function (originalState) {
    $obj = $('#btnChangePasswordVisible');
    if (originalState === undefined) {
        originalState = $obj.data('state');
    }

    if (originalState === 1) {
        $obj.attr({ title: '显示密码' }).data('state', 0);
        $obj.find('span').removeClass('fa-eye-slash').addClass('fa-eye');
        $obj.parent().prev().attr({ type: 'password' });
    } else {
        $obj.attr({ title: '隐藏密码' }).data('state', 1);
        $obj.find('span').removeClass('fa-eye').addClass('fa-eye-slash');
        $obj.parent().prev().attr({ type: 'text' });
    }
};

var generatePassword = function () {
    $('#NewPassword').val(generateRandomPassword(8)).valid();
    changePasswordVisible(0);
};

var generateAppSecret = function () {
    $.get('/api/common/appsecret', function (responseData) {
        $('#AppSecret').val(responseData).valid();
    });
};

$(function () {
    var needUpdatePassword = $('#NeedUpdatePassword').val();
    if (needUpdatePassword && needUpdatePassword.toUpperCase() === 'TRUE') {
        alert('请先修改登录密码');
        needUpdatePassword = true;
    } else {
        needUpdatePassword = false;
    }

    var isNew = !$('#Account').val();
    if (isNew || needUpdatePassword) {
        showUpdatePassword(0);
        $('#Account').focus();
    } else {
        $('#Account').attr({ readonly: true });
    }

    $('form').appSubmit({
        onSuccess: function (responseData) {
            handleAjaxResult(responseData, function () {
                if (isNew) {
                    location.href = '/system/user/list';
                } else {
                    location.reload();
                }
            });
        }
    });
});
