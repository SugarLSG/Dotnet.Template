﻿using APP.Core.Enums;
using APP.Core.Models;
using APP.Core.Utilities;
using Framework.Extensions;
using Project.Template.Web.App_Start.SignalR;
using Project.Template.Web.Core.Attributes;
using Senparc.Weixin.MP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static APP.Core.Extensions.Json.ContractResolverExtension;
using static Project.Template.Web.Core.Attributes.HandleRequestAPIAuthenticationAttribute;

namespace Project.Template.Web.Controllers
{
    [AllowAnonymous]
    public class TestController : Controller
    {
        private static Random _random = new Random();


        #region Template

        public ActionResult Admin()
        {
            ConfigExtension.CopyLess();
            ConfigExtension.CopyScript();
            return View();
        }

        public ActionResult Mobile()
        {
            ConfigExtension.CopyLess();
            ConfigExtension.CopyScript();
            return View();
        }

        public ActionResult Partial(bool isLong = true)
        {
            ViewBag.IsLong = isLong;

            return View();
        }

        #endregion

        #region SignalR

        public ActionResult ChatRoom()
        {
            return View();
        }

        public string SayHi(string group = null)
        {
            return JsonUtility.Serialize(TestHub.SayHi(group));
        }

        #endregion

        #region Upload

        public ActionResult File()
        {
            return View();
        }

        #endregion

        #region Chart

        public ActionResult Chart()
        {
            return View();
        }

        public string ChartInfo(ChartType? type)
        {
            var seriesLength = _random.Next(1, 5);
            var dataLength = _random.Next(10, 20);
            var types = EnumExtension.GetEnumList<ChartType>().ToArray();

            var result = new List<ChartSeriesInfo>();
            for (var i = 0; i < seriesLength; ++i)
            {
                if (!type.HasValue)
                {
                    if (i != 0 && result[i - 1].Type == ChartType.Pie)
                        type = ChartType.Pie;
                    else
                        type = types[_random.Next(0, types.Length)];
                }
                if (i > 0 && type == ChartType.Pie)
                    break;

                var data = new ChartSeriesItemInfo[dataLength];
                for (var j = 0; j < dataLength; ++j)
                {
                    data[j] = new ChartSeriesItemInfo
                    {
                        XAxis = "x" + RandomCodeUtility.RandomNumberCode(2),
                        Name = "t" + RandomCodeUtility.RandomNumberCode(2),
                        Value = type == ChartType.Pie ?
                            (_random.NextDouble() * 100).ToPrecision() :
                            ((_random.NextDouble() - 0.5) * 100).ToPrecision()
                    };
                }

                result.Add(new ChartSeriesInfo
                {
                    Type = type.Value,
                    Name = "i" + RandomCodeUtility.RandomNumberCode(2),
                    Data = data
                });
            }

            return JsonUtility.Serialize(result);
        }

        #endregion

        #region Color

        public ActionResult Color()
        {
            return View();
        }

        #endregion

        #region Short Link

        public ActionResult ShortUrl()
        {
            return View();
        }

        public string GenerateShortUrl(string url)
        {
            return JsonUtility.SerializeAjaxSuccessJSON(data: new
            {
                suoim = ShortUrlUtility.SUO_IM(url),
                u6gg = ShortUrlUtility.U6_GG(url),
                c7gg = ShortUrlUtility.C7_GG(url),
                kksme = ShortUrlUtility.KKS_ME(url),
                ueeme = ShortUrlUtility.UEE_ME(url),
                rrdme = ShortUrlUtility.RRD_ME(url),
                tcn = ShortUrlUtility.T_CN(url),
                dwzis = ShortUrlUtility.DWZ_IS(url)
            });
        }

        #endregion

        #region Wechat

        public ActionResult WeUserInfo(OAuthScope scope, string accessToken, string openId, string transfer)
        {
            ViewBag.Scope = scope;
            ViewBag.User = WechatUtility.GetUserInfo(scope, accessToken, openId);
            ViewBag.Transfer = transfer;

            return View();
        }

        #endregion

        #region Select2

        public string Search(string keyword, int? age, int page, int size)
        {
            var totalCount = 25;
            var result = new List<object>();
            for (int i = (page - 1) * size, max = page * size; i < totalCount && i < max; ++i)
            {
                result.Add(new
                {
                    Id = i + 1,
                    Name = (i + 1) + "_" + (keyword.HasValue() ? keyword : RandomCodeUtility.RandomNumberCode(2)) + "_" + RandomCodeUtility.RandomNumberCode(2),
                    Age = age.HasValue ? age.Value : _random.Next(60),
                    Sex = _random.Next(60) % 2
                });
            }

            return JsonUtility.Serialize(new PagingResult<object>
            {
                Data = result,
                Additional = new
                {
                    Name = keyword,
                    Age = age,
                    Page = page,
                    Size = size
                },
                Size = size,
                TotalCount = totalCount
            });
        }

        #endregion

        #region API

        [HandleRequestAPIAuthentication(VerifyLevel.Token)]
        public string Info(object getParam1, object getParam2, object postParam1, object postParam2)
        {
            var query_string = new Dictionary<string, string>();
            foreach (var key in Request.QueryString.AllKeys)
                query_string.Add(key, Request.QueryString[key]);

            var form = new Dictionary<string, string>();
            foreach (var key in Request.Form.AllKeys)
                form.Add(key, Request.Form[key]);

            return JsonUtility.Serialize(new
            {
                user_id = AuthenticationUtility.AuthorizedUserId,
                get_param_1 = getParam1,
                get_param_2 = getParam2,
                post_param_1 = postParam1,
                post_param_2 = postParam2,
                query_string = query_string,
                form = form
            }, ignoreNull: false, propertynametype: PropertyNameType.None);
        }

        #endregion
    }
}