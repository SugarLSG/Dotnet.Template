﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("无效的请求参数")]
    public sealed class InvalidParameterException : APPBaseException
    {
        /// <summary>
        /// message: (0005) 无效的请求参数
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public InvalidParameterException(params object[] args)
        {
            Code = "0005";
            Args = args;
        }
    }
}
