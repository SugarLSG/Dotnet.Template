﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Attributes;
using APP.Core.Extensions.Nhibernate;
using Newtonsoft.Json;
using Framework.Enums;
using NHibernate.Mapping.Attributes;
using NHibernate.Type;
using System;
using System.ComponentModel;

namespace Framework.DBEntity
{
    /// <summary>
    /// 角色权限
    /// </summary>
    [Class(0, Table = "sys_role_privilege")]
    [Cache(1, Usage = CacheUsage.ReadWrite)]
    public class SysRolePrivilegeEntity
    {
        public const string TABLE_NAME = "sys_role_privilege";
        public const string COL_ROLEPRIVILEGEID = "RolePrivilegeId";
        public const string COL_ROLECODE = "RoleCode";
        public const string COL_PRIVILEGECODE = "PrivilegeCode";
        public const string COL_CREATEDBY = "CreatedBy";
        public const string COL_CREATEDTIME = "CreatedTime";
        public const string COL_MODIFIEDBY = "ModifiedBy";
        public const string COL_MODIFIEDTIME = "ModifiedTime";

        /// <summary>
        /// 角色权限Id
        /// </summary>
        [Description("角色权限Id")]
        [Id(0, Name = "RolePrivilegeId", Column = "RolePrivilegeId")]
        [Key(1)]
        [Generator(2, Class = "native")]
        public virtual int? RolePrivilegeId { get; set; }

        /// <summary>
        /// 角色Code
        /// ENUM[string,Role.RoleCode]
        /// </summary>
        [Description("角色Code")]
        [Property(Column = "RoleCode", NotNull = true, Length = 128, TypeType = typeof(EnumStringType<Role.RoleCode>))]
        public virtual Role.RoleCode RoleCode { get; set; }

        private SysRoleEntity _role = null;
        /// <summary>
        /// 外键 RoleCode 对应 sys_role 类实例;
        /// </summary>
        [IgnoreConvertTypeScalar]
        [JsonIgnore]
        public virtual SysRoleEntity Role { get { return _role ?? (_role = NHibernateExtension.GetEntityById<SysRoleEntity>(RoleCode)); } }

        /// <summary>
        /// 权限Code
        /// ENUM[string,Privilege.PrivilegeCode]
        /// </summary>
        [Description("权限Code")]
        [Property(Column = "PrivilegeCode", NotNull = true, Length = 128, TypeType = typeof(EnumStringType<Privilege.PrivilegeCode>))]
        public virtual Privilege.PrivilegeCode PrivilegeCode { get; set; }

        private SysPrivilegeEntity _privilege = null;
        /// <summary>
        /// 外键 PrivilegeCode 对应 sys_privilege 类实例;
        /// </summary>
        [IgnoreConvertTypeScalar]
        [JsonIgnore]
        public virtual SysPrivilegeEntity Privilege { get { return _privilege ?? (_privilege = NHibernateExtension.GetEntityById<SysPrivilegeEntity>(PrivilegeCode)); } }

        /// <summary>
        /// 创建者
        /// </summary>
        [Description("创建者")]
        [Property(Column = "CreatedBy", NotNull = true)]
        public virtual int CreatedBy { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Property(Column = "CreatedTime", NotNull = true)]
        public virtual DateTime CreatedTime { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        [Description("修改者")]
        [Property(Column = "ModifiedBy", NotNull = true)]
        public virtual int ModifiedBy { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        [Property(Column = "ModifiedTime", NotNull = true)]
        public virtual DateTime ModifiedTime { get; set; }
    }
}
