﻿using Framework.Privilege;
using System.Collections.Generic;

namespace Service.Abstracts
{
    public interface IPrivilegeService
    {
        /// <summary>
        /// 同步权限Code
        /// </summary>
        void Sync();


        /// <summary>
        /// 根据用户Id，获取该用户所有权限
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="getCache">是否获取缓存数据</param>
        /// <returns></returns>
        IEnumerable<PrivilegeCode> GetUserPrivileges(int userId, bool getCache = true);

        /// <summary>
        /// 判断用户是否有某个权限
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="privilege"></param>
        /// <returns></returns>
        bool HasPrivilege(int userId, PrivilegeCode privilege);
    }
}
