﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using System.ComponentModel;

namespace Framework.Role
{
    public enum RoleCode
    {
        /// <summary>
        /// 个人信息
        /// </summary>
        [Description("个人信息")]
        GeRenXinXi_0F283DDD9F12BA75,
        /// <summary>
        /// 管理员
        /// </summary>
        [Description("管理员")]
        GuanLiYuan_D8A4B9DEE8AA8FD8,
        /// <summary>
        /// 系统管理员
        /// </summary>
        [Description("系统管理员")]
        XiTongGuanLiYuan_173CD1D08E15F40C,
    }
}
