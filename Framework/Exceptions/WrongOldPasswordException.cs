﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("旧密码不正确")]
    public sealed class WrongOldPasswordException : APPBaseException
    {
        /// <summary>
        /// message: (2006) 旧密码不正确
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public WrongOldPasswordException(params object[] args)
        {
            Code = "2006";
            Args = args;
        }
    }
}
