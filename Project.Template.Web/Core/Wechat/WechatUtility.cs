﻿using APP.Core.Utilities;
using Project.Template.Web.Models.Wechat;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.CommonAPIs;
using System.Web;

public class WechatUtility
{
    /// <summary>
    /// 获取微信服务器IP
    /// </summary>
    /// <returns></returns>
    public static string[] GetServerIP()
    {
        return CommonApi.GetCallBackIp(WechatSetting.AppId).ip_list;
    }

    /// <summary>
    /// 生成 OAuth 跳转 Url（本站）
    /// </summary>
    /// <param name="path">相对路径</param>
    /// <param name="transfer">透传参数</param>
    /// <param name="scope">类型</param>
    /// <returns></returns>
    public static string GenerateAuthorizeUrl(string path, string transfer = null, OAuthScope scope = OAuthScope.snsapi_base)
    {
        var param = JsonUtility.Serialize(new object[] { scope, path, transfer });
        return OAuthApi.GetAuthorizeUrl(
            WechatSetting.AppId,
            WechatSetting.CallbackUrl + SecurityUtility.DESEncrypt_Hex(param),
            SecurityUtility.SHA1Encrypt(param),
            scope
        );
    }

    /// <summary>
    /// 解析参数
    /// </summary>
    /// <param name="state"></param>
    /// <param name="param"></param>
    /// <returns></returns>
    public static CallBackParamInfo DecryptParam(string state, string param)
    {
        if (!state.HasValue() || !param.HasValue())
            return null;

        try
        {
            var value = SecurityUtility.DESDecrypt_Hex(param);
            if (SecurityUtility.SHA1Encrypt(value) != state)
                return null;
            var values = JsonUtility.Deserialize<object[]>(value);

            var scope = int.Parse(values[0].ToString()).ToEnum<OAuthScope>();
            var redirectUrl = values[1].ToString();
            var transfer = values[2];

            if (!redirectUrl.HasValue())
                return null;

            return new CallBackParamInfo
            {
                Scope = scope,
                RedirectUrl = redirectUrl,
                Transfer = transfer == null ? null : transfer.ToString()
            };
        }
        catch { return null; }
    }

    /// <summary>
    /// 获取用户信息
    /// （scope=snsapi_base，返回UserInfoJson类型；scope=snsapi_userinfo，返回OAuthUserInfo类型，请自行转换）
    /// </summary>
    /// <param name="scope"></param>
    /// <param name="accessToken"></param>
    /// <param name="openId"></param>
    /// <returns></returns>
    public static object GetUserInfo(OAuthScope scope, string accessToken, string openId)
    {
        var sessionCode = string.Format("{0}_{1}", scope, openId);
        var info = HttpContext.Current.Session[sessionCode];
        if (info == null)
        {
            if (scope == OAuthScope.snsapi_base)
                info = UserApi.Info(WechatSetting.AppId, openId);
            else
                info = OAuthApi.GetUserInfo(accessToken, openId);
            HttpContext.Current.Session[sessionCode] = info;
        }

        return info;
    }
}
