﻿namespace Framework.Models.API
{
    public class APIResponseInfo<T>
    {
        /// <summary>
        /// 请求结果
        /// </summary>
        public bool Result { get; set; }

        /// <summary>
        /// 请求结果Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 请求结果描述
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 请求结果数据
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public long Timestamp { get; set; }

        /// <summary>
        /// 请求结果签名
        /// </summary>
        public string Sign { get; set; }
    }
}
