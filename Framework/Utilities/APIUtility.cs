﻿using APP.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Framework.Utilities
{
    public static class APIUtility
    {
        public static readonly string SignedKey = SecurityUtility.MD5Encrypt(CommonSetting.SOLUTION_CODE).ToLower();
        public static readonly string SignedKeyHEX = SecurityUtility.HEXEncrypt(SignedKey).ToLower();
        private static Random Random = new Random();


        /// <summary>
        /// 转为随机大小写
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        private static string RandomCaseCode(string code)
        {
            return string.Join("", code.Select(c => Random.Next(100) % 2 == 0 ? c.ToString().ToLower() : c.ToString().ToUpper()));
        }


        /// <summary>
        /// 生成凭证Id
        /// </summary>
        /// <returns></returns>
        public static string AppId()
        {
            return CommonSetting.KEY_API_APPID_PREFIX + SecurityUtility.MD5Encrypt_16(Guid.NewGuid().ToString()).ToLower();
        }

        /// <summary>
        /// 生成凭证秘钥
        /// </summary>
        /// <returns></returns>
        public static string AppSecret()
        {
            return RandomCaseCode(SecurityUtility.MD5Encrypt(Guid.NewGuid().ToString()));
        }


        /// <summary>
        /// 加密参数（16进制 & UTF8 -> 异或 -> Base64 & UTF8）
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static string EncryptParam(string json)
        {
            var jsonHEX = SecurityUtility.HEXEncrypt(json).ToLower();
            return SecurityUtility.BASE64Encrypt(SecurityUtility.XOREncrypt(jsonHEX, SignedKeyHEX));
        }

        /// <summary>
        /// 解密参数（Base64 & UTF8 -> 异或 -> 16进制 & UTF8）
        /// </summary>
        /// <param name="crypto"></param>
        /// <returns></returns>
        public static string DecryptParam(string crypto)
        {
            var jsonHEX = SecurityUtility.XORDecrypt(SecurityUtility.BASE64Decrypt(crypto), SignedKeyHEX);
            return SecurityUtility.HEXDecrypt(jsonHEX);
        }


        /// <summary>
        /// 生成API验证指纹（数据有效性）
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string APISign(IDictionary<string, string> parameters)
        {
            return SecurityUtility.MD5Encrypt(SecurityUtility.MD5Encrypt(
                string.Join("&", parameters
                    .Select(i => new KeyValuePair<string, string>(i.Key.ToLower(), i.Value))
                    .OrderByASCII(i => i.Key)
                    .Select(i => string.Format("{0}={1}", i.Key, i.Value))
                )
            ));
        }

        /// <summary>
        /// 生成API验证令牌（身份认证）
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="appSecret"></param>
        /// <param name="sign"></param>
        /// <returns></returns>
        public static string APIToken(string appId, string appSecret, string sign)
        {
            return SecurityUtility.SHA1Encrypt(
                string.Join("", new string[] { appId, appSecret, sign }.OrderByASCII(i => i))
            ).ToLower();
        }

        /// <summary>
        /// 生成Response验证指纹（数据有效性）
        /// </summary>
        /// <param name="crypto"></param>
        /// <param name="requestSign"></param>
        /// <param name="timestamp"></param>
        /// <returns></returns>
        public static string ResponseSign(string crypto, string requestSign, long timestamp)
        {
            return SecurityUtility.MD5Encrypt(SecurityUtility.MD5Encrypt(
                string.Format("{0}&{1}&{2}", crypto, requestSign, timestamp)
            ));
        }
    }
}
