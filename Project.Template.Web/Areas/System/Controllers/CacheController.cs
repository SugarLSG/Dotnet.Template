﻿using APP.Core.Caches;
using APP.Core.Utilities;
using System.ComponentModel;
using System.Web.Mvc;

namespace Project.Template.Web.Areas.System.Controllers
{
    public class CacheController : Controller
    {
        [Description("系统缓存")]
        public ActionResult Index()
        {
            ViewBag.HTTPCaches = CacheManager.HTTPCacheProvider.GetAllKeys();
            ViewBag.MemoryCaches = CacheManager.MemoryCacheProvider.GetAllKeys();

            return View();
        }

        [HttpPost]
        [Description("清除缓存")]
        public string Clear(CacheEngine? engine, string key)
        {
            if (engine.HasValue)
            {
                if (key.HasValue())
                    CacheUtility.DeleteCacheByKeyword(key, engine.Value);
                else
                {
                    if (engine.Value == CacheEngine.HTTPCache)
                        CacheManager.HTTPCacheProvider.RemoveAll();
                    else if (engine.Value == CacheEngine.MemoryCache)
                        CacheManager.MemoryCacheProvider.RemoveAll();
                }
            }
            else
            {
                if (key.HasValue())
                {
                    CacheUtility.DeleteCacheByKeyword(key, CacheEngine.HTTPCache);
                    CacheUtility.DeleteCacheByKeyword(key, CacheEngine.MemoryCache);
                }
                else
                {
                    CacheManager.HTTPCacheProvider.RemoveAll();
                    CacheManager.MemoryCacheProvider.RemoveAll();
                }
            }

            return JsonUtility.SerializeAjaxSuccessJSON();
        }
    }
}