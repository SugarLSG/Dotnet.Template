﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("请求过于频繁")]
    public sealed class RequestsTooFrequentException : APPBaseException
    {
        /// <summary>
        /// message: (0009) 请求过于频繁
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public RequestsTooFrequentException(params object[] args)
        {
            Code = "0009";
            Args = args;
        }
    }
}
