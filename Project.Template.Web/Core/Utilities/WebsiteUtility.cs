﻿using APP.Core.Utilities;
using Framework.Privilege;
using Service.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

public class WebsiteUtility
{
    /// <summary>
    /// 项目权限路径配置
    /// </summary>
    public static IDictionary<PrivilegeCode, string> PrivilegeUrlConfigs { get; set; }

    private static IPrivilegeService PrivilegeService { get; set; }


    /// <summary>
    /// 生成 Machine Key（web.config => system.web => machineKey）
    /// </summary>
    /// <returns></returns>
    public static string GenerateMachineKey()
    {
        return string.Format(
            "<machineKey validation=\"SHA1\" validationKey=\"{0}\" decryption=\"3DES\" decryptionKey=\"{1}\" />",
            RandomCodeUtility.RandomCode(128),
            RandomCodeUtility.RandomCode(48)
        );
    }


    /// <summary>
    /// 初始化权限路径配置
    /// </summary>
    public static void InitPrivilegeUrlConfigs()
    {
        PrivilegeUrlConfigs = new Dictionary<PrivilegeCode, string>();
        Assembly.LoadFrom(HttpContext.Current.Server.MapPath("/") + "bin\\" + CommonSetting.SOLUTION_CODE + ".Web.dll")
            .ExportedTypes
            .Where(t =>
            {
                if (!t.IsClass) return false;
                if (!t.Name.ToLower().EndsWith("controller")) return false;
                if (t.GetCustomAttribute<System.Web.Mvc.AllowAnonymousAttribute>() != null) return false;
                if (t.GetCustomAttribute<System.Web.Http.AllowAnonymousAttribute>() != null) return false;
                if (t.GetCustomAttribute<APP.Core.Attributes.UnprivilegedAttribute>() != null) return false;

                return true;
            })
            .SelectMany(t =>
            {
                return t.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly)
                    .Where(m => m.GetCustomAttribute<System.Web.Mvc.AllowAnonymousAttribute>() == null && m.GetCustomAttribute<System.Web.Http.AllowAnonymousAttribute>() == null)
                    .Select(m =>
                    {
                        // 请求路径
                        var area = m.ReflectedType.Namespace
                            .Replace(m.Module.Name.Replace(".dll", string.Empty), string.Empty)
                            .Replace(".Areas", string.Empty)
                            .Replace(".Controllers", string.Empty)
                            .TrimStart('.');
                        var controller = m.ReflectedType.Name.Replace("Controller", string.Empty);
                        var action = m.Name;
                        var privilegeUrl = string.Format("/{0}/{1}/{2}", area, controller, action)
                            .Replace("//", "/")
                            .ToLower();

                        if (!privilegeUrl.Contains('.'))
                        {
                            var code = ("URL_" + string.Join("_", new string[] { area, controller, action }))
                                .Replace("__", "_")
                                .ToUpper();

                            var description = code;
                            var descriptionAttribute = m.GetCustomAttribute<System.ComponentModel.DescriptionAttribute>();
                            if (descriptionAttribute != null)
                                description = descriptionAttribute.Description;
                            var httpPostAttribute = m.GetCustomAttribute<System.Web.Mvc.HttpPostAttribute>();
                            if (httpPostAttribute != null)
                                description = "POST-" + description;

                            return new Tuple<string, string, string>(code, description, privilegeUrl);
                        }

                        return null;
                    });
            })
            .Where(i => i != null)
            .OrderByDescending(i => i.Item1)
            .DistinctBy(i => i.Item1)
            .OrderBy(i => i.Item1)
            .ToList()
            .ForEach(i =>
            {
                if (!Enum.IsDefined(typeof(PrivilegeCode), i.Item1))
                    throw new Exception("请先执行（T4）Framework.Builders.GeneratePrivilege.tt");

                PrivilegeUrlConfigs.Add(i.Item1.ToEnum<PrivilegeCode>(), i.Item3);
            });
    }

    /// <summary>
    /// 初始化权限
    /// </summary>
    public static void SyncPrivilege()
    {
        PrivilegeService.Sync();
    }


    /// <summary>
    /// 获取当前请求路径权限Code
    /// </summary>
    public static PrivilegeCode? CurrentPrivilege
    {
        get
        {
            // 获取当前请求路径
            var requestPath = ClientRequestUtility.RequestRealPath;

            // 路径转为权限
            return WebsiteUtility.PrivilegeUrlConfigs.Any(i => i.Value == requestPath) ?
                WebsiteUtility.PrivilegeUrlConfigs.First(i => i.Value == requestPath).Key :
                (PrivilegeCode?)null;
        }
    }
}