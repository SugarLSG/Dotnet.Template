﻿var add = function () {
    location.href = '/system/user/add';
};

var edit = function (userId, name) {
    location.href = '/system/user/edit/' + encodeParameters(userId);
};

var updateStatus = function (userId, name, status) {
    confirm('确定' + (status === SysUserStatus.Enabled ? '启用' : '禁用') + '用户「' + name + '」吗？', function (result) {
        if (result) {
            $.appAjax({
                url: '/system/user/updatestatus',
                data: { userId: userId, status: status },
                onSuccess: function (responseData) {
                    handleAjaxResult(responseData, function () {
                        $('#datalist').appDatagridReload();
                    });
                }
            });
        }
    });
};

$(function () {
    // 搜索
    $('form').appAutoSubmit().on({
        submit: function (event) {
            event.preventDefault();

            $('#datalist').appDatagrid({
                url: $('form').attr('action'),
                data: $('form').appSerialize(),
                toolbar: hasPrivilege('URL_SYSTEM_USER_ADD') ? [
                    { id: 'btnAdd', text: '添加', iconCls: 'fa fa-plus', handler: add }
                ] : undefined,
                columns: [
                    { field: 'account', title: '账号', width: 5 },
                    { field: 'name', title: '名称', width: 4 },
                    { field: 'type', title: '类型', width: 4, align: 'center' },
                    { field: 'status', title: '状态', width: 4, align: 'center' },
                    { field: 'createdTime', title: '创建时间', width: 4 },
                    { field: 'operate', title: '操作', width: 3, align: 'center' }
                ],
                onBeforeRenderRow: function (rowData, rowIndex) {
                    rowData.type = '<label class="label ' + (rowData.type === UserType.SystemUser ? 'label-info' : 'label-default') + '">' + getEnumMessage('UserType', rowData.type) + '</label>';

                    var original = rowData.status;
                    rowData.status = '<label class="label ' + (original === SysUserStatus.Enabled ? 'label-success' : 'label-default') + '">' + getEnumMessage('SysUserStatus', original) + '</label>';

                    var operates = [];
                    if (hasPrivilege('URL_SYSTEM_USER_EDIT')) {
                        operates.push('<a href="javascript: edit(' + rowData.userId + ', \'' + rowData.name + '\');">编辑</a>');
                    }
                    if (hasPrivilege('URL_SYSTEM_USER_UPDATESTATUS')) {
                        if (original === SysUserStatus.Enabled) {
                            operates.push('<a href="javascript: updateStatus(' + rowData.userId + ', \'' + rowData.name + '\', ' + SysUserStatus.Disabled + ');" class="ml10">禁用</a>');
                        } else if (original === SysUserStatus.Disabled) {
                            operates.push('<a href="javascript: updateStatus(' + rowData.userId + ', \'' + rowData.name + '\', ' + SysUserStatus.Enabled + ');" class="ml10">启用</a>');
                        }
                    }
                    if (operates.length) {
                        rowData.operate = operates.join('');
                    }
                }
            });
        }
    }).submit();
});
