﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using System.ComponentModel;

namespace Framework.Enums
{
    /// <summary>
    /// 状态
    /// ENUM[int]
    /// -1-Disabled-已禁用
    /// 0-Inactived-未激活
    /// 1-Enabled-已启用
    /// </summary>
    public enum SysUserStatus
    {
        /// <summary>
        /// 已禁用
        /// </summary>
        [Description("已禁用")]
        Disabled = -1,
        /// <summary>
        /// 未激活
        /// </summary>
        [Description("未激活")]
        Inactived = 0,
        /// <summary>
        /// 已启用
        /// </summary>
        [Description("已启用")]
        Enabled = 1,
    }
}
