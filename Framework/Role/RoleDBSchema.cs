﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;

namespace Framework.Role
{
    public class RoleDBSchema
    {
        public IEnumerable<KeyValuePair<string, string>> RoleCodes { get; }


        public RoleDBSchema(string connString)
        {
            using (var conn = new MySqlConnection(connString))
            {
                conn.Open();
                RoleCodes = GetRoleCodeList(conn);
            }
        }


        private IEnumerable<KeyValuePair<string, string>> GetRoleCodeList(IDbConnection conn)
        {
            var cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT RoleCode, Name FROM sys_role";

            var codeList = new List<KeyValuePair<string, string>>();
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    codeList.Add(new KeyValuePair<string, string>(Convert.ToString(reader[0]), Convert.ToString(reader[1])));
                }
            }
            return codeList;
        }
    }
}
