﻿using Framework.Models.Menu;
using System.Collections.Generic;

namespace Service.Abstracts
{
    public interface IMenuService
    {
        /// <summary>
        /// 获取所有菜单信息
        /// </summary>
        /// <returns></returns>
        IEnumerable<MenuInfo> GetAllMenu();

        /// <summary>
        /// 更新所有菜单
        /// </summary>
        /// <param name="model"></param>
        void Update(IEnumerable<MenuInfo> model);
    }
}
