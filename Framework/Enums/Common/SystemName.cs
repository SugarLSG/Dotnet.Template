﻿using System.ComponentModel;

namespace Framework.Enums
{
    public enum SystemName
    {
        /// <summary>
        /// Web系统
        /// </summary>
        [Description("Web系统")]
        Web,

        /// <summary>
        /// 任务系统
        /// </summary>
        [Description("任务系统")]
        Task
    }
}
