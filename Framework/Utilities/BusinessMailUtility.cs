﻿using APP.Core.Utilities;
using System.Collections.Generic;

namespace Framework.Utilities
{
    public static class BusinessMailUtility
    {
        private static readonly string SMTPHost = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_MAILSETTINGS_SECTION, CommonSetting.APPSETTINGS_MAILSETTINGS_SMTPHOST);
        private static readonly int SMTPPort = ConfigurationUtility.GetSectionInt(CommonSetting.APPSETTINGS_MAILSETTINGS_SECTION, CommonSetting.APPSETTINGS_MAILSETTINGS_SMTPPORT, 25);
        private static readonly string Address = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_MAILSETTINGS_SECTION, CommonSetting.APPSETTINGS_MAILSETTINGS_ADDRESS);
        private static readonly string Password = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_MAILSETTINGS_SECTION, CommonSetting.APPSETTINGS_MAILSETTINGS_PASSWORD);
        private static readonly string Name = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_MAILSETTINGS_SECTION, CommonSetting.APPSETTINGS_MAILSETTINGS_NAME);
        private static readonly string[] ReceiveList = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_MAILSETTINGS_SECTION, CommonSetting.APPSETTINGS_MAILSETTINGS_RECEIVELIST, "").Split(',');

        private static readonly MailUtility Instance = new MailUtility(SMTPHost, SMTPPort, Address, Password, Name);


        /// <summary>
        /// 发送系统邮件
        /// </summary>
        /// <param name="receiver">收件地址</param>
        /// <param name="title">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        /// <param name="attachmentFileUrl">邮件附件（本地路径）</param>
        public static void SendSystemMail(string receiver, string title, string content, string attachmentFileUrl = null)
        {
            Instance.SendEmail(receiver, null, null, title, content, attachmentFileUrl);
        }

        /// <summary>
        /// 发送系统邮件
        /// </summary>
        /// <param name="receivers">收件地址</param>
        /// <param name="title">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        /// <param name="attachmentFileUrls">邮件附件（本地路径）</param>
        public static void SendSystemMail(IEnumerable<string> receivers, string title, string content, IEnumerable<string> attachmentFileUrls = null)
        {
            Instance.SendEmail(receivers, null, null, title, content, attachmentFileUrls);
        }

        /// <summary>
        /// 发送系统邮件给管理员
        /// </summary>
        /// <param name="title">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        /// <param name="attachmentFileUrls">邮件附件（本地路径）</param>
        public static void SendSystemMailToManager(string title, string content, IEnumerable<string> attachmentFileUrls = null)
        {
            SendSystemMail(ReceiveList, title, content, attachmentFileUrls);
        }
    }
}
