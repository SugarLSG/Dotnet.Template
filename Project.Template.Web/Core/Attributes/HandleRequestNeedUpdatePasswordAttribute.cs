﻿using System.Web;
using System.Web.Mvc;

namespace Project.Template.Web.Core.Attributes
{
    public class HandleRequestNeedUpdatePasswordAttribute : ActionFilterAttribute
    {
        private const string REDIRECT_PATH = "/system/user/info";


        /// <summary>
        /// （请求）检查是否已更新登录密码（Order: 15）
        /// </summary>
        public HandleRequestNeedUpdatePasswordAttribute() { }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userInfo = AuthenticationUtility.AuthorizedUserInfo;
            var url = HttpContext.Current.Request.Url;
            if (userInfo != null && userInfo.NeedUpdatePassword && HttpContext.Current.Request.Url.AbsolutePath.ToLower() != REDIRECT_PATH)
                filterContext.Result = new RedirectResult(REDIRECT_PATH);
        }
    }
}