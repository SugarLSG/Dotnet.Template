﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Attributes;
using APP.Core.Extensions.Nhibernate;
using Newtonsoft.Json;
using Framework.Enums;
using NHibernate.Mapping.Attributes;
using NHibernate.Type;
using System;
using System.ComponentModel;

namespace Framework.DBEntity
{
    /// <summary>
    /// 附件
    /// </summary>
    [Class(0, Table = "sys_attachment")]
    [Cache(1, Usage = CacheUsage.ReadWrite)]
    public class SysAttachmentEntity
    {
        public const string TABLE_NAME = "sys_attachment";
        public const string COL_ATTACHMENTID = "AttachmentId";
        public const string COL_TYPE = "Type";
        public const string COL_NAME = "Name";
        public const string COL_PATH = "Path";
        public const string COL_SIZE = "Size";
        public const string COL_REMARK = "Remark";
        public const string COL_CREATEDBY = "CreatedBy";
        public const string COL_CREATEDTIME = "CreatedTime";

        /// <summary>
        /// 附件Id
        /// </summary>
        [Description("附件Id")]
        [Id(0, Name = "AttachmentId", Column = "AttachmentId")]
        [Key(1)]
        [Generator(2, Class = "native")]
        public virtual int? AttachmentId { get; set; }

        /// <summary>
        /// 附件类型
        /// ENUM[int]
        /// 0-File-文件
        /// 10-Image-图片
        /// 20-Audio-音频
        /// 30-Video-视频
        /// </summary>
        [Description("附件类型")]
        [Property(Column = "Type", NotNull = true)]
        public virtual SysAttachmentType Type { get; set; }

        /// <summary>
        /// 附件名称
        /// </summary>
        [Description("附件名称")]
        [Property(Column = "Name", NotNull = true, Length = 128)]
        public virtual string Name { get; set; }

        /// <summary>
        /// 附件路径
        /// </summary>
        [Description("附件路径")]
        [Property(Column = "Path", NotNull = true, Length = 256)]
        public virtual string Path { get; set; }

        /// <summary>
        /// 附件大小（KB）
        /// </summary>
        [Description("附件大小（KB）")]
        [Property(Column = "Size", NotNull = true)]
        public virtual int Size { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        [Property(Column = "Remark", Length = 256)]
        public virtual string Remark { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        [Description("创建者")]
        [Property(Column = "CreatedBy", NotNull = true)]
        public virtual int CreatedBy { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Property(Column = "CreatedTime", NotNull = true)]
        public virtual DateTime CreatedTime { get; set; }
    }
}
