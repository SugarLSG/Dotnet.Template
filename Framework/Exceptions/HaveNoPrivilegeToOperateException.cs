﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("无权限进行操作")]
    public sealed class HaveNoPrivilegeToOperateException : APPBaseException
    {
        /// <summary>
        /// message: (0003) 无权限进行操作
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public HaveNoPrivilegeToOperateException(params object[] args)
        {
            Code = "0003";
            Args = args;
        }
    }
}
