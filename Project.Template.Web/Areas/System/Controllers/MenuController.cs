﻿using APP.Core.Utilities;
using Framework.Models.Menu;
using Service.Abstracts;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace Project.Template.Web.Areas.System.Controllers
{
    public class MenuController : Controller
    {
        private IMenuService MenuService { get; set; }


        [Description("配置菜单")]
        public ActionResult Index()
        {
            ViewBag.MenuList = MenuService.GetAllMenu();

            return View();
        }

        [Description("添加父菜单")]
        public ActionResult ParentMenu()
        {
            return View(new MenuInfo());
        }

        [Description("添加子菜单")]
        public ActionResult SubMenu(bool canAddParentMenu)
        {
            ViewBag.CanAddParentMenu = canAddParentMenu;

            return View();
        }

        [Description("编辑菜单")]
        public ActionResult Edit(MenuInfo info)
        {
            return View(info);
        }


        [HttpPost]
        [Description("更新菜单")]
        public string Update(IEnumerable<MenuInfo> model)
        {
            MenuService.Update(model);

            return JsonUtility.SerializeAjaxSuccessJSON();
        }
    }
}