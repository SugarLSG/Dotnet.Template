﻿using APP.Core.Utilities;
using Framework.Utilities;
using Service.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Project.Template.Web.Areas.API.Controllers
{
    [AllowAnonymous]
    public class CommonController : Controller
    {
        private static readonly bool TEST_API_PARAM = ConfigurationUtility.GetBool(CommonSetting.APPSETTINGS_TEST_API_PARAM);

        private IUserService UserService { get; set; }
        private IAreaService AreaService { get; set; }


        public string AppId()
        {
            return APIUtility.AppId();
        }

        public string AppSecret()
        {
            return APIUtility.AppSecret();
        }


        public string EncryptParam(string json, bool showProcess = false)
        {
            if (!TEST_API_PARAM)
                return "have no privilege.";
            if (!json.HasValue())
                return "json is required.";

            var operates = new string[]
            {
                "json UrlDecode",
                "json HEXEncrypt（UTF8）",
                "json XOREncrypt",
                "json BASE64Encrypt（UTF8）"
            };

            if (showProcess)
            {
                var jsonHEX = SecurityUtility.HEXEncrypt(json);
                var finalKey = new StringBuilder(APIUtility.SignedKeyHEX);
                var keyLength = jsonHEX.Length;
                while (finalKey.Length < keyLength)
                    finalKey.Append(APIUtility.SignedKeyHEX);

                return JsonUtility.Serialize(new
                {
                    operates = operates,
                    jsonHex = jsonHEX,
                    key = APIUtility.SignedKey,
                    keyHex = APIUtility.SignedKeyHEX,
                    xorChars = jsonHEX.Zip(finalKey.ToString().Substring(0, keyLength), (c1, c2) => c1 ^ c2),
                    finalCrypto = APIUtility.EncryptParam(HttpUtility.UrlDecode(json))
                });
            }
            else
            {
                return JsonUtility.Serialize(new
                {
                    operates = operates,
                    key = APIUtility.SignedKey,
                    finalCrypto = APIUtility.EncryptParam(HttpUtility.UrlDecode(json))
                });
            }
        }

        public string APISign(bool showProcess = false)
        {
            if (!TEST_API_PARAM)
                return "have no privilege.";

            var requestParams = new Dictionary<string, string>();
            foreach (var key in Request.QueryString.AllKeys.Concat(Request.Form.AllKeys).Where(i => i.HasValue()))
            {
                if (key.Equals("showprocess", StringComparison.CurrentCultureIgnoreCase))
                    continue;

                requestParams.Add(key.ToLower(), Request.Params[key]);
            }

            if (showProcess)
            {
                return JsonUtility.Serialize(new
                {
                    parameters = requestParams,
                    queryString = string.Join("&", requestParams.OrderByASCII(i => i.Key).Select(i => string.Format("{0}={1}", i.Key, i.Value))),
                    sign = APIUtility.APISign(requestParams)
                });
            }
            else
            {
                return JsonUtility.Serialize(new
                {
                    sign = APIUtility.APISign(requestParams)
                });
            }
        }

        public string APIToken(string appId, string secret, string sign, bool showProcess = false)
        {
            if (!TEST_API_PARAM)
                return "have no privilege.";

            if (showProcess)
            {
                return JsonUtility.Serialize(new
                {
                    parameters = string.Join("", new string[] { appId, secret, sign }.OrderByASCII(i => i)),
                    token = APIUtility.APIToken(appId, secret, sign)
                });
            }
            else
            {
                return JsonUtility.Serialize(new
                {
                    token = APIUtility.APIToken(appId, secret, sign)
                });
            }
        }


        public ActionResult CheckUserAccount(int? userId, string account)
        {
            return Json(!UserService.IsAccountExisted(userId, account), JsonRequestBehavior.AllowGet);
        }


        public string AllAreas()
        {
            return JsonUtility.Serialize(AreaService.AllAreas());
        }
    }
}