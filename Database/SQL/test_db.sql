/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2017-12-05 15:04:39                          */
/*==============================================================*/


drop trigger T_Privilege_Before_Delete;

drop table if exists sys_attachment;

drop index UIX_City_Code on sys_city;

drop table if exists sys_city;

drop index UIX_Country_ISOCodeNumber on sys_country;

drop index UIX_Country_ISOCode3 on sys_country;

drop index UIX_Country_ISOCode2 on sys_country;

drop table if exists sys_country;

drop index IX_District_CityId on sys_district;

drop index UIX_District_Code on sys_district;

drop table if exists sys_district;

drop table if exists sys_menu;

drop table if exists sys_privilege;

drop index UIX_Province_ShortNameCN on sys_province;

drop index UIX_Province_Code on sys_province;

drop table if exists sys_province;

drop table if exists sys_role;

drop index UIX_RolePrivilege on sys_role_privilege;

drop table if exists sys_role_privilege;

drop index UIX_User_Account on sys_user;

drop table if exists sys_user;

drop index UIX_UserRole on sys_user_role;

drop table if exists sys_user_role;

/*==============================================================*/
/* Table: sys_attachment                                        */
/*==============================================================*/
create table sys_attachment
(
   AttachmentId         int not null auto_increment comment '附件Id',
   Type                 int not null comment '附件类型
            ENUM[int]
            0-File-文件
            10-Image-图片
            20-Audio-音频
            30-Video-视频',
   Name                 varchar(128) not null comment '附件名称',
   Path                 varchar(256) not null comment '附件路径',
   Size                 int not null comment '附件大小（KB）',
   Remark               varchar(256) comment '备注',
   CreatedBy            int not null default 0 comment '创建者',
   CreatedTime          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   primary key (AttachmentId)
);

alter table sys_attachment comment '附件';

/*==============================================================*/
/* Table: sys_city                                              */
/*==============================================================*/
create table sys_city
(
   CityId               int not null auto_increment comment '城市Id',
   ProvinceId           int not null comment '省份Id',
   Code                 varchar(8) not null comment '编码',
   NameEN               varchar(256) not null comment '英文名',
   NameCN               varchar(256) not null comment '中文名',
   CreatedTime          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   ModifiedTime         datetime not null default CURRENT_TIMESTAMP comment '修改时间',
   primary key (CityId)
);

alter table sys_city comment '城市';

/*==============================================================*/
/* Index: UIX_City_Code                                         */
/*==============================================================*/
create unique index UIX_City_Code on sys_city
(
   Code
);

/*==============================================================*/
/* Table: sys_country                                           */
/*==============================================================*/
create table sys_country
(
   CountryId            int not null auto_increment comment '国家Id',
   ContinentCode        char(2) not null comment '洲际码
            ENUM[string]
            AS-AS-亚洲
            EU-EU-欧洲
            AM-AM-美洲
            OA-OA-大洋洲
            AF-AF-非洲
            OT-OT-其他',
   ISOCode2             char(2) not null comment 'ISO代码（2位字母）',
   ISOCode3             char(3) not null comment 'ISO代码（3位字母）',
   ISOCodeNumber        char(3) not null comment 'ISO代码（3位数字）',
   NameEN               varchar(256) not null comment '英文名',
   NameCN               varchar(256) not null comment '中文名',
   CreatedTime          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   ModifiedTime         datetime not null default CURRENT_TIMESTAMP comment '修改时间',
   primary key (CountryId)
);

alter table sys_country comment '国家';

/*==============================================================*/
/* Index: UIX_Country_ISOCode2                                  */
/*==============================================================*/
create unique index UIX_Country_ISOCode2 on sys_country
(
   ISOCode2
);

/*==============================================================*/
/* Index: UIX_Country_ISOCode3                                  */
/*==============================================================*/
create unique index UIX_Country_ISOCode3 on sys_country
(
   ISOCode3
);

/*==============================================================*/
/* Index: UIX_Country_ISOCodeNumber                             */
/*==============================================================*/
create unique index UIX_Country_ISOCodeNumber on sys_country
(
   ISOCodeNumber
);

/*==============================================================*/
/* Table: sys_district                                          */
/*==============================================================*/
create table sys_district
(
   DistrictId           int not null auto_increment comment '地区Id',
   CityId               int not null comment '城市Id',
   Code                 varchar(8) not null comment '编码',
   NameEN               varchar(256) not null comment '英文名',
   NameCN               varchar(256) not null comment '中文名',
   CreatedTime          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   ModifiedTime         datetime not null default CURRENT_TIMESTAMP comment '修改时间',
   primary key (DistrictId)
);

alter table sys_district comment '地区';

/*==============================================================*/
/* Index: UIX_District_Code                                     */
/*==============================================================*/
create unique index UIX_District_Code on sys_district
(
   Code
);

/*==============================================================*/
/* Index: IX_District_CityId                                    */
/*==============================================================*/
create index IX_District_CityId on sys_district
(
   CityId
);

/*==============================================================*/
/* Table: sys_menu                                              */
/*==============================================================*/
create table sys_menu
(
   MenuId               int not null auto_increment comment '菜单Id',
   ParentMenuId         int comment '父级菜单Id',
   PrivilegeCode        varchar(128) comment '权限Code',
   Name                 varchar(128) not null comment '菜单名称',
   Icon                 varchar(32) comment '菜单图标（样式）',
   Description          varchar(256) comment '菜单说明',
   Sorted               int not null comment '排序',
   IsVisible            bit not null comment '是否显示',
   CreatedBy            int not null default 0 comment '创建者',
   CreatedTime          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   ModifiedBy           int not null default 0 comment '修改者',
   ModifiedTime         datetime not null default CURRENT_TIMESTAMP comment '修改时间',
   primary key (MenuId)
);

alter table sys_menu comment '菜单';

/*==============================================================*/
/* Table: sys_privilege                                         */
/*==============================================================*/
create table sys_privilege
(
   PrivilegeCode        varchar(128) not null comment '权限Code
            ENUM[string,Privilege.PrivilegeCode]',
   Name                 varchar(128) not null comment '权限名称',
   Description          varchar(256) comment '权限说明',
   Status               int not null default 1 comment '状态
            ENUM[int]
            -1-Disabled-已禁用
            1-Enabled-已启用',
   CreatedBy            int not null default 0 comment '创建者',
   CreatedTime          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   ModifiedBy           int not null default 0 comment '修改者',
   ModifiedTime         datetime not null default CURRENT_TIMESTAMP comment '修改时间',
   primary key (PrivilegeCode)
);

alter table sys_privilege comment '权限';

/*==============================================================*/
/* Table: sys_province                                          */
/*==============================================================*/
create table sys_province
(
   ProvinceId           int not null auto_increment comment '省份Id',
   CountryId            int not null comment '国家Id',
   Code                 varchar(8) not null comment '编码',
   NameEN               varchar(256) not null comment '英文名',
   NameCN               varchar(256) not null comment '中文名',
   ShortNameCN          char(1) comment '中文简称',
   CreatedTime          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   ModifiedTime         datetime not null default CURRENT_TIMESTAMP comment '修改时间',
   primary key (ProvinceId)
);

alter table sys_province comment '省份';

/*==============================================================*/
/* Index: UIX_Province_Code                                     */
/*==============================================================*/
create unique index UIX_Province_Code on sys_province
(
   Code
);

/*==============================================================*/
/* Index: UIX_Province_ShortNameCN                              */
/*==============================================================*/
create unique index UIX_Province_ShortNameCN on sys_province
(
   ShortNameCN
);

/*==============================================================*/
/* Table: sys_role                                              */
/*==============================================================*/
create table sys_role
(
   RoleCode             varchar(128) not null comment '角色Code
            ENUM[string,Role.RoleCode]',
   Name                 varchar(128) not null comment '角色名称',
   Description          varchar(256) comment '角色说明',
   Status               int not null default 1 comment '状态
            ENUM[int]
            -1-Disabled-已禁用
            1-Enabled-已启用',
   CreatedBy            int not null default 0 comment '创建者',
   CreatedTime          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   ModifiedBy           int not null default 0 comment '修改者',
   ModifiedTime         datetime not null default CURRENT_TIMESTAMP comment '修改时间',
   primary key (RoleCode)
);

alter table sys_role comment '角色';

/*==============================================================*/
/* Table: sys_role_privilege                                    */
/*==============================================================*/
create table sys_role_privilege
(
   RolePrivilegeId      int not null auto_increment comment '角色权限Id',
   RoleCode             varchar(128) not null comment '角色Code
            ENUM[string,Role.RoleCode]',
   PrivilegeCode        varchar(128) not null comment '权限Code
            ENUM[string,Privilege.PrivilegeCode]',
   CreatedBy            int not null default 0 comment '创建者',
   CreatedTime          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   ModifiedBy           int not null default 0 comment '修改者',
   ModifiedTime         datetime not null default CURRENT_TIMESTAMP comment '修改时间',
   primary key (RolePrivilegeId)
);

alter table sys_role_privilege comment '角色权限';

/*==============================================================*/
/* Index: UIX_RolePrivilege                                     */
/*==============================================================*/
create unique index UIX_RolePrivilege on sys_role_privilege
(
   RoleCode,
   PrivilegeCode
);

/*==============================================================*/
/* Table: sys_user                                              */
/*==============================================================*/
create table sys_user
(
   UserId               int not null auto_increment comment '用户Id',
   Account              varchar(128) not null comment '用户账号',
   Password             varchar(128) not null comment '用户密码',
   UpdatedPassword      bit not null default 0 comment '已更新用户密码',
   Type                 int not null comment '用户类型
            ENUM[int,UserType]',
   Name                 varchar(128) not null comment '用户名称',
   Sex                  int not null default 0 comment '用户性别
            ENUM[int,UserSex]',
   HeadImageId          int comment '头像附件Id',
   Mail                 varchar(64) comment '邮箱地址',
   PhoneNumber          varchar(32) comment '联系电话',
   QQ                   varchar(32) comment 'QQ号码',
   AppId                varchar(32) comment '凭证Id',
   AppSecret            varchar(32) comment '凭证秘钥',
   Status               int not null default 0 comment '状态
            ENUM[int]
            -1-Disabled-已禁用
            0-Inactived-未激活
            1-Enabled-已启用',
   CreatedBy            int not null default 0 comment '创建者',
   CreatedTime          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   ModifiedBy           int not null default 0 comment '修改者',
   ModifiedTime         datetime not null default CURRENT_TIMESTAMP comment '修改时间',
   primary key (UserId)
);

alter table sys_user comment '用户';

/*==============================================================*/
/* Index: UIX_User_Account                                      */
/*==============================================================*/
create unique index UIX_User_Account on sys_user
(
   Account
);

/*==============================================================*/
/* Table: sys_user_role                                         */
/*==============================================================*/
create table sys_user_role
(
   UserRoleId           int not null auto_increment comment '用户角色Id',
   UserId               int not null comment '用户Id',
   RoleCode             varchar(128) not null comment '角色Code
            ENUM[string,Role.RoleCode]',
   CreatedBy            int not null default 0 comment '创建者',
   CreatedTime          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   ModifiedBy           int not null default 0 comment '修改者',
   ModifiedTime         datetime not null default CURRENT_TIMESTAMP comment '修改时间',
   primary key (UserRoleId)
);

alter table sys_user_role comment '用户角色';

/*==============================================================*/
/* Index: UIX_UserRole                                          */
/*==============================================================*/
create unique index UIX_UserRole on sys_user_role
(
   UserId,
   RoleCode
);

alter table sys_city add constraint FK_City_ProvinceId foreign key (ProvinceId)
      references sys_province (ProvinceId) on delete restrict on update restrict;

alter table sys_district add constraint FK_District_CityId foreign key (CityId)
      references sys_city (CityId) on delete restrict on update restrict;

alter table sys_menu add constraint FK_Menu_ParentMenuId foreign key (ParentMenuId)
      references sys_menu (MenuId) on delete restrict on update restrict;

alter table sys_menu add constraint FK_Menu_PrivilegeCode foreign key (PrivilegeCode)
      references sys_privilege (PrivilegeCode) on delete restrict on update restrict;

alter table sys_province add constraint FK_Province_CountryId foreign key (CountryId)
      references sys_country (CountryId) on delete restrict on update restrict;

alter table sys_role_privilege add constraint FK_RolePrivilege_PrivilegeCode foreign key (PrivilegeCode)
      references sys_privilege (PrivilegeCode) on delete restrict on update restrict;

alter table sys_role_privilege add constraint FK_RolePrivilege_RoleCode foreign key (RoleCode)
      references sys_role (RoleCode) on delete restrict on update restrict;

alter table sys_user add constraint FK_User_HeadImageId foreign key (HeadImageId)
      references sys_attachment (AttachmentId) on delete restrict on update restrict;

alter table sys_user_role add constraint FK_UserRole_RoleCode foreign key (RoleCode)
      references sys_role (RoleCode) on delete restrict on update restrict;

alter table sys_user_role add constraint FK_UserRole_UserId foreign key (UserId)
      references sys_user (UserId) on delete restrict on update restrict;


create trigger T_Privilege_Before_Delete before delete
on sys_privilege for each row
begin
   -- 清除菜单、角色权限对应数据
   DELETE FROM sys_menu WHERE PrivilegeCode = old.PrivilegeCode;
   DELETE FROM sys_role_privilege WHERE PrivilegeCode = old.PrivilegeCode;
end;

