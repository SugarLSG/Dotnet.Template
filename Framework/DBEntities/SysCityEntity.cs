﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Attributes;
using APP.Core.Extensions.Nhibernate;
using Newtonsoft.Json;
using Framework.Enums;
using NHibernate.Mapping.Attributes;
using NHibernate.Type;
using System;
using System.ComponentModel;

namespace Framework.DBEntity
{
    /// <summary>
    /// 城市
    /// </summary>
    [Class(0, Table = "sys_city")]
    [Cache(1, Usage = CacheUsage.ReadWrite)]
    public class SysCityEntity
    {
        public const string TABLE_NAME = "sys_city";
        public const string COL_CITYID = "CityId";
        public const string COL_PROVINCEID = "ProvinceId";
        public const string COL_CODE = "Code";
        public const string COL_NAMEEN = "NameEN";
        public const string COL_NAMECN = "NameCN";
        public const string COL_CREATEDTIME = "CreatedTime";
        public const string COL_MODIFIEDTIME = "ModifiedTime";

        /// <summary>
        /// 城市Id
        /// </summary>
        [Description("城市Id")]
        [Id(0, Name = "CityId", Column = "CityId")]
        [Key(1)]
        [Generator(2, Class = "native")]
        public virtual int? CityId { get; set; }

        /// <summary>
        /// 省份Id
        /// </summary>
        [Description("省份Id")]
        [Property(Column = "ProvinceId", NotNull = true)]
        public virtual int ProvinceId { get; set; }

        private SysProvinceEntity _province = null;
        /// <summary>
        /// 外键 ProvinceId 对应 sys_province 类实例;
        /// </summary>
        [IgnoreConvertTypeScalar]
        [JsonIgnore]
        public virtual SysProvinceEntity Province { get { return _province ?? (_province = NHibernateExtension.GetEntityById<SysProvinceEntity>(ProvinceId)); } }

        /// <summary>
        /// 编码
        /// </summary>
        [Description("编码")]
        [Property(Column = "Code", NotNull = true, Length = 8)]
        public virtual string Code { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        [Description("英文名")]
        [Property(Column = "NameEN", NotNull = true, Length = 256)]
        public virtual string NameEN { get; set; }

        /// <summary>
        /// 中文名
        /// </summary>
        [Description("中文名")]
        [Property(Column = "NameCN", NotNull = true, Length = 256)]
        public virtual string NameCN { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Property(Column = "CreatedTime", NotNull = true)]
        public virtual DateTime CreatedTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        [Property(Column = "ModifiedTime", NotNull = true)]
        public virtual DateTime ModifiedTime { get; set; }
    }
}
