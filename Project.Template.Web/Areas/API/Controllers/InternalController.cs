﻿using Service.Abstracts;
using System.ComponentModel;
using System.Web.Mvc;

namespace Project.Template.Web.Areas.API.Controllers
{
    public class InternalController : Controller
    {
        private IUserService UserService { get; set; }


        [Description("API测试")]
        public ActionResult Index()
        {
            ViewBag.UserInfo = UserService.GetInfo(AuthenticationUtility.AuthorizedUserId.Value);

            return View();
        }
    }
}