﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("用户未登录")]
    public sealed class UserIsNotLoggedInException : APPBaseException
    {
        /// <summary>
        /// message: (2005) 用户未登录
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public UserIsNotLoggedInException(params object[] args)
        {
            Code = "2005";
            Args = args;
        }
    }
}
