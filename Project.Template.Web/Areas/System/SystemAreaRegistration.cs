﻿using System.Web.Mvc;

namespace Project.Template.Web.Areas.System
{
    public class SystemAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "System";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "System_default",
                "System/{controller}/{action}/{param}",
                new { action = "Index", param = UrlParameter.Optional }
            );
        }
    }
}