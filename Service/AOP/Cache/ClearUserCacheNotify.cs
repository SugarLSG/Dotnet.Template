﻿using APP.Core.AOP.Notify;
using APP.Core.Utilities;
using System.Reflection;

namespace Service.AOP.Cache
{
    public class ClearUserCacheNotify : IAOPPostAsyncNotify
    {
        public void Post(MethodInfo methodInfo, object[] args, object returnValue)
        {
            if (returnValue is int)
            {
                CacheUtility.DeleteCacheByKey(string.Format(CommonSetting.CACHEKEY_USER_MENU_FORMAT, returnValue));
                CacheUtility.DeleteCacheByKey(string.Format(CommonSetting.CACHEKEY_USER_PRIVILEGE_FORMAT, returnValue));
            }
            else
            {
                CacheUtility.DeleteCacheByKeyword(CommonSetting.CACHEKEY_USER_MENU_FORMAT.Substring(0, "."));
                CacheUtility.DeleteCacheByKeyword(CommonSetting.CACHEKEY_USER_PRIVILEGE_FORMAT.Substring(0, "."));
            }
        }
    }
}
