﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("无效的请求")]
    public sealed class InvalidRequestException : APPBaseException
    {
        /// <summary>
        /// message: (0004) 无效的请求
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public InvalidRequestException(params object[] args)
        {
            Code = "0004";
            Args = args;
        }
    }
}
