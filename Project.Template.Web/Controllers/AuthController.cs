﻿using APP.Core.Utilities;
using Framework.Exceptions;
using Framework.Models.Auth;
using Service.Abstracts;
using System.Web.Mvc;

namespace Project.Template.Web.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        private IAuthService AuthService { get; set; }


        #region 登录/忘记密码/注册

        public ActionResult Index(string returnUrl)
        {
            // 是否已登录
            if (AuthenticationUtility.IsAuthenticated())
            {
                if (returnUrl.HasValue())
                    return Redirect(returnUrl);

                var menu = AuthenticationUtility.AuthorizedUserFirstMenu;
                if (menu != null)
                    return Redirect(menu.Url);

                Logout();
            }

            return View(new LoginInfo { ReturnUrl = returnUrl });
        }

        [HttpPost]
        public string Login(LoginInfo model)
        {
            var userInfo = AuthService.AuthenticatUser(model);
            AuthenticationUtility.SignIn(userInfo);

            var returnUrl = model.ReturnUrl;
            if (!returnUrl.HasValue())
            {
                var menu = AuthenticationUtility.AuthorizedUserFirstMenu;
                if (menu != null)
                    returnUrl = menu.Url;
            }

            if (!returnUrl.HasValue())
            {
                Logout();
                throw new HaveNoUserPrivilegeException();
            }

            return JsonUtility.SerializeAjaxSuccessJSON(data: returnUrl);
        }

        #endregion

        #region 登出

        public ActionResult Logout()
        {
            AuthenticationUtility.SignOut();
            return Redirect("/");
        }

        #endregion
    }
}