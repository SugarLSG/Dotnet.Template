﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Attributes;
using APP.Core.Extensions.Nhibernate;
using Newtonsoft.Json;
using Framework.Enums;
using NHibernate.Mapping.Attributes;
using NHibernate.Type;
using System;
using System.ComponentModel;

namespace Framework.DBEntity
{
    /// <summary>
    /// 角色
    /// </summary>
    [Class(0, Table = "sys_role")]
    [Cache(1, Usage = CacheUsage.ReadWrite)]
    public class SysRoleEntity
    {
        public const string TABLE_NAME = "sys_role";
        public const string COL_ROLECODE = "RoleCode";
        public const string COL_NAME = "Name";
        public const string COL_DESCRIPTION = "Description";
        public const string COL_STATUS = "Status";
        public const string COL_CREATEDBY = "CreatedBy";
        public const string COL_CREATEDTIME = "CreatedTime";
        public const string COL_MODIFIEDBY = "ModifiedBy";
        public const string COL_MODIFIEDTIME = "ModifiedTime";

        /// <summary>
        /// 角色Code
        /// ENUM[string,Role.RoleCode]
        /// </summary>
        [Description("角色Code")]
        [Id(0, Name = "RoleCode", Column = "RoleCode", Length = 128, TypeType = typeof(EnumStringType<Role.RoleCode>))]
        [Key(1)]
        [Generator(2, Class = "assigned")]
        public virtual Role.RoleCode? RoleCode { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        [Description("角色名称")]
        [Property(Column = "Name", NotNull = true, Length = 128)]
        public virtual string Name { get; set; }

        /// <summary>
        /// 角色说明
        /// </summary>
        [Description("角色说明")]
        [Property(Column = "Description", Length = 256)]
        public virtual string Description { get; set; }

        /// <summary>
        /// 状态
        /// ENUM[int]
        /// -1-Disabled-已禁用
        /// 1-Enabled-已启用
        /// </summary>
        [Description("状态")]
        [Property(Column = "Status", NotNull = true)]
        public virtual SysRoleStatus Status { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        [Description("创建者")]
        [Property(Column = "CreatedBy", NotNull = true)]
        public virtual int CreatedBy { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Property(Column = "CreatedTime", NotNull = true)]
        public virtual DateTime CreatedTime { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        [Description("修改者")]
        [Property(Column = "ModifiedBy", NotNull = true)]
        public virtual int ModifiedBy { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        [Property(Column = "ModifiedTime", NotNull = true)]
        public virtual DateTime ModifiedTime { get; set; }
    }
}
