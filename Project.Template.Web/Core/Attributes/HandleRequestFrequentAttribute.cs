﻿using APP.Core.Utilities;
using Framework.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Project.Template.Web.Core.Attributes
{
    public class HandleRequestFrequentAttribute : ActionFilterAttribute
    {
        public enum FilterLevel
        {
            /// <summary>
            /// 包括参数（仅Route配置）
            /// </summary>
            IncludeParameters,

            /// <summary>
            /// 不包括参数
            /// </summary>
            NotIncludedParameters
        }

        public enum LimitLevel
        {
            /// <summary>
            /// 小时
            /// </summary>
            Hour,

            /// <summary>
            /// 分钟
            /// </summary>
            Minute,

            /// <summary>
            /// 秒
            /// </summary>
            Second
        }

        private class CacheItem
        {
            public string UserIP { get; set; }

            public string Path { get; set; }

            public DateTime Time { get; set; }

            public int Count { get; set; }
        }


        public int[] Limits { get; }
        public FilterLevel PFilterLevel { get; }
        public LimitLevel PLimitLevel { get; }

        private IList<CacheItem> Caches = new List<CacheItem>();


        /// <summary>
        /// （请求）限制频繁请求（Order: 11）
        /// </summary>
        /// <param name="limits">限制次数（24个值，分别对应每个小时，解释为在某个小时中，指定时间级别(limitLevel)上限制次数）</param>
        /// <param name="filterLevel">限制路径级别</param>
        /// <param name="limitLevel">限制时间级别</param>
        public HandleRequestFrequentAttribute(int[] limits, FilterLevel filterLevel, LimitLevel limitLevel = LimitLevel.Minute)
        {
            if (limits == null || limits.Length != 24)
                throw new ArgumentException("limits");

            Order = 11;
            Limits = limits;
            PFilterLevel = filterLevel;
            PLimitLevel = limitLevel;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // 访问路径
            var area = filterContext.RouteData.DataTokens["area"];
            var controller = filterContext.RouteData.Values["controller"];
            var action = filterContext.RouteData.Values["action"];
            var path = string.Format("/{0}/{1}/{2}?", area, controller, action).Replace("//", "/").ToLower();
            if (PFilterLevel == FilterLevel.NotIncludedParameters)
            {
                path += string.Join("&", filterContext.RouteData.Values.Keys
                    .Where(k => !k.Equals("controller", StringComparison.CurrentCultureIgnoreCase) && !k.Equals("action", StringComparison.CurrentCultureIgnoreCase))
                    .Select(k => string.Format("{0}={1}", k, filterContext.RouteData.Values[k])));
            }

            // 时间
            var now = DateTime.Now;
            var hours = now.Hour;
            var currentTime = new DateTime(now.Year, now.Month, now.Day, now.Hour, PLimitLevel < LimitLevel.Minute ? 0 : now.Minute, PLimitLevel < LimitLevel.Second ? 0 : now.Second);

            // 用户IP
            var ip = ClientRequestUtility.ClientIP;

            // 增加计数
            var current = Caches.FirstOrDefault(c => c.UserIP == ip && c.Path == path);
            if (current == null)
            {
                current = new CacheItem { UserIP = ip, Path = path, Time = currentTime, Count = 1 };
                Caches.Add(current);
            }
            else if (current.Time != currentTime)
            {
                current.Time = currentTime;
                current.Count = 1;
            }
            else ++current.Count;

            // 判断次数
            var limitCount = Limits[hours];
            if (current.Count > limitCount)
                throw new RequestsTooFrequentException();
        }
    }
}