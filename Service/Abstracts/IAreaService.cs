﻿using Framework.DBEntity;
using System.Collections.Generic;

namespace Service.Abstracts
{
    public interface IAreaService
    {
        /// <summary>
        /// 获取所有国家数据
        /// </summary>
        /// <returns></returns>
        IEnumerable<SysCountryEntity> AllCountries();

        /// <summary>
        /// 获取所有省份数据
        /// </summary>
        /// <returns></returns>
        IEnumerable<SysProvinceEntity> AllProvinces();

        /// <summary>
        /// 获取所有城市数据
        /// </summary>
        /// <returns></returns>
        IEnumerable<SysCityEntity> AllCities();

        /// <summary>
        /// 获取所有地区数据
        /// </summary>
        /// <returns></returns>
        IEnumerable<SysDistrictEntity> AllDistricts();

        /// <summary>
        /// 获取所有地区数据（洲、国、省、市、区）
        /// </summary>
        /// <returns></returns>
        object AllAreas();
    }
}
