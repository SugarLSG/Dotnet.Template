﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

public static class ModelStateExtension
{
    /// <summary>
    /// 获取验证错误信息列表
    /// </summary>
    /// <param name="modelState"></param>
    /// <returns></returns>
    public static IEnumerable<string> GetModelInvalidMessages(this ModelStateDictionary modelState)
    {
        return modelState.Values
            .Where(ms => ms.Errors.Any())
            .SelectMany(ms => ms.Errors.Select(e => e.ErrorMessage));
    }

    /// <summary>
    /// 获取验证错误信息列表，并组装成前端显示内容
    /// </summary>
    /// <param name="modelState"></param>
    /// <returns></returns>
    public static string GetModelInvalidMessagesContent(this ModelStateDictionary modelState)
    {
        return string.Format("<ol><li>{0}</li></ol>", string.Join("</li><li>", modelState.GetModelInvalidMessages()));
    }
}
