﻿using Framework.Privilege;
using Framework.Role;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Framework.Models.Role
{
    public class RoleInfo
    {
        /// <summary>
        /// 角色Code
        /// </summary>
        [DisplayName("角色Code")]
        [Required]
        public RoleCode? RoleCode { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [DisplayName("名称")]
        [Required]
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 说明
        /// </summary>
        [DisplayName("说明")]
        [MaxLength(32)]
        public string Description { get; set; }

        /// <summary>
        /// 启用角色
        /// </summary>
        [DisplayName("启用角色")]
        public bool IsEnabled { get; set; }


        /// <summary>
        /// 权限列表
        /// </summary>
        [DisplayName("权限列表")]
        [CollectionLength(int.MaxValue, 1, ErrorMessage = "至少选择一个权限")]
        public IEnumerable<PrivilegeCode> PrivilegeList { get; set; }
    }
}
