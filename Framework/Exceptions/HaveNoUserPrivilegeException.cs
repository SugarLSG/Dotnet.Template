﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("该账号无任何权限")]
    public sealed class HaveNoUserPrivilegeException : APPBaseException
    {
        /// <summary>
        /// message: (2004) 该账号无任何权限
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public HaveNoUserPrivilegeException(params object[] args)
        {
            Code = "2004";
            Args = args;
        }
    }
}
