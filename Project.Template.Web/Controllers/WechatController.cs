﻿using APP.Core.Utilities;
using Project.Template.Web.Core.Wechat;
using Senparc.Weixin;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.CommonAPIs;
using Senparc.Weixin.MP.Entities.Menu;
using Senparc.Weixin.MP.Entities.Request;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Project.Template.Web.Controllers
{
    [AllowAnonymous]
    public class WechatController : Controller
    {
        /// <summary>
        /// 微信后台验证
        /// </summary>
        /// <param name="postModel"></param>
        /// <param name="echostr"></param>
        /// <returns></returns>
        [HttpGet]
        public string Index(PostModel postModel, string echostr)
        {
            if (CheckSignature.Check(postModel.Signature, postModel.Timestamp, postModel.Nonce, WechatSetting.Token))
                return echostr;

            return CommonSetting.MESSAGE_INVALIDREQUEST;
        }

        /// <summary>
        /// 微信用户信息推送
        /// </summary>
        /// <param name="postModel"></param>
        /// <returns></returns>
        [HttpPost]
        public string Index(PostModel postModel)
        {
            if (!CheckSignature.Check(postModel.Signature, postModel.Timestamp, postModel.Nonce, WechatSetting.Token))
                return CommonSetting.MESSAGE_INVALIDREQUEST;

            var messageHandler = new WechatMessageHandler(postModel);
            return messageHandler.ResponseDocument != null ? messageHandler.ResponseDocument.ToString() : string.Empty;
        }

        #region OAuth

        public string OAuthUrl(string path, string transfer = null, OAuthScope scope = OAuthScope.snsapi_base)
        {
            if (!path.HasValue())
                return CommonSetting.MESSAGE_INVALIDREQUEST;

            return WechatUtility.GenerateAuthorizeUrl(path, transfer, scope);
        }

        public ActionResult OAuth(string path, string transfer = null, OAuthScope scope = OAuthScope.snsapi_base)
        {
            return Redirect(OAuthUrl(path, transfer, scope));
        }

        public ActionResult Callback(string code, string state, string param)
        {
            if (!code.HasValue())
                return Content(CommonSetting.MESSAGE_INVALIDREQUEST);

            var model = WechatUtility.DecryptParam(state, param);
            if (model == null)
                return Content(CommonSetting.MESSAGE_INVALIDREQUEST);

            try
            {
                var result = OAuthApi.GetAccessToken(WechatSetting.AppId, WechatSetting.AppSecret, code);
                if (result.errcode != ReturnCode.请求成功)
                    return Content(result.errmsg);

                return Redirect(string.Format(
                    "{0}{1}{2}scope={3}&accesstoken={4}&openid={5}&transfer={6}",
                    AppSetting.WebsiteHost,
                    model.RedirectUrl,
                    model.RedirectUrl.IndexOf('?') >= 0 ? "&" : "?",
                    model.Scope,
                    result.access_token,
                    result.openid,
                    model.Transfer
                ));
            }
            catch (Exception ex) { return Content(ex.Message); }
        }

        #endregion

        #region Menu

        public string GetMenu()
        {
            var result = CommonApi.GetMenu(WechatSetting.AppId);

            return JsonUtility.Serialize(result);
        }

        public string CreateMenu()
        {
            var bg = new ButtonGroup();

            // button1
            bg.button.Add(new SingleViewButton
            {
                url = WechatUtility.GenerateAuthorizeUrl(""),
                name = "button1"
            });

            // button2
            bg.button.Add(new SubButton
            {
                name = "button group",
                sub_button = new List<SingleButton>
                {
                    new SingleViewButton
                    {
                        url = WechatUtility.GenerateAuthorizeUrl(""),
                        name = "sub button1"
                    },
                    new SingleViewButton
                    {
                        url = WechatUtility.GenerateAuthorizeUrl(""),
                        name = "sub button2"
                    }
                }
            });

            var result = CommonApi.CreateMenu(WechatSetting.AppId, bg);

            return JsonUtility.Serialize(result);
        }

        public string DeleteMenu()
        {
            var result = CommonApi.DeleteMenu(WechatSetting.AppId);

            return JsonUtility.Serialize(result);
        }

        #endregion
    }
}