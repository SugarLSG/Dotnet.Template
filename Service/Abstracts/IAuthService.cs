﻿using Framework.Models.Auth;

namespace Service.Abstracts
{
    public interface IAuthService
    {
        string PasswordEncrypt(string originalPassword);

        UserAuthorizedInfo AuthenticatUser(int userId);

        UserAuthorizedInfo AuthenticatUser(LoginInfo model);

        UserAPIAuthorizedInfo APIAuthenticatUser(string appId);
    }
}
