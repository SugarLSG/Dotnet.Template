﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("仅支持POST方式请求")]
    public sealed class OnlySupportsPOSTRequestException : APPBaseException
    {
        /// <summary>
        /// message: (0008) 仅支持POST方式请求
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public OnlySupportsPOSTRequestException(params object[] args)
        {
            Code = "0008";
            Args = args;
        }
    }
}
