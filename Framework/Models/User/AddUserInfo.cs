﻿using Framework.Enums;
using Framework.Role;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Framework.Models.User
{
    public class AddUserInfo
    {
        /// <summary>
        /// 账号
        /// </summary>
        [DisplayName("账号")]
        [RegularExpression(CoreConfig.RegexAccount)]
        [Required]
        [Remote("CheckUserAccount", "Common", "API", AdditionalFields = "UserId", ErrorMessage = "该账号已被注册", HttpMethod = "POST")]
        public string Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [DisplayName("密码")]
        [Password]
        [Required]
        public string NewPassword { get; set; }


        /// <summary>
        /// 姓名
        /// </summary>
        [DisplayName("姓名")]
        [Required]
        [TextLength(32)]
        public string Name { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [DisplayName("类型")]
        [Required(ErrorMessage = "请选择{0}")]
        public UserType? Type { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [DisplayName("性别")]
        public UserSex Sex { get; set; }


        /// <summary>
        /// 联系电话
        /// </summary>
        [DisplayName("联系电话")]
        [RegularExpression(CoreConfig.RegexTelMobile)]
        [TextLength(32)]
        public string Phone { get; set; }

        /// <summary>
        /// 邮箱地址
        /// </summary>
        [DisplayName("邮箱地址")]
        [RegularExpression(CoreConfig.RegexEmail)]
        [TextLength(64)]
        public string Email { get; set; }

        /// <summary>
        /// 微信号
        /// </summary>
        [DisplayName("微信号")]
        [RegularExpression(CoreConfig.RegexWechat4_32)]
        [TextLength(32)]
        public string Wechat { get; set; }

        /// <summary>
        /// QQ号
        /// </summary>
        [DisplayName("QQ号码")]
        [RegularExpression(CoreConfig.RegexQQ4_16)]
        [TextLength(32)]
        public string QQ { get; set; }


        /// <summary>
        /// 角色列表
        /// </summary>
        [DisplayName("角色列表")]
        [CollectionLength(int.MaxValue, 1, ErrorMessage = "至少选择一个角色")]
        public IEnumerable<RoleCode> RoleList { get; set; }
    }
}
