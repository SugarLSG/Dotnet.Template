﻿using APP.Core.Models;
using Framework.Enums;
using Framework.Models.Role;
using Framework.Role;

namespace Service.Abstracts
{
    public interface IRoleService
    {
        /// <summary>
        /// 搜索角色列表
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        PagingResult<SearchResult> Search(SearchCondition condition);

        /// <summary>
        /// 保存角色状态
        /// </summary>
        /// <param name="code"></param>
        /// <param name="status"></param>
        void UpdateStatus(RoleCode code, SysRoleStatus status);

        /// <summary>
        /// 获取角色信息
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        RoleInfo GetInfo(RoleCode code);

        /// <summary>
        /// 保存角色信息
        /// </summary>
        /// <param name="info"></param>
        RoleCode SaveInfo(RoleInfo info);
    }
}
