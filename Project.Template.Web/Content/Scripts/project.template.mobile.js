﻿var wechatJSAPIList;


/* ajax */
ajaxBeforeSend = function () {
    viewUtility.loading();
};

ajaxComplete = function () {
    viewUtility.closeLoading();
};

var handleAjaxResult = function (responseData, successCallBack, failureCallBack) {
    setTimeout(function () {
        if (responseData.result) {
            viewUtility.success(responseData.message, 1500, successCallBack);
        } else {
            alert(responseData.message, failureCallBack);
        }
    }, 250);
};


/* modal */
/*
 callback:
 */
alert = function (message, callback) {
    var $modal = $.modal({
        title: '提示',
        text: message,
        buttons: [
            {
                text: '确定', className: 'primary', onClick: function () {
                    $.closeModal();
                    viewUtility.closeSubview();
                    callback && callback();
                }
            }
        ]
    });
    viewUtility.openSubview($modal);
    return $modal;
};

/*
 callback: Boolean result
 */
confirm = function (message, callback) {
    var $modal = $.modal({
        title: '请选择',
        text: message,
        buttons: [
            {
                text: '取消', onClick: function () {
                    $.closeModal();
                    viewUtility.closeSubview();
                    callback && callback(false);
                }
            },
            {
                text: '确定', className: 'primary', onClick: function () {
                    $.closeModal();
                    viewUtility.closeSubview();
                    callback && callback(true);
                }
            }
        ]
    });
    viewUtility.openSubview($modal);
    return $modal;
};

/*
 loadedCallBack: Object modalObject
 */
var loadUrl = function (title, url, loadedCallBack) {
    var $modal;
    $.appAjax({
        url: url,
        type: 'GET',
        dataType: 'text',
        executeBeforeSendFunc: false,
        executeCompleteFunc: false,
        onBeforeSend: function (requestData) {
            // 勿使用 viewUtility.loading
            $.showLoading('加载中...');
            $('.weui-toast').animate({ opacity: 0 }, 0);
            setTimeout(function () { $('.weui-toast').animate({ opacity: 1 }, 150); }, 250);
        },
        onSuccess: function (responseData, successString, response) {
            // 勿使用 viewUtility.closeLoading
            setTimeout(function () {
                $.hideLoading();

                $modal = $.modal({
                    title: title,
                    text: responseData,
                    buttons: []
                });
                viewUtility.openSubview($modal);

                var $close = $('<button type="button" class="weui-dialog__close">&times;</button>').on({
                    click: function () {
                        $.closeModal();
                        viewUtility.closeSubview();
                    }
                });
                $modal.find('.weui-dialog__hd').append($close);
                $modal.find('.weui-dialog__bd').addClass('text-left');
                var maxHeight = window.innerHeight * 0.96,
                    bodyMaxHeight = maxHeight - parseFloat($modal.find('.weui-dialog__hd').css('height')) - 20,
                    bodyHeight = parseFloat($modal.find('.weui-dialog__bd').css('height')) + 30;
                if (bodyHeight > bodyMaxHeight) {
                    $('.weui-dialog__bd').css({ height: (bodyMaxHeight - 30) + 'px', 'overflow-y': 'auto' });
                }
                $modal.appBindValidator && $modal.appBindValidator();
                $modal.appBindView && $modal.appBindView();

                loadedCallBack && loadedCallBack($modal);
            }, 500);
        }
    });

    return $modal;
};


/* viewUtility */
var viewUtility = function () {
    return {
        // 禁用界面滚动
        disbaledScroll: function (maskClickable) {
            $.appViewUtility.disbaledScroll();

            if (maskClickable && $('body').data('disbaledscroll') === true) {
                setTimeout(function () {
                    $('body .weui-mask').on({
                        click: function () {
                            viewUtility.enabledScroll();
                        }
                    })
                }, 250);
            }
        },
        // 启用界面滚动
        enabledScroll: function () {
            setTimeout(function () {
                if ($('body .weui-mask').length === 0) {
                    $.appViewUtility.enabledScroll();
                }
            }, 500);
        },

        // 显示加载
        loading: function (message) {
            $.showLoading(message || '加载中...');
            viewUtility.openSubview($('.weui-toast'));
        },
        // 关闭加载
        closeLoading: function () {
            $.hideLoading();
            viewUtility.closeSubview();
        },

        // 显示文字提示
        text: function (message, duration, callback) {
            var defaultDuration = $.toast.prototype.defaults.duration;

            $.toast.prototype.defaults.duration = duration || 3000;
            $.toast(message || '提示', 'text', function () {
                $.toast.prototype.defaults.duration = defaultDuration;

                viewUtility.closeSubview();
                callback && callback();
            });
            viewUtility.openSubview($('.weui-toast'));
        },
        // 显示成功提示
        success: function (message, duration, callback) {
            $.toast(message || '操作成功', duration || 3000, function () {
                viewUtility.closeSubview();
                callback && callback();
            });
            $('.weui-toast .weui-icon_toast').addClass('weui-icon_toast-scale');
            viewUtility.openSubview($('.weui-toast'));
        },
        // 显示失败提示
        failure: function (message, duration, callback) {
            var defaultDuration = $.toast.prototype.defaults.duration;

            $.toast.prototype.defaults.duration = duration || 3000;
            $.toast(message || '操作失败', 'cancel', function () {
                $.toast.prototype.defaults.duration = defaultDuration;

                viewUtility.closeSubview();
                callback && callback();
            });
            $('.weui-toast .weui-icon_toast').addClass('weui-icon_toast-scale');
            viewUtility.openSubview($('.weui-toast'));
        }
    };
}();


$.extend({
    _appOnOpen: function ($obj) {
        $obj.focus().attr({ disabled: true });
        setTimeout(function () { $obj.attr({ disabled: false }).blur(); }, 100);

        if (!$('body .weui-mask').length) {
            $('body').append('<div class="weui-mask weui-mask--visible"></div>');
        }
        viewUtility.disbaledScroll();
    },
    _appOnClose: function () {
        $('body .weui-mask').remove();
        viewUtility.enabledScroll();
    },

    appActions: function (title, actions) {
        viewUtility.disbaledScroll(true);
        $.actions({
            title: title,
            actions: actions,
            onClose: function () { viewUtility.enabledScroll(); }
        });
    }
});

$.fn.extend({
    appCalendar: function (opts) {
        opts = $.extend({
            executeOpenFunc: true,
            executeCloseFunc: true,
            onChange: undefined,                // Object input, String value
            onOpen: undefined,                  // Object input
            onClose: undefined                  // Object input
        }, opts);

        return $(this).each(function () {
            var $item = $(this),
                id = $item.attr('id'),
                max = $item.data('max') || '',
                min = $item.data('min') || '',
                maxValue = '',
                minValue = '',
                canSame = $item.data('cansame');

            // 处理最大最小值
            maxValue = max && max.indexOf('#') === 0 ? $(max).val() || '' : max;
            minValue = min && min.indexOf('#') === 0 ? $(min).val() || '' : min;
            if (canSame === undefined || canSame) {
                maxValue && (maxValue = parseDatetime(maxValue).toFormat('yyyy-MM-dd'))
                minValue && (minValue = parseDatetime(minValue).addDays(-1).toFormat('yyyy-MM-dd'));
            } else {
                maxValue && (maxValue = parseDatetime(maxValue).addDays(-1).toFormat('yyyy-MM-dd'));
                minValue && (minValue = parseDatetime(minValue).toFormat('yyyy-MM-dd'));
            }

            $item.addClass('select').appMobileCalendarPicker({
                format: $item.data('format'),
                min: minValue,
                max: maxValue,
                closeFor: $item.data('closefor'),
                onChange: opts.onChange,
                onOpen: function ($input) {
                    opts.executeOpenFunc && $._appOnOpen($item);
                    opts.onOpen && opts.onOpen($input);
                },
                onClose: function ($input) {
                    opts.executeCloseFunc && $._appOnClose($item);
                    opts.onClose && opts.onClose($input);

                    if (id) {
                        $('input[data-toggle="date"][data-min="#' + id + '"]').appCalendar(opts);
                        $('input[data-toggle="date"][data-max="#' + id + '"]').appCalendar(opts);
                    }
                }
            });
        });
    },

    appTime: function (opts) {
        opts = $.extend({
            executeOpenFunc: true,
            executeCloseFunc: true,
            onChange: undefined,                // Object input, Array values
            onOpen: undefined,                  // Object input
            onClose: undefined                  // Object input
        }, opts);

        return $(this).each(function (i, v) {
            var $item = $(v),
                generateValues = function (min, max, step) {
                    var values = [];
                    for (var i = min; i < max; i += step) values.push(i.toString().padLeft(2, '0'));
                    return values;
                },
                hourStep = parseInt($item.data('hourstep') || 1),
                minuteStep = parseInt($item.data('minutestep') || 1),
                secondStep = parseInt($item.data('secondstep') || 1),
                hours = hourStep === 1 ? undefined : generateValues(0, 24, hourStep),
                minutes = minuteStep === 1 ? undefined : generateValues(0, 60, minuteStep),
                seconds = secondStep === 1 ? undefined : generateValues(0, 60, secondStep);

            $item.addClass('select').appMobileTimePicker({
                format: $item.data('format'),
                view: $item.data('view'),
                hours: hours,
                minutes: minutes,
                seconds: seconds,
                onChange: opts.onChange,
                onOpen: function () {
                    opts.executeOpenFunc && $._appOnOpen($item);
                    opts.onOpen && opts.onOpen($input);
                },
                onClose: function () {
                    opts.executeCloseFunc && $._appOnClose($item);
                    opts.onClose && opts.onClose($input);
                }
            });
        });
    },

    appDatetime: function (opts) {
        opts = $.extend({
            executeOpenFunc: true,
            executeCloseFunc: true,
            onChange: undefined,                // Object input, Array ids
            onOpen: undefined,                  // Object input
            onClose: undefined                  // Object input
        }, opts);

        var $this = $(this).each(function (i, v) {
            var $item = $(v);

            // 若已生成，先销毁
            $item.data('datetime') !== undefined && $item.datetimePicker('destroy');

            $item.addClass('select').datetimePicker({
                onChange: opts.onChange,
                onOpen: function (p) {
                    opts.executeOpenFunc && $._appOnOpen($item);
                    opts.onOpen && opts.onOpen($input);
                },
                onClose: function (p) {
                    opts.executeCloseFunc && $._appOnClose($item);
                    opts.onClose && opts.onClose($input);
                }
            });
        });

        $this.data('opts', opts);
        return $this;
    },

    appSelect: function (opts) {
        opts = $.extend({
            executeOpenFunc: true,
            executeCloseFunc: true,
            onChange: undefined,                // Object input, Array ids
            onOpen: undefined,                  // Object input
            onClose: undefined                  // Object input
        }, opts);

        var $this = $(this).hide().each(function (i, v) {
            var $item = $(v),
                classes = $item.attr('class'),
                disabled = $item.attr('disabled'),
                placeholder = $item.attr('placeholder'),
                multiple = ($item.attr('multiple') !== undefined) || false,
                min = $item.data('min'),
                max = $item.data('max'),
                items = [];

            // 处理选项
            var selecteds = [];
            $item.find('option').each(function (oi, ov) {
                var $option = $(ov),
                    value = $option.val(),
                    text = $option.html();

                if (value === '') {
                    placeholder = text;
                } else {
                    items.push({
                        title: text,
                        value: value
                    })
                    $option.is(':selected') && selecteds.push(text);
                }
            });

            // 若已生成，先销毁
            $item.data('input') !== undefined && $item.data('input').remove();

            var $input = $('<input type="text" class="' + classes + ' select"' + (disabled ? ' disabled="disabled"' : '') + ' placeholder="' + (placeholder || '') + '" data-clear="' + $item.data('clear') + '" />');
            $input.insertBefore($item)
                .select({
                    title: placeholder,
                    items: items,
                    multi: multiple,
                    input: selecteds,
                    onChange: function (p) {
                        $item.val(p.values.split(',')).change() && $.validator && $item.valid();
                        opts.onChange && opts.onChange($item);
                    },
                    onOpen: function () {
                        opts.executeOpenFunc && $._appOnOpen($input);
                        opts.onOpen && opts.onOpen($item);
                    },
                    beforeClose: function () {
                        if (multiple) {
                            var values = $item.val() || [];
                            if (min && values.length < min) {
                                viewUtility.text('至少选择' + min + '个选项');
                                return false;
                            } else if (max && values.length > max) {
                                viewUtility.text('至多选择' + max + '个选项');
                                return false;
                            }

                            $.hideLoading();
                        }

                        opts.executeCloseFunc && $._appOnClose($input);
                        opts.onClose && opts.onClose($item);
                    }
                });
            $item.data('input', $input);
        });

        $this.data('opts', opts);
        return $this;
    },

    appSelectSetValue: function (value) {
        var $this = $(this);

        return $this.val(value).appSelect($this.data('opts'));
    },

    appAreas: function (opts) {
        opts = $.extend({
            executeOpenFunc: true,
            executeCloseFunc: true,
            onChange: undefined,                // Object input, Array ids
            onOpen: undefined,                  // Object input
            onClose: undefined                  // Object input
        }, opts);

        var $this = $(this).each(function () {
            var $item = $(this);

            $item.addClass('select').appMobileAreas({
                view: $item.data('view'),
                onChange: function (p) {
                    opts.onChange && opts.onChange($item);
                },
                onOpen: function () {
                    opts.executeOpenFunc && $._appOnOpen($item);
                    opts.onChange && opts.onChange($item);
                },
                onClose: function () {
                    opts.executeCloseFunc && $._appOnClose($item);
                    opts.onClose && opts.onClose($item);
                }
            });
        });

        $this.data('opts', opts);
        return $this;
    },

    appPullToRefresh: function (callback) {
        var $this = $(this);

        return $this.pullToRefresh().on({
            'pull-to-refresh': function () {
                callback && callback(function () {
                    $this.pullToRefreshDone();
                });
            }
        });
    },

    appInfinite: function (distance, callback) {
        var $this = $(this);

        return $this.infinite(distance).on({
            'infinite': function () {
                var loading = $this.data('loading') || false,
                    finish = $this.data('finish') || false;
                if (loading || finish) return;

                // 标识加载中
                $this.data('loading', true);

                callback && callback(function (isFinish) {
                    // 标识加载完成
                    $this.data('loading', false);
                    // 标识是否已结束
                    $this.data('finish', isFinish || false);
                });
            }
        });
    },

    appBindView: function () {
        var $view = $(this);

        // alert
        $view.find('.alert .close').on({
            click: function () {
                var $alert = $(this).parents('.alert').animate({
                    opacity: 0,
                    overflow: 'hidden',
                    height: 0,
                    'padding-top': 0,
                    'padding-bottom': 0,
                    'margin-top': 0,
                    'margin-bottom': 0
                }, 250, function () {
                    $alert.remove();
                });
            }
        });

        // search bar
        $view.find('.weui-search-bar .weui-search-bar__form .weui-search-bar__input').each(function () {
            var $this = $(this);
            $this.on({
                click: function () { $this.addClass('weui-search-bar__input-focusing'); },
                blur: function () { $this.val() === '' && $this.removeClass('weui-search-bar__input-focusing'); }
            });
        });

        // calendar
        $view.find('input[data-toggle="date"]').appCalendar();
        // time
        $view.find('input[data-toggle="time"]').appTime();
        // datetime
        $view.find('input[data-toggle="datetime"]').appDatetime();
        // select
        $view.find('select[data-toggle="select"]').appSelect();
        // areas
        $view.find('input[data-toggle="areas"]').appAreas();

        // clear button
        var $clearInputs = $view.find('.weui-cells_form input[type=text].select[data-clear!="false"], .weui-search-bar__form input[type=search][data-clear!="false"]').each(function () {
            var $this = $(this),
                _handle = function () {
                    var hasValue = $this.val() !== '',
                        isDisabled = $this.attr('disabled');
                    // 非禁用 && 有值
                    if (!isDisabled && hasValue) {
                        $this.addClass('input-notempty');
                    } else {
                        $this.removeClass('input-notempty');
                    }
                };

            if ($this.data('unclear')) return;

            $this.bind('input change propertychange', _handle);
            _handle();
        });
        $('.weui-icon-clear').remove();
        $('<span class="weui-icon-clear"></span>').insertAfter($clearInputs).on({
            click: function () { $(this).parent().find('input').val('').removeClass('input-notempty'); }
        });

        return $view;
    }
});


$(function () {
    // wechatJSAPIList，在需要用到微信接口的页面中，配置要注册的API列表
    if (wechatJSAPIList && wechatJSAPIList.length) {
        var wechatParameterList = WechatParameters.base64Decrypt().split('|');
        wx.config({
            debug: Environment === SystemEnvironment.name.DEV,
            appId: wechatParameterList[0],
            timestamp: wechatParameterList[1],
            nonceStr: wechatParameterList[2],
            signature: wechatParameterList[3],
            jsApiList: wechatJSAPIList
        });

        wx.error(function (res) {
            console.log('wx config error: ' + JSON.stringify(res));
        });
    }

    /* fast click */
    // 影响到 calendar
    //FastClick.attach(document.body);

    /* 绑定界面元素 */
    AreaInitPath = '/api/common/allareas';
    $('html').appBindView();

    /* toast */
    $.toast.prototype.defaults.duration = 3000;

    /* 监听关闭事件 */
    $(window).on({ beforeunload: function () { viewUtility.enabledScroll(); } });
});
