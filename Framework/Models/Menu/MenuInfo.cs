﻿using Framework.Privilege;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Framework.Models.Menu
{
    public class MenuInfo
    {
        /// <summary>
        /// 菜单Id
        /// </summary>
        public int? MenuId { get; set; }

        /// <summary>
        /// 菜单权限Code
        /// </summary>
        [DisplayName("菜单权限Code")]
        public PrivilegeCode? Privilege { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [DisplayName("名称")]
        [Required]
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 图标样式
        /// </summary>
        [DisplayName("图标")]
        [MaxLength(16)]
        public string Icon { get; set; }

        /// <summary>
        /// 说明
        /// </summary>
        [DisplayName("说明")]
        [MaxLength(32)]
        public string Description { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sorted { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public bool IsVisible { get; set; }


        /// <summary>
        /// 子菜单列表
        /// </summary>
        public IEnumerable<MenuInfo> SubMenuList { get; set; }
    }
}
