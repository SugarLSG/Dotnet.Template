﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using System.ComponentModel;

namespace Framework.Enums
{
    /// <summary>
    /// 洲际码
    /// ENUM[string]
    /// AS-AS-亚洲
    /// EU-EU-欧洲
    /// AM-AM-美洲
    /// OA-OA-大洋洲
    /// AF-AF-非洲
    /// OT-OT-其他
    /// </summary>
    public enum SysCountryContinentCode
    {
        /// <summary>
        /// 亚洲
        /// </summary>
        [Description("亚洲")]
        AS,
        /// <summary>
        /// 欧洲
        /// </summary>
        [Description("欧洲")]
        EU,
        /// <summary>
        /// 美洲
        /// </summary>
        [Description("美洲")]
        AM,
        /// <summary>
        /// 大洋洲
        /// </summary>
        [Description("大洋洲")]
        OA,
        /// <summary>
        /// 非洲
        /// </summary>
        [Description("非洲")]
        AF,
        /// <summary>
        /// 其他
        /// </summary>
        [Description("其他")]
        OT,
    }
}
