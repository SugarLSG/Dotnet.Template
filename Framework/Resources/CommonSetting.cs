﻿using System;

public static class CommonSetting
{
    #region Company & Project

    public static readonly DateTime COMPANY_START_DATE = new DateTime(2016, 1, 1);

    public static readonly string COMPANY_YEARS = COMPANY_START_DATE.Year == DateTime.Today.Year ? DateTime.Today.Year.ToString() : COMPANY_START_DATE.Year + "-" + DateTime.Today.Year;

    public const string COMPANY_NAME_CN = "Template";

    public const string COMPANY_NAME_EN = "template technology";

    public const string BRAND_NAME_CN = "Template";

    public const string BRAND_NAME_EN = "Template";

    public const string SOLUTION_CODE = "Project.Template";

    public const string PROJECT_SLOGAN = "project slogan";

    public const string PROJECT_WEB_CODE = "Project.Template.Web";

    public const string PROJECT_WEB_NAME = "Web";

    public const string PROJECT_TASK_CODE = "Project.Template.Task";

    #endregion

    #region Key

    public const string KEY_ACCOUNT_PREFIX = "";

    public const string KEY_API_APPID_PREFIX = "";


    public const string NAME_CHINA_EN = "China";

    public const string NAME_OTHER_CN = "其他";


    public const string CACHEKEY_USER_API_AUTHORIZED_FORMAT = "USER_API_AUTHORIZED.{0}";

    public const string CACHEKEY_USER_PRIVILEGE_FORMAT = "USER_PRIVILEGE.{0}";

    public const string CACHEKEY_USER_MENU_FORMAT = "USER_MENU.{0}";


    public const string APPSETTINGS_QINIUSETTINGS_SECTION = "qiniuSettings";

    public const string APPSETTINGS_QINIUSETTINGS_HOST = "host";

    public const string APPSETTINGS_QINIUSETTINGS_ACCESSKEY = "accessKey";

    public const string APPSETTINGS_QINIUSETTINGS_SECRETKEY = "secretKey";

    public const string APPSETTINGS_QINIUSETTINGS_ZONE = "zone";

    public const string APPSETTINGS_QINIUSETTINGS_BUCKET = "bucket";


    public const string APPSETTINGS_MAILSETTINGS_SECTION = "mailSettings";

    public const string APPSETTINGS_MAILSETTINGS_SMTPHOST = "smtpHost";

    public const string APPSETTINGS_MAILSETTINGS_SMTPPORT = "smtpPort";

    public const string APPSETTINGS_MAILSETTINGS_ADDRESS = "address";

    public const string APPSETTINGS_MAILSETTINGS_PASSWORD = "password";

    public const string APPSETTINGS_MAILSETTINGS_NAME = "name";

    public const string APPSETTINGS_MAILSETTINGS_RECEIVELIST = "receiveList";


    public const string APPSETTINGS_APISETTINGS_SECTION = "apiSettings";

    public const string APPSETTINGS_APISETTINGS_HOST = "host";

    public const string APPSETTINGS_APISETTINGS_SIGNEDKEY = "signedKey";

    public const string APPSETTINGS_APISETTINGS_APPID = "appId";

    public const string APPSETTINGS_APISETTINGS_APPSECRET = "appSecret";


    public const string APPSETTINGS_WECHATSETTINGS_SECTION = "wechatSettings";

    public const string APPSETTINGS_WECHATSETTINGS_APPID = "appId";

    public const string APPSETTINGS_WECHATSETTINGS_APPSECRET = "appSecret";

    public const string APPSETTINGS_WECHATSETTINGS_TOKEN = "token";

    public const string APPSETTINGS_WECHATSETTINGS_ENCODINGAESKEY = "encodingAESKey";


    public const string APPSETTINGS_WEBSITE_HOST = "websiteHost";

    public const string APPSETTINGS_AUTH_COOKIE_KEY = "authCookieKey";

    public const string APPSETTINGS_AUTH_COOKIE_VERSION = "authCookieVersion";

    public const string APPSETTINGS_MENU_MAX_DEEP = "menuMaxDeep";

    public const string APPSETTINGS_TEST_API_PARAM = "testAPIParam";

    public const string APPSETTINGS_SHARED_CACHE_ENGINE = "sharedCacheEngine";

    #endregion

    #region Value

    public const string VALUE_HTML_VOIDLINK = "javascript:void(0);";

    public const string VALUE_HTML_RELOADLINK = "javascript:location.reload();";

    public const string VALUE_HTML_HISTORYBACKLINK = "javascript:history.back(-1);";

    #endregion

    #region Message

    public const string MESSAGE_INVALIDREQUEST = "无效的请求";

    #endregion

    #region 正则表达式

    /// <summary>
    /// 用户AppSecret（可空）
    /// </summary>
    public const string RegexUserAppSecret = @"^([A-Za-z0-9]{8,32})?$";

    #endregion
}
