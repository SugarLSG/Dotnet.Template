﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Attributes;
using APP.Core.Extensions.Nhibernate;
using Newtonsoft.Json;
using Framework.Enums;
using NHibernate.Mapping.Attributes;
using NHibernate.Type;
using System;
using System.ComponentModel;

namespace Framework.DBEntity
{
    /// <summary>
    /// 菜单
    /// </summary>
    [Class(0, Table = "sys_menu")]
    [Cache(1, Usage = CacheUsage.ReadWrite)]
    public class SysMenuEntity
    {
        public const string TABLE_NAME = "sys_menu";
        public const string COL_MENUID = "MenuId";
        public const string COL_PARENTMENUID = "ParentMenuId";
        public const string COL_PRIVILEGECODE = "PrivilegeCode";
        public const string COL_NAME = "Name";
        public const string COL_ICON = "Icon";
        public const string COL_DESCRIPTION = "Description";
        public const string COL_SORTED = "Sorted";
        public const string COL_ISVISIBLE = "IsVisible";
        public const string COL_CREATEDBY = "CreatedBy";
        public const string COL_CREATEDTIME = "CreatedTime";
        public const string COL_MODIFIEDBY = "ModifiedBy";
        public const string COL_MODIFIEDTIME = "ModifiedTime";

        /// <summary>
        /// 菜单Id
        /// </summary>
        [Description("菜单Id")]
        [Id(0, Name = "MenuId", Column = "MenuId")]
        [Key(1)]
        [Generator(2, Class = "native")]
        public virtual int? MenuId { get; set; }

        /// <summary>
        /// 父级菜单Id
        /// </summary>
        [Description("父级菜单Id")]
        [Property(Column = "ParentMenuId")]
        public virtual int? ParentMenuId { get; set; }

        private SysMenuEntity _parentMenu = null;
        /// <summary>
        /// 外键 ParentMenuId 对应 sys_menu 类实例;
        /// </summary>
        [IgnoreConvertTypeScalar]
        [JsonIgnore]
        public virtual SysMenuEntity ParentMenu { get { return _parentMenu ?? (_parentMenu = NHibernateExtension.GetEntityById<SysMenuEntity>(ParentMenuId)); } }

        /// <summary>
        /// 权限Code
        /// </summary>
        [Description("权限Code")]
        [Property(Column = "PrivilegeCode", Length = 128)]
        public virtual string PrivilegeCode { get; set; }

        private SysPrivilegeEntity _privilege = null;
        /// <summary>
        /// 外键 PrivilegeCode 对应 sys_privilege 类实例;
        /// </summary>
        [IgnoreConvertTypeScalar]
        [JsonIgnore]
        public virtual SysPrivilegeEntity Privilege { get { return _privilege ?? (_privilege = NHibernateExtension.GetEntityById<SysPrivilegeEntity>(PrivilegeCode)); } }

        /// <summary>
        /// 菜单名称
        /// </summary>
        [Description("菜单名称")]
        [Property(Column = "Name", NotNull = true, Length = 128)]
        public virtual string Name { get; set; }

        /// <summary>
        /// 菜单图标（样式）
        /// </summary>
        [Description("菜单图标（样式）")]
        [Property(Column = "Icon", Length = 32)]
        public virtual string Icon { get; set; }

        /// <summary>
        /// 菜单说明
        /// </summary>
        [Description("菜单说明")]
        [Property(Column = "Description", Length = 256)]
        public virtual string Description { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Description("排序")]
        [Property(Column = "Sorted", NotNull = true)]
        public virtual int Sorted { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        [Description("是否显示")]
        [Property(Column = "IsVisible", NotNull = true)]
        public virtual bool IsVisible { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        [Description("创建者")]
        [Property(Column = "CreatedBy", NotNull = true)]
        public virtual int CreatedBy { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Property(Column = "CreatedTime", NotNull = true)]
        public virtual DateTime CreatedTime { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        [Description("修改者")]
        [Property(Column = "ModifiedBy", NotNull = true)]
        public virtual int ModifiedBy { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        [Property(Column = "ModifiedTime", NotNull = true)]
        public virtual DateTime ModifiedTime { get; set; }
    }
}
