﻿var IsSubmiting = false,
    TrimLeftReg = /^(\s|\u00A0)+/,
    TrimRightReg = /(\s|\u00A0)+$/,
    DataRegs = [
        /^(\d{4})?(?:-|\/|年)?(\d{1,2})?(?:-|\/|月)?(\d{1,2})?(?:日)?(?: )?(\d{1,2})?(?::)?(\d{1,2})?(?::)?(\d{1,2})?(?:,|\.|')?(\d{3})?$/,
        /^(\d{4})-(\d{1,2})-(\d{1,2})T(\d{1,2}):(\d{1,2}):(\d{1,2}).(\d{3})/,
        /^(?:[A-Za-z]{3}) ([A-Za-z]{3}) (\d{1,2}) (\d{4}) (\d{1,2}):(\d{1,2}):(\d{1,2})/,
        /^(?:[A-Za-z]{3},) (\d{1,2}) ([A-Za-z]{3}) (\d{4}) (\d{1,2}):(\d{1,2}):(\d{1,2})/,
        /^(?:\/?Date\(?)?\d+\)?\/?/
    ],
    EnumValueReg = /^-?\d+$/,
    FileTypeRegs = {
        All: { Reg: undefined, Desc: '请选择上传文件' },
        Text: { Reg: /(\.|\/)(txt)$/i, Desc: '请选择文本文件' },
        Image: { Reg: /(\.|\/)(jpg|jpeg|png|gif|bmp)$/i, Desc: '请选择图片文件' },
        Word: { Reg: /(\.|\/)(doc|docx|docm|dotx|dotm)$/i, Desc: '请选择Word文件' },
        Excel: { Reg: /(\.|\/)(xls|xlsx|xlsm|xltx|xltm|xlsb|xlam)$/i, Desc: '请选择Excel文件' },
        PowerPoint: { Reg: /(\.|\/)(ppt|pptx|pptm|ppsx|ppsm|potx|potm|ppam)$/i, Desc: '请选择PowerPoint文件' },
        Sound: { Reg: /(\.|\/)(mp3|wav|wma|act|amr|ava|cd|ogg|asf|midi|rm)$/i, Desc: '请选择声音文件' },
        Vedio: { Reg: /(\.|\/)(avi|wma|rmvb|rm|flash|mp4|mid|3gp|mpeg|mpg|dat)$/i, Desc: '请选择视频文件' }
    },
    EscapeCharMappings = [
        { original: '"', escape: '&quot;' }, { original: '&', escape: '&amp;' }, { original: '<', escape: '&lt;' }, { original: '>', escape: '&gt;' }, { original: ' ', escape: '&nbsp;' },
        { original: '©', escape: '&copy;' }, { original: '®', escape: '&reg;' },
        { original: '‘', escape: '&lsquo;' }, { original: '’', escape: '&rsquo;' }, { original: '“', escape: '&ldquo;' }, { original: '”', escape: '&rdquo;' }, { original: '′', escape: '&prime;' }, { original: '″', escape: '&Prime;' },
        { original: '‹', escape: '&lsaquo;' }, { original: '›', escape: '&rsaquo;' }, { original: 'ˆ', escape: '&circ;' }, { original: '˜', escape: '&tilde;' }, { original: '…', escape: '&hellip;' }, { original: '–', escape: '&ndash;' }, { original: '—', escape: '&mdash;' }, { original: '•', escape: '&bull;' }
    ],
    ajaxBeforeSend, ajaxComplete;

/* String */
String.prototype.replaceAll = function (search, replace) {
    var regex = new RegExp(search, 'g');
    return this.toString().replace(regex, replace);
};

String.prototype.contains = function (str) {
    if (this.toString().indexOf(str) >= 0) return true;
    else return false;
};

String.prototype.trimLeft = function (trimChar) {
    if (trimChar === undefined || trimChar === null || trimChar === '') {
        return this.toString().replace(TrimLeftReg, '');
    } else {
        var regex = new RegExp('^(' + trimChar + ')+');
        return this.toString().replace(regex, '');
    }
};

String.prototype.trimRight = function (trimChar) {
    if (trimChar === undefined || trimChar === null || trimChar === '') {
        return this.toString().replace(TrimRightReg, '');
    } else {
        var regex = new RegExp('(' + trimChar + ')+$');
        return this.toString().replace(regex, '');
    }
};

String.prototype.trim = function (trimChar) {
    return this.toString().trimLeft(trimChar).trimRight(trimChar);
};

String.prototype.padLeft = function (totalWidth, paddingChar) {
    if (this.length >= totalWidth) {
        return this.toString();
    }

    var paddingChars = '';
    for (var i = 0, count = totalWidth - this.length; i < count; ++i) {
        paddingChars += paddingChar;
    }
    return paddingChars + this.toString();
};

String.prototype.padRight = function (totalWidth, paddingChar) {
    if (this.length >= totalWidth) {
        return this.toString();
    }

    var paddingChars = '';
    for (var i = 0, count = totalWidth - this.length; i < count; ++i) {
        paddingChars += paddingChar;
    }
    return this.toString() + paddingChars;
};

/* Security */
String.prototype.passwordEncrypt = function () {
    var codes = 'ghijklmnopqrstuvwxyz'.split(''),
        t1 = this.base64Encrypt(), t2 = '', t3 = '', signed,
        value = '';
    for (var i = 0, length = t1.length; i < length; ++i) {
        t2 += t1.charCodeAt(i).toString().padLeft(3, '0');
    }
    if (t2.length % 2 === 1)
        t2 = '0' + t2;
    signed = t2.sha1Encrypt().toLowerCase();

    for (var i = 0, length = t2.length / 2 ; i < length ; ++i) {
        var code = parseInt(Math.random() * 100) % 10;
        if (code % 2 === 0) {
            t3 += (code + t2.charAt(i * 2) + t2.charAt(i * 2 + 1));
        } else {
            t3 += (code + t2.charAt(i * 2 + 1) + t2.charAt(i * 2));
        }
    }

    var code = 0;
    for (var i = 0, t3l = t3.length, signedl = signed.length, max = Math.max(t3l, signedl) ; i < max ; ++i) {
        var random = parseInt(Math.random() * 20);
        if (code % 2 === 0) {
            value += (i < t3l ? t3.charAt(i) : codes[random]) + (i < signedl ? signed.charAt(i) : codes[random]);
        } else {
            value += (i < signedl ? signed.charAt(i) : codes[random]) + (i < t3l ? t3.charAt(i) : codes[random]);
        }
        code = i < t3l ? parseInt(t3.charAt(i)) : random;
    }

    return value;
};

String.prototype.md5Encrypt = function () {
    return CryptoJS.MD5(this.toString()).toString().toUpperCase();
};

String.prototype.md5Encrypt_16 = function () {
    return this.md5Encrypt().substr(8, 16);
};

String.prototype.sha1Encrypt = function () {
    return CryptoJS.SHA1(this.toString()).toString().toUpperCase();
};

String.prototype.sha256Encrypt = function () {
    return CryptoJS.SHA256(this.toString()).toString().toUpperCase();
};

String.prototype.sha384Encrypt = function () {
    return CryptoJS.SHA384(this.toString()).toString().toUpperCase();
};

String.prototype.sha512Encrypt = function () {
    return CryptoJS.SHA512(this.toString()).toString().toUpperCase();
};

String.prototype.hexEncrypt = function () {
    return CryptoJS.enc.Utf8.parse(this.toString()).toString().toUpperCase();
};

String.prototype.base64Encrypt = function () {
    return CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(this.toString()));
};

String.prototype.base64Decrypt = function () {
    return CryptoJS.enc.Base64.parse(this.toString()).toString(CryptoJS.enc.Utf8);
};

String.prototype.desEncrypt = function (key) {
    return CryptoJS.DES.encrypt(
        this.toString(),
        CryptoJS.enc.Utf8.parse(key),
        {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        }
    ).toString();
};

String.prototype.desDecrypt = function (key) {
    return CryptoJS.DES.decrypt(
        {
            ciphertext: CryptoJS.enc.Base64.parse(this.toString())
        },
        CryptoJS.enc.Utf8.parse(key),
        {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        }
    ).toString(CryptoJS.enc.Utf8);
};

String.prototype.tripleDESEncrypt = function (key) {
    return CryptoJS.TripleDES.encrypt(
        this.toString(),
        CryptoJS.enc.Utf8.parse(key),
        {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        }
    ).toString();
};

String.prototype.tripleDESDecrypt = function (key) {
    return CryptoJS.TripleDES.decrypt(
        {
            ciphertext: CryptoJS.enc.Base64.parse(this.toString())
        },
        CryptoJS.enc.Utf8.parse(key),
        {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        }
    ).toString(CryptoJS.enc.Utf8);
};

String.prototype.aesEncrypt = function (key) {
    return CryptoJS.AES.encrypt(
        this.toString(),
        CryptoJS.enc.Utf8.parse(key),
        {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        }
    ).toString();
};

String.prototype.aesDecrypt = function (key) {
    return CryptoJS.AES.decrypt(
        {
            ciphertext: CryptoJS.enc.Base64.parse(this.toString())
        },
        CryptoJS.enc.Utf8.parse(key),
        {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        }
    ).toString(CryptoJS.enc.Utf8);
};

String.prototype.xorEncrypt = function (key) {
    var length = this.length;
    while (key.length < length) {
        key += key;
    }

    var result = '';
    for (var i = 0; i < length; ++i) {
        result += String.fromCharCode(this.charCodeAt(i) ^ key.charCodeAt(i));
    }
    return result;
};

String.prototype.xorDecrypt = function (key) {
    return this.xorEncrypt(key);
};

String.prototype.escapeCharEncrypt = function () {
    var result = this;
    $.each(EscapeCharMappings, function (index, value) {
        result = value.replaceAll(value.original, value.escape);
    });
    return result;
};

String.prototype.escapeCharDecrypt = function () {
    var result = this;
    $.each(EscapeCharMappings, function (index, value) {
        result = result.replaceAll(value.escape, value.original);
    });
    return result;
};

var encodeParameters = function () {
    var cryptos = 'P782ahA_jByC69c-lQ0DovSUOrg1z!tEdFGZeHxI3JV5KRwYLfTnX4puksMmWbiqN';

    var vals = [];
    for (var i = 0, length = arguments.length; i < length; ++i) {
        vals.push(arguments[i]);
    }
    var base64Str = JSON.stringify(vals).base64Encrypt();

    var crypto = '!';
    $.each(base64Str.split(''), function (i, v) {
        var code = v.charCodeAt();
        if (code === 43) code = 0;
        else if (code === 47) code = 1;
        else if (code >= 48 && code <= 57) code -= 46;
        else if (code === 61) code = 12;
        else if (code >= 65 && code <= 90) code -= 52;
        else if (code >= 97 && code <= 122) code -= 58;

        crypto += cryptos[code];
    });

    return crypto;
};

var generateRandomPassword = function (length) {
    var numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
        chars = [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
        ];

    var password = '';
    for (var i = 0, numberCount = 0, charCount = 0, numberMaxCount = numbers.length, charMaxCount = chars.length; i < length; ++i) {
        var isNumber = parseInt(Math.random() * 10000 % 2) === 0;
        // 保证至少有1个数字，1个字母
        if (isNumber) {
            numberCount === length - 1 && (isNumber = false);
        } else {
            charCount === length - 1 && (isNumber = true);
        }

        if (isNumber) {
            password += numbers[parseInt(Math.random() * numberMaxCount)];
            ++numberCount;
        } else {
            password += chars[parseInt(Math.random() * charMaxCount)];
            ++charCount;
        }
    }
    return password;
};

/* Array */
Array.prototype.contains = function (item) {
    var isIn = false;
    $.each(this, function (index, value) {
        if (item === value) isIn = true;
    });
    return isIn;
};

/* Date */
var millisecondsTime = 1,
    secondsTime = millisecondsTime * 1000,
    minutesTime = secondsTime * 60,
    hoursTime = minutesTime * 60,
    daysTime = hoursTime * 24;

var today = function () {
    var now = new Date();
    return new Date(now.getFullYear(), now.getMonth(), now.getDate());
}();

Date.prototype.addYears = function (years) {
    this.setFullYear(this.getFullYear() + years);
    return this;
}

Date.prototype.addMonths = function (months) {
    this.setMonth(this.getMonth() + months);
    return this;
}

Date.prototype.addDays = function (days) {
    this.setDate(this.getDate() + days);
    return this;
}

Date.prototype.addHours = function (hours) {
    this.setHours(this.getHours() + hours);
    return this;
}

Date.prototype.addMinutes = function (minutes) {
    this.setMinutes(this.getMinutes() + minutes);
    return this;
}

Date.prototype.addSeconds = function (seconds) {
    this.setSeconds(this.getSeconds() + seconds);
    return this;
}

Date.prototype.addMilliseconds = function (milliseconds) {
    this.setMilliseconds(this.getMilliseconds() + milliseconds);
    return this;
}

Date.prototype.toFormat = function (formater) {
    var p = {
        'y+': this.getFullYear(),                                   // 年
        'M+': this.getMonth() + 1,                                  // 月
        'd+': this.getDate(),                                       // 日
        'h+': this.getHours() === 12 ? 12 : this.getHours() % 12,   // 小时（12小时制）
        'H+': this.getHours(),                                      // 小时（24小时制）
        'm+': this.getMinutes(),                                    // 分
        's+': this.getSeconds(),                                    // 秒
        'fff': this.getMilliseconds()                               // 毫秒
    };
    for (var k in p) {
        if (new RegExp('(' + k + ')').test(formater)) {
            if (RegExp.$1.length === 1) {
                formater = formater.replace(RegExp.$1, p[k]);
            } else {
                var v = '000' + p[k];
                formater = formater.replace(RegExp.$1, v.substr(v.length - RegExp.$1.length));
            }
        }
    }
    return formater;
};

Date.prototype.toWeekDay = function () {
    var day = this.getDay();
    switch (day) {
        case 0: return '周日';
        case 1: return '周一';
        case 2: return '周二';
        case 3: return '周三';
        case 4: return '周四';
        case 5: return '周五';
        case 6: return '周六';
    }
    return null;
};

var parseDatetime = function (string) {
    // 1900-01-01 00:00:00,000
    var result = DataRegs[0].exec(string);
    if (result !== null) {
        var month = result[2] === undefined ? 0 : (result[2] - 1);
        return new Date(result[1], month, result[3] || 1, result[4] || 0, result[5] || 0, result[6] || 0, result[7] || 0);
    }

    // 1900-01-01T00:00:00.000Z（toISOString）
    var result = DataRegs[1].exec(string);
    if (result !== null) {
        var month = result[2] === undefined ? 0 : (result[2] - 1);
        var time = new Date(result[1], month, result[3] || 1, (result[4] || 0), result[5] || 0, result[6] || 0, result[7] || 0);
        return time.addHours(-(new Date().getTimezoneOffset() / 60));
    }

    var convertMonth = function (name) {
        switch (name) {
            case 'Jan': return 0;
            case 'Feb': return 1;
            case 'Mar': return 2;
            case 'Apr': return 3;
            case 'May': return 4;
            case 'Jun': return 5;
            case 'Jul': return 6;
            case 'Aug': return 7;
            case 'Sep': return 8;
            case 'Oct': return 9;
            case 'Nov': return 10;
            case 'Dec': return 11;
        }
    };

    // Thu Jan 01 1900 00:00:00 GMT+0800 (中国标准时间)（toString）
    result = DataRegs[2].exec(string);
    if (result !== null) {
        var month = convertMonth(result[1]);
        return new Date(result[3], month, result[2] || 1, result[4] || 0, result[5] || 0, result[6] || 0);
    }

    // Thu, 01 Jan 1900 00:00:00 GMT（toUTCString、toGMTString）
    result = DataRegs[3].exec(string);
    if (result !== null) {
        var month = convertMonth(result[2]);
        var time = new Date(result[3], month, result[1] || 1, (result[4] || 0), result[5] || 0, result[6] || 0);
        return time.addHours(-(new Date().getTimezoneOffset() / 60));
    }

    // Date(0)
    result = DataRegs[4].exec(string);
    if (result !== null) {
        return new Date(parseInt(result[1]));
    }

    return new Date(NaN);
};

/* Cookie */
var cookie = function () {
    return {
        set: function (name, value, expiredMinutes, domain) {
            var expires = undefined;
            if (expiredMinutes) {
                // 设置过期时间
                expires = new Date();
                expires.setTime(expires.getTime() + (expiredMinutes * 60 * 1000));
            }
            document.cookie = name + '=' + encodeURI(value) + (expires ? ('; expires=' + expires.toUTCString()) : '') + '; path=/' + (domain ? ('; domain=' + domain) : '');
        },
        get: function (name) {
            var cookies = document.cookie.split('; ');
            for (var i = 0, length = cookies.length; i < length; ++i) {
                var c = cookies[i];
                if (c.indexOf(name + '=') === 0) {
                    return decodeURI(c.substring(name.length + 1, c.length));
                }
            }
            return undefined;
        },
        remove: function (name) {
            cookie.set(name, undefined, -1);
        },
        clear: function () {
            var cookies = document.cookie.split('; ');
            for (var i = 0, length = cookies.length; i < length; ++i) {
                var name = cookies[i].split('=')[0];
                cookie.remove(name);
            }
        }
    };
}();

/* Digital */
var isNumber = function (parameter) {
    var digital = parseFloat(parameter);
    return !isNaN(digital) && isFinite(digital);
};

var digital = function () {
    var codes = {
        numbers: ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'],
        units: ['', '拾', '佰', '仟'],
        levels: ['元', '万', '亿', '万', '亿亿']
    };

    return {
        convertCN: function (digital) {
            var value = parseFloat(digital).toString(),
                pointIndex = value.indexOf('.'),
                integer = pointIndex > 0 ? value.substr(0, pointIndex) : value,
                decimal = pointIndex > 0 ? value.substr(pointIndex + 1, 2) : undefined;

            var cn = '';
            for (var i = 0, length = integer.length ; i < length; ++i) {
                if (i % 4 === 0) {
                    cn = codes.levels[i / 4] + cn;
                }
                cn = codes.numbers[parseInt(integer[length - i - 1])] + codes.units[i % 4] + cn;
            }
            cn = cn
                .replace(/零(拾|佰|仟)/g, '零')
                .replace(/零+/g, '零')
                .replace(/零(元|万|亿|亿亿)/g, '$1')
                .replace(/(亿)万/g, '$1$2')
                .trimLeft('元')
                .trim('零');

            if (decimal !== undefined) {
                if (parseInt(decimal[0])) {
                    cn += codes.numbers[parseInt(decimal[0])] + '角';
                }
                if (parseInt(decimal[1])) {
                    cn += codes.numbers[parseInt(decimal[1])] + '分';
                }
            } else {
                cn = cn === '' || cn === '元' ? '零元整' : cn + '整';
            }

            return cn;
        }
    };
}();

/* Guid */
var guid = function () {
    var time = new Date().getTime();
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (time + Math.random() * 16) % 16 | 0;
        time = Math.floor(time / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
};

/* areaUtility */
var AreaInitPath = undefined;
var areaUtility = function () {
    var _data,              // 地区数据
        _inited = false,    // 是否已初始化
        _queues = [];       // 回调函数队列

    return {
        execute: function (callback) {
            if (!_data) {
                // 未有数据，放入队列
                _queues.push(callback);

                if (!_inited && (_inited = true)) {
                    if (!AreaInitPath) {
                        throw new Error('请先指定数据 api 路径（AreaInitPath）.');
                    }

                    $.appAjax({
                        url: AreaInitPath,
                        cache: true,
                        executeBeforeSendFunc: false,
                        executeCompleteFunc: false,
                        onSuccess: function (responseData) {
                            _data = responseData;

                            // 处理所有队列
                            while (_queues.length) {
                                var cb = _queues.shift();
                                cb && cb();
                            }
                        }
                    });
                }
            } else {
                if (_queues.length) {
                    // 队列未执行完，放入队列
                    _queues.push(callback);
                } else {
                    // 队列已执行完，直接回调
                    callback && callback();
                }
            }
        },
        data: function () {
            return _data;
        }
    };
}();

/* Extend */
$.extend({
    appAjax: function (opts) {
        opts = $.extend({
            url: undefined,
            type: 'POST',
            data: undefined,
            dataType: 'json',
            contentType: 'application/x-www-form-urlencoded',
            async: true,
            cache: false,
            executeBeforeSendFunc: true,
            executeCompleteFunc: true,
            onBeforeSend: undefined,    // Object requestData
            onSuccess: undefined,       // Object responseData, String successString = success, XMLHttpResponse response
            onError: undefined,         // XMLHttpResponse response, String errorString = error, String statusText
            onComplete: undefined       // XMLHttpResponse response, String completeString
        }, opts);

        if ((opts.onBeforeSend && opts.onBeforeSend(opts.data)) !== false) {
            $.ajax({
                url: opts.url,
                type: opts.type,
                data: opts.data,
                dataType: opts.dataType,
                contentType: opts.contentType,
                async: opts.async,
                cache: opts.cache,
                beforeSend: function (request, ajaxObject) {
                    opts.executeBeforeSendFunc && ajaxBeforeSend && ajaxBeforeSend();
                },
                success: function (responseData, successString, response) {
                    opts.onSuccess && opts.onSuccess(responseData, successString, response);
                },
                error: function (response, errorString, statusText) {
                    console.log('response: ' + JSON.stringify(response));
                    console.log('errorString: ' + errorString);
                    console.log('statusText: ' + statusText);

                    opts.onError && opts.onError(response, errorString, statusText);
                },
                complete: function (response, completeString) {
                    opts.executeCompleteFunc && ajaxComplete && ajaxComplete();
                    opts.onComplete && opts.onComplete(response, completeString);
                }
            });
        }
    },

    /*
     打开新界面，并以 POST 方式提交请求数据
     url: 请求 URL
     postData: 提交的数据，格式：{ key1: value1, key2: value2 ... }
     */
    appBlankPost: function (url, postData) {
        var $postForm = $('<form>').attr({
            action: url,
            target: '_blank',
            method: 'post'
        });
        $.each(postData, function (i, v) {
            $postForm.append('<input type="hidden" name="' + i + '" value="' + v + '">');
        });
        $('body').append($postForm);
        $postForm.submit().remove();
    },

    appViewUtility: function () {
        return {
            // 禁用界面滚动
            disbaledScroll: function () {
                var $body = $('body');
                if ($body.data('disbaledscroll') !== true) {
                    $body.data('disbaledscroll', true)
                        .data('yOffset', window.pageYOffset)
                        .css({ position: 'fixed', top: '-' + window.pageYOffset + 'px', left: '0', right: '0' });
                }
            },
            // 启用界面滚动
            enabledScroll: function () {
                var $body = $('body');
                if ($body.data('disbaledscroll') === true) {
                    var yOffset = $('body').data('yOffset');

                    $body.css({ position: '', top: '', left: '', right: '' })
                        .removeData('yOffset')
                        .data('disbaledscroll', false);
                    window.scrollTo(0, yOffset);
                }
            }
        };
    }()
});

$.fn.extend({
    appSerialize: function () {
        var data = {},
            serializeArray = [],
            multiLevelArray = [];

        // 排重（取第一个），重新排序
        var temp = [];
        var val_arr_index = [];
        $.each(this.serializeArray(), function (i, v) {
            var is_val_arr = Object.prototype.toString.call($('[name="' + v.name + '"]').val()) === '[object Array]';
            if (!temp.contains(v.name) || is_val_arr) {
                if (is_val_arr) {
                    if ($('[name="' + v.name + '"]').length > 1)
                        throw new Error('存在多个相同 name 控件.');
                    var index = val_arr_index[v.name] || 0,
                        name = '',
                        dot_index = v.name.lastIndexOf('.');
                    if (dot_index > 0)
                        name = v.name.substr(0, dot_index) + '[' + index + ']' + v.name.substr(dot_index, v.name.length - dot_index);
                    else
                        name = v.name + '[' + index + ']';
                    var item = { name: name, value: v.value };
                    if (!temp.contains(name)) {
                        temp.push(name);
                        serializeArray.push(item);
                        val_arr_index[v.name] = ++index;
                    }
                }
                else {
                    serializeArray.push(v);
                    temp.push(v.name);
                }
            }
        });
        serializeArray = serializeArray.sort(function (a, b) {
            return a.name.localeCompare(b.name);
        });

        // 分解 Name
        $.each(serializeArray, function (i, v) {
            var names = v.name.split('.'),
                nameList = [];

            $.each(names, function (i, v) {
                if (v.contains('[')) {
                    nameList.push(v.substring(0, v.indexOf('[')));
                    nameList.push(v.substring(v.indexOf('['), v.indexOf(']') + 1));
                } else {
                    nameList.push(v);
                }
            });

            multiLevelArray.push({ names: nameList, value: v.value || '' });
        });

        // 重新排列数组 Name 顺序
        if (multiLevelArray && multiLevelArray.length) {
            var _convertName = function (array, deep) {
                // 按当前层级 Name 分组
                var items = [],
                        currentItem = undefined;
                $.each(array, function (i, v) {
                    if (deep < v.names.length) {
                        var name = v.names[deep];
                        if (currentItem && currentItem.name === name) {
                            currentItem.value.push(v);
                        } else {
                            currentItem = { name: name, value: [v] };
                            items.push(currentItem);
                        }
                    }
                });

                if (items && items.length) {
                    $.each(items, function (i, v) {
                        // 重新排列数组 Name 顺序
                        if (v.name.contains('[')) {
                            $.each(v.value, function (vi, vv) {
                                v.value[vi].names[deep] = '[' + i + ']';
                            });
                        }

                        _convertName(v.value, deep + 1);
                    });
                }
            };
            _convertName(multiLevelArray, 0);

            // 赋值 Data
            $.each(multiLevelArray, function (i, v) {
                var name = v.names.join('.').replaceAll('\\.\\[', '[');
                if (data[name] === undefined) {
                    data[name] = v.value;
                }
            });
        }

        return data;
    },

    appSubmit: function (opts) {
        opts = $.extend({
            dataType: 'json',
            contentType: 'application/x-www-form-urlencoded',
            executeBeforeSendFunc: true,
            executeCompleteFunc: true,
            onBeforeSend: undefined,            // Object requestData
            onSuccess: undefined,               // Object responseData, String successString = success, XMLHttpResponse response
            onError: undefined,                 // XMLHttpResponse response, String errorString = error, String statusText
            onComplete: undefined               // XMLHttpResponse response, String completeString
        }, opts);

        var $this = $(this);
        return $this.on({
            submit: function (event) {
                IsSubmiting = true;
                event.preventDefault();

                if ($this.valid()) {
                    $.appAjax({
                        url: $this.attr('action'),
                        data: $this.appSerialize(),
                        dataType: opts.dataType,
                        contentType: opts.contentType,
                        executeBeforeSendFunc: opts.executeBeforeSendFunc,
                        executeCompleteFunc: opts.executeCompleteFunc,
                        onBeforeSend: function (requestData) {
                            $this.find('[data-val-password]').each(function (i, v) {
                                var name = $(v).attr('name'),
                                    value = requestData[name];
                                if (value !== undefined && (typeof value).toUpperCase() === 'STRING' && value !== '') {
                                    requestData[name] = value.passwordEncrypt();
                                }
                            });

                            return opts.onBeforeSend && opts.onBeforeSend(requestData);
                        },
                        onSuccess: opts.onSuccess,
                        onError: opts.onError,
                        onComplete: function (response, completeString) {
                            IsSubmiting = false;
                            opts.onComplete && opts.onComplete(response, completeString);
                        }
                    });
                } else {
                    IsSubmiting = false;
                }
            }
        });
    },

    appAutoSubmit: function () {
        var $this = $(this);

        $this.find('input,select,textarea').on({ change: function () { $this.submit(); } });
        return $this;
    },

    appBindValidator: function () {
        var $this = this.parents()
            .addBack()
            .filter('form')
            .add(this.find('form'))
            .has('[data-val=true]');

        if ($this && $this.length) {
            $this.removeData('validator');
            $.validator && $.validator.unobtrusive.parse($this);
        }

        return $(this);
    },

    appDatagrid: function (opts) {
        opts = $.extend({
            url: undefined,                     // 远程请求路径
            data: undefined,                    // 远程请求参数
            dataList: undefined,                // 本地数据

            minWidth: 1000,                     // 表格最小宽度（界面小于该宽度，添加横向滚动条）
            showHeader: true,                   // 是否显示表头（包括 toolbar、columns）
            showFooter: true,                   // 是否显示表脚（包括分页）
            showCheckboxColumn: false,          // 是否显示 checkbox 列
            toolbar: undefined,
            columns: undefined,
            page: 1,                            // 当前页
            pageList: [15, 20, 30, 50, 100],    // 每页显示数量列表
            pageSize: 15,                       // 每页显示数量
            rowTemplate: undefined,             // 行模板（不指定则使用默认模板）
            emptyCellValue: '-',                // 空白单元格默认值

            onAfterResponse: undefined,         // 远程数据响应后（Object responseData）
            onBeforeRenderRow: undefined,       // 渲染每行数据前（Object rowData, Int rowIndex; return Object rowData）
            onAfterRender: undefined            // 渲染结束后（Object responseData）
        }, opts);

        // 处理响应数据
        var _render = function () {
            var responseData = opts.responseData;

            // onAfterResponse
            var result = opts.onAfterResponse && opts.onAfterResponse(responseData);
            if (result !== undefined) {
                responseData = result;
            }

            // data
            if (responseData.data && responseData.data.length) {
                var $tbody = $table.find('tbody');
                $.each(responseData.data, function (i, v) {
                    var result = opts.onBeforeRenderRow && opts.onBeforeRenderRow(v, i);
                    if (result !== undefined) {
                        v = result;
                    }

                    var content = rowTemplates;
                    $.each(opts.columns, function (ci, cv) {
                        var val = cv.formatter ? cv.formatter(v[cv.field], v, i) : v[cv.field];
                        content = content.replaceAll('{{' + cv.field + '}}', val === undefined ? opts.emptyCellValue : val);
                    });
                    var $content = $(content);
                    $tbody.append($content);
                    if (opts.showCheckboxColumn) {
                        $content.find('input[data-column="rowcheckbox"]')
                            .attr({ 'data-index': i })
                            .on({
                                change: function () {
                                    $table.find('input[data-column="headercheckbox"]').prop({ checked: $table.find('input[data-column="rowcheckbox"]:checked').length === responseData.data.length });
                                }
                            });
                    }
                });
            } else {
                $table.find('tbody').append('<tr><td class="text-center" colspan="' + columnCount + '">暂无数据</td></tr>');
            }

            // pagination
            if (opts.showFooter) {
                if (opts.data.page === 1) {
                    $datagrid_pagination_info_first.addClass('text-muted');
                    $datagrid_pagination_info_prev.addClass('text-muted');
                } else {
                    $datagrid_pagination_info_first.removeClass('text-muted');
                    $datagrid_pagination_info_prev.removeClass('text-muted');
                }
                if (opts.data.page === responseData.pageCount) {
                    $datagrid_pagination_info_next.addClass('text-muted');
                    $datagrid_pagination_info_last.addClass('text-muted');
                } else {
                    $datagrid_pagination_info_next.removeClass('text-muted');
                    $datagrid_pagination_info_last.removeClass('text-muted');
                }
                $datagrid_pagination_info_current.val(opts.data.page)
                    .data('current', opts.data.page)
                    .data('max', responseData.pageCount);
                $datagrid_pagination_info_totalpage.html(responseData.pageCount);
                $datagrid_pagination_info_totalrecord.html(responseData.totalCount || 0);
                $datagrid_pagination_record_total.html(responseData.totalCount || 0);
            }

            // onAfterRender
            opts.onAfterRender && opts.onAfterRender(responseData);
        };

        if (!opts.data) {
            opts.data = {};
        }

        // 整理宽度、排序
        var totalWidth = 0,
            sorts = {};
        if (opts.columns && opts.columns.length) {
            $.each(opts.columns.slice(0).sort(function (i1, i2) {
                if (i1.orderIndex && i2.orderIndex) {
                    return i2.orderIndex - i1.orderIndex;
                } else if (i1.orderIndex && !i2.orderIndex) {
                    return -1;
                } else if (!i1.orderIndex && i2.orderIndex) {
                    return 1;
                } else {
                    return 0;
                }
            }), function (i, v) {
                if (v.width) {
                    totalWidth += v.width;
                }

                if (v.sortable) {
                    if (v.order !== 'asc') {
                        v.order = 'desc';
                    }
                    sorts[v.field] = v.order;
                } else {
                    v.sortable = false;
                    v.order = undefined;
                }
            });
        }
        opts.data.sorts = sorts;

        // 页码 & 页数
        opts.data.page = opts.page;
        opts.data.size = opts.pageSize;

        // 总列数
        var columnCount = opts.columns.length + (opts.showCheckboxColumn ? 1 : 0);

        var $this = $(this);

        // 初始化
        $this.addClass('datalist').empty();

        // datagrid
        var $table = $('<table id="datagrid" class="table table-striped table-hover"' + (opts.minWidth ? ' style="min-width: ' + opts.minWidth + 'px;"' : '') + '><thead></thead><tbody></tbody></table>');
        $this.append($('<div class="datalist-body table-responsive"></div>').append($table));

        // datagrid toolbar
        if (opts.showHeader && opts.toolbar && opts.toolbar.length) {
            var $datagrid_toolbar = $('<tr id="datagrid_toolbar"><th id="datagrid_toolbar_container" colspan="' + columnCount + '"></th></tr>');
            $table.find('thead').append($datagrid_toolbar);
            var $datagrid_toolbar_container = $datagrid_toolbar.find('#datagrid_toolbar_container');
            $.each(opts.toolbar, function (i, v) {
                $datagrid_toolbar_container.append(
                    $('<a href="javascript:void(0);" data-index="' + i + '">' + (v.iconCls ? '<span class="mr5 ' + v.iconCls + '"></span>' : '') + v.text + '</a>').on({
                        click: function () {
                            var tb = opts.toolbar[$(this).data('index')];
                            tb && tb.handler && tb.handler();
                        }
                    })
                );
            });
        }

        // datagrid title
        var $datagrid_title = $('<tr id="datagrid_title"></tr>');
        if (opts.showHeader) {
            $table.find('thead').append($datagrid_title);
        }
        var rowTemplates = '';
        if (opts.showCheckboxColumn) {
            var $th = $('<th><label class="checkbox checkbox-primary"><input type="checkbox" data-column="headercheckbox" /><span>&nbsp;</span></label></th>');
            $datagrid_title.append($th);
            $th.find('input[data-column="headercheckbox"]').on({
                change: function () {
                    $table.find('input[data-column="rowcheckbox"]').prop({ checked: $(this).is(':checked') });
                }
            });

            // 行模板
            rowTemplates += '<td><label class="checkbox checkbox-primary"><input type="checkbox" data-column="rowcheckbox" /><span>&nbsp;</span></label></td>';
        }
        var lastIndex = opts.columns.length - 1, lastWidth = 100;
        $.each(opts.columns, function (i, v) {
            var $th = $('<th data-index="' + i + '"></th>').text(v.title);
            $datagrid_title.append($th);

            // 宽度
            if (v.width) {
                var width = 0;
                if (i === lastIndex) {
                    width = lastWidth;
                } else {
                    width = parseFloat((v.width / totalWidth * 100).toFixed(2));
                    lastWidth -= width;
                }
                $th.css({ width: width + '%' });
            }
            // 对齐
            var css = '';
            if (v.align === 'center') {
                css = 'text-center';
                $th.addClass('text-center');
            } else if (v.align === 'right') {
                css = 'text-right';
                $th.addClass('text-right');
            }
            // 排序
            if (v.sortable) {
                $th.addClass('sorted')
                    .attr({ title: '更改排序方式' })
                    .append($('<span class="fa fa-angle-' + (v.order === 'desc' ? 'down' : 'up') + ' text-bold ml10"></span>')).on({
                        click: function () {
                            var column = opts.columns[$(this).data('index')];

                            if (column.sortable) {
                                column.order = column.order === 'desc' ? 'asc' : 'desc';
                                column.orderIndex = (new Date()).getTime();
                                $this.appDatagrid(opts);
                            }
                        }
                    });
            }

            // 行模板
            rowTemplates += '<td class="' + css + '">{{' + v.field + '}}</td>';
        });
        // 行模板
        if (opts.rowTemplate === undefined) {
            rowTemplates = '<tr>' + rowTemplates + '</tr>';
        } else {
            rowTemplates = '<tr><td colspan="' + columnCount + '">' + opts.rowTemplate + '</td></tr>';
        }

        // datagrid footer
        if (opts.showFooter) {
            var funcs = {
                checkPage: function () {
                    var newPage = parseInt($datagrid_pagination_info_current.val());
                    var current = $datagrid_pagination_info_current.data('current');
                    var max = $datagrid_pagination_info_current.data('max');

                    if (isNaN(newPage)) {
                        $datagrid_pagination_info_current.val(current);
                    } else {
                        if (newPage < 1) {
                            $datagrid_pagination_info_current.val(1);
                        } else if (newPage > max) {
                            $datagrid_pagination_info_current.val(max);
                        } else {
                            $datagrid_pagination_info_current.val(newPage);
                        }
                    }

                    return {
                        newPage: newPage,
                        current: current,
                        max: max
                    };
                },
                go: function () {
                    opts.page = parseInt($datagrid_pagination_info_current.val());
                    $this.appDatagrid(opts);
                }
            };

            var $datagrid_pagination = $('<div id="datagrid_pagination" class="datalist-footer row"></div>');
            $this.append($datagrid_pagination);

            // datagrid pagination info
            var $datagrid_pagination_info = $(
'<div id="datagrid_pagination_info" class="col-md-16 pagination-info"><ul class="pagination pagination-noborder pagination-sm">' +
    '<li class="pagination-info-rows">' +
        '<span class="text-gray">每页显示</span>' +
        '<select id="datagrid_pagination_info_rows" class="form-control input-sm pull-left"></select>' +
        '<span class="text-gray">条记录，共</span>' +
        '<span id="datagrid_pagination_info_totalpage" class="text-gray pl0 pr0">1</span>' +
        '<span class="text-gray hidden-xs hidden-sm">页</span>' +
        '<span class="text-gray visible-xs pr0 visible-sm">页（</span>' +
        '<span id="datagrid_pagination_info_totalrecord" class="text-gray pl0 pr0 visible-xs visible-sm">0</span>' +
        '<span class="text-gray visible-xs visible-sm">条数据）</span>' +
    '</li>' +
    '<li><span class="text-muted hidden-xs">|</span></li>' +
    '<li class="pagination-info-pages">' +
        '<a id="datagrid_pagination_info_first" class="text-muted" href="javascript:void(0);" title="跳转到首页">首页</span></a>' +
        '<a id="datagrid_pagination_info_prev" class="text-muted" href="javascript:void(0);" title="跳转到上一页"><span class="fa fa-chevron-left"></span></a>' +
        '<span class="text-gray">当前第</span>' +
        '<input id="datagrid_pagination_info_current" class="form-control input-sm pull-left" type="text" value="1" title="跳转到指定页" />' +
        '<a id="datagrid_pagination_info_next" class="text-muted" href="javascript:void(0);" title="跳转到下一页"><span class="fa fa-chevron-right"></span></a>' +
        '<a id="datagrid_pagination_info_last" class="text-muted" href="javascript:void(0);" title="跳转到尾页">尾页</span></a>' +
    '</li>' +
    '<li><span class="text-muted">|</span></li>' +
    '<li class="pagination-info-refresh">' +
        '<a id="datagrid_pagination_info_refresh" href="javascript:void(0);" title="刷新数据"><span class="hidden-xs">刷新数据</span><span class="fa fa-refresh"></span></a>' +
    '</li>' +
'</ul></div>'
            );
            $datagrid_pagination.append($datagrid_pagination_info);

            // datagrid pagination info-rowslist
            var $datagrid_pagination_info_rows = $datagrid_pagination_info.find('#datagrid_pagination_info_rows').on({
                change: function () {
                    opts.pageSize = parseInt($(this).val());
                    $this.appDatagrid(opts);
                }
            });
            $.each(opts.pageList, function (i, v) {
                $datagrid_pagination_info_rows.append('<option' + (v === opts.pageSize ? ' selected="selected"' : '') + ' value="' + v + '">' + v + '</option>');
            });

            // datagrid pagination info-totalpage
            var $datagrid_pagination_info_totalpage = $datagrid_pagination_info.find('#datagrid_pagination_info_totalpage');

            // datagrid pagination info-totalrecord
            var $datagrid_pagination_info_totalrecord = $datagrid_pagination_info.find('#datagrid_pagination_info_totalrecord');

            // datagrid pagination info-first
            var $datagrid_pagination_info_first = $datagrid_pagination_info.find('#datagrid_pagination_info_first').on({
                click: function () {
                    var current = $datagrid_pagination_info_current.data('current');
                    if (current > 1) {
                        opts.page = 1;
                        $this.appDatagrid(opts);
                    }
                }
            });

            // datagrid pagination info-prev
            var $datagrid_pagination_info_prev = $datagrid_pagination_info.find('#datagrid_pagination_info_prev').on({
                click: function () {
                    var current = $datagrid_pagination_info_current.data('current');
                    if (current > 1) {
                        opts.page = current - 1;
                        $this.appDatagrid(opts);
                    }
                }
            });

            // datagrid pagination info-current
            var $datagrid_pagination_info_current = $datagrid_pagination_info.find('#datagrid_pagination_info_current').on({
                keyup: function (original) {
                    if (original.keyCode === 13) {
                        // key = Enter
                        var infos = funcs.checkPage();
                        if (infos.newPage !== infos.current) {
                            funcs.go();
                        }
                    } else {
                        funcs.checkPage();
                    }
                },
                blur: function () {
                    var infos = funcs.checkPage();
                    if (infos.newPage !== infos.current) {
                        funcs.go();
                    }
                }
            }).data('current', 1).data('max', 1);

            // datagrid pagination info-next
            var $datagrid_pagination_info_next = $datagrid_pagination_info.find('#datagrid_pagination_info_next').on({
                click: function () {
                    var current = $datagrid_pagination_info_current.data('current');
                    var max = $datagrid_pagination_info_current.data('max');
                    if (current < max) {
                        opts.page = current + 1;
                        $this.appDatagrid(opts);
                    }
                }
            });

            // datagrid pagination info-last
            var $datagrid_pagination_info_last = $datagrid_pagination_info.find('#datagrid_pagination_info_last').on({
                click: function () {
                    var current = $datagrid_pagination_info_current.data('current');
                    var max = $datagrid_pagination_info_current.data('max');
                    if (current < max) {
                        opts.page = max;
                        $this.appDatagrid(opts);
                    }
                }
            });

            // datagrid pagination info-refresh
            var $datagrid_pagination_info_refresh = $datagrid_pagination_info.find('#datagrid_pagination_info_refresh').on({
                click: function () {
                    $this.appDatagrid(opts);
                }
            });


            // datagrid pagination record
            var $datagrid_pagination_record = $(
'<div id="datagrid_pagination_record" class="col-md-8 pagination-record hidden-xs hidden-sm"><ul class="pagination pagination-noborder pagination-sm">' +
    '<li class="pagination-record-total">' +
        '<span class="text-gray">共</span>' +
        '<span id="datagrid_pagination_record_total" class="text-gray pl0 pr0">0</span>' +
        '<span class="text-gray">条记录</span>' +
    '</li>' +
'</ul></div>'
            );
            $datagrid_pagination.append($datagrid_pagination_record);

            // datagrid pagination record-total
            var $datagrid_pagination_record_total = $datagrid_pagination_record.find('#datagrid_pagination_record_total');
        }

        if (opts.url) {
            // remoted data
            $.appAjax({
                url: opts.url,
                data: opts.data,
                onSuccess: function (responseData) {
                    opts.responseData = responseData;
                    _render();
                }
            });
        } else {
            // local data
            if (!opts.dataList) {
                opts.dataList = [];
            }
            opts.responseData = {
                size: opts.pageSize,
                data: opts.dataList,
                totalCount: opts.dataList.length,
                pageCount: Math.ceil(opts.dataList.length / opts.pageSize)
            };
            _render();
        }

        $this.data('opts', opts);
        return $this;
    },

    appDatagridReload: function () {
        var $this = $(this);

        return $this.appDatagrid($this.data('opts'));
    },

    appDatagridSelectedRows: function () {
        var $this = $(this);
        var data = $this.data('opts').responseData.data;
        var selected = [];
        $this.find('input[data-column="rowcheckbox"]:checked').each(function (i, v) {
            selected.push(data[$(this).data('index')]);
        });

        return selected;
    },

    appUploadFile: function (opts) {
        var $this = $(this);
        if ($this.fileupload === undefined) {
            throw new Error('需要 jquery.fileupload 插件.');
        }

        opts = $.extend({
            url: undefined,                 // 提交文件路径
            postName: 'fileData',           // 提交参数名称
            multiple: false,                // 是否多选
            autoUpload: true,               // 是否自动提交
            fileListId: 'files',            // 显示文件列表
            fileType: 'All',                // 请参考 FileTypeRegs
            maxFileSize: 9 * 1024 * 1024,   // 请谨慎修改（单位B，1MB=1024KB=1048576B）

            onFileAdd: undefined,           // Object fileInfo, Object $obj, (可选 return actionObj，控件对象，将绑定 click 事件，用于提交操作)
            onBeforeSubmit: undefined,      // Object fileInfo, Object formData, Object $obj, (可选 return false，取消提交数据)
            onSuccess: undefined,           // Object responseData, Object $obj
            onError: undefined,             // Object responseData, Object $obj
            onComplete: undefined,          // Object responseData, String completeString (success|error), Object $obj
            onProgressing: undefined        // Object fileInfo, int progress, Object $obj
        }, opts);

        var fileTypeReg = FileTypeRegs[opts.fileType];
        if (fileTypeReg === undefined) {
            fileTypeReg = FileTypeRegs.All;
        }
        var $fileList = $('#' + opts.fileListId);
        var _convertFileInfo = function (file) {
            return {
                id: file.id,
                type: file.type,
                name: file.name,
                size: file.size
            };
        };

        $this.attr({
            name: opts.postName,
            multiple: opts.multiple
        }).each(function (i, v) {
            var $file = $(v);
            $file.fileupload({
                url: opts.url,
                type: 'POST',
                dataType: 'json',
                autoUpload: false,

                change: function (e, data) {
                    // 清空文件列表
                    $fileList.empty();
                },
                add: function (e, data) {
                    $.each(data.files, function (i, file) {
                        // 检查限制
                        if (fileTypeReg.Reg !== undefined && !fileTypeReg.Reg.test(file.type) && !fileTypeReg.Reg.test(file.name)) {
                            alert(fileTypeReg.Desc + '(' + file.name + ').');
                            return false;
                        }
                        if (file.size > opts.maxFileSize) {
                            var fileSize = file.size < 1024 ? (file.size.toFixed(2) + 'B') : (file.size / 1024 < 1024 ? ((file.size / 1024).toFixed(2) + 'KB') : ((file.size / 1024 / 1024).toFixed(2) + 'MB'));
                            var limitSize = opts.maxFileSize < 1024 ? (opts.maxFileSize + 'B') : (opts.maxFileSize / 1024 < 1024 ? ((opts.maxFileSize / 1024) + 'KB') : ((opts.maxFileSize / 1024 / 1024) + 'MB'));

                            alert('文件大小(' + fileSize + ')超过限制(' + limitSize + ').');
                            return false;
                        }

                        // 添加文件列表
                        file.id = guid();
                        $fileList.append(
'<div id="' + file.id + '" class="files-item alert alert-default">' +
'   <div class="progress">' +
'       <div class="progress-bar progress-bar-success"></div>' +
'   </div>' +
'   <div class="text-ellipsis">' + file.name + '</div>' +
'</div>'
                        );

                        // 用户自定义处理
                        $(opts.onFileAdd && opts.onFileAdd(_convertFileInfo(file), $file)).on({
                            click: function () {
                                data.submit();
                            }
                        });

                        // 提交数据
                        if (opts.autoUpload) {
                            data.submit();
                        }
                    });
                },
                submit: function (e, data) {
                    var isOk = true;
                    $.each(data.files, function (i, file) {
                        // 表单数据
                        var formData = {
                            id: file.id
                        };

                        // 用户自定义处理
                        var result = (opts.onBeforeSubmit && opts.onBeforeSubmit(_convertFileInfo(file), formData, $file));
                        if (result === false) {
                            isOk = false;
                        } else {
                            formData.id = file.id;  // 防止篡改
                            data.formData = formData;
                        }
                    });

                    if (!isOk) {
                        return false;
                    }
                },
                done: function (e, data) {
                    var isOk = false;
                    // 判断是否成功
                    if (data.result.result && data.result.code === '200') {
                        $.each(data.files, function (i, file) {
                            if (data.result.id === file.id) {   // 判断是否成功
                                // 处理对应文件项
                                setTimeout(function () {
                                    var $item = $fileList.find('#' + file.id);
                                    $item.removeClass('alert-default').addClass('alert-success')
                                        .append($('<div class="result-layer"><span class="fa fa-check mr5"></span>上传成功</div>').css({ opacity: 0 }).animate({ opacity: 1 }, 250));

                                    setTimeout(function () {
                                        $item.animate({ opacity: 0 }, 1000, function () { $item.remove(); });
                                    }, 1000);
                                }, 250);

                                // 用户自定义处理
                                opts.onSuccess && opts.onSuccess($.extend(_convertFileInfo(file), { data: data.result.data }), $file);
                                opts.onComplete && opts.onComplete($.extend(_convertFileInfo(file), { data: data.result.data }), 'success', $file);

                                isOk = true;
                            }
                        });
                    }

                    if (!isOk) {
                        e.type = 'fileuploadfail';
                        data.fail(e, data);
                    }
                },
                fail: function (e, data) {
                    $.each(data.files, function (i, file) {
                        // 处理对应文件项
                        setTimeout(function () {
                            var $item = $fileList.find('#' + file.id);
                            $item.removeClass('alert-default').addClass('alert-danger')
                                .append($(
'<div class="result-layer">' +
'   <span class="fa fa-exclamation-circle mr5"></span>' +
    (data.result && (data.result.message || '上传失败')) +
'   <a href="javascript:void(0);" class="fileupload-close">&times;</a>' +
'</div>'
                                    ).css({ opacity: 0 }).animate({ opacity: 1 }, 250)
                                ).find('.progress .progress-bar').removeClass('progress-bar-success').addClass('progress-bar-danger');

                            $item.find('.fileupload-close').on({
                                click: function () {
                                    $item.animate({ opacity: 0 }, 500, function () { $item.remove(); });
                                }
                            });
                        }, 250);

                        // 用户自定义处理
                        opts.onError && opts.onError($.extend(_convertFileInfo(file), { result: data.result }), $file);
                        opts.onComplete && opts.onComplete($.extend(_convertFileInfo(file), { result: data.result }), 'error', $file);
                    });
                },

                progress: function (e, data) {
                    $.each(data.files, function (i, file) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $fileList.find('#' + file.id + ' .progress .progress-bar').css({ width: progress + '%' });

                        // 用户自定义处理
                        opts.onProgressing && opts.onProgressing(_convertFileInfo(file), progress, $file);
                    });
                }
            });
        });

        $this.data('opts', opts);
        return $this;
    },

    appChart: function (opts) {
        var $this = $(this);
        if ($this.highcharts === undefined) {
            throw new Error('需要 highcharts 插件.');
        }

        opts = $.extend({
            url: undefined,                     // 远程请求路径
            data: undefined,                    // 远程请求参数
            dataList: undefined,                // 本地数据（ChartSeriesInfo 列表）

            minWidth: 300,                      // 图表最小宽度（界面小于该宽度，添加横向滚动条）
            height: 300,                        // 图表高度（默认 400）
            title: undefined,                   // 标题
            yAxisTitle: undefined,              // y 轴标题
            copyright: '',                      // 版权信息

            xAxisCategories: undefined,         // x 轴信息
            xAxisTickInterval: 1,               // x 轴刻度间隔（type !== 'pie'）
            tooltipValueSuffix: '',             // 提示框值后缀
            plotOptions: undefined,             // 图表选项

            onAfterResponse: undefined,         // 远程数据响应后（Array renderData）
            onAfterRender: undefined,           // 渲染结束后（Array renderData, Object<Highcharts> chart）
            onAfterClick: undefined             // 点击选择后（Array selectedList, Array renderData, Object<Highcharts> chart）
        }, opts);

        // 处理响应数据
        var _render = function (renderData) {
            // onAfterResponse
            var result = opts.onAfterResponse && opts.onAfterResponse(renderData);
            if (result !== undefined) {
                renderData = result;
            }

            // 预处理数据
            var types = [];
            var xAxis = [];
            opts.renderData = [];
            opts.selectedList = [];
            $.each(renderData, function (i, v) {
                if (!types.contains(v.type)) {
                    types.push(v.type);
                }
                if (v.type === 'pie') {
                    v.size = '100%';
                }

                if (i === 0) {
                    $.each(v.data, function (ii, iv) {
                        xAxis.push(iv.xAxis);
                    });
                }

                opts.renderData.push(v);
            });
            if (types.contains('pie') && types.length > 1) {
                throw new Error('pie 图表不能与其他类型图表一同渲染.');
            }
            var isPie = types.contains('pie');
            var isSpline = types.contains('spline') && types.length === 1;

            // 重新渲染图表
            if (opts.chart) {
                opts.chart.destroy();
            }
            opts.plotOptions = $.extend({
                pie: {
                    showInLegend: true,
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return '<span style="color: ' + this.point.color + ';">' + this.point.name + '：' + this.y + '（' + this.percentage.toFixed(2) + '%）</span>';
                        }
                    }
                }
            }, opts.plotOptions);
            opts.plotOptions.series = {
                allowPointSelect: !!opts.onAfterClick,
                cursor: opts.onAfterClick ? 'pointer' : 'default',
                events: {
                    click: function (event) {
                        if (opts.onAfterClick) {
                            var itemIndex = this.index,
                                dataIndex = event.point.x,
                                selectedKey = itemIndex + '-' + dataIndex;

                            // ctrlKey === true OR shiftKey === true，则为多选，已选中则移除单个，未选中则添加单个
                            // 其余为单选，已选中则移除全部，未选中则标识选中单个
                            if (event.ctrlKey === true || event.shiftKey === true) {
                                // 多选
                                if (opts.selectedList.contains(selectedKey)) {
                                    opts.selectedList.splice($.inArray(selectedKey, opts.selectedList), 1);
                                } else {
                                    opts.selectedList.push(selectedKey);
                                }
                            } else {
                                // 单选
                                if (opts.selectedList.contains(selectedKey)) {
                                    opts.selectedList = [];
                                } else {
                                    opts.selectedList = [selectedKey];
                                }
                            }

                            var result = [];
                            var renderData = opts.renderData;
                            $.each(opts.selectedList, function (i, v) {
                                var values = v.split('-');
                                result.push({
                                    itemIndex: parseInt(values[0]),
                                    dataIndex: parseInt(values[1]),
                                    data: renderData[itemIndex].data[dataIndex]
                                })
                            });

                            opts.onAfterClick(result, renderData, opts.chart);
                        }
                    }
                }
            };
            opts.chart = new Highcharts.Chart({
                chart: {
                    renderTo: id,
                    zoomType: 'x',
                    marginTop: 45
                },
                colors: colors,
                exporting: {
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                text: '导出PNG',
                                onclick: function () {
                                    this.exportChart({
                                        type: 'image/png'
                                    });
                                }
                            }, {
                                text: '导出JPEG',
                                onclick: function () {
                                    this.exportChart({
                                        type: 'image/jpeg'
                                    });
                                }
                            }, {
                                text: '导出PDF',
                                onclick: function () {
                                    this.exportChart({
                                        type: 'application/pdf'
                                    });
                                }
                            }, {
                                text: '导出SVG',
                                onclick: function () {
                                    this.exportChart({
                                        type: 'image/svg+xml'
                                    });
                                }
                            }]
                        }
                    }
                },
                title: {
                    text: opts.title
                },
                legend: {
                    layout: 'horizontal',
                },
                credits: {
                    text: opts.copyright
                },
                xAxis: {
                    categories: opts.xAxisCategories && opts.xAxisCategories.length ? opts.xAxisCategories : xAxis,
                    tickmarkPlacement: 'on',
                    tickInterval: opts.xAxisTickInterval,
                },
                yAxis: {
                    title: {
                        text: opts.yAxisTitle
                    }
                },
                tooltip: {
                    shared: isPie ? false : true,
                    useHTML: true,
                    headerFormat: '<h5>{' + (isPie ? 'series.name' : 'point.key') + '}</h5><table>',
                    pointFormat:
'<tr>' +
    '<td>' +
        '<span style="color: {' + (isPie ? 'point' : 'series') + '.color}; padding-right: 5px;">●</span>' +
        '{' + (isPie ? 'point' : 'series') + '.name}' +
    '</td>' +
    '<td style="padding-left: 10px; text-align: right;">' +
        '<b>{point.y} ' + opts.tooltipValueSuffix + (isPie ? '（{point.percentage:.2f}%）' : '') + '</b>' +
    '</td>' +
'</tr>',
                    footerFormat: '</table>',
                    crosshairs: [
                        isSpline ? {
                            width: 1,
                            color: '#dedede'
                        } : true
                    ]
                },
                plotOptions: opts.plotOptions,
                series: opts.renderData
            });

            // onAfterRender
            opts.onAfterRender && opts.onAfterRender(renderData, opts.chart);
        };

        // 处理默认值
        if (!opts.height) {
            opts.height = 400;
        }
        var colors = ['#99CCFF', '#999999', '#FFCC33', '#CCCCFF', '#6699CC', '#FFCCCC', '#E57F99', '#99CCCC', '#D98C40', '#FFACD6', '#ACD659', '#8383AC'];

        // 初始化
        $this.css({ width: '100%', minWidth: opts.minWidth ? (opts.minWidth + 'px') : 'auto', height: opts.height ? (opts.height + 'px') : 'auto' }).empty();
        var id = $this.attr('id');
        if (id === undefined) {
            id = guid();
            $this.attr('id', id);
        }

        if (opts.url) {
            // remoted data
            $.appAjax({
                url: opts.url,
                data: opts.data,
                onSuccess: function (responseData) {
                    _render(responseData);
                }
            });
        } else {
            // local data
            _render(opts.dataList);
        }

        $this.data('opts', opts);
        return $this;
    },

    appSelect2: function (opts) {
        var $this = $(this);
        if ($this.select2 === undefined) {
            throw new Error('需要 select2 插件.');
        }

        opts = $.extend({
            placeholder: undefined,             // 占位提示文字
            disabled: undefined,                // 是否禁用
            multiple: undefined,                // 是否多选
            maxSelectLength: 9999,              // 最多选择数量（multiple: true）
            search: undefined,                  // 是否显示搜索框
            keywordName: 'keyword',             // 搜索关键字名
            createNewItem: false,               // 是否可新建选项

            dataList: undefined,                // 本地数据（Array items { id, text }）
            url: undefined,                     // 远程请求路径
            data: undefined,                    // 远程请求参数
            size: 10,                           // 远程请求数量
            convertItems: undefined,            // 转换选项值（Object responseData, Int page, Int size; return Array items { id, text }）
            onOpen: undefined,                  // 打开选项时（Event event, Object $obj）
            onClose: undefined,                 // 关闭选项时（Event event, Object $obj）
            onSelect: undefined,                // 选择选项时（Event event, Object $obj）
            onUnSelect: undefined               // 取消选择选项时（multiple: true, Event event, Object $obj）
        }, opts);

        // 处理禁用
        $this.attr({ disabled: opts.disabled });

        // 处理多选
        $this.attr({ multiple: opts.multiple });

        // 处理初始化数据
        var dataList = undefined;
        if (opts.dataList) {
            dataList = [];
            $.each(opts.dataList, function (i, v) {
                if (v.id !== undefined) {
                    dataList.push({
                        id: v.id.toString(),
                        text: v.text
                    });
                }
            });
            $this.empty();
        }

        // 处理远程数据
        var ajax = undefined;
        if (opts.url) {
            opts.search === undefined && (opts.search = true);
            ajax = {
                delay: 250,     // 延迟搜索，防止快速输入
                url: opts.url,
                type: 'POST',
                dataType: 'json',
                data: function (params) {
                    // 封装请求参数
                    var requestData = $.extend({
                        page: params.page || 1,
                        size: opts.size
                    }, opts.data);
                    requestData[opts.keywordName ? opts.keywordName : 'keyword'] = params.term;

                    return requestData;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    // 自定义处理响应数据逻辑
                    var results = opts.convertItems && opts.convertItems(data, params.page, opts.size);
                    return {
                        results: results || data,
                        pagination: {
                            more: (params.page * opts.size) < data.totalCount
                        }
                    };
                }
            };
        }

        // 每个 select 控件分别设置
        $this.each(function (i, v) {
            var $select = $(v);
            var multiple = !!$select.attr('multiple');
            var hasEmptyValue = false;
            var placeholder = opts.placeholder || $select.attr('placeholder');
            $select.find('option').each(function (i, v) {
                var $option = $(v);
                if ($option.val() === undefined || $option.val() === '') {
                    hasEmptyValue = true;

                    if ((placeholder === undefined || placeholder === '') && ($option.html() !== undefined && $option.html() !== '')) {
                        placeholder = $option.html();
                        $select.attr({ placeholder: placeholder });
                        multiple && $option.remove();
                    }
                }
            });

            $select.select2({
                language: 'zh-CN',
                placeholder: placeholder,
                minimumResultsForSearch: !multiple && opts.search ? undefined : Infinity,
                allowClear: multiple ? false : hasEmptyValue,
                maximumSelectionLength: opts.maxSelectLength,
                tags: opts.createNewItem,
                data: dataList,
                ajax: ajax
            }).on({
                'select2:open': function (event) {
                    opts.onOpen && opts.onOpen(event, $select);
                },
                'select2:close': function (event) {
                    opts.onClose && opts.onClose(event, $select);
                },
                'select2:select': function (event) {
                    opts.onSelect && opts.onSelect(event, $select);
                },
                'select2:unselect': function (event) {
                    opts.onUnSelect && opts.onUnSelect(event, $select);
                }
            });
        });

        $this.data('opts', opts);
        return $this;
    },

    appSelect2SetValue: function (value) {
        return $(this).val(value).trigger('change');
    },

    appDateTimePicker: function (opts) {
        var $this = $(this);
        if ($this.daterangepicker === undefined) {
            throw new Error('需要 bootstrap.daterangepicker 插件.');
        }

        opts = $.extend({
            format: undefined,                  // 日期格式（例如：YYYY-MM-DD HH:mm:ss）
            min: undefined,                     // 最小可选日期
            max: undefined,                     // 最大可选日期
            start: undefined,                   // 选择开始日期
            startName: 'start',                 // 开始日期Name
            end: undefined,                     // 选择结束日期
            endName: 'end',                     // 结束日期Name
            close: true,                        // 是否能关闭
            minutesIncrement: 1,                // 分钟选择器增量值（for datetime & datetimerange）
            secondsSelector: false,             // 是否显示秒选择器（for datetime & datetimerange）
            separator: '~',                     // 分隔符（for range）
            days: undefined,                    // 限制选择日期长度（for range）
            dateRange: undefined,               // 快速可选日期（for range）
            drops: undefined,                   // 打开方式（垂直）
            opens: undefined,                   // 打开方向（水平）

            onChange: undefined                 // 选择日期时（Array values { start, end（for range） }, Array $hiddens { $start, $end（for range） }, Object $obj）
        }, opts);

        // 初始化
        opts.separator = opts.separator ? opts.separator.trim() : '~';

        // 每个 input 控件分别设置
        $this.each(function (i, v) {
            var $datetime = $(v),
                id = $datetime.attr('id'),
                name = $datetime.attr('name'),
                type = $datetime.data('toggle'),
                isTime = type.indexOf('time') >= 0,
                isRange = type.indexOf('range') >= 0,
                start = opts.start || new Date(),
                end = isRange && opts.end ? opts.end : start,
                startName = opts.startName || 'start',
                endName = isRange ? (opts.endName || 'end') : undefined;

            // 设置值
            var _setValues = function (isNotify) {
                var vals = $datetime.val().split(' ' + opts.separator + ' ');
                var start = vals.length >= 1 && vals[0] ? vals[0] : '';
                var end = isRange && vals.length >= 2 ? vals[1] : '';

                // hidden input
                var $start = $('input[type="hidden"][data-for="' + id + '"][name="' + startName + '"]');
                var $end = $('input[type="hidden"][data-for="' + id + '"][name="' + endName + '"]');

                $start.val(start).change();
                $end.val(end).change();

                isNotify && opts.onChange && opts.onChange(isRange ? [start, end] : [start], isRange ? [$start, $end] : [$start], $datetime);
            };

            // name
            if (name === startName || name === endName) {
                $datetime.removeAttr('name');
                $datetime.data('name', name);
            }

            // destroy
            var daterangepicker = $datetime.data('daterangepicker');
            if (daterangepicker) {
                daterangepicker.container.remove();
            }

            // parent
            var $inputicon = $datetime.parent('.datetimepicker');
            if ($inputicon || $inputicon.length) {
                $datetime.insertAfter($inputicon);
                $inputicon.remove();
            }
            $inputicon = $('<div class="input-group datetimepicker"></div>').insertAfter($datetime);

            // icon
            var $icon = $('<span class="fa fa-' + (isTime ? 'clock-o' : 'calendar') + ' input-group-addon-float"></span>')
                .appendTo($inputicon)
                .on({ click: function () { $datetime.click(); } });

            // caret
            var $caret = $('<span class="fa fa-caret-down input-group-addon-float input-group-addon-float-right"></span>')
                .appendTo($inputicon)
                .on({ click: function () { $datetime.click(); } });

            // clear
            var $clear = $('<span class="fa fa-clear input-group-addon-float input-group-addon-float-right" title="清空">&times;</span>')
                .appendTo($inputicon)
                .on({
                    click: function () {
                        $datetime.data('daterangepicker').setStartDate(new Date());
                        $datetime.data('daterangepicker').setEndDate(new Date());

                        $datetime.val('');
                        _setValues(true);
                        $clear.hide();
                    }
                });

            // picker
            $datetime.addClass('form-datetimepicker')
                .remove()
                .attr({ readonly: true })
                .insertAfter($icon)
                .daterangepicker({
                    autoApply: true,
                    locale: {
                        format: opts.format || (isTime ? (opts.secondsSelector ? 'YYYY-MM-DD HH:mm:ss' : 'YYYY-MM-DD HH:mm') : 'YYYY-MM-DD'),
                        separator: ' ' + opts.separator + ' ',
                        applyLabel: '确定',
                        cancelLabel: '取消',
                        customRangeLabel: '自定义',
                        daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
                        monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
                    },
                    drops: opts.drops,
                    opens: opts.opens,
                    applyClass: 'hide',
                    cancelClass: 'hide',
                    singleDatePicker: !isRange,
                    timePicker: isTime,
                    timePicker24Hour: true,
                    timePickerIncrement: opts.minutesIncrement,
                    timePickerSeconds: opts.secondsSelector,
                    alwaysShowCalendars: true,
                    minDate: opts.min || '1990-01-01',
                    maxDate: opts.max || '9999-12-31',
                    startDate: start,
                    endDate: end,
                    dateLimit: isRange && opts.days ? { days: opts.days } : undefined,
                    ranges: isRange && opts.dateRange ? opts.dateRange : undefined
                }).on({
                    'show.daterangepicker': function () {
                        $caret.removeClass('fa-caret-down').addClass('fa-caret-up');
                    },
                    'hide.daterangepicker': function () {
                        $caret.removeClass('fa-caret-up').addClass('fa-caret-down');
                        opts.close && $clear.show();
                        _setValues(true);
                    }
                });

            if (!opts.start) {
                $datetime.data('daterangepicker').setStartDate(new Date());
                $datetime.data('daterangepicker').setEndDate(new Date());

                $datetime.val('');
                $clear.hide();
            }
            _setValues(false);
            !opts.close && $clear.hide();
        });

        $this.data('opts', opts);
        return $this;
    },

    appDateTimePickerSetValue: function (type, value) {
        var $this = $(this);

        // 每个 input 控件分别设置
        $this.each(function (i, v) {
            var $datetime = $(v),
                opts = $datetime.data('opts');

            if ((typeof type).toUpperCase() === 'OBJECT') {
                $.each(type, function (i, v) {
                    opts[i] = v;
                });
            } else {
                opts[type] = value;
            }

            $datetime.appDateTimePicker(opts);
        });

        return $this;
    },

    appMobileCalendarPicker: function (opts) {
        opts = $.extend({
            format: 'yyyy-MM-dd',               // 日期格式
            min: undefined,                     // 最小值
            max: undefined,                     // 最大值
            closeFor: undefined,                // 关闭后打开（Id）

            onChange: undefined,                // Object input, String value
            onOpen: undefined,                  // Object input
            onClose: undefined                  // Object input
        }, opts);

        var $this = $(this);
        $this.each(function (i, v) {
            var $item = $(v),
                value = $this.val();

            // 若已生成，先销毁
            $item.data('calendar') !== undefined && $item.calendar('destroy');

            $item.calendar({
                value: value ? [parseDatetime(value).toFormat('yyyy-MM-dd')] : [],
                minDate: opts.min,  // 当天不可选
                maxDate: opts.max,  // 当天可选
                formatValue: function (p, values, displayValues) {
                    return values.length ? new Date(values[0]).toFormat(opts.format) : '';
                },
                onChange: function (p, values, displayValues) {
                    opts.onChange && opts.onChange($item, values[0]);
                },
                onOpen: function (p) {
                    opts.onOpen && opts.onOpen($item);
                },
                onClose: function (p) {
                    opts.onClose && opts.onClose($item);
                    $item.val() && opts.closeFor && $(opts.closeFor.indexOf('#') === 0 ? opts.closeFor : ('#' + opts.closeFor)).click();
                }
            });
        });

        $this.data('opts', opts);
        return $this;
    },

    appMobileCalendarPickerSetValue: function (value) {
        var $this = $(this);

        return $this.val(value).appMobileCalendarPicker($this.data('opts'));
    },

    appMobileTimePicker: function (opts) {
        opts = $.extend({
            format: 'HH:mm',                    // 时间格式
            view: 'minute',                     // 视图 ['hour', 'minute', 'second']
            hours: undefined,                   // 小时值列表 [0 - 23]
            minutes: undefined,                 // 分钟值列表 [0 - 59]
            seconds: undefined,                 // 秒值列表 [0 - 59]

            onChange: undefined,                // Object input, Array values
            onOpen: undefined,                  // Object input
            onClose: undefined                  // Object input
        }, opts);

        var $this = $(this);
        $this.each(function (i, v) {
            var $item = $(v),
                value = $item.val(),
                generateValues = function (min, max, step) {
                    var values = [];
                    for (var i = min; i < max; i += step) values.push(i.toString().padLeft(2, '0'));
                    return values;
                },
                cols = [
                    { textAlign: 'center', values: opts.hours ? opts.hours : generateValues(0, 24, 1) },
                    { divider: true, content: '时' }
                ];

            // 初始化选项
            if (opts.view) {
                opts.view = opts.view.toUpperCase();
                if (opts.view === 'MINUTE' || opts.view === 'SECOND') {
                    cols.push({ textAlign: 'center', values: opts.minutes ? opts.minutes : generateValues(0, 60, 1) });
                    cols.push({ divider: true, content: '分' });
                }
                if (opts.view === 'SECOND') {
                    cols.push({ textAlign: 'center', values: opts.seconds ? opts.seconds : generateValues(0, 60, 1) });
                    cols.push({ divider: true, content: '秒' });
                }
            }

            // 若已生成，先销毁
            $item.data('picker') !== undefined && $item.picker('destroy');

            $item.picker({
                value: value ? value.split(':') : undefined,
                cols: cols,
                formatValue: function (p, values, displayValues) {
                    if (values.length) {
                        var value = opts.format.replace('HH', values[0]).replace('hh', values[0]);
                        if (values.length >= 2) {
                            value = value.replace('MM', values[1]).replace('mm', values[1]);
                        }
                        if (values.length >= 3) {
                            value = value.replace('SS', values[2]).replace('ss', values[2]);
                        }
                        return value;
                    } else {
                        return '';
                    }
                },
                onChange: function (p, values, displayValues) {
                    opts.onChange && opts.onChange($item, values);
                },
                onOpen: function (p) {
                    opts.onOpen && opts.onOpen($item);
                },
                onClose: function (p) {
                    opts.onClose && opts.onClose($item);
                }
            });
        });

        $this.data('opts', opts);
        return $this;
    },

    appMobileTimePickerSetValue: function (value) {
        var $this = $(this);

        return $this.val(value).appMobileTimePicker($this.data('opts'));
    },

    appMobileAreas: function (opts) {
        opts = $.extend({
            view: 'district',                   // 视图 ['province', 'city', 'district']

            onChange: undefined,                // Object input, Array ids
            onOpen: undefined,                  // Object input
            onClose: undefined                  // Object input
        }, opts);

        var $this = $(this);

        areaUtility.execute(function () {
            var areas = areaUtility.data();
            $this.each(function (i, v) {
                var $item = $(v),
                    getInitValues = function () {
                        var value = $item.val();
                        if (!value) { return []; }

                        var values = value.split(','),
                            provinceName = values[0],
                            cityName = values[1],
                            districtName = values[2],
                            result = [];
                        // 省份
                        if (provinceName) {
                            for (var i = 0, length = areas.provinces.length; i < length; ++i) {
                                if (areas.provinces[i].nameCN === provinceName) {
                                    result.push(areas.provinces[i].provinceId);
                                    break;
                                }
                            }
                            // 城市
                            if (result[0] && cityName) {
                                for (var i = 0, length = areas.cities.length; i < length; ++i) {
                                    if (areas.cities[i].provinceId === result[0] && areas.cities[i].nameCN === cityName) {
                                        result.push(areas.cities[i].cityId);
                                        break;
                                    }
                                }
                                // 城市
                                if (result[1] && districtName) {
                                    for (var i = 0, length = areas.districts.length; i < length; ++i) {
                                        if (areas.districts[i].cityId === result[1] && areas.districts[i].nameCN === districtName) {
                                            result.push(areas.districts[i].districtId);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        return result;
                    },
                    getProvinces = function () {
                        var result = { ids: [], names: [] };
                        $.each(areas.provinces, function (i, v) {
                            result.ids.push(v.provinceId);
                            result.names.push(v.nameCN);
                        });
                        return result;
                    },
                    getCities = function (provinceId) {
                        var result = { ids: [], names: [] };
                        $.each(areas.cities, function (i, v) {
                            if (v.provinceId === provinceId) {
                                result.ids.push(v.cityId);
                                result.names.push(v.nameCN);
                            }
                        });
                        return result;
                    },
                    getDistricts = function (cityId) {
                        var result = { ids: [], names: [] };
                        $.each(areas.districts, function (i, v) {
                            if (v.cityId === cityId) {
                                result.ids.push(v.districtId);
                                result.names.push(v.nameCN);
                            }
                        });
                        return result;
                    };

                // 初始化数据
                var initValues = getInitValues();
                var provinces = getProvinces(),
                    provinceId = initValues[0] ? initValues[0] : provinces.ids[0],
                    cities = getCities(provinceId),
                    cityId = initValues[1] ? initValues[1] : cities.ids[0],
                    districts = getDistricts(cityId),
                    districtId = initValues[2] ? initValues[2] : districts.ids[0];
                var cols = [{
                    textAlign: 'center',
                    values: provinces.ids,
                    displayValues: provinces.names,
                    cssClass: 'col-provinces'
                }];
                if (opts.view) {
                    opts.view = opts.view.toUpperCase();
                    if (opts.view === 'CITY' || opts.view === 'DISTRICT') {
                        cols.push({
                            textAlign: 'center',
                            values: cities.ids,
                            displayValues: cities.names,
                            cssClass: 'col-cities'
                        });
                    }
                    if (opts.view === 'DISTRICT') {
                        cols.push({
                            textAlign: 'center',
                            values: districts.ids,
                            displayValues: districts.names,
                            cssClass: 'col-districts'
                        });
                    }
                }

                // 若已生成，先销毁
                $item.data('picker') !== undefined && $item.picker('destroy');

                $item.picker({
                    title: $item.attr('placeholder'),
                    cols: cols,
                    value: [provinceId, cityId, districtId],
                    formatValue: function (p, values, displayValues) {
                        return displayValues.join(',');
                    },
                    onChange: function (p, values, displayValues) {
                        var newProvinceId = parseInt(values[0]),
                            newCityId = parseInt(values[1]),
                            newDistrictId = parseInt(values[2]);

                        if (newProvinceId !== provinceId && !isNaN(newCityId)) {
                            // 更新城市数据
                            cities = getCities(newProvinceId);
                            newCityId = cities.ids[0];
                            p.cols[1].replaceValues(cities.ids, cities.names);

                            if (!isNaN(newDistrictId)) {
                                // 更新地区数据
                                districts = getDistricts(newCityId);
                                newDistrictId = districts.ids[0];
                                p.cols[2].replaceValues(districts.ids, districts.names);
                            }

                            // 更新当前Id
                            provinceId = newProvinceId;
                            cityId = newCityId;
                            districtId = newDistrictId;

                            // 更新值
                            p.updateValue();
                            return;
                        } else if (newCityId !== cityId && !isNaN(newDistrictId)) {
                            // 更新地区数据
                            districts = getDistricts(newCityId);
                            newDistrictId = districts.ids[0];
                            p.cols[2].replaceValues(districts.ids, districts.names);

                            // 更新当前Id
                            provinceId = newProvinceId;
                            cityId = newCityId;
                            districtId = newDistrictId;

                            // 更新值
                            p.updateValue();
                            return;
                        }

                        // 赋值新值
                        var ids = [newProvinceId];
                        !isNaN(newCityId) && ids.push(newCityId);
                        !isNaN(newCityId) && !isNaN(newDistrictId) && ids.push(newDistrictId);
                        $item.data('ids', ids);
                        opts.onChange && opts.onChange($item, ids);
                    },
                    onOpen: function (p) {
                        opts.onOpen && opts.onOpen($item);
                    },
                    onClose: function (p) {
                        opts.onClose && opts.onClose($item);
                    }
                });
            });
        });

        $this.data('opts', opts);
        return $this;
    }
});

/*
 获取枚举值名称
 enumName: 枚举名称
 value: 枚举值
 */
var getEnumValueName = function (enumName, value) {
    if (EnumValueReg.test(value)) {
        try {
            var val = parseInt(value),
                enumObject = eval(enumName);

            for (var i in enumObject) {
                if (enumObject[i] === val) {
                    return i;
                }
            }
        } catch (e) {
            return undefined;
        }
    }

    return undefined;
};

/*
 获取枚举翻译信息
 enumName: 枚举名称
 value: 枚举值名称，或枚举值
 */
var getEnumMessage = function (enumName, value) {
    // 是枚举值，则转为枚举值名称
    var valueName = EnumValueReg.test(value) ? getEnumValueName(enumName, value) : value;

    try {
        return eval(enumName + '.message.' + valueName);
    } catch (e) {
        return undefined;
    }
};

/* 浏览器信息 */
var browser = {
    versions: function () {
        var u = navigator.userAgent, v = navigator.appVersion;
        return {
            versionCode: v,                                         // 浏览器版本
            trident: u.contains('Trident'),                         // IE内核
            presto: u.contains('Presto'),                           // opera内核
            webKit: u.contains('AppleWebKit'),                      // 苹果、谷歌内核
            gecko: u.contains('Gecko') && u.contains('KHTML'),      // 火狐内核
            mobile: !!u.match(/AppleWebKit.*Mobile.*/),             // 是否为移动终端
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),        // ios终端
            android: u.contains('Android') || u.contains('Linux'),  // android终端或者uc浏览器
            iPhone: u.contains('iPhone') || u.contains('Mac'),      // 是否为iPhone或者QQHD浏览器
            iPad: u.contains('iPad'),                               // 是否iPad
            webApp: !u.contains('Safari'),                          // 是否web应该程序，没有头部与底部
            google: u.indexOf('Chrome') > -1,                       // 是否Chrome
            isWechat: !!u.match(/MicroMessenger/i)                  // 是否微信
        };
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
};

$(function () {
    // 图表配置
    if ($('body').highcharts) {
        Highcharts.setOptions({
            lang: {
                decimalPoint: '.',
                thousandsSep: ',',
                numericSymbols: 'k,M,G,T,P,E',

                loading: '加载中',
                noData: '暂无数据',

                contextButtonTitle: '导出图表',
                downloadJPEG: '导出JPEG',
                downloadPDF: '导出PDF',
                downloadPNG: '导出PNG',
                downloadSVG: '导出SVG',
                printChart: '打印图表',

                months: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
                shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                weekdays: ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天'],

                drillUpText: '返回',
                resetZoom: '恢复缩放',
                resetZoomTitle: '恢复图表'
            }
        });
    }
});
