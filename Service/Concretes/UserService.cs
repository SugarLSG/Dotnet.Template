﻿using APP.Core.AOP.Attributes;
using APP.Core.Models;
using APP.Core.Repositories;
using APP.Core.Utilities;
using DAO.Abstracts;
using Framework.DBEntity;
using Framework.Enums;
using Framework.Exceptions;
using Framework.Models.User;
using NHibernate.Criterion;
using Service.Abstracts;
using Service.AOP.Cache;
using System.Collections.Generic;
using System.Linq;

namespace Service.Concretes
{
    public class UserService : IUserService
    {
        private IBaseRepository BaseRepository { get; set; }
        private IUserRepository UserRepository { get; set; }
        private IAuthService AuthService { get; set; }


        public PagingResult<SearchResult> Search(SearchCondition condition)
        {
            return UserRepository.Search(condition);
        }


        public void UpdateStatus(int userId, SysUserStatus status)
        {
            var user = BaseRepository.GetEntityById<SysUserEntity>(userId);
            user.Status = status;
            BaseRepository.UpdateEntity(user);
        }

        public EditUserInfo GetInfo(int userId)
        {
            var user = BaseRepository.GetEntityById<SysUserEntity>(userId);

            return new EditUserInfo
            {
                UserId = user.UserId,
                Account = user.Account,
                NeedUpdatePassword = !user.UpdatedPassword,
                IsUpdatePassword = false,
                Name = user.Name,
                Type = user.Type,
                Sex = user.Sex,
                Phone = user.PhoneNumber,
                Email = user.Mail,
                Wechat = user.Wechat,
                QQ = user.QQ,
                AppId = user.AppId,
                AppSecret = user.AppSecret,
                IsEnabled = user.Status == SysUserStatus.Enabled,
                RoleList = BaseRepository.GetEntityList<SysUserRoleEntity>(new ICriterion[]
                {
                    Restrictions.Eq(SysUserRoleEntity.COL_USERID, userId)
                }).Select(ur => ur.RoleCode)
            };
        }

        public void CheckOldPassword(int userId, string password)
        {
            var user = BaseRepository.GetEntityById<SysUserEntity>(userId);
            if (user.Password != AuthService.PasswordEncrypt(password))
                throw new WrongOldPasswordException();
        }

        [Notify(typeof(ClearUserCacheNotify))]
        public int SaveInfo(EditUserInfo info)
        {
            var user = BaseRepository.GetOrCreateEntityById<SysUserEntity>(info.UserId);

            if (!user.UserId.HasValue)
                user.Account = info.Account;

            if (info.IsUpdatePassword && info.NewPassword.HasValue())
            {
                user.Password = AuthService.PasswordEncrypt(info.NewPassword);
                user.UpdatedPassword = AuthenticationUtility.AuthorizedUserId.Value == user.UserId;
            }
            user.Name = info.Name;
            user.Type = info.Type.Value;
            user.Sex = info.Sex;
            user.PhoneNumber = info.Phone;
            user.Mail = info.Email;
            user.Wechat = info.Wechat;
            user.QQ = info.QQ;
            user.AppId = info.AppId ?? user.AppId;
            user.AppSecret = info.AppSecret ?? user.AppSecret;
            user.Status = info.IsEnabled ? SysUserStatus.Enabled : SysUserStatus.Disabled;
            BaseRepository.AddOrUpdateEntity(user);

            UserRepository.UpdateUserRoles(user.UserId.Value, info.RoleList);

            return user.UserId.Value;
        }

        public bool IsAccountExisted(int? userId, string account)
        {
            var criterions = new List<ICriterion>
            {
                Restrictions.Eq(SysUserEntity.COL_ACCOUNT, account.HasValue() ? account.Trim() : account)
            };
            if (userId.HasValue)
                criterions.Add(Restrictions.Not(Restrictions.Eq(SysUserEntity.COL_USERID, userId.Value)));

            return BaseRepository.GetEntityCount<SysUserEntity>(criterions) > 0;
        }
    }
}
