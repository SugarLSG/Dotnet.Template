﻿var $MenuContainer = $('#menuContainer');


var reloadMenu = function () {
    $MenuContainer.empty();

    var addList = function (parentId, $container) {
        // 找出当前数据
        var list = [];
        $.each(MenuList, function (i, v) {
            if (v.parentId === parentId) {
                list.push(v);
            }
        });

        // 渲染界面
        var $ol = $('<ol class="dd-list"></ol>');
        $.each(list, function (i, v) {
            $li = $(
'<li class="dd-item dd3-item">' +
'   <div class="dd-handle dd3-handle"></div>' +
'   <div class="dd3-content">' +
(v.icon ?
'       <span class="' + v.icon + ' w20"></span>' : '') +
'       <span style="cursor: default;' + (v.isVisible ? '' : ' color: #ccc;') + '" title="' + v.description + '(' + (v.isVisible ? '显示' : '隐藏') + '状态，' + (v.privilege === undefined ? '未' : '已') + '绑定权限)">' +
            v.name +
(v.privilege !== undefined ?
'           <span class="fa fa-chain ml10"></span>' : '') +
'       </span>' +
'       <div class="pull-right">' +
(v.deep < MenuMaxDeep && v.privilege === undefined ?
'           <a href="javascript:addSubMenu(\'' + v.id + '\', ' + (v.deep + 1) + ');" class="mr10" title="添加子菜单"><span class="fa fa-plus"></span></span></a>' : '') +
'           <a href="javascript:editMenu(\'' + v.id + '\');" class="mr10" title="修改"><span class="fa fa-edit text-success"></span></a>' +
'           <a href="javascript:showOrHideMenu(\'' + v.id + '\');" class="mr30" title="' + (v.isVisible ? '隐藏' : '显示') + '菜单"><span class="fa fa-' + (v.isVisible ? 'eye-slash text-muted' : 'eye text-info') + '"></span></a>' +
'           <a href="javascript:deleteMenu(\'' + v.id + '\');" title="删除"><span class="fa fa-remove text-danger"></span></a>' +
'       </div>' +
'   </div>' +
'</li>'
            ).data('info', v);
            $ol.append($li);

            // 子菜单
            addList(v.id, $li);
        });
        $container.append($ol);
    };
    addList(undefined, $MenuContainer);
};

var addParentMenu = function (parentId, deep) {
    loadUrl(
        '添加父级菜单',
        '/system/menu/parentmenu',
        undefined,
        function (result, modalObject) {
            var $form = modalObject.find('form').on({
                submit: function (event) {
                    event.preventDefault();

                    if ($form.valid()) {
                        var info = $form.appSerialize();
                        MenuList.push({
                            id: guid(),
                            parentId: parentId,
                            deep: deep,
                            privilege: undefined,
                            name: info.Name,
                            icon: info.Icon,
                            description: info.Description,
                            sorted: MenuList.length,
                            isVisible: true
                        });

                        reloadMenu();
                        modalObject.modal('hide');
                    }
                }
            });
        }
    );
};

var addSubMenu = function (parentId, deep) {
    loadUrl(
        '添加' + deep + '级菜单',
        '/system/menu/submenu/' + encodeParameters(deep < MenuMaxDeep),
        undefined,
        function (result, modalObject) {
            var $form = modalObject.find('form').on({
                submit: function (event) {
                    event.preventDefault();

                    var $list = $form.find('input[type="checkbox"]:checked');
                    if ($list.length) {
                        $list.each(function (i, v) {
                            MenuList.push({
                                id: guid(),
                                parentId: parentId,
                                deep: deep,
                                privilege: $(v).val(),
                                name: $(v).data('name'),
                                icon: undefined,
                                description: $(v).data('name'),
                                sorted: MenuList.length,
                                isVisible: true
                            });
                        });

                        reloadMenu();
                        modalObject.modal('hide');
                    } else {
                        $form.find('#divInvalidMessage').show();
                    }
                }
            });

            $form.find('#btnAddParentMenu').on({
                click: function () {
                    modalObject.modal('hide');
                    addParentMenu(parentId, deep);
                }
            });
        }
    );
};

var editMenu = function (id) {
    $.each(MenuList, function (i, v) {
        if (v.id === id) {
            loadUrl(
                '修改菜单',
                '/system/menu/edit' +
                    '?privilege=' + (v.privilege !== undefined ? v.privilege : '') +
                    '&name=' + (v.name !== undefined ? v.name : '') +
                    '&icon=' + (v.icon !== undefined ? v.icon : '') +
                    '&description=' + (v.description !== undefined ? v.description : ''),
                undefined,
                function (result, modalObject) {
                    var $form = modalObject.find('form').on({
                        submit: function (event) {
                            event.preventDefault();

                            if ($form.valid()) {
                                var info = $form.appSerialize();
                                v.name = info.Name;
                                v.icon = info.Icon;
                                v.description = info.Description;

                                reloadMenu();
                                modalObject.modal('hide');
                            }
                        }
                    });
                }
            );
        }
    });
};

var showOrHideMenu = function (id) {
    for (var i = 0, length = MenuList.length; i < length ; ++i) {
        var item = MenuList[i];

        if (item.id === id) {
            item.isVisible = !item.isVisible;
            reloadMenu();
            return;
        }
    }
};

var deleteMenu = function (id) {
    confirm('确定删除该菜单(组)吗？', function (result) {
        if (result) {
            for (var i = 0, length = MenuList.length; i < length ; ++i) {
                var item = MenuList[i];

                if (item.id === id) {
                    MenuList.splice(i, 1);
                    reloadMenu();
                    return;
                }
            }
        }
    });
};

$(function () {
    // 重新转化菜单列表
    (function () {
        var list = [];
        var convert = function (data, deep, parentId) {
            if (data) {
                $.each(data, function (i, v) {
                    var id = guid();
                    list.push({
                        id: id,
                        parentId: parentId,
                        deep: deep,
                        privilege: v.privilege,
                        name: v.name,
                        icon: v.icon,
                        description: v.description,
                        sorted: i,
                        isVisible: v.isVisible
                    });

                    if (v.subMenuList) {
                        convert(v.subMenuList, deep + 1, id);
                    }
                });
            }
        };
        convert(MenuList, 1, undefined);
        MenuList = list;
    })();
    reloadMenu();

    // 绑定拖拉控件
    $MenuContainer.nestable().on({
        change: function () {
            var list = serializeMenu();

            // 检查限制
            for (var i = 0, length = list.length; i < length ; ++i) {
                var item = list[i];

                // 深度
                if (item.deep > MenuMaxDeep) {
                    alert('最多只能配置' + MenuMaxDeep + '级菜单.');
                    reloadMenu();
                    return;
                }
            }

            // 刷新列表数据
            MenuList = list;
            reloadMenu();
        }
    });

    // 序列化菜单数据
    var serializeMenu = function () {
        var list = [];
        var serialize = function (data, deep, parentId) {
            if (data) {
                $.each(data, function (i, v) {
                    list.push({
                        id: v.info.id,
                        parentId: parentId,
                        deep: deep,
                        privilege: v.info.privilege,
                        name: v.info.name,
                        icon: v.info.icon,
                        description: v.info.description,
                        sorted: i,
                        isVisible: v.info.isVisible
                    });

                    if (v.children) {
                        serialize(v.children, deep + 1, v.info.id);
                    }
                });
            }
        };
        serialize($MenuContainer.nestable('serialize'), 1, undefined);

        return list;
    };

    // 保存
    $('#btnSave').on({
        click: function () {
            var menuList = serializeMenu();
            var serialize = function (parentId) {
                // 找出当前数据
                var list = [];
                $.each(menuList, function (i, v) {
                    if (v.parentId === parentId) {
                        list.push(v);

                        v.subMenuList = serialize(v.id);
                    }
                });

                return list;
            };

            $.appAjax({
                url: '/system/menu/update',
                data: { model: serialize(undefined) },
                onSuccess: function (responseData) {
                    handleAjaxResult(responseData, function () {
                        location.reload();
                    });
                }
            });
        }
    });
});
