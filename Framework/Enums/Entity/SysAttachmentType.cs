﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using System.ComponentModel;

namespace Framework.Enums
{
    /// <summary>
    /// 附件类型
    /// ENUM[int]
    /// 0-File-文件
    /// 10-Image-图片
    /// 20-Audio-音频
    /// 30-Video-视频
    /// </summary>
    public enum SysAttachmentType
    {
        /// <summary>
        /// 文件
        /// </summary>
        [Description("文件")]
        File = 0,
        /// <summary>
        /// 图片
        /// </summary>
        [Description("图片")]
        Image = 10,
        /// <summary>
        /// 音频
        /// </summary>
        [Description("音频")]
        Audio = 20,
        /// <summary>
        /// 视频
        /// </summary>
        [Description("视频")]
        Video = 30,
    }
}
