﻿using APP.Core.Utilities;

public static class WechatSetting
{
    public static readonly string CallbackPath = "/wechat/callback?param=";

    public static readonly string CallbackUrl = AppSetting.WebsiteHost + CallbackPath;
    public static readonly string AppId = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_WECHATSETTINGS_SECTION, CommonSetting.APPSETTINGS_WECHATSETTINGS_APPID);
    public static readonly string AppSecret = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_WECHATSETTINGS_SECTION, CommonSetting.APPSETTINGS_WECHATSETTINGS_APPSECRET);
    public static readonly string Token = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_WECHATSETTINGS_SECTION, CommonSetting.APPSETTINGS_WECHATSETTINGS_TOKEN);
    public static readonly string EncodingAESKey = ConfigurationUtility.GetSectionString(CommonSetting.APPSETTINGS_WECHATSETTINGS_SECTION, CommonSetting.APPSETTINGS_WECHATSETTINGS_ENCODINGAESKEY);
}
