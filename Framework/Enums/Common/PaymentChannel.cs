﻿using System.ComponentModel;

namespace Framework.Enums
{
    public enum PaymentChannel
    {
        #region 系统

        /// <summary>
        /// 余额支付
        /// </summary>
        [Description("余额支付")]
        Balance = 0,

        #endregion

        #region 支付宝

        /// <summary>
        /// 支付宝支付
        /// </summary>
        [Description("支付宝支付")]
        Alipay = 10,

        /// <summary>
        /// 支付宝免密支付
        /// </summary>
        [Description("支付宝免密支付")]
        AlipayNonPassword = 11,

        /// <summary>
        /// 支付宝wap支付
        /// </summary>
        [Description("支付宝wap支付")]
        AlipayWap = 12,

        /// <summary>
        /// 支付宝APP支付
        /// </summary>
        [Description("支付宝APP支付")]
        AlipayAPP = 13,

        #endregion

        #region 微信

        /// <summary>
        /// 微信支付
        /// </summary>
        [Description("微信支付")]
        Wxpay = 20,

        /// <summary>
        /// 微信免密支付
        /// </summary>
        [Description("微信免密支付")]
        WxpayPasswordFree = 21,

        /// <summary>
        /// 微信wap支付
        /// </summary>
        [Description("微信wap支付")]
        WxpayWap = 22,

        /// <summary>
        /// 微信APP支付
        /// </summary>
        [Description("微信APP支付")]
        WxpayAPP = 23,

        #endregion

        #region 银行

        /// <summary>
        /// 中国农业银行
        /// </summary>
        [Description("中国农业银行")]
        ABOC = 100,

        /// <summary>
        /// 中国银行
        /// </summary>
        [Description("中国银行")]
        BC = 101,

        /// <summary>
        /// 渤海银行
        /// </summary>
        [Description("渤海银行")]
        BHB = 102,

        /// <summary>
        /// 北京银行
        /// </summary>
        [Description("北京银行")]
        BJB = 103,

        /// <summary>
        /// 交通银行
        /// </summary>
        [Description("交通银行")]
        BOCM = 104,

        /// <summary>
        /// 北京农商银行
        /// </summary>
        [Description("北京农商银行")]
        BRCB = 105,

        /// <summary>
        /// 中国建设银行
        /// </summary>
        [Description("中国建设银行")]
        CCB = 106,

        /// <summary>
        /// 中信银行
        /// </summary>
        [Description("中信银行")]
        CCIB = 107,

        /// <summary>
        /// 成都银行
        /// </summary>
        [Description("成都银行")]
        CDB = 108,

        /// <summary>
        /// 中国光大银行
        /// </summary>
        [Description("中国光大银行")]
        CEB = 109,

        /// <summary>
        /// 兴业银行
        /// </summary>
        [Description("兴业银行")]
        CIB = 110,

        /// <summary>
        /// 招商银行
        /// </summary>
        [Description("招商银行")]
        CMB = 111,

        /// <summary>
        /// 中国民生银行
        /// </summary>
        [Description("中国民生银行")]
        CMBC = 112,

        /// <summary>
        /// 中国邮政储蓄银行
        /// </summary>
        [Description("中国邮政储蓄银行")]
        CPSB = 113,

        /// <summary>
        /// 广发银行
        /// </summary>
        [Description("广发银行")]
        GDB = 114,

        /// <summary>
        /// 华夏银行
        /// </summary>
        [Description("华夏银行")]
        HB = 115,

        /// <summary>
        /// 汉口银行
        /// </summary>
        [Description("汉口银行")]
        HKB = 116,

        /// <summary>
        /// 杭州银行
        /// </summary>
        [Description("杭州银行")]
        HZB = 117,

        /// <summary>
        /// 中国工商银行
        /// </summary>
        [Description("中国工商银行")]
        ICBC = 118,

        /// <summary>
        /// 金华银行
        /// </summary>
        [Description("金华银行")]
        JHB = 119,

        /// <summary>
        /// 宁波银行
        /// </summary>
        [Description("宁波银行")]
        NBB = 120,

        /// <summary>
        /// 南京银行
        /// </summary>
        [Description("南京银行")]
        NJB = 121,

        /// <summary>
        /// 平安银行
        /// </summary>
        [Description("平安银行")]
        PAB = 122,

        /// <summary>
        /// 恒丰银行
        /// </summary>
        [Description("恒丰银行")]
        PB = 123,

        /// <summary>
        /// 青岛银行
        /// </summary>
        [Description("青岛银行")]
        QDB = 124,

        /// <summary>
        /// 上海银行
        /// </summary>
        [Description("上海银行")]
        SHB = 125,

        /// <summary>
        /// 上海浦东发展银行
        /// </summary>
        [Description("上海浦东发展银行")]
        SPDB = 126,

        /// <summary>
        /// 上海农村商业银行
        /// </summary>
        [Description("上海农村商业银行")]
        SRCB = 127,

        /// <summary>
        /// 温州银行
        /// </summary>
        [Description("温州银行")]
        WZB = 128

        #endregion
    }
}
