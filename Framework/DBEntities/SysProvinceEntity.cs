﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Attributes;
using APP.Core.Extensions.Nhibernate;
using Newtonsoft.Json;
using Framework.Enums;
using NHibernate.Mapping.Attributes;
using NHibernate.Type;
using System;
using System.ComponentModel;

namespace Framework.DBEntity
{
    /// <summary>
    /// 省份
    /// </summary>
    [Class(0, Table = "sys_province")]
    [Cache(1, Usage = CacheUsage.ReadWrite)]
    public class SysProvinceEntity
    {
        public const string TABLE_NAME = "sys_province";
        public const string COL_PROVINCEID = "ProvinceId";
        public const string COL_COUNTRYID = "CountryId";
        public const string COL_CODE = "Code";
        public const string COL_NAMEEN = "NameEN";
        public const string COL_NAMECN = "NameCN";
        public const string COL_SHORTNAMECN = "ShortNameCN";
        public const string COL_CREATEDTIME = "CreatedTime";
        public const string COL_MODIFIEDTIME = "ModifiedTime";

        /// <summary>
        /// 省份Id
        /// </summary>
        [Description("省份Id")]
        [Id(0, Name = "ProvinceId", Column = "ProvinceId")]
        [Key(1)]
        [Generator(2, Class = "native")]
        public virtual int? ProvinceId { get; set; }

        /// <summary>
        /// 国家Id
        /// </summary>
        [Description("国家Id")]
        [Property(Column = "CountryId", NotNull = true)]
        public virtual int CountryId { get; set; }

        private SysCountryEntity _country = null;
        /// <summary>
        /// 外键 CountryId 对应 sys_country 类实例;
        /// </summary>
        [IgnoreConvertTypeScalar]
        [JsonIgnore]
        public virtual SysCountryEntity Country { get { return _country ?? (_country = NHibernateExtension.GetEntityById<SysCountryEntity>(CountryId)); } }

        /// <summary>
        /// 编码
        /// </summary>
        [Description("编码")]
        [Property(Column = "Code", NotNull = true, Length = 8)]
        public virtual string Code { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        [Description("英文名")]
        [Property(Column = "NameEN", NotNull = true, Length = 256)]
        public virtual string NameEN { get; set; }

        /// <summary>
        /// 中文名
        /// </summary>
        [Description("中文名")]
        [Property(Column = "NameCN", NotNull = true, Length = 256)]
        public virtual string NameCN { get; set; }

        /// <summary>
        /// 中文简称
        /// </summary>
        [Description("中文简称")]
        [Property(Column = "ShortNameCN", Length = 1)]
        public virtual string ShortNameCN { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Property(Column = "CreatedTime", NotNull = true)]
        public virtual DateTime CreatedTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        [Property(Column = "ModifiedTime", NotNull = true)]
        public virtual DateTime ModifiedTime { get; set; }
    }
}
