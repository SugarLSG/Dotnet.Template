﻿using APP.Core.Utilities;

public static class WebsiteSetting
{
    public static int MenuMaxDeep = ConfigurationUtility.GetInt(CommonSetting.APPSETTINGS_MENU_MAX_DEEP, 2);
}
