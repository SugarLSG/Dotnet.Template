﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Attributes;
using APP.Core.Extensions.Nhibernate;
using Newtonsoft.Json;
using Framework.Enums;
using NHibernate.Mapping.Attributes;
using NHibernate.Type;
using System;
using System.ComponentModel;

namespace Framework.DBEntity
{
    /// <summary>
    /// 用户
    /// </summary>
    [Class(0, Table = "sys_user")]
    [Cache(1, Usage = CacheUsage.ReadWrite)]
    public class SysUserEntity
    {
        public const string TABLE_NAME = "sys_user";
        public const string COL_USERID = "UserId";
        public const string COL_ACCOUNT = "Account";
        public const string COL_PASSWORD = "Password";
        public const string COL_UPDATEDPASSWORD = "UpdatedPassword";
        public const string COL_TYPE = "Type";
        public const string COL_NAME = "Name";
        public const string COL_SEX = "Sex";
        public const string COL_HEADIMAGEID = "HeadImageId";
        public const string COL_MAIL = "Mail";
        public const string COL_PHONENUMBER = "PhoneNumber";
        public const string COL_WECHAT = "Wechat";
        public const string COL_QQ = "QQ";
        public const string COL_APPID = "AppId";
        public const string COL_APPSECRET = "AppSecret";
        public const string COL_STATUS = "Status";
        public const string COL_CREATEDBY = "CreatedBy";
        public const string COL_CREATEDTIME = "CreatedTime";
        public const string COL_MODIFIEDBY = "ModifiedBy";
        public const string COL_MODIFIEDTIME = "ModifiedTime";

        /// <summary>
        /// 用户Id
        /// </summary>
        [Description("用户Id")]
        [Id(0, Name = "UserId", Column = "UserId")]
        [Key(1)]
        [Generator(2, Class = "native")]
        public virtual int? UserId { get; set; }

        /// <summary>
        /// 用户账号
        /// </summary>
        [Description("用户账号")]
        [Property(Column = "Account", NotNull = true, Length = 128)]
        public virtual string Account { get; set; }

        /// <summary>
        /// 用户密码
        /// </summary>
        [Description("用户密码")]
        [Property(Column = "Password", NotNull = true, Length = 128)]
        public virtual string Password { get; set; }

        /// <summary>
        /// 已更新用户密码
        /// </summary>
        [Description("已更新用户密码")]
        [Property(Column = "UpdatedPassword", NotNull = true)]
        public virtual bool UpdatedPassword { get; set; }

        /// <summary>
        /// 用户类型
        /// ENUM[int,UserType]
        /// </summary>
        [Description("用户类型")]
        [Property(Column = "Type", NotNull = true)]
        public virtual UserType Type { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        [Description("用户名称")]
        [Property(Column = "Name", NotNull = true, Length = 128)]
        public virtual string Name { get; set; }

        /// <summary>
        /// 用户性别
        /// ENUM[int,UserSex]
        /// </summary>
        [Description("用户性别")]
        [Property(Column = "Sex", NotNull = true)]
        public virtual UserSex Sex { get; set; }

        /// <summary>
        /// 头像附件Id
        /// </summary>
        [Description("头像附件Id")]
        [Property(Column = "HeadImageId")]
        public virtual int? HeadImageId { get; set; }

        private SysAttachmentEntity _headImage = null;
        /// <summary>
        /// 外键 HeadImageId 对应 sys_attachment 类实例;
        /// </summary>
        [IgnoreConvertTypeScalar]
        [JsonIgnore]
        public virtual SysAttachmentEntity HeadImage { get { return _headImage ?? (_headImage = NHibernateExtension.GetEntityById<SysAttachmentEntity>(HeadImageId)); } }

        /// <summary>
        /// 邮箱地址
        /// </summary>
        [Description("邮箱地址")]
        [Property(Column = "Mail", Length = 64)]
        public virtual string Mail { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        [Description("联系电话")]
        [Property(Column = "PhoneNumber", Length = 32)]
        public virtual string PhoneNumber { get; set; }

        /// <summary>
        /// 微信号码
        /// </summary>
        [Description("微信号码")]
        [Property(Column = "Wechat", Length = 32)]
        public virtual string Wechat { get; set; }

        /// <summary>
        /// QQ号码
        /// </summary>
        [Description("QQ号码")]
        [Property(Column = "QQ", Length = 32)]
        public virtual string QQ { get; set; }

        /// <summary>
        /// 凭证Id
        /// </summary>
        [Description("凭证Id")]
        [Property(Column = "AppId", Length = 32)]
        public virtual string AppId { get; set; }

        /// <summary>
        /// 凭证秘钥
        /// </summary>
        [Description("凭证秘钥")]
        [Property(Column = "AppSecret", Length = 32)]
        public virtual string AppSecret { get; set; }

        /// <summary>
        /// 状态
        /// ENUM[int]
        /// -1-Disabled-已禁用
        /// 0-Inactived-未激活
        /// 1-Enabled-已启用
        /// </summary>
        [Description("状态")]
        [Property(Column = "Status", NotNull = true)]
        public virtual SysUserStatus Status { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        [Description("创建者")]
        [Property(Column = "CreatedBy", NotNull = true)]
        public virtual int CreatedBy { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Property(Column = "CreatedTime", NotNull = true)]
        public virtual DateTime CreatedTime { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        [Description("修改者")]
        [Property(Column = "ModifiedBy", NotNull = true)]
        public virtual int ModifiedBy { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        [Property(Column = "ModifiedTime", NotNull = true)]
        public virtual DateTime ModifiedTime { get; set; }
    }
}
