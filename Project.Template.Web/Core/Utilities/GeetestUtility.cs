﻿using APP.Core.Utilities;
using Project.Template.Web.Models.Geetest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;

public static class GeetestUtility
{
    private static readonly string KEY_GEETEST_SECTION = "geetestSettings";
    private static readonly string Host = ConfigurationUtility.GetSectionString(KEY_GEETEST_SECTION, "host");
    private static readonly string Id = ConfigurationUtility.GetSectionString(KEY_GEETEST_SECTION, "id");
    private static readonly string Key = ConfigurationUtility.GetSectionString(KEY_GEETEST_SECTION, "key");

    private const string RegisterPath = "/register.php";
    private const string ValidatePath = "/validate.php";


    public static object Preprocess(string userId = null)
    {
        var response = HttpRequestUtility.Get(string.Format("{0}{1}?gt={2}&user_id={3}&client_type=web", Host, RegisterPath, Id, userId));
        if (response.HasValue() && response.Length == 32)
        {
            // 请求成功
            var challenge = SecurityUtility.MD5Encrypt(response + Key).ToLower();
            HttpContext.Current.Session["CalculateChallenge"] = challenge;

            return new
            {
                success = true,
                gt = Id,
                challenge = challenge,
                new_captcha = true
            };
        }
        else
        {
            // 请求失败
            var challenge = SecurityUtility.MD5Encrypt(Guid.NewGuid().ToString("N")).ToLower();
            HttpContext.Current.Session["CalculateChallenge"] = challenge;

            return new
            {
                success = false,
                gt = Id,
                challenge = challenge,
                new_captcha = true
            };
        }
    }

    public static bool Validate(GeetestValidateInfo info)
    {
        if (info.Challenge.HasValue() && info.Validate.HasValue() && info.Seccode.HasValue())
        {
            var challenge = HttpContext.Current.Session["CalculateChallenge"];
            if (challenge != null && info.Challenge.StartsWith(challenge.ToString()))
            {
                // 线上验证
                if (info.Online)
                {
                    if (SecurityUtility.MD5Encrypt(Key + "geetest" + info.Challenge).ToLower() == info.Validate)
                    {
                        // 同步结果
                        Task.Run(() =>
                        {
                            HttpRequestUtility.PostURL(Host + ValidatePath, new Dictionary<string, object>
                            {
                                { "seccode", info.Seccode },
                                { "sdk", "csharp_3.2.0" }
                            });
                        });

                        return true;
                    }
                }
                // 线下验证
                else return SecurityUtility.MD5Encrypt(info.Challenge).ToLower() == info.Validate;
            }
        }

        return false;
    }
}