﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("无效的凭证令牌")]
    public sealed class InvalidAppTokenException : APPBaseException
    {
        /// <summary>
        /// message: (1002) 无效的凭证令牌
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public InvalidAppTokenException(params object[] args)
        {
            Code = "1002";
            Args = args;
        }
    }
}
