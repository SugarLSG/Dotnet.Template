﻿namespace Framework.Models.Auth
{
    public class UserAPIAuthorizedInfo
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 凭证Id
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 凭证秘钥
        /// </summary>
        public string AppSecret { get; set; }
    }
}
