﻿using APP.Core.Models;
using Framework.Enums;
using Framework.Role;
using System.Collections.Generic;

namespace Framework.Models.Auth
{
    public class UserAuthorizedInfo : AuthorizedInfo
    {
        /// <summary>
        /// 是否需要更新密码
        /// </summary>
        public bool NeedUpdatePassword { get; set; }

        /// <summary>
        /// 用户性别
        /// </summary>
        public UserSex Sex { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public UserType Type { get; set; }

        /// <summary>
        /// 角色数据
        /// </summary>
        public IEnumerable<RoleCode> RoleList { get; set; }
    }
}
