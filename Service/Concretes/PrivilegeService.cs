﻿using APP.Core.Repositories;
using APP.Core.Utilities;
using DAO.Abstracts;
using Framework.DBEntity;
using Framework.Enums;
using Framework.Privilege;
using Framework.Role;
using Service.Abstracts;
using System.Collections.Generic;
using System.Linq;

namespace Service.Concretes
{
    public class PrivilegeService : IPrivilegeService
    {
        private IBaseRepository BaseRepository { get; set; }
        private IPrivilegeRepository PrivilegeRepository { get; set; }


        public void Sync()
        {
            // 所有权限Code
            var newPrivilegeCodes = EnumExtension.GetEnumList<PrivilegeCode>();

            // 清除已删除的权限
            var currentPrivilegeCodes = PrivilegeRepository.GetAllCodes();
            foreach (var code in currentPrivilegeCodes.Where(cpc => !newPrivilegeCodes.Any(npc => npc.ToString() == cpc)))
                PrivilegeRepository.Delete(code);

            // 更新、添加新权限
            var currentPrivileges = BaseRepository.GetEntityList<SysPrivilegeEntity>();
            foreach (var privilege in currentPrivileges)
            {
                if (privilege.Name != privilege.PrivilegeCode.Value.ToMessage())
                {
                    privilege.Name = privilege.PrivilegeCode.Value.ToMessage();
                    privilege.Description = privilege.Name;

                    BaseRepository.UpdateEntity(privilege);
                }
            }
            foreach (var code in newPrivilegeCodes.Where(npc => !currentPrivileges.Any(cp => npc == cp.PrivilegeCode)))
            {
                BaseRepository.AddEntity(new SysPrivilegeEntity
                {
                    PrivilegeCode = code,
                    Name = code.ToMessage(),
                    Description = code.ToMessage(),
                    Status = SysPrivilegeStatus.Enabled,
                });

                // 自动绑定系统管理员角色
                BaseRepository.AddEntity(new SysRolePrivilegeEntity
                {
                    RoleCode = RoleCode.XiTongGuanLiYuan_173CD1D08E15F40C,
                    PrivilegeCode = code,
                });
            }
        }


        public IEnumerable<PrivilegeCode> GetUserPrivileges(int userId, bool getCache = true)
        {
            return CacheUtility.GetOrSetFromCache(
                string.Format(CommonSetting.CACHEKEY_USER_PRIVILEGE_FORMAT, userId),
                () => PrivilegeRepository.GetUserPrivilegeCodes(userId),
                getCache
            );
        }

        public bool HasPrivilege(int userId, PrivilegeCode privilege)
        {
            return GetUserPrivileges(userId).Contains(privilege);
        }
    }
}
