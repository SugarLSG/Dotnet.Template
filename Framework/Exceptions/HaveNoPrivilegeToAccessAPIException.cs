﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("无权限访问该接口")]
    public sealed class HaveNoPrivilegeToAccessAPIException : APPBaseException
    {
        /// <summary>
        /// message: (0001) 无权限访问该接口
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public HaveNoPrivilegeToAccessAPIException(params object[] args)
        {
            Code = "0001";
            Args = args;
        }
    }
}
