﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("无权限访问该页面")]
    public sealed class HaveNoPrivilegeToAccessPageException : APPBaseException
    {
        /// <summary>
        /// message: (0002) 无权限访问该页面
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public HaveNoPrivilegeToAccessPageException(params object[] args)
        {
            Code = "0002";
            Args = args;
        }
    }
}
