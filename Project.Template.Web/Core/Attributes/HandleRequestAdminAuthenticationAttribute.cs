﻿using APP.Core.Attributes;
using APP.Core.Utilities;
using Framework.Exceptions;
using System.Web;

namespace Project.Template.Web.Core.Attributes
{
    public class HandleRequestAdminAuthenticationAttribute : HandleRequestAuthenticationAttribute
    {
        /// <summary>
        /// （请求）Admin 功能授权验证（Order: 10）
        /// </summary>
        /// <param name="filterMode">筛选模式</param>
        /// <param name="patterns">筛选规则</param>
        public HandleRequestAdminAuthenticationAttribute(FilterMode filterMode = FilterMode.Unset, params string[] patterns) : base(filterMode, patterns)
        {
        }

        protected override bool IsAuthenticated(HttpContextBase httpContext)
        {
            if (AuthenticationUtility.IsAuthenticated())
            {
                // 获取当前请求路径权限Code
                var privilege = WebsiteUtility.CurrentPrivilege;
                if (privilege.HasValue && AuthenticationUtility.HasPrivilege(privilege.Value))
                    return true;

                // 无请求权限
                // 判断是否 Ajax & Post 请求
                if (ClientRequestUtility.IsAjaxRequest && ClientRequestUtility.IsPostRequest)
                    throw new HaveNoPrivilegeToAccessAPIException();
                else
                    throw new HaveNoPrivilegeToAccessPageException();
            }

            return false;
        }
    }
}
