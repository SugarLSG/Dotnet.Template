﻿using Microsoft.AspNet.SignalR;
using System;
using System.Threading.Tasks;

namespace Project.Template.Web.App_Start.SignalR
{
    public class TestHub : Hub
    {
        private static IHubContext _hubContext = GlobalHost.ConnectionManager.GetHubContext<TestHub>();


        public override Task OnConnected()
        {
            return Task.Run(() =>
            {
                var group = Context.RequestCookies["AUTH.GROUP"].Value;
                Groups.Add(Context.ConnectionId, group);

                base.OnConnected();
            });
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            return Task.Run(() =>
            {
                var group = Context.RequestCookies["AUTH.GROUP"].Value;
                Groups.Remove(Context.ConnectionId, group);

                base.OnDisconnected(stopCalled);
            });
        }

        public override Task OnReconnected()
        {
            return Task.Run(() =>
            {
                var group = Context.RequestCookies["AUTH.GROUP"].Value;
                Groups.Add(Context.ConnectionId, group);

                base.OnReconnected();
            });
        }


        public Task Send(string name, string message, bool isAll)
        {
            return Task.Run(() =>
            {
                if (isAll)
                    Clients.All.Talk(string.Format("{0}: {1} ({2})", name, message, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                else
                {
                    var group = Context.RequestCookies["AUTH.GROUP"].Value;
                    Clients.Group(group).Talk(string.Format("{0}: {1} ({2})", name, message, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                }
            });
        }

        public static Task SayHi(string group)
        {
            return Task.Run(() =>
            {
                if (group.HasValue())
                    _hubContext.Clients.Group(group).SayHi("Hi everybody.");
                else
                    _hubContext.Clients.All.SayHi("Hi everybody.");
            });
        }
    }
}