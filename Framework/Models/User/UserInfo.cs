﻿using Framework.Enums;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Framework.Models.User
{
    public class UserInfo
    {
        /// <summary>
        /// 账号
        /// </summary>
        [DisplayName("账号")]
        public string Account { get; set; }


        /// <summary>
        /// 是否需要更新密码
        /// </summary>
        public bool NeedUpdatePassword { get; set; }

        /// <summary>
        /// 是否更新密码
        /// </summary>
        public bool IsUpdatePassword { get; set; }

        /// <summary>
        /// 旧密码
        /// </summary>
        [DisplayName("旧密码")]
        [RequiredIfEquals("IsUpdatePassword", true)]
        [Password(".*")]
        public string OldPassword { get; set; }

        /// <summary>
        /// 新密码
        /// </summary>
        [DisplayName("新密码")]
        [RequiredIfEquals("IsUpdatePassword", true)]
        [Password]
        public string NewPassword { get; set; }


        /// <summary>
        /// 姓名
        /// </summary>
        [DisplayName("姓名")]
        [Required]
        [TextLength(32)]
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [DisplayName("性别")]
        public UserSex Sex { get; set; }


        /// <summary>
        /// 联系电话
        /// </summary>
        [DisplayName("联系电话")]
        [RegularExpression(CoreConfig.RegexTelMobile)]
        [TextLength(32)]
        public string Phone { get; set; }

        /// <summary>
        /// 邮箱地址
        /// </summary>
        [DisplayName("邮箱地址")]
        [RegularExpression(CoreConfig.RegexEmail)]
        [TextLength(64)]
        public string Email { get; set; }

        /// <summary>
        /// 微信号
        /// </summary>
        [DisplayName("微信号")]
        [RegularExpression(CoreConfig.RegexWechat4_32)]
        [TextLength(32)]
        public string Wechat { get; set; }

        /// <summary>
        /// QQ号
        /// </summary>
        [DisplayName("QQ号码")]
        [RegularExpression(CoreConfig.RegexQQ4_16)]
        [TextLength(32)]
        public string QQ { get; set; }
    }
}
