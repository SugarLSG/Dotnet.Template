﻿/* ajax */
ajaxBeforeSend = function () {
    $('#loadingLayer').show();
};

ajaxComplete = function () {
    setTimeout(function () {
        $('#loadingLayer').animate({ opacity: 0 }, 300, function () {
            $(this).css({ opacity: '' }).hide();
        });
    }, 300);
};

var handleAjaxResult = function (response, successCallBack, failureCallBack) {
    if (response.result) {
        toastr.options.positionClass = 'toast-top-right';
        toastr.options.showDuration = 300;
        toastr.options.hideDuration = 300;
        toastr.options.timeOut = 2500;
        toastr['success'](response.message);

        setTimeout(function () {
            successCallBack && successCallBack();
        }, 1500);
    } else {
        alert(response.message, failureCallBack);
    }
};


/*
 callback: 
 opts:
   opts.size: [large, middle, small(default)]
 */
alert = function (message, callback, opts) {
    var $modal = initModal(
        '提示',
        '<div>' + message + '</div>',
        {
            size: opts && opts.size ? opts.size : 'small',
            btns: [
                '<button type="button" class="btn btn-primary" data-dismiss="modal" data-btntype="ok">确定</button>'
            ]
        }
    );
    $modal.modal({
        backdrop: 'static',
        show: true
    }).on('shown.bs.modal', function (e) {
        $.appViewUtility.disbaledScroll();
    }).on('hidden.bs.modal', function (e) {
        $.appViewUtility.enabledScroll();
        callback && callback();
    });
    return $modal;
};

/*
 callback: Boolean result
 opts:
   opts.size: [large, middle, small(default)]
 */
confirm = function (message, callback, opts) {
    var $modal = initModal(
        '请选择',
        '<div>' + message + '</div>',
        {
            size: opts && opts.size ? opts.size : 'small',
            btns: [
                '<button type="button" class="btn btn-primary" data-dismiss="modal" data-btntype="ok" onclick="this.dataset.marked=true">确定</button>',
                '<button type="button" class="btn btn-default" data-dismiss="modal" data-btntype="cancel" onclick="this.dataset.marked=true">取消</button>'
            ]
        }
    );
    $modal.modal({
        backdrop: 'static',
        show: true
    }).on('shown.bs.modal', function (e) {
        $.appViewUtility.disbaledScroll();
    }).on('hidden.bs.modal', function (e) {
        $.appViewUtility.enabledScroll();
        if (callback) {
            var $btn = $modal.find('button[data-marked=true]');
            if (!$btn || !$btn.length || $btn.data('btntype') === 'cancel') {
                callback(false);
            } else if ($btn.data('btntype') === 'ok') {
                callback(true);
            }
        }
    });
    return $modal;
};

/*
 opts:
   opts.size: [large, middle(default), small]
 loadedCallBack: Boolean result, Object modalObject
 closedCallBack: Boolean result, Object modalObject
 */
var loadUrl = function (title, url, opts, loadedCallBack, closedCallBack) {
    var $modal;
    $.appAjax({
        url: url,
        type: 'GET',
        dataType: 'text',
        onSuccess: function (responseData, successString, response) {
            $modal = initModal(
                title,
                responseData,
                {
                    size: opts && opts.size ? opts.size : 'middle',
                    close: true
                }
            );
            $modal.appBindValidator();
            $modal.appBindView();
            $modal.modal({
                backdrop: 'static',
                show: true
            }).on('shown.bs.modal', function (e) {
                $.appViewUtility.disbaledScroll();
                loadedCallBack && loadedCallBack(true, $modal);
            }).on('hidden.bs.modal', function (e) {
                $.appViewUtility.enabledScroll();
                closedCallBack && closedCallBack(true, $modal);
            });
        },
        onError: function (response, errorString, statusText) {
            var $modal = initModal(
                title,
                '<p>' + errorString + ': ' + statusText + '</p>',
                {
                    size: opts && opts.size ? opts.size : 'middle',
                    close: true
                }
            );
            $modal.modal({
                backdrop: 'static',
                show: true
            }).on('shown.bs.modal', function (e) {
                $.appViewUtility.disbaledScroll();
                loadedCallBack && loadedCallBack(false, $modal);
            }).on('hidden.bs.modal', function (e) {
                $.appViewUtility.enabledScroll();
                closedCallBack && closedCallBack(false, $modal);
            });
        }
    });

    return $modal;
};

/*
 title: 标题
 content: 内容，支持 html
 opts:
   opts.size: [large, middle(default), small]
   opts.close: [true(default), false]
 loadedCallBack: Boolean result, Object modalObject
 closedCallBack: Boolean result, Object modalObject
 */
var showModal = function (title, content, opts, loadedCallBack, closedCallBack) {
    var $modal = initModal(
        title,
        '<div>' + content + '</div>',
        {
            size: opts && opts.size ? opts.size : 'middle',
            close: opts && opts.close != undefined ? opts.close : true,
        }
    );
    $modal.appBindValidator();
    $modal.appBindView();
    $modal.modal({
        backdrop: 'static',
        show: true
    }).on('shown.bs.modal', function (e) {
        $.appViewUtility.disbaledScroll();
        loadedCallBack && loadedCallBack(true, $modal);
    }).on('hidden.bs.modal', function (e) {
        $.appViewUtility.enabledScroll();
        closedCallBack && closedCallBack(true, $modal);
    });

    return $modal;
};

/*
 初始化模态框，用于各种弹出框
 title: 标题
 content: 内容，支持 html
 opts:
   opts.size: [large, middle(default), small]
   opts.close: [true, false(default)]
   opts.btns: [btn1, btn2...]
 */
var initModal = function (title, content, opts) {
    var sizeClass = undefined,
        btns = undefined;
    if (opts) {
        if (opts.size === 'large') {
            sizeClass = ' modal-lg';
        } else if (opts.size === 'middle') {
            sizeClass = '';
        } else if (opts.size === 'small') {
            sizeClass = ' modal-sm';
        } else {
            sizeClass = '';
        }

        if (opts.btns) {
            btns = opts.btns.join('');
        }
    }

    var hasTitle = title !== undefined && title !== null && title !== '',
        title = hasTitle ? title : '&nbsp;',
        hasColse = opts && (opts.close === true || opts.close === 'true');

    return $(
'<div class="modal fade">' +
    '<div class="modal-dialog ' + sizeClass + '">' +
        '<div class="modal-content">' +
            (hasTitle || hasColse ?
            '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal">' +
                    '<span>&times;</span>' +
                '</button>' +
                '<h4 class="modal-title">' + title + '</h4>' +
            '</div>' : ''
            ) +
            '<div class="modal-body">' + content + '</div>' +
            (btns ? '<div class="modal-footer">' + btns + '</div>' : '') +
        '</div>' +
    '</div>' +
'</div>');
};


/* privilege */
var hasPrivilege = function (code) {
    return PRIVILEGELIST.contains(code);
};


/* select2 */
var convertSelect2Items = function (responseData, idName, textName) {
    var result = [];
    if (responseData.data) {
        $.each(responseData.data, function (i, v) {
            result.push({ id: v[idName ? idName : 'id'].toString(), text: v[textName ? textName : 'name'] });
        });
    }

    return result;
};


$.extend({
    appSubview: function (title, subTitle, content) {
        var $subview = $(
'<div class="subview-layer" style="display: none;">' +
    '<div class="subview-header">' +
        '<h1>' +
            title +
            '<small>' + subTitle + '</small>' +
        '</h1>' +
    '</div>' +
    '<div class="subview-content">' +
    '</div>' +
'</div>'),
            $close = $('<a href="javascript:void(0);" class="subview-close"><span class="fa fa-times"></span>关闭</a>').on({
                click: function () {
                    $subview.slideUp(200, function () { $subview.remove(); });
                }
            });
        $subview.find('.subview-header').append($close);

        $('.main-content').append($subview);
        $subview.slideDown(200);
    }
});


$.fn.extend({
    appLoading: function () {
        var $this = $(this);
        var $formLoading = $('<div class="form-loading"></div>');
        for (var i = 0; i < 8; ++i) {
            $formLoading.append('<div></div>');
        }
        $this.addClass('loading-layer').append($formLoading);

        return $this;
    },

    appBindView: function () {
        var $view = $(this);

        // 表单禁用
        $view.find('form[disabled]').each(function (i, v) {
            var $form = $(v);

            $form.find('button,input[type="button"],input[type="submit"]').remove();
            $form.find('input[type="hidden"]').remove();
            $form.find('.field-validation-valid').remove();
            $form.find('.input-group-btn').remove();

            $form.find('input[type="radio"],input[type="checkbox"]').each(function () {
                var $this = $(this);
                var name = $this.attr('name');
                if (name.indexOf('[') >= 0) {
                    name = name.substr(0, name.indexOf('['));
                }
                var $items = $form.find('[name^="' + name + '"]');
                if ($items && $items.length) {
                    var values = [];
                    $items.each(function () {
                        var $this = $(this);
                        if ($this.is(':checked')) {
                            values.push($this.next().html());
                        }
                    });
                    $('<div class="form-disabled">' + (values.length ? values.join() : '-') + '</div>').insertAfter($this.parent());
                }
                $items.parent().remove();
            });
            $form.find('input,textarea').each(function () {
                var $this = $(this);
                var value = $this.val();
                $('<div class="form-disabled">' + (value === undefined || value === '' ? '-' : value) + '</div>').insertAfter($this);
                $this.remove();
            });
            $form.find('select').each(function () {
                var $this = $(this);
                var value = $this.find('option[selected]').html();
                $('<div class="form-disabled">' + (value === undefined || value === '' ? '-' : value) + '</div>').insertAfter($this);
                $this.remove();
            });
        });

        /* Remote 验证 */
        $view.find('form *[data-val-remote]').each(function (i, v) {
            if ($(v).val() !== '') {
                $.validator && $(v).valid();
            }
        });

        /* 下拉框 */
        $view.find('select[data-select2!="custom"]').on({
            change: function () { $.validator && $(this).valid(); }
        }).appSelect2();

        /* 日期时间 */
        $view.find('input[type="date"]').attr({ type: 'text', 'data-toggle': 'date' });
        $view.find('input[type="datetime"]').attr({ type: 'text', 'data-toggle': 'datetime' });
        $view.find('input[data-toggle^="date"]').each(function (i, v) {
            var $datetime = $(v),
                close = $datetime.data('close');
            $datetime.appDateTimePicker({
                format: $datetime.data('format'),
                min: $datetime.data('min'),
                max: $datetime.data('max'),
                start: $datetime.data('start'),
                startName: $datetime.data('startname'),
                end: $datetime.data('end'),
                endName: $datetime.data('endname'),
                close: close === undefined ? false : close.toString().toUpperCase() === 'TRUE'
            });
        });

        /* 提示 */
        $view.find('[data-toggle="tooltip"]').tooltip();
        $view.find('[data-toggle="popover"]').popover();

        return $view;
    }
});


$(function () {
    Main.init();

    /* 前端判断是否授权已过期（1分钟判断1次） */
    try {
        var authExpiration = parseDatetime(cookie.get('AUTH.EXPIRED').base64Decrypt());
        var expiredInterval = setInterval(function () {
            if (new Date() > authExpiration) {
                clearInterval(expiredInterval);
                alert('登录授权已过期，请重新登录。', function () {
                    location.href = '/auth/index?returnurl=' + location.pathname;
                });
            }
        }, 60 * 1000);
    } catch (e) {
        console.log(e);
    }

    /* Loading */
    $('#loadingLayer').appLoading();

    /* Data Grid */
    if (document.body.clientWidth <= 767) {
        $('.datagrid-search').each(function (i, v) {
            var $form = $(v),
                $children = $form.children().remove(),
                $datalist = $form.next();

            var $conditions = $('<div id="datagrid-conditions" class="collapse"></div>').append($children);
            $conditions.on({
                'show.bs.collapse': function () {
                    $showAction.animate({ opacity: 0, height: 0 }, 100, function () { $showAction.hide().css({ height: '' }); });
                },
                'hide.bs.collapse': function () {
                    $showAction.show(150).animate({ opacity: 1 }, 100);
                }
            });

            var $showAction = $(
'<div class="form-group mb0">' +
'    <div class="col-xs-24 text-right">' +
'        <a href="javascript:void(0);">' +
'            展开搜索条件' +
'            <span class="fa fa-angle-double-down"></span>' +
'        </a>' +
'    </div>' +
'</div>'
            );
            $showAction.find('a').on({
                click: function () {
                    $conditions.collapse('show');
                }
            });

            var $hideAction = $(
'<div class="pull-left mt5">' +
'    <a href="javascript:void(0);">' +
'        收起搜索条件' +
'        <span class="fa fa-angle-double-up"></span>' +
'    </a>' +
'</div>'
            );
            $hideAction.find('a').on({
                click: function () {
                    $conditions.collapse('hide');
                }
            });
            $conditions.find('.datalist-action').prepend($hideAction);

            var slide = $datalist.data('slide');
            if (slide === undefined || slide.toString().toUpperCase() !== 'FALSE') {
                var $slideAction = $(
'<div class="form-group mt10">' +
'    <div class="col-xs-12">' +
'        <a id="linkSlidLeft" href="javascript:void(0);" class="disabled">' +
'            <span class="fa fa-chevron-left"></span>' +
'            左划' +
'        </a>' +
'    </div>' +
'    <div class="col-xs-12 text-right">' +
'        <a id="linkSlidRight" href="javascript:void(0);">' +
'            右划' +
'            <span class="fa fa-chevron-right"></span>' +
'        </a>' +
'    </div>' +
'</div>'
                );
                var $linkSlidLeft = $slideAction.find('#linkSlidLeft').on({
                    click: function () {
                        var $this = $(this);
                        if (!$this.hasClass('disabled')) {
                            var $datalistBody = $datalist.find('.datalist-body'),
                                bodyWidth = $datalistBody.width(),
                                $datagrid = $datalistBody.find('table#datagrid'),
                                datagridWidth = $datagrid.width() + 2,
                                left = $datalistBody.scrollLeft(),
                                maxScrollLeft = datagridWidth - bodyWidth,
                                scrollLeft = Math.max(left - bodyWidth, 0);

                            $datalistBody.animate({ scrollLeft: scrollLeft }, 250);
                            scrollLeft === 0 ? $linkSlidLeft.addClass('disabled') : $linkSlidLeft.removeClass('disabled');
                            scrollLeft === maxScrollLeft ? $linkSlidRight.addClass('disabled') : $linkSlidRight.removeClass('disabled');
                        }
                    }
                });
                var $linkSlidRight = $slideAction.find('#linkSlidRight').on({
                    click: function () {
                        var $this = $(this);
                        if (!$this.hasClass('disabled')) {
                            var $datalistBody = $datalist.find('.datalist-body'),
                                bodyWidth = $datalistBody.width(),
                                $datagrid = $datalistBody.find('table#datagrid'),
                                datagridWidth = $datagrid.width() + 2,
                                left = $datalistBody.scrollLeft(),
                                maxScrollLeft = datagridWidth - bodyWidth,
                                scrollLeft = Math.min(left + bodyWidth, maxScrollLeft);

                            $datalistBody.animate({ scrollLeft: scrollLeft }, 250);
                            scrollLeft === 0 ? $linkSlidLeft.addClass('disabled') : $linkSlidLeft.removeClass('disabled');
                            scrollLeft === maxScrollLeft ? $linkSlidRight.addClass('disabled') : $linkSlidRight.removeClass('disabled');
                        }
                    }
                });
            }

            $form.append($conditions).append($showAction).append($slideAction);
        });
    }

    /* 绑定界面元素 */
    $('html').appBindView();

    /* 监听关闭事件 */
    $(window).on({ beforeunload: function () { $.appViewUtility.enabledScroll(); } });
});
