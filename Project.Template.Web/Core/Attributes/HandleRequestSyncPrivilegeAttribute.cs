﻿using APP.Core.Utilities;
using System.Web.Mvc;

namespace Project.Template.Web.Core.Attributes
{
    /// <summary>
    /// 必须放在 Attribute
    /// 无法在 Global 中直接使用 Spring
    /// </summary>
    public class HandleRequestSyncPrivilegeAttribute : ActionFilterAttribute
    {
        private static bool HasSynced = false;


        /// <summary>
        /// （请求）同步权限（Order: 1）
        /// 必须放在 Attribute，无法在 Global 中直接使用 Spring
        /// </summary>
        public HandleRequestSyncPrivilegeAttribute()
        {
            Order = 1;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ThreadSafetyUtility.Handle(
                () => !HasSynced,
                () =>
                {
                    // 初始化权限
                    WebsiteUtility.SyncPrivilege();
                    HasSynced = true;
                }
            );
        }
    }
}