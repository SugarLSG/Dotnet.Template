﻿using APP.Core.Repositories;
using DAO.Abstracts;
using Framework.Enums;
using Framework.Privilege;
using System.Collections.Generic;
using System.Linq;

namespace DAO.Concretes
{
    public class PrivilegeRepository : IPrivilegeRepository
    {
        private IBaseRepository BaseRepository { get; set; }


        public IEnumerable<string> GetAllCodes()
        {
            return BaseRepository.ExecuteList<string>(@"select PrivilegeCode from sys_privilege");
        }

        public void Delete(string code)
        {
            BaseRepository.ExecuteUpdate(string.Format(@"delete from sys_privilege where PrivilegeCode = '{0}'", code));
        }

        public IEnumerable<PrivilegeCode> GetUserPrivilegeCodes(int userId)
        {
            return BaseRepository.ExecuteList<string>(@"
SELECT sp.PrivilegeCode
  FROM sys_user su,
       sys_user_role sur,
       sys_role sr,
       sys_role_privilege srp,
       sys_privilege sp
 WHERE     su.UserId = :userid
       AND su.Status = :userstatus
       AND sur.UserId = su.UserId
       AND sr.RoleCode = sur.RoleCode
       AND sr.Status = :rolestatus
       AND srp.RoleCode = sr.RoleCode
       AND sp.PrivilegeCode = srp.PrivilegeCode
       AND sp.Status = :privilegestatus",
                new Dictionary<string, object>
                {
                    { "userid", userId },
                    { "userstatus", (int)SysUserStatus.Enabled },
                    { "rolestatus", (int)SysRoleStatus.Enabled },
                    { "privilegestatus", (int)SysPrivilegeStatus.Enabled }
                }
            ).Select(c => c.ToEnum<PrivilegeCode>());
        }
    }
}
