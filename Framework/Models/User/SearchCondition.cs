﻿using APP.Core.Models;
using Framework.Enums;
using Framework.Role;
using System.ComponentModel;

namespace Framework.Models.User
{
    public class SearchCondition : PagingCondition
    {
        /// <summary>
        /// 账号
        /// </summary>
        [DisplayName("账号")]
        public string Account { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [DisplayName("名称")]
        public string Name { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [DisplayName("类型")]
        public UserType? Type { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [DisplayName("状态")]
        public SysUserStatus? Status { get; set; }

        /// <summary>
        /// 角色
        /// </summary
        [DisplayName("角色")]
        public RoleCode? Role { get; set; }
    }
}
