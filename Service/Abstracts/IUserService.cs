﻿using APP.Core.Models;
using Framework.Enums;
using Framework.Models.User;

namespace Service.Abstracts
{
    public interface IUserService
    {
        /// <summary>
        /// 搜索用户列表
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        PagingResult<SearchResult> Search(SearchCondition condition);

        /// <summary>
        /// 修改用户状态
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="status"></param>
        void UpdateStatus(int userId, SysUserStatus status);

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        EditUserInfo GetInfo(int userId);

        /// <summary>
        /// 检查旧密码
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        void CheckOldPassword(int userId, string password);

        /// <summary>
        /// 保存用户信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns>User Id</returns>
        int SaveInfo(EditUserInfo info);

        /// <summary>
        /// 账号是否已存在
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        bool IsAccountExisted(int? userId, string account);
    }
}
