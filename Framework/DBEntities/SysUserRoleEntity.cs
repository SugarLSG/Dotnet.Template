﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Attributes;
using APP.Core.Extensions.Nhibernate;
using Newtonsoft.Json;
using Framework.Enums;
using NHibernate.Mapping.Attributes;
using NHibernate.Type;
using System;
using System.ComponentModel;

namespace Framework.DBEntity
{
    /// <summary>
    /// 用户角色
    /// </summary>
    [Class(0, Table = "sys_user_role")]
    [Cache(1, Usage = CacheUsage.ReadWrite)]
    public class SysUserRoleEntity
    {
        public const string TABLE_NAME = "sys_user_role";
        public const string COL_USERROLEID = "UserRoleId";
        public const string COL_USERID = "UserId";
        public const string COL_ROLECODE = "RoleCode";
        public const string COL_CREATEDBY = "CreatedBy";
        public const string COL_CREATEDTIME = "CreatedTime";
        public const string COL_MODIFIEDBY = "ModifiedBy";
        public const string COL_MODIFIEDTIME = "ModifiedTime";

        /// <summary>
        /// 用户角色Id
        /// </summary>
        [Description("用户角色Id")]
        [Id(0, Name = "UserRoleId", Column = "UserRoleId")]
        [Key(1)]
        [Generator(2, Class = "native")]
        public virtual int? UserRoleId { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        [Description("用户Id")]
        [Property(Column = "UserId", NotNull = true)]
        public virtual int UserId { get; set; }

        private SysUserEntity _user = null;
        /// <summary>
        /// 外键 UserId 对应 sys_user 类实例;
        /// </summary>
        [IgnoreConvertTypeScalar]
        [JsonIgnore]
        public virtual SysUserEntity User { get { return _user ?? (_user = NHibernateExtension.GetEntityById<SysUserEntity>(UserId)); } }

        /// <summary>
        /// 角色Code
        /// ENUM[string,Role.RoleCode]
        /// </summary>
        [Description("角色Code")]
        [Property(Column = "RoleCode", NotNull = true, Length = 128, TypeType = typeof(EnumStringType<Role.RoleCode>))]
        public virtual Role.RoleCode RoleCode { get; set; }

        private SysRoleEntity _role = null;
        /// <summary>
        /// 外键 RoleCode 对应 sys_role 类实例;
        /// </summary>
        [IgnoreConvertTypeScalar]
        [JsonIgnore]
        public virtual SysRoleEntity Role { get { return _role ?? (_role = NHibernateExtension.GetEntityById<SysRoleEntity>(RoleCode)); } }

        /// <summary>
        /// 创建者
        /// </summary>
        [Description("创建者")]
        [Property(Column = "CreatedBy", NotNull = true)]
        public virtual int CreatedBy { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Property(Column = "CreatedTime", NotNull = true)]
        public virtual DateTime CreatedTime { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        [Description("修改者")]
        [Property(Column = "ModifiedBy", NotNull = true)]
        public virtual int ModifiedBy { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        [Property(Column = "ModifiedTime", NotNull = true)]
        public virtual DateTime ModifiedTime { get; set; }
    }
}
