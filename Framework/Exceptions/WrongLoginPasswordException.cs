﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("登录密码不正确")]
    public sealed class WrongLoginPasswordException : APPBaseException
    {
        /// <summary>
        /// message: (2003) 登录密码不正确
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public WrongLoginPasswordException(params object[] args)
        {
            Code = "2003";
            Args = args;
        }
    }
}
