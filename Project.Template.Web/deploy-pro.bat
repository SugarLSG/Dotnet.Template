@echo off

set PROJECT_NAME=Project.Template.Web
set PROJECT_NAME_LOWERCASE=project.template.web

set SOURCE_DRIVE=D:
set SOURCE_FOLDER=D:\Project\Template\%PROJECT_NAME%\
set TARGET_DRIVE=D:
set TARGET_FOLDER=D:\Publish\%PROJECT_NAME%\
set PACKAGE_DRIVE=D:
set PACKAGE_FOLDER=D:\Publish\
set PACKAGE_NAME=%PROJECT_NAME_LOWERCASE%
set ZIP_DRIVE=D:
set ZIP_FOLDER=D:\Software\7-Zip\
set YUI_FOLDER=D:\Software\yuicompressor\build\yuicompressor.jar


echo start deploy %PROJECT_NAME% ...


echo copy files -

rd /s/q %TARGET_FOLDER%

%SOURCE_DRIVE%
cd %SOURCE_FOLDER%

xcopy /s/y bin\*.dll %TARGET_FOLDER%bin\
xcopy /s/y bin\*.pdb %TARGET_FOLDER%bin\
xcopy /s/y Areas\*.js %TARGET_FOLDER%Areas\
xcopy /s/y Areas\*.css %TARGET_FOLDER%Areas\
xcopy /s/y Areas\*.cshtml %TARGET_FOLDER%Areas\
xcopy /s/y Areas\*.config %TARGET_FOLDER%Areas\
xcopy /s/y Configs\*.* %TARGET_FOLDER%Configs\
del /q %TARGET_FOLDER%Configs\log4net.xml
del /q %TARGET_FOLDER%Configs\log4net-sit.xml
del /q %TARGET_FOLDER%Configs\log4net-uat.xml
ren %TARGET_FOLDER%Configs\log4net-pro.xml log4net.xml
xcopy /s/y Content\*.* %TARGET_FOLDER%Content\
xcopy /s/y Favicons\*.* %TARGET_FOLDER%Favicons\
xcopy /s/y Views\*.* %TARGET_FOLDER%Views\

copy /y favicon.ico %TARGET_FOLDER%favicon.ico
copy /y Global.asax %TARGET_FOLDER%Global.asax
copy /y Web-pro.config %TARGET_FOLDER%Web.config


echo compress files -

for /r "%TARGET_FOLDER%Areas\" %%a in (*.css) do (
	@echo 正在压缩 %%~a ...
	@java -jar %YUI_FOLDER% --type css --charset UTF-8 "%%~a" -o "%%~a"
)

for /r "%TARGET_FOLDER%Areas\" %%a in (*.js) do (
	@echo 正在压缩 %%~a ...
	@java -jar %YUI_FOLDER% --type js --charset UTF-8 "%%~a" -o "%%~a"
)

for /r "%TARGET_FOLDER%Content\Styles\" %%a in (*.css) do (
	@echo 正在压缩 %%~a ...
	@java -jar %YUI_FOLDER% --type css --charset UTF-8 "%%~a" -o "%%~a"
)

for /r "%TARGET_FOLDER%Content\Scripts\" %%a in (*.js) do (
	@echo 正在压缩 %%~a ...
	@java -jar %YUI_FOLDER% --type js --charset UTF-8 "%%~a" -o "%%~a"
)


echo package files -

%ZIP_DRIVE%
cd %ZIP_FOLDER%

del /q %PACKAGE_FOLDER%%PACKAGE_NAME%.7z
7z a %PACKAGE_FOLDER%%PACKAGE_NAME%.7z %TARGET_FOLDER%


echo delete files -

rd /s/q %TARGET_FOLDER%


echo finish deploy %PROJECT_NAME% ...