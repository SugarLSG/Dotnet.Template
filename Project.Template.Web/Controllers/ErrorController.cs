﻿using APP.Core.Exceptions;
using APP.Core.Utilities;
using Framework.Exceptions;
using Project.Template.Web.Models.Error;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Project.Template.Web.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            var model = new IndexInfo();

            var httpException = RouteData.Values["httpException"] as HttpException;
            var httpCode = (httpException == null) ?
                HttpStatusCode.InternalServerError :
                httpException.GetHttpCode().ToEnum<HttpStatusCode>();
            var errorMessage = string.Empty;

            if (httpException != null && httpException.InnerException is APPBaseException)
            {
                // 无访问权限，Http Code 设为 403
                if (httpException.InnerException is HaveNoPrivilegeToAccessPageException)
                    httpCode = HttpStatusCode.Forbidden;

                errorMessage = (httpException.InnerException as APPBaseException).ToMessage();
            }

            Response.StatusCode = (int)httpCode;
            Response.ContentType = CoreConfig.CONTENTTYPE_TEXT_HTML;

            model.Code = ((int)httpCode).ToString();
            switch (httpCode)
            {
                case HttpStatusCode.Forbidden:
                    model.Message = string.IsNullOrEmpty(errorMessage) ? CoreConfig.HTTPREQUEST_403 : errorMessage;
                    break;

                case HttpStatusCode.NotFound:
                    model.Message = string.IsNullOrEmpty(errorMessage) ? CoreConfig.HTTPREQUEST_404 : errorMessage;
                    break;

                case HttpStatusCode.InternalServerError:
                default:
                    model.Message = string.IsNullOrEmpty(errorMessage) ? CoreConfig.HTTPREQUEST_500 : errorMessage;
                    break;
            }

            if (ClientRequestUtility.IsMobileRequest)
                return View("Mobile", model);
            else
                return View(model);
        }
    }
}