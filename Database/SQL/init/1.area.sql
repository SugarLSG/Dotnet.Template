﻿INSERT INTO sys_country(ContinentCode, ISOCode2, ISOCode3, ISOCodeNumber, NameEN, NameCN) VALUES ('AS', 'CN', 'CHN', '156', 'China', '中国');
SET @CountryId = LAST_INSERT_ID();

/*****************************北京 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '110000', 'BeiJing', '北京', '京');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '110100', 'BeiJing', '北京');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110101', 'DongChengQu', '东城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110102', 'XiChengQu', '西城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110105', 'ChaoYangQu', '朝阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110106', 'FengTaiQu', '丰台区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110107', 'ShiJingShanQu', '石景山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110108', 'HaiDianQu', '海淀区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110109', 'MenTouGouQu', '门头沟区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110111', 'FangShanQu', '房山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110112', 'TongZhouQu', '通州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110113', 'ShunYiQu', '顺义区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110114', 'ChangPingQu', '昌平区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110115', 'DaXingQu', '大兴区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110116', 'HuaiRouQu', '怀柔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110117', 'PingGuQu', '平谷区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110228', 'MiYunXian', '密云县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '110229', 'YanQingXian', '延庆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1101DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '11CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '11CCDD', 'QiTa', '其他');

/*****************************北京 结束*****************************/

/*****************************天津 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '120000', 'TianJin', '天津', '津');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '120100', 'TianJin', '天津');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120101', 'HePingQu', '和平区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120102', 'HeDongQu', '河东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120103', 'HeXiQu', '河西区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120104', 'NanKaiQu', '南开区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120105', 'HeBeiQu', '河北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120106', 'HongQiaoQu', '红桥区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120110', 'DongLiQu', '东丽区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120111', 'XiQingQu', '西青区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120112', 'JinNanQu', '津南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120113', 'BeiChenQu', '北辰区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120114', 'WuQingQu', '武清区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120115', 'BaoChiQu', '宝坻区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120116', 'BinHaiXinQu', '滨海新区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120117', 'NingHeQu', '宁河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120118', 'JingHaiQu', '静海区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '120225', 'JiXian', '蓟县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1201DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '12CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '12CCDD', 'QiTa', '其他');

/*****************************天津 结束*****************************/

/*****************************河北 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '130000', 'HeBei', '河北', '冀');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '130100', 'ShiJiaZhuang', '石家庄');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130102', 'ChangAnQu', '长安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130104', 'QiaoXiQu', '桥西区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130105', 'XinHuaQu', '新华区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130107', 'JingXingKuangQu', '井陉矿区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130108', 'YuHuaQu', '裕华区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130109', 'GaoChengQu', '藁城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130110', 'LuQuanQu', '鹿泉区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130111', 'LuanChengQu', '栾城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130121', 'JingXingXian', '井陉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130123', 'ZhengDingXian', '正定县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130125', 'XingTangXian', '行唐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130126', 'LingShouXian', '灵寿县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130127', 'GaoYiXian', '高邑县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130128', 'ShenZeXian', '深泽县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130129', 'ZanHuangXian', '赞皇县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130130', 'WuJiXian', '无极县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130131', 'PingShanXian', '平山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130132', 'YuanShiXian', '元氏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130133', 'ZhaoXian', '赵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130183', 'JinZhouShi', '晋州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130184', 'XinLeShi', '新乐市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '139002', 'XinJiShi', '辛集市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1301DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '130200', 'TangShan', '唐山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130202', 'LuNanQu', '路南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130203', 'LuBeiQu', '路北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130204', 'GuYeQu', '古冶区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130205', 'KaiPingQu', '开平区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130207', 'FengNanQu', '丰南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130208', 'FengRunQu', '丰润区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130209', 'CaoFeiDianQu', '曹妃甸区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130223', 'LuanXian', '滦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130224', 'LuanNanXian', '滦南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130225', 'LeTingXian', '乐亭县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130227', 'QianXiXian', '迁西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130229', 'YuTianXian', '玉田县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130281', 'ZunHuaShi', '遵化市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130283', 'QianAnShi', '迁安市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1302DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '130300', 'QinHuangDao', '秦皇岛');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130302', 'HaiGangQu', '海港区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130303', 'ShanHaiGuanQu', '山海关区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130304', 'BeiDaiHeQu', '北戴河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130306', 'FuNingQu', '抚宁区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130321', 'QingLongManZuZiZhiXian', '青龙满族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130322', 'ChangLiXian', '昌黎县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130324', 'LuLongXian', '卢龙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1303DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '130400', 'HanDan', '邯郸');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130402', 'HanShanQu', '邯山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130403', 'CongTaiQu', '丛台区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130404', 'FuXingQu', '复兴区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130406', 'FengFengKuangQu', '峰峰矿区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130421', 'HanDanXian', '邯郸县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130423', 'LinZhangXian', '临漳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130424', 'ChengAnXian', '成安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130425', 'DaMingXian', '大名县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130426', 'SheXian', '涉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130427', 'CiXian', '磁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130428', 'FeiXiangXian', '肥乡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130429', 'YongNianXian', '永年县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130430', 'QiuXian', '邱县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130431', 'JiZeXian', '鸡泽县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130432', 'GuangPingXian', '广平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130433', 'GuanTaoXian', '馆陶县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130434', 'WeiXian', '魏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130435', 'QuZhouXian', '曲周县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130481', 'WuAnShi', '武安市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1304DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '130500', 'XingTai', '邢台');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130502', 'QiaoDongQu', '桥东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130503', 'QiaoXiQu', '桥西区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130521', 'XingTaiXian', '邢台县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130522', 'LinChengXian', '临城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130523', 'NeiQiuXian', '内丘县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130524', 'BaiXiangXian', '柏乡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130525', 'LongYaoXian', '隆尧县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130526', 'RenXian', '任县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130527', 'NanHeXian', '南和县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130528', 'NingJinXian', '宁晋县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130529', 'JuLuXian', '巨鹿县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130530', 'XinHeXian', '新河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130531', 'GuangZongXian', '广宗县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130532', 'PingXiangXian', '平乡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130533', 'WeiXian', '威县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130534', 'QingHeXian', '清河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130535', 'LinXiXian', '临西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130581', 'NanGongShi', '南宫市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130582', 'ShaHeShi', '沙河市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1305DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '130600', 'BaoDing', '保定');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130602', 'JingXiuQu', '竞秀区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130606', 'LianChiQu', '莲池区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130607', 'ManChengQu', '满城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130608', 'QingYuanQu', '清苑区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130609', 'XuShuiQu', '徐水区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130623', 'LaiShuiXian', '涞水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130624', 'FuPingXian', '阜平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130626', 'DingXingXian', '定兴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130627', 'TangXian', '唐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130628', 'GaoYangXian', '高阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130629', 'RongChengXian', '容城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130630', 'LaiYuanXian', '涞源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130631', 'WangDuXian', '望都县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130632', 'AnXinXian', '安新县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130633', 'YiXian', '易县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130634', 'QuYangXian', '曲阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130635', 'LiXian', '蠡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130636', 'ShunPingXian', '顺平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130637', 'BoYeXian', '博野县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130638', 'XiongXian', '雄县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130681', 'ZhuoZhouShi', '涿州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130683', 'AnGuoShi', '安国市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130684', 'GaoBeiDianShi', '高碑店市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '139001', 'DingZhouShi', '定州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1306DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '130700', 'ZhangJiaKou', '张家口');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130702', 'QiaoDongQu', '桥东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130703', 'QiaoXiQu', '桥西区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130705', 'XuanHuaQu', '宣化区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130706', 'XiaHuaYuanQu', '下花园区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130721', 'XuanHuaXian', '宣化县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130722', 'ZhangBeiXian', '张北县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130723', 'KangBaoXian', '康保县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130724', 'GuYuanXian', '沽源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130725', 'ShangYiXian', '尚义县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130726', 'YuXian', '蔚县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130727', 'YangYuanXian', '阳原县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130728', 'HuaiAnXian', '怀安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130729', 'WanQuanXian', '万全县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130730', 'HuaiLaiXian', '怀来县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130731', 'ZhuoLuXian', '涿鹿县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130732', 'ChiChengXian', '赤城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130733', 'ChongLiXian', '崇礼县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1307DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '130800', 'ChengDe', '承德');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130802', 'ShuangQiaoQu', '双桥区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130803', 'ShuangLuanQu', '双滦区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130804', 'YingShouYingZiKuangQu', '鹰手营子矿区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130821', 'ChengDeXian', '承德县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130822', 'XingLongXian', '兴隆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130823', 'PingQuanXian', '平泉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130824', 'LuanPingXian', '滦平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130825', 'LongHuaXian', '隆化县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130826', 'FengNingManZuZiZhiXian', '丰宁满族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130827', 'KuanChengManZuZiZhiXian', '宽城满族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130828', 'WeiChangManZuMengGuZuZiZhiXian', '围场满族蒙古族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1308DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '130900', 'CangZhou', '沧州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130902', 'XinHuaQu', '新华区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130903', 'YunHeQu', '运河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130921', 'CangXian', '沧县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130922', 'QingXian', '青县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130923', 'DongGuangXian', '东光县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130924', 'HaiXingXian', '海兴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130925', 'YanShanXian', '盐山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130926', 'SuNingXian', '肃宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130927', 'NanPiXian', '南皮县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130928', 'WuQiaoXian', '吴桥县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130929', 'XianXian', '献县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130930', 'MengCunHuiZuZiZhiXian', '孟村回族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130981', 'PoTouShi', '泊头市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130982', 'RenQiuShi', '任丘市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130983', 'HuangHuaShi', '黄骅市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '130984', 'HeJianShi', '河间市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1309DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '131000', 'LangFang', '廊坊');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131002', 'AnCiQu', '安次区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131003', 'GuangYangQu', '广阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131022', 'GuAnXian', '固安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131023', 'YongQingXian', '永清县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131024', 'XiangHeXian', '香河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131025', 'DaChengXian', '大城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131026', 'WenAnXian', '文安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131028', 'DaChangHuiZuZiZhiXian', '大厂回族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131081', 'BaZhouShi', '霸州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131082', 'SanHeShi', '三河市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1310DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '131100', 'HengShui', '衡水');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131102', 'TaoChengQu', '桃城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131121', 'ZaoQiangXian', '枣强县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131122', 'WuYiXian', '武邑县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131123', 'WuQiangXian', '武强县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131124', 'RaoYangXian', '饶阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131125', 'AnPingXian', '安平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131126', 'GuChengXian', '故城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131127', 'JingXian', '景县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131128', 'FuChengXian', '阜城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131181', 'JiZhouShi', '冀州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '131182', 'ShenZhouShi', '深州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1311DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '13CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '13CCDD', 'QiTa', '其他');

/*****************************河北 结束*****************************/

/*****************************山西 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '140000', 'ShanXi', '山西', '晋');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '140100', 'TaiYuan', '太原');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140105', 'XiaoDianQu', '小店区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140106', 'YingZeQu', '迎泽区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140107', 'XingHuaLingQu', '杏花岭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140108', 'JianCaoPingQu', '尖草坪区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140109', 'WanBoLinQu', '万柏林区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140110', 'JinYuanQu', '晋源区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140121', 'QingXuXian', '清徐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140122', 'YangQuXian', '阳曲县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140123', 'LouFanXian', '娄烦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140181', 'GuJiaoShi', '古交市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1401DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '140200', 'DaTong', '大同');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140202', 'ChengQu', '城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140203', 'KuangQu', '矿区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140211', 'NanJiaoQu', '南郊区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140212', 'XinRongQu', '新荣区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140221', 'YangGaoXian', '阳高县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140222', 'TianZhenXian', '天镇县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140223', 'GuangLingXian', '广灵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140224', 'LingQiuXian', '灵丘县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140225', 'HunYuanXian', '浑源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140226', 'ZuoYunXian', '左云县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140227', 'DaTongXian', '大同县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1402DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '140300', 'YangQuan', '阳泉');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140302', 'ChengQu', '城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140303', 'KuangQu', '矿区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140311', 'JiaoQu', '郊区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140321', 'PingDingXian', '平定县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140322', 'YuXian', '盂县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1403DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '140400', 'ChangZhi', '长治');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140402', 'ChengQu', '城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140411', 'JiaoQu', '郊区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140421', 'ChangZhiXian', '长治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140423', 'XiangYuanXian', '襄垣县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140424', 'TunLiuXian', '屯留县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140425', 'PingShunXian', '平顺县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140426', 'LiChengXian', '黎城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140427', 'HuGuanXian', '壶关县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140428', 'ChangZiXian', '长子县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140429', 'WuXiangXian', '武乡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140430', 'QinXian', '沁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140431', 'QinYuanXian', '沁源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140481', 'LuChengShi', '潞城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1404DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '140500', 'JinCheng', '晋城');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140502', 'ChengQu', '城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140521', 'QinShuiXian', '沁水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140522', 'YangChengXian', '阳城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140524', 'LingChuanXian', '陵川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140525', 'ZeZhouXian', '泽州县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140581', 'GaoPingShi', '高平市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1405DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '140600', 'ShuoZhou', '朔州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140602', 'ShuoChengQu', '朔城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140603', 'PingLuQu', '平鲁区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140621', 'ShanYinXian', '山阴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140622', 'YingXian', '应县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140623', 'YouYuXian', '右玉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140624', 'HuaiRenXian', '怀仁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1406DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '140700', 'JinZhong', '晋中');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140702', 'YuCiQu', '榆次区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140721', 'YuSheXian', '榆社县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140722', 'ZuoQuanXian', '左权县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140723', 'HeShunXian', '和顺县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140724', 'XiYangXian', '昔阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140725', 'ShouYangXian', '寿阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140726', 'TaiGuXian', '太谷县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140727', 'QiXian', '祁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140728', 'PingYaoXian', '平遥县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140729', 'LingShiXian', '灵石县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140781', 'JieXiuShi', '介休市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1407DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '140800', 'YunCheng', '运城');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140802', 'YanHuQu', '盐湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140821', 'LinYiXian', '临猗县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140822', 'WanRongXian', '万荣县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140823', 'WenXiXian', '闻喜县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140824', 'JiShanXian', '稷山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140825', 'XinJiangXian', '新绛县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140826', 'JiangXian', '绛县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140827', 'YuanQuXian', '垣曲县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140828', 'XiaXian', '夏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140829', 'PingLuXian', '平陆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140830', 'RuiChengXian', '芮城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140881', 'YongJiShi', '永济市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140882', 'HeJinShi', '河津市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1408DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '140900', 'XinZhou', '忻州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140902', 'XinFuQu', '忻府区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140921', 'DingXiangXian', '定襄县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140922', 'WuTaiXian', '五台县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140923', 'DaiXian', '代县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140924', 'FanZhiXian', '繁峙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140925', 'NingWuXian', '宁武县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140926', 'JingLeXian', '静乐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140927', 'ShenChiXian', '神池县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140928', 'WuZhaiXian', '五寨县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140929', 'KeLanXian', '岢岚县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140930', 'HeQuXian', '河曲县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140931', 'BaoDeXian', '保德县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140932', 'PianGuanXian', '偏关县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '140981', 'YuanPingShi', '原平市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1409DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '141000', 'LinFen', '临汾');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141002', 'YaoDuQu', '尧都区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141021', 'QuWoXian', '曲沃县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141022', 'YiChengXian', '翼城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141023', 'XiangFenXian', '襄汾县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141024', 'HongDongXian', '洪洞县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141025', 'GuXian', '古县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141026', 'AnZeXian', '安泽县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141027', 'FuShanXian', '浮山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141028', 'JiXian', '吉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141029', 'XiangNingXian', '乡宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141030', 'DaNingXian', '大宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141031', 'XiXian', '隰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141032', 'YongHeXian', '永和县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141033', 'PuXian', '蒲县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141034', 'FenXiXian', '汾西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141081', 'HouMaShi', '侯马市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141082', 'HuoZhouShi', '霍州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1410DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '141100', 'LvLiang', '吕梁');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141102', 'LiShiQu', '离石区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141121', 'WenShuiXian', '文水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141122', 'JiaoChengXian', '交城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141123', 'XingXian', '兴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141124', 'LinXian', '临县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141125', 'LiuLinXian', '柳林县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141126', 'ShiLouXian', '石楼县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141127', 'LanXian', '岚县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141128', 'FangShanXian', '方山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141129', 'ZhongYangXian', '中阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141130', 'JiaoKouXian', '交口县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141181', 'XiaoYiShi', '孝义市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '141182', 'FenYangShi', '汾阳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1411DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '14CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '14CCDD', 'QiTa', '其他');

/*****************************山西 结束*****************************/

/*****************************内蒙古 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '150000', 'NeiMengGu', '内蒙古', '蒙');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '150100', 'HuHeHaoTe', '呼和浩特');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150102', 'XinChengQu', '新城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150103', 'HuiMinQu', '回民区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150104', 'YuQuanQu', '玉泉区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150105', 'SaiHanQu', '赛罕区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150121', 'TuMoTeZuoQi', '土默特左旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150122', 'TuoKeTuoXian', '托克托县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150123', 'HeLinGeErXian', '和林格尔县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150124', 'QingShuiHeXian', '清水河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150125', 'WuChuanXian', '武川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1501DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '150200', 'BaoTou', '包头');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150202', 'DongHeQu', '东河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150203', 'KunDuLunQu', '昆都仑区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150204', 'QingShanQu', '青山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150205', 'ShiGuaiQu', '石拐区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150206', 'BaiYunEBoKuangQu', '白云鄂博矿区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150207', 'JiuYuanQu', '九原区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150221', 'TuMoTeYouQi', '土默特右旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150222', 'GuYangXian', '固阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150223', 'DaErHanMaoMingAnLianHeQi', '达尔罕茂明安联合旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1502DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '150300', 'WuHai', '乌海');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150302', 'HaiBoWanQu', '海勃湾区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150303', 'HaiNanQu', '海南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150304', 'WuDaQu', '乌达区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1503DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '150400', 'ChiFeng', '赤峰');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150402', 'HongShanQu', '红山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150403', 'YuanBaoShanQu', '元宝山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150404', 'SongShanQu', '松山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150421', 'ALuKeErQinQi', '阿鲁科尔沁旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150422', 'BaLinZuoQi', '巴林左旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150423', 'BaLinYouQi', '巴林右旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150424', 'LinXiXian', '林西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150425', 'KeShenKeTengQi', '克什克腾旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150426', 'WengNiuTeQi', '翁牛特旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150428', 'KaLaQinQi', '喀喇沁旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150429', 'NingChengXian', '宁城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150430', 'AoHanQi', '敖汉旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1504DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '150500', 'TongLiao', '通辽');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150502', 'KeErQinQu', '科尔沁区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150521', 'KeErQinZuoYiZhongQi', '科尔沁左翼中旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150522', 'KeErQinZuoYiHouQi', '科尔沁左翼后旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150523', 'KaiLuXian', '开鲁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150524', 'KuLunQi', '库伦旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150525', 'NaiManQi', '奈曼旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150526', 'ZhaLuTeQi', '扎鲁特旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150581', 'HuoLinGuoLeShi', '霍林郭勒市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1505DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '150600', 'EErDuoSi', '鄂尔多斯');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150602', 'DongShengQu', '东胜区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150621', 'DaLaTeQi', '达拉特旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150622', 'ZhunGeErQi', '准格尔旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150623', 'ETuoKeQianQi', '鄂托克前旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150624', 'ETuoKeQi', '鄂托克旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150625', 'HangJinQi', '杭锦旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150626', 'WuShenQi', '乌审旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150627', 'YiJinHuoLuoQi', '伊金霍洛旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1506DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '150700', 'HuLunBeiEr', '呼伦贝尔');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150702', 'HaiLaErQu', '海拉尔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150703', 'ZhaLaiNuoErQu', '扎赉诺尔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150721', 'ARongQi', '阿荣旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150722', 'MoLiDaWaDaWoErZuZiZhiQi', '莫力达瓦达斡尔族自治旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150723', 'ELunChunZiZhiQi', '鄂伦春自治旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150724', 'EWenKeZuZiZhiQi', '鄂温克族自治旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150725', 'ChenBaErHuQi', '陈巴尔虎旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150726', 'XinBaErHuZuoQi', '新巴尔虎左旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150727', 'XinBaErHuYouQi', '新巴尔虎右旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150781', 'ManZhouLiShi', '满洲里市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150782', 'YaKeShiShi', '牙克石市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150783', 'ZhaLanTunShi', '扎兰屯市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150784', 'EErGuNaShi', '额尔古纳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150785', 'GenHeShi', '根河市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1507DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '150800', 'BaYanNaoEr', '巴彦淖尔');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150802', 'LinHeQu', '临河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150821', 'WuYuanXian', '五原县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150822', 'DengKouXian', '磴口县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150823', 'WuLaTeQianQi', '乌拉特前旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150824', 'WuLaTeZhongQi', '乌拉特中旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150825', 'WuLaTeHouQi', '乌拉特后旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150826', 'HangJinHouQi', '杭锦后旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1508DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '150900', 'WuLanChaBu', '乌兰察布');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150902', 'JiNingQu', '集宁区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150921', 'ZhuoZiXian', '卓资县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150922', 'HuaDeXian', '化德县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150923', 'ShangDuXian', '商都县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150924', 'XingHeXian', '兴和县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150925', 'LiangChengXian', '凉城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150926', 'ChaHaErYouYiQianQi', '察哈尔右翼前旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150927', 'ChaHaErYouYiZhongQi', '察哈尔右翼中旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150928', 'ChaHaErYouYiHouQi', '察哈尔右翼后旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150929', 'SiZiWangQi', '四子王旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '150981', 'FengZhenShi', '丰镇市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1509DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '152200', 'XingAnMeng', '兴安盟');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152201', 'WuLanHaoTeShi', '乌兰浩特市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152202', 'AErShanShi', '阿尔山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152221', 'KeErQinYouYiQianQi', '科尔沁右翼前旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152222', 'KeErQinYouYiZhongQi', '科尔沁右翼中旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152223', 'ZhaLaiTeQi', '扎赉特旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152224', 'TuQuanXian', '突泉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1522DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '152500', 'XiLinGuoLeMeng', '锡林郭勒盟');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152501', 'ErLianHaoTeShi', '二连浩特市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152502', 'XiLinHaoTeShi', '锡林浩特市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152522', 'ABaGaQi', '阿巴嘎旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152523', 'SuNiTeZuoQi', '苏尼特左旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152524', 'SuNiTeYouQi', '苏尼特右旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152525', 'DongWuZhuMuQinQi', '东乌珠穆沁旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152526', 'XiWuZhuMuQinQi', '西乌珠穆沁旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152527', 'TaiPuSiQi', '太仆寺旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152528', 'XiangHuangQi', '镶黄旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152529', 'ZhengXiangBaiQi', '正镶白旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152530', 'ZhengLanQi', '正蓝旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152531', 'DuoLunXian', '多伦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1525DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '152900', 'ALaShanMeng', '阿拉善盟');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152921', 'ALaShanZuoQi', '阿拉善左旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152922', 'ALaShanYouQi', '阿拉善右旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '152923', 'EJiNaQi', '额济纳旗');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '1529DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '15CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '15CCDD', 'QiTa', '其他');

/*****************************内蒙古 结束*****************************/

/*****************************辽宁 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '210000', 'LiaoNing', '辽宁', '辽');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '210100', 'ShenYang', '沈阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210102', 'HePingQu', '和平区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210103', 'ShenHeQu', '沈河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210104', 'DaDongQu', '大东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210105', 'HuangGuQu', '皇姑区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210106', 'TieXiQu', '铁西区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210111', 'SuJiaTunQu', '苏家屯区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210112', 'HunNanQu', '浑南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210113', 'ShenBeiXinQu', '沈北新区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210114', 'YuHongQu', '于洪区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210122', 'LiaoZhongXian', '辽中县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210123', 'KangPingXian', '康平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210124', 'FaKuXian', '法库县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210181', 'XinMinShi', '新民市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2101DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '210200', 'DaLian', '大连');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210202', 'ZhongShanQu', '中山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210203', 'XiGangQu', '西岗区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210204', 'ShaHeKouQu', '沙河口区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210211', 'GanJingZiQu', '甘井子区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210212', 'LvShunKouQu', '旅顺口区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210213', 'JinZhouQu', '金州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210224', 'ChangHaiXian', '长海县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210281', 'WaFangDianShi', '瓦房店市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210282', 'PuLanDianShi', '普兰店市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210283', 'ZhuangHeShi', '庄河市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2102DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '210300', 'AnShan', '鞍山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210302', 'TieDongQu', '铁东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210303', 'TieXiQu', '铁西区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210304', 'LiShanQu', '立山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210311', 'QianShanQu', '千山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210321', 'TaiAnXian', '台安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210323', 'XiuYanManZuZiZhiXian', '岫岩满族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210381', 'HaiChengShi', '海城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2103DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '210400', 'FuShun', '抚顺');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210402', 'XinFuQu', '新抚区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210403', 'DongZhouQu', '东洲区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210404', 'WangHuaQu', '望花区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210411', 'ShunChengQu', '顺城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210421', 'FuShunXian', '抚顺县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210422', 'XinBinManZuZiZhiXian', '新宾满族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210423', 'QingYuanManZuZiZhiXian', '清原满族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2104DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '210500', 'BenXi', '本溪');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210502', 'PingShanQu', '平山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210503', 'XiHuQu', '溪湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210504', 'MingShanQu', '明山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210505', 'NanFenQu', '南芬区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210521', 'BenXiManZuZiZhiXian', '本溪满族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210522', 'HuanRenManZuZiZhiXian', '桓仁满族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2105DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '210600', 'DanDong', '丹东');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210602', 'YuanBaoQu', '元宝区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210603', 'ZhenXingQu', '振兴区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210604', 'ZhenAnQu', '振安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210624', 'KuanDianManZuZiZhiXian', '宽甸满族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210681', 'DongGangShi', '东港市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210682', 'FengChengShi', '凤城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2106DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '210700', 'JinZhou', '锦州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210702', 'GuTaQu', '古塔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210703', 'LingHeQu', '凌河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210711', 'TaiHeQu', '太和区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210726', 'HeiShanXian', '黑山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210727', 'YiXian', '义县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210781', 'LingHaiShi', '凌海市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210782', 'BeiZhenShi', '北镇市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2107DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '210800', 'YingKou', '营口');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210802', 'ZhanQianQu', '站前区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210803', 'XiShiQu', '西市区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210804', 'BaYuQuanQu', '鲅鱼圈区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210811', 'LaoBianQu', '老边区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210881', 'GaiZhouShi', '盖州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210882', 'DaShiQiaoShi', '大石桥市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2108DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '210900', 'FuXin', '阜新');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210902', 'HaiZhouQu', '海州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210903', 'XinQiuQu', '新邱区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210904', 'TaiPingQu', '太平区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210905', 'QingHeMenQu', '清河门区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210911', 'XiHeQu', '细河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210921', 'FuXinMengGuZuZiZhiXian', '阜新蒙古族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '210922', 'ZhangWuXian', '彰武县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2109DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '211000', 'LiaoYang', '辽阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211002', 'BaiTaQu', '白塔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211003', 'WenShengQu', '文圣区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211004', 'HongWeiQu', '宏伟区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211005', 'GongChangLingQu', '弓长岭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211011', 'TaiZiHeQu', '太子河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211021', 'LiaoYangXian', '辽阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211081', 'DengTaShi', '灯塔市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2110DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '211100', 'PanJin', '盘锦');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211102', 'ShuangTaiZiQu', '双台子区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211103', 'XingLongTaiQu', '兴隆台区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211121', 'DaWaXian', '大洼县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211122', 'PanShanXian', '盘山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2111DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '211200', 'TieLing', '铁岭');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211202', 'YinZhouQu', '银州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211204', 'QingHeQu', '清河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211221', 'TieLingXian', '铁岭县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211223', 'XiFengXian', '西丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211224', 'ChangTuXian', '昌图县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211281', 'DiaoBingShanShi', '调兵山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211282', 'KaiYuanShi', '开原市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2112DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '211300', 'ChaoYang', '朝阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211302', 'ShuangTaQu', '双塔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211303', 'LongChengQu', '龙城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211321', 'ChaoYangXian', '朝阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211322', 'JianPingXian', '建平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211324', 'KaLaQinZuoYiMengGuZuZiZhiXian', '喀喇沁左翼蒙古族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211381', 'BeiPiaoShi', '北票市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211382', 'LingYuanShi', '凌源市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2113DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '211400', 'HuLuDao', '葫芦岛');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211402', 'LianShanQu', '连山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211403', 'LongGangQu', '龙港区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211404', 'NanPiaoQu', '南票区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211421', 'SuiZhongXian', '绥中县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211422', 'JianChangXian', '建昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '211481', 'XingChengShi', '兴城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2114DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '21CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '21CCDD', 'QiTa', '其他');

/*****************************辽宁 结束*****************************/

/*****************************吉林 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '220000', 'JiLin', '吉林', '吉');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '220100', 'ChangChun', '长春');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220102', 'NanGuanQu', '南关区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220103', 'KuanChengQu', '宽城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220104', 'ChaoYangQu', '朝阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220105', 'ErDaoQu', '二道区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220106', 'LvYuanQu', '绿园区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220112', 'ShuangYangQu', '双阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220113', 'JiuTaiQu', '九台区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220122', 'NongAnXian', '农安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220182', 'YuShuShi', '榆树市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220183', 'DeHuiShi', '德惠市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2201DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '220200', 'JiLin', '吉林');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220202', 'ChangYiQu', '昌邑区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220203', 'LongTanQu', '龙潭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220204', 'ChuanYingQu', '船营区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220211', 'FengManQu', '丰满区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220221', 'YongJiXian', '永吉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220281', 'JiaoHeShi', '蛟河市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220282', 'HuaDianShi', '桦甸市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220283', 'ShuLanShi', '舒兰市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220284', 'PanShiShi', '磐石市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2202DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '220300', 'SiPing', '四平');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220302', 'TieXiQu', '铁西区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220303', 'TieDongQu', '铁东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220322', 'LiShuXian', '梨树县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220323', 'YiTongManZuZiZhiXian', '伊通满族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220381', 'GongZhuLingShi', '公主岭市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220382', 'ShuangLiaoShi', '双辽市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2203DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '220400', 'LiaoYuan', '辽源');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220402', 'LongShanQu', '龙山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220403', 'XiAnQu', '西安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220421', 'DongFengXian', '东丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220422', 'DongLiaoXian', '东辽县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2204DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '220500', 'TongHua', '通化');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220502', 'DongChangQu', '东昌区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220503', 'ErDaoJiangQu', '二道江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220521', 'TongHuaXian', '通化县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220523', 'HuiNanXian', '辉南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220524', 'LiuHeXian', '柳河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220581', 'MeiHeKouShi', '梅河口市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220582', 'JiAnShi', '集安市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2205DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '220600', 'BaiShan', '白山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220602', 'HunJiangQu', '浑江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220605', 'JiangYuanQu', '江源区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220621', 'FuSongXian', '抚松县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220622', 'JingYuXian', '靖宇县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220623', 'ChangBaiChaoXianZuZiZhiXian', '长白朝鲜族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220681', 'LinJiangShi', '临江市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2206DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '220700', 'SongYuan', '松原');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220702', 'NingJiangQu', '宁江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220721', 'QianGuoErLuoSiMengGuZuZiZhiXian', '前郭尔罗斯蒙古族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220722', 'ChangLingXian', '长岭县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220723', 'QianAnXian', '乾安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220781', 'FuYuShi', '扶余市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2207DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '220800', 'BaiCheng', '白城');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220802', 'TaoBeiQu', '洮北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220821', 'ZhenLaiXian', '镇赉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220822', 'TongYuXian', '通榆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220881', 'TaoNanShi', '洮南市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '220882', 'DaAnShi', '大安市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2208DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '222400', 'YanBian', '延边');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '222401', 'YanJiShi', '延吉市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '222402', 'TuMenShi', '图们市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '222403', 'DunHuaShi', '敦化市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '222404', 'HunChunShi', '珲春市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '222405', 'LongJingShi', '龙井市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '222406', 'HeLongShi', '和龙市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '222424', 'WangQingXian', '汪清县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '222426', 'AnTuXian', '安图县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2224DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '22CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '22CCDD', 'QiTa', '其他');

/*****************************吉林 结束*****************************/

/*****************************黑龙江 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '230000', 'HeiLongJiang', '黑龙江', '黑');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '230100', 'HaErBin', '哈尔滨');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230102', 'DaoLiQu', '道里区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230103', 'NanGangQu', '南岗区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230104', 'DaoWaiQu', '道外区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230108', 'PingFangQu', '平房区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230109', 'SongBeiQu', '松北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230110', 'XiangFangQu', '香坊区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230111', 'HuLanQu', '呼兰区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230112', 'AChengQu', '阿城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230113', 'ShuangChengQu', '双城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230123', 'YiLanXian', '依兰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230124', 'FangZhengXian', '方正县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230125', 'BinXian', '宾县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230126', 'BaYanXian', '巴彦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230127', 'MuLanXian', '木兰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230128', 'TongHeXian', '通河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230129', 'YanShouXian', '延寿县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230183', 'ShangZhiShi', '尚志市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230184', 'WuChangShi', '五常市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2301DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '230200', 'QiQiHaEr', '齐齐哈尔');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230202', 'LongShaQu', '龙沙区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230203', 'JianHuaQu', '建华区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230204', 'TieFengQu', '铁锋区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230205', 'AngAngXiQu', '昂昂溪区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230206', 'FuLaErJiQu', '富拉尔基区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230207', 'NianZiShanQu', '碾子山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230208', 'MeiLiSiDaWoErZuQu', '梅里斯达斡尔族区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230221', 'LongJiangXian', '龙江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230223', 'YiAnXian', '依安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230224', 'TaiLaiXian', '泰来县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230225', 'GanNanXian', '甘南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230227', 'FuYuXian', '富裕县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230229', 'KeShanXian', '克山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230230', 'KeDongXian', '克东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230231', 'BaiQuanXian', '拜泉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230281', 'NeHeShi', '讷河市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2302DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '230300', 'JiXi', '鸡西');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230302', 'JiGuanQu', '鸡冠区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230303', 'HengShanQu', '恒山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230304', 'DiDaoQu', '滴道区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230305', 'LiShuQu', '梨树区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230306', 'ChengZiHeQu', '城子河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230307', 'MaShanQu', '麻山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230321', 'JiDongXian', '鸡东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230381', 'HuLinShi', '虎林市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230382', 'MiShanShi', '密山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2303DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '230400', 'HeGang', '鹤岗');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230402', 'XiangYangQu', '向阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230403', 'GongNongQu', '工农区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230404', 'NanShanQu', '南山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230405', 'XingAnQu', '兴安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230406', 'DongShanQu', '东山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230407', 'XingShanQu', '兴山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230421', 'LuoBeiXian', '萝北县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230422', 'SuiBinXian', '绥滨县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2304DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '230500', 'ShuangYaShan', '双鸭山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230502', 'JianShanQu', '尖山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230503', 'LingDongQu', '岭东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230505', 'SiFangTaiQu', '四方台区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230506', 'BaoShanQu', '宝山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230521', 'JiXianXian', '集贤县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230522', 'YouYiXian', '友谊县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230523', 'BaoQingXian', '宝清县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230524', 'RaoHeXian', '饶河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2305DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '230600', 'DaQing', '大庆');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230602', 'SaErTuQu', '萨尔图区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230603', 'LongFengQu', '龙凤区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230604', 'RangHuLuQu', '让胡路区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230605', 'HongGangQu', '红岗区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230606', 'DaTongQu', '大同区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230621', 'ZhaoZhouXian', '肇州县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230622', 'ZhaoYuanXian', '肇源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230623', 'LinDianXian', '林甸县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230624', 'DuErBoTeMengGuZuZiZhiXian', '杜尔伯特蒙古族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2306DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '230700', 'YiChun', '伊春');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230702', 'YiChunQu', '伊春区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230703', 'NanChaQu', '南岔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230704', 'YouHaoQu', '友好区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230705', 'XiLinQu', '西林区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230706', 'CuiLuanQu', '翠峦区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230707', 'XinQingQu', '新青区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230708', 'MeiXiQu', '美溪区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230709', 'JinShanTunQu', '金山屯区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230710', 'WuYingQu', '五营区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230711', 'WuMaHeQu', '乌马河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230712', 'TangWangHeQu', '汤旺河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230713', 'DaiLingQu', '带岭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230714', 'WuYiLingQu', '乌伊岭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230715', 'HongXingQu', '红星区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230716', 'ShangGanLingQu', '上甘岭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230722', 'JiaYinXian', '嘉荫县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230781', 'TieLiShi', '铁力市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2307DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '230800', 'JiaMuSi', '佳木斯');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230803', 'XiangYangQu', '向阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230804', 'QianJinQu', '前进区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230805', 'DongFengQu', '东风区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230811', 'JiaoQu', '郊区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230822', 'HuaNanXian', '桦南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230826', 'HuaChuanXian', '桦川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230828', 'TangYuanXian', '汤原县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230833', 'FuYuanXian', '抚远县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230881', 'TongJiangShi', '同江市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230882', 'FuJinShi', '富锦市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2308DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '230900', 'QiTaiHe', '七台河');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230902', 'XinXingQu', '新兴区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230903', 'TaoShanQu', '桃山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230904', 'QieZiHeQu', '茄子河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '230921', 'BoLiXian', '勃利县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2309DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '231000', 'MuDanJiang', '牡丹江');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231002', 'DongAnQu', '东安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231003', 'YangMingQu', '阳明区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231004', 'AiMinQu', '爱民区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231005', 'XiAnQu', '西安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231024', 'DongNingXian', '东宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231025', 'LinKouXian', '林口县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231081', 'SuiFenHeShi', '绥芬河市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231083', 'HaiLinShi', '海林市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231084', 'NingAnShi', '宁安市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231085', 'MuLengShi', '穆棱市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2310DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '231100', 'HeiHe', '黑河');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231102', 'AiHuiQu', '爱辉区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231121', 'NenJiangXian', '嫩江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231123', 'XunKeXian', '逊克县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231124', 'SunWuXian', '孙吴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231181', 'BeiAnShi', '北安市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231182', 'WuDaLianChiShi', '五大连池市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2311DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '231200', 'SuiHua', '绥化');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231202', 'BeiLinQu', '北林区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231221', 'WangKuiXian', '望奎县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231222', 'LanXiXian', '兰西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231223', 'QingGangXian', '青冈县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231224', 'QingAnXian', '庆安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231225', 'MingShuiXian', '明水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231226', 'SuiLengXian', '绥棱县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231281', 'AnDaShi', '安达市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231282', 'ZhaoDongShi', '肇东市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '231283', 'HaiLunShi', '海伦市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2312DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '232700', 'DaXingAnLing', '大兴安岭');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '232721', 'HuMaXian', '呼玛县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '232722', 'TaHeXian', '塔河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '232723', 'MoHeXian', '漠河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '2327DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '23CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '23CCDD', 'QiTa', '其他');

/*****************************黑龙江 结束*****************************/

/*****************************上海 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '310000', 'ShangHai', '上海', '沪');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '310100', 'ShangHai', '上海');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310101', 'HuangPuQu', '黄浦区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310104', 'XuHuiQu', '徐汇区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310105', 'ChangNingQu', '长宁区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310106', 'JingAnQu', '静安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310107', 'PuTuoQu', '普陀区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310108', 'ZhaBeiQu', '闸北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310109', 'HongKouQu', '虹口区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310110', 'YangPuQu', '杨浦区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310112', 'MinXingQu', '闵行区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310113', 'BaoShanQu', '宝山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310114', 'JiaDingQu', '嘉定区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310115', 'PuDongXinQu', '浦东新区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310116', 'JinShanQu', '金山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310117', 'SongJiangQu', '松江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310118', 'QingPuQu', '青浦区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310120', 'FengXianQu', '奉贤区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '310230', 'ChongMingXian', '崇明县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3101DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '31CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '31CCDD', 'QiTa', '其他');

/*****************************上海 结束*****************************/

/*****************************江苏 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '320000', 'JiangSu', '江苏', '苏');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '320100', 'NanJing', '南京');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320102', 'XuanWuQu', '玄武区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320104', 'QinHuaiQu', '秦淮区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320105', 'JianYeQu', '建邺区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320106', 'GuLouQu', '鼓楼区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320111', 'PuKouQu', '浦口区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320113', 'QiXiaQu', '栖霞区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320114', 'YuHuaTaiQu', '雨花台区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320115', 'JiangNingQu', '江宁区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320116', 'LiuHeQu', '六合区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320117', 'LiShuiQu', '溧水区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320118', 'GaoChunQu', '高淳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3201DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '320200', 'WuXi', '无锡');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320202', 'ChongAnQu', '崇安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320203', 'NanChangQu', '南长区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320204', 'BeiTangQu', '北塘区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320205', 'XiShanQu', '锡山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320206', 'HuiShanQu', '惠山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320211', 'BinHuQu', '滨湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320281', 'JiangYinShi', '江阴市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320282', 'YiXingShi', '宜兴市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3202DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '320300', 'XuZhou', '徐州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320302', 'GuLouQu', '鼓楼区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320303', 'YunLongQu', '云龙区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320305', 'JiaWangQu', '贾汪区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320311', 'QuanShanQu', '泉山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320312', 'TongShanQu', '铜山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320321', 'FengXian', '丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320322', 'PeiXian', '沛县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320324', 'SuiNingXian', '睢宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320381', 'XinYiShi', '新沂市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320382', 'PiZhouShi', '邳州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3203DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '320400', 'ChangZhou', '常州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320402', 'TianNingQu', '天宁区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320404', 'ZhongLouQu', '钟楼区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320411', 'XinBeiQu', '新北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320412', 'WuJinQu', '武进区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320413', 'JinTanQu', '金坛区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320481', 'LiYangShi', '溧阳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3204DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '320500', 'SuZhou', '苏州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320505', 'HuQiuQu', '虎丘区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320506', 'WuZhongQu', '吴中区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320507', 'XiangChengQu', '相城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320508', 'GuSuQu', '姑苏区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320509', 'WuJiangQu', '吴江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320581', 'ChangShuShi', '常熟市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320582', 'ZhangJiaGangShi', '张家港市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320583', 'KunShanShi', '昆山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320585', 'TaiCangShi', '太仓市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3205DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '320600', 'NanTong', '南通');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320602', 'ChongChuanQu', '崇川区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320611', 'GangZhaQu', '港闸区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320612', 'TongZhouQu', '通州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320621', 'HaiAnXian', '海安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320623', 'RuDongXian', '如东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320681', 'QiDongShi', '启东市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320682', 'RuGaoShi', '如皋市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320684', 'HaiMenShi', '海门市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3206DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '320700', 'LianYunGang', '连云港');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320703', 'LianYunQu', '连云区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320706', 'HaiZhouQu', '海州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320707', 'GanYuQu', '赣榆区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320722', 'DongHaiXian', '东海县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320723', 'GuanYunXian', '灌云县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320724', 'GuanNanXian', '灌南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3207DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '320800', 'HuaiAn', '淮安');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320802', 'QingHeQu', '清河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320803', 'HuaiAnQu', '淮安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320804', 'HuaiYinQu', '淮阴区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320811', 'QingPuQu', '清浦区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320826', 'LianShuiXian', '涟水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320829', 'HongZeXian', '洪泽县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320830', 'XuYiXian', '盱眙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320831', 'JinHuXian', '金湖县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3208DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '320900', 'YanCheng', '盐城');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320902', 'TingHuQu', '亭湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320903', 'YanDuQu', '盐都区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320904', 'DaFengQu', '大丰区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320921', 'XiangShuiXian', '响水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320922', 'BinHaiXian', '滨海县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320923', 'FuNingXian', '阜宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320924', 'SheYangXian', '射阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320925', 'JianHuXian', '建湖县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '320981', 'DongTaiShi', '东台市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3209DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '321000', 'YangZhou', '扬州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321002', 'GuangLingQu', '广陵区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321003', 'HanJiangQu', '邗江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321012', 'JiangDuQu', '江都区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321023', 'BaoYingXian', '宝应县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321081', 'YiZhengShi', '仪征市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321084', 'GaoYouShi', '高邮市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3210DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '321100', 'ZhenJiang', '镇江');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321102', 'JingKouQu', '京口区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321111', 'RunZhouQu', '润州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321112', 'DanTuQu', '丹徒区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321181', 'DanYangShi', '丹阳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321182', 'YangZhongShi', '扬中市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321183', 'JuRongShi', '句容市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3211DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '321200', 'TaiZhou', '泰州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321202', 'HaiLingQu', '海陵区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321203', 'GaoGangQu', '高港区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321204', 'JiangYanQu', '姜堰区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321281', 'XingHuaShi', '兴化市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321282', 'JingJiangShi', '靖江市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321283', 'TaiXingShi', '泰兴市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3212DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '321300', 'SuQian', '宿迁');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321302', 'SuChengQu', '宿城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321311', 'SuYuQu', '宿豫区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321322', 'ShuYangXian', '沭阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321323', 'SiYangXian', '泗阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '321324', 'SiHongXian', '泗洪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3213DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '32CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '32CCDD', 'QiTa', '其他');

/*****************************江苏 结束*****************************/

/*****************************浙江 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '330000', 'ZheJiang', '浙江', '浙');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '330100', 'HangZhou', '杭州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330102', 'ShangChengQu', '上城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330103', 'XiaChengQu', '下城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330104', 'JiangGanQu', '江干区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330105', 'GongShuQu', '拱墅区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330106', 'XiHuQu', '西湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330108', 'BinJiangQu', '滨江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330109', 'XiaoShanQu', '萧山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330110', 'YuHangQu', '余杭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330111', 'FuYangQu', '富阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330122', 'TongLuXian', '桐庐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330127', 'ChunAnXian', '淳安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330182', 'JianDeShi', '建德市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330185', 'LinAnShi', '临安市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3301DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '330200', 'NingBo', '宁波');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330203', 'HaiShuQu', '海曙区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330204', 'JiangDongQu', '江东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330205', 'JiangBeiQu', '江北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330206', 'BeiLunQu', '北仑区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330211', 'ZhenHaiQu', '镇海区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330212', 'YinZhouQu', '鄞州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330225', 'XiangShanXian', '象山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330226', 'NingHaiXian', '宁海县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330281', 'YuYaoShi', '余姚市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330282', 'CiXiShi', '慈溪市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330283', 'FengHuaShi', '奉化市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3302DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '330300', 'WenZhou', '温州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330302', 'LuChengQu', '鹿城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330303', 'LongWanQu', '龙湾区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330304', 'OuHaiQu', '瓯海区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330305', 'DongTouQu', '洞头区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330324', 'YongJiaXian', '永嘉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330326', 'PingYangXian', '平阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330327', 'CangNanXian', '苍南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330328', 'WenChengXian', '文成县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330329', 'TaiShunXian', '泰顺县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330381', 'RuiAnShi', '瑞安市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330382', 'LeQingShi', '乐清市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3303DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '330400', 'JiaXing', '嘉兴');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330402', 'NanHuQu', '南湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330411', 'XiuZhouQu', '秀洲区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330421', 'JiaShanXian', '嘉善县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330424', 'HaiYanXian', '海盐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330481', 'HaiNingShi', '海宁市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330482', 'PingHuShi', '平湖市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330483', 'TongXiangShi', '桐乡市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3304DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '330500', 'HuZhou', '湖州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330502', 'WuXingQu', '吴兴区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330503', 'NanXunQu', '南浔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330521', 'DeQingXian', '德清县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330522', 'ChangXingXian', '长兴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330523', 'AnJiXian', '安吉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3305DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '330600', 'ShaoXing', '绍兴');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330602', 'YueChengQu', '越城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330603', 'KeQiaoQu', '柯桥区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330604', 'ShangYuQu', '上虞区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330624', 'XinChangXian', '新昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330681', 'ZhuJiShi', '诸暨市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330683', 'ShengZhouShi', '嵊州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3306DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '330700', 'JinHua', '金华');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330702', 'WuChengQu', '婺城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330703', 'JinDongQu', '金东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330723', 'WuYiXian', '武义县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330726', 'PuJiangXian', '浦江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330727', 'PanAnXian', '磐安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330781', 'LanXiShi', '兰溪市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330782', 'YiWuShi', '义乌市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330783', 'DongYangShi', '东阳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330784', 'YongKangShi', '永康市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3307DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '330800', 'QuZhou', '衢州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330802', 'KeChengQu', '柯城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330803', 'QuJiangQu', '衢江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330822', 'ChangShanXian', '常山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330824', 'KaiHuaXian', '开化县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330825', 'LongYouXian', '龙游县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330881', 'JiangShanShi', '江山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3308DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '330900', 'ZhouShan', '舟山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330902', 'DingHaiQu', '定海区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330903', 'PuTuoQu', '普陀区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330921', 'DaiShanXian', '岱山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '330922', 'ShengSiXian', '嵊泗县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3309DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '331000', 'TaiZhou', '台州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331002', 'JiaoJiangQu', '椒江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331003', 'HuangYanQu', '黄岩区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331004', 'LuQiaoQu', '路桥区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331021', 'YuHuanXian', '玉环县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331022', 'SanMenXian', '三门县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331023', 'TianTaiXian', '天台县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331024', 'XianJuXian', '仙居县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331081', 'WenLingShi', '温岭市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331082', 'LinHaiShi', '临海市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3310DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '331100', 'LiShui', '丽水');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331102', 'LianDuQu', '莲都区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331121', 'QingTianXian', '青田县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331122', 'JinYunXian', '缙云县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331123', 'SuiChangXian', '遂昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331124', 'SongYangXian', '松阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331125', 'YunHeXian', '云和县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331126', 'QingYuanXian', '庆元县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331127', 'JingNingSheZuZiZhiXian', '景宁畲族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '331181', 'LongQuanShi', '龙泉市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3311DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '33CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '33CCDD', 'QiTa', '其他');

/*****************************浙江 结束*****************************/

/*****************************安徽 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '340000', 'AnHui', '安徽', '皖');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '340100', 'HeFei', '合肥');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340102', 'YaoHaiQu', '瑶海区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340103', 'LuYangQu', '庐阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340104', 'ShuShanQu', '蜀山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340111', 'BaoHeQu', '包河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340121', 'ChangFengXian', '长丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340122', 'FeiDongXian', '肥东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340123', 'FeiXiXian', '肥西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340124', 'LuJiangXian', '庐江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340181', 'ChaoHuShi', '巢湖市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3401DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '340200', 'WuHu', '芜湖');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340202', 'JingHuQu', '镜湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340203', 'YiJiangQu', '弋江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340207', 'JiuJiangQu', '鸠江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340208', 'SanShanQu', '三山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340221', 'WuHuXian', '芜湖县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340222', 'FanChangXian', '繁昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340223', 'NanLingXian', '南陵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340225', 'WuWeiXian', '无为县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3402DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '340300', 'BengBu', '蚌埠');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340302', 'LongZiHuQu', '龙子湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340303', 'BangShanQu', '蚌山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340304', 'YuHuiQu', '禹会区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340311', 'HuaiShangQu', '淮上区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340321', 'HuaiYuanXian', '怀远县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340322', 'WuHeXian', '五河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340323', 'GuZhenXian', '固镇县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3403DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '340400', 'HuaiNan', '淮南');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340402', 'DaTongQu', '大通区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340403', 'TianJiaAnQu', '田家庵区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340404', 'XieJiaJiQu', '谢家集区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340405', 'BaGongShanQu', '八公山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340406', 'PanJiQu', '潘集区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340421', 'FengTaiXian', '凤台县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3404DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '340500', 'MaAnShan', '马鞍山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340503', 'HuaShanQu', '花山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340504', 'YuShanQu', '雨山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340506', 'BoWangQu', '博望区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340521', 'DangTuXian', '当涂县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340522', 'HanShanXian', '含山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340523', 'HeXian', '和县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3405DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '340600', 'HuaiBei', '淮北');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340602', 'DuJiQu', '杜集区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340603', 'XiangShanQu', '相山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340604', 'LieShanQu', '烈山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340621', 'SuiXiXian', '濉溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3406DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '340700', 'TongLing', '铜陵');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340702', 'TongGuanShanQu', '铜官山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340703', 'ShiZiShanQu', '狮子山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340711', 'JiaoQu', '郊区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340721', 'TongLingXian', '铜陵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3407DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '340800', 'AnQing', '安庆');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340802', 'YingJiangQu', '迎江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340803', 'DaGuanQu', '大观区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340811', 'YiXiuQu', '宜秀区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340822', 'HuaiNingXian', '怀宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340823', 'ZongYangXian', '枞阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340824', 'QianShanXian', '潜山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340825', 'TaiHuXian', '太湖县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340826', 'SuSongXian', '宿松县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340827', 'WangJiangXian', '望江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340828', 'YueXiXian', '岳西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '340881', 'TongChengShi', '桐城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3408DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '341000', 'HuangShan', '黄山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341002', 'TunXiQu', '屯溪区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341003', 'HuangShanQu', '黄山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341004', 'HuiZhouQu', '徽州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341021', 'SheXian', '歙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341022', 'XiuNingXian', '休宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341023', 'YiXian', '黟县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341024', 'QiMenXian', '祁门县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3410DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '341100', 'ChuZhou', '滁州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341102', 'LangYaQu', '琅琊区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341103', 'NanQiaoQu', '南谯区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341122', 'LaiAnXian', '来安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341124', 'QuanJiaoXian', '全椒县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341125', 'DingYuanXian', '定远县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341126', 'FengYangXian', '凤阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341181', 'TianChangShi', '天长市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341182', 'MingGuangShi', '明光市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3411DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '341200', 'FuYang', '阜阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341202', 'YingZhouQu', '颍州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341203', 'YingDongQu', '颍东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341204', 'YingQuanQu', '颍泉区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341221', 'LinQuanXian', '临泉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341222', 'TaiHeXian', '太和县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341225', 'FuNanXian', '阜南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341226', 'YingShangXian', '颍上县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341282', 'JieShouShi', '界首市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3412DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '341300', 'SuZhou', '宿州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341302', 'YongQiaoQu', '埇桥区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341321', 'DangShanXian', '砀山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341322', 'XiaoXian', '萧县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341323', 'LingBiXian', '灵璧县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341324', 'SiXian', '泗县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3413DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '341500', 'LiuAn', '六安');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341502', 'JinAnQu', '金安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341503', 'YuAnQu', '裕安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341521', 'ShouXian', '寿县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341522', 'HuoQiuXian', '霍邱县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341523', 'ShuChengXian', '舒城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341524', 'JinZhaiXian', '金寨县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341525', 'HuoShanXian', '霍山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3415DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '341600', 'BoZhou', '亳州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341602', 'QiaoChengQu', '谯城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341621', 'WoYangXian', '涡阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341622', 'MengChengXian', '蒙城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341623', 'LiXinXian', '利辛县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3416DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '341700', 'ChiZhou', '池州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341702', 'GuiChiQu', '贵池区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341721', 'DongZhiXian', '东至县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341722', 'ShiTaiXian', '石台县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341723', 'QingYangXian', '青阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3417DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '341800', 'XuanCheng', '宣城');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341802', 'XuanZhouQu', '宣州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341821', 'LangXiXian', '郎溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341822', 'GuangDeXian', '广德县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341823', 'JingXian', '泾县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341824', 'JiXiXian', '绩溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341825', 'JingDeXian', '旌德县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '341881', 'NingGuoShi', '宁国市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3418DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '34CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '34CCDD', 'QiTa', '其他');

/*****************************安徽 结束*****************************/

/*****************************福建 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '350000', 'FuJian', '福建', '闽');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '350100', 'FuZhou', '福州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350102', 'GuLouQu', '鼓楼区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350103', 'TaiJiangQu', '台江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350104', 'CangShanQu', '仓山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350105', 'MaWeiQu', '马尾区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350111', 'JinAnQu', '晋安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350121', 'MinHouXian', '闽侯县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350122', 'LianJiangXian', '连江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350123', 'LuoYuanXian', '罗源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350124', 'MinQingXian', '闽清县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350125', 'YongTaiXian', '永泰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350128', 'PingTanXian', '平潭县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350181', 'FuQingShi', '福清市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350182', 'ChangLeShi', '长乐市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3501DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '350200', 'XiaMen', '厦门');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350203', 'SiMingQu', '思明区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350205', 'HaiCangQu', '海沧区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350206', 'HuLiQu', '湖里区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350211', 'JiMeiQu', '集美区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350212', 'TongAnQu', '同安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350213', 'XiangAnQu', '翔安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3502DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '350300', 'PuTian', '莆田');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350302', 'ChengXiangQu', '城厢区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350303', 'HanJiangQu', '涵江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350304', 'LiChengQu', '荔城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350305', 'XiuYuQu', '秀屿区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350322', 'XianYouXian', '仙游县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3503DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '350400', 'SanMing', '三明');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350402', 'MeiLieQu', '梅列区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350403', 'SanYuanQu', '三元区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350421', 'MingXiXian', '明溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350423', 'QingLiuXian', '清流县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350424', 'NingHuaXian', '宁化县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350425', 'DaTianXian', '大田县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350426', 'YouXiXian', '尤溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350427', 'ShaXian', '沙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350428', 'JiangLeXian', '将乐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350429', 'TaiNingXian', '泰宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350430', 'JianNingXian', '建宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350481', 'YongAnShi', '永安市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3504DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '350500', 'QuanZhou', '泉州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350502', 'LiChengQu', '鲤城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350503', 'FengZeQu', '丰泽区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350504', 'LuoJiangQu', '洛江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350505', 'QuanGangQu', '泉港区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350521', 'HuiAnXian', '惠安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350524', 'AnXiXian', '安溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350525', 'YongChunXian', '永春县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350526', 'DeHuaXian', '德化县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350527', 'JinMenXian', '金门县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350581', 'ShiShiShi', '石狮市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350582', 'JinJiangShi', '晋江市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350583', 'NanAnShi', '南安市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3505DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '350600', 'ZhangZhou', '漳州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350602', 'XiangChengQu', '芗城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350603', 'LongWenQu', '龙文区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350622', 'YunXiaoXian', '云霄县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350623', 'ZhangPuXian', '漳浦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350624', 'ZhaoAnXian', '诏安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350625', 'ChangTaiXian', '长泰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350626', 'DongShanXian', '东山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350627', 'NanJingXian', '南靖县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350628', 'PingHeXian', '平和县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350629', 'HuaAnXian', '华安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350681', 'LongHaiShi', '龙海市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3506DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '350700', 'NanPing', '南平');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350702', 'YanPingQu', '延平区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350703', 'JianYangQu', '建阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350721', 'ShunChangXian', '顺昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350722', 'PuChengXian', '浦城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350723', 'GuangZeXian', '光泽县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350724', 'SongXiXian', '松溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350725', 'ZhengHeXian', '政和县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350781', 'ShaoWuShi', '邵武市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350782', 'WuYiShanShi', '武夷山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350783', 'JianOuShi', '建瓯市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3507DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '350800', 'LongYan', '龙岩');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350802', 'XinLuoQu', '新罗区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350803', 'YongDingQu', '永定区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350821', 'ChangTingXian', '长汀县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350823', 'ShangHangXian', '上杭县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350824', 'WuPingXian', '武平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350825', 'LianChengXian', '连城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350881', 'ZhangPingShi', '漳平市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3508DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '350900', 'NingDe', '宁德');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350902', 'JiaoChengQu', '蕉城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350921', 'XiaPuXian', '霞浦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350922', 'GuTianXian', '古田县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350923', 'PingNanXian', '屏南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350924', 'ShouNingXian', '寿宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350925', 'ZhouNingXian', '周宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350926', 'ZheRongXian', '柘荣县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350981', 'FuAnShi', '福安市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '350982', 'FuDingShi', '福鼎市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3509DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '35CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '35CCDD', 'QiTa', '其他');

/*****************************福建 结束*****************************/

/*****************************江西 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '360000', 'JiangXi', '江西', '赣');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '360100', 'NanChang', '南昌');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360102', 'DongHuQu', '东湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360103', 'XiHuQu', '西湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360104', 'QingYunPuQu', '青云谱区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360105', 'WanLiQu', '湾里区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360111', 'QingShanHuQu', '青山湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360112', 'XinJianQu', '新建区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360121', 'NanChangXian', '南昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360123', 'AnYiXian', '安义县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360124', 'JinXianXian', '进贤县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3601DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '360200', 'JingDeZhen', '景德镇');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360202', 'ChangJiangQu', '昌江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360203', 'ZhuShanQu', '珠山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360222', 'FuLiangXian', '浮梁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360281', 'LePingShi', '乐平市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3602DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '360300', 'PingXiang', '萍乡');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360302', 'AnYuanQu', '安源区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360313', 'XiangDongQu', '湘东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360321', 'LianHuaXian', '莲花县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360322', 'ShangLiXian', '上栗县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360323', 'LuXiXian', '芦溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3603DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '360400', 'JiuJiang', '九江');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360402', 'LuShanQu', '庐山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360403', 'XunYangQu', '浔阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360421', 'JiuJiangXian', '九江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360423', 'WuNingXian', '武宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360424', 'XiuShuiXian', '修水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360425', 'YongXiuXian', '永修县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360426', 'DeAnXian', '德安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360427', 'XingZiXian', '星子县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360428', 'DuChangXian', '都昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360429', 'HuKouXian', '湖口县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360430', 'PengZeXian', '彭泽县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360481', 'RuiChangShi', '瑞昌市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360482', 'GongQingChengShi', '共青城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3604DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '360500', 'XinYu', '新余');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360502', 'YuShuiQu', '渝水区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360521', 'FenYiXian', '分宜县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3605DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '360600', 'YingTan', '鹰潭');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360602', 'YueHuQu', '月湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360622', 'YuJiangXian', '余江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360681', 'GuiXiShi', '贵溪市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3606DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '360700', 'GanZhou', '赣州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360702', 'ZhangGongQu', '章贡区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360703', 'NanKangQu', '南康区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360721', 'GanXian', '赣县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360722', 'XinFengXian', '信丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360723', 'DaYuXian', '大余县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360724', 'ShangYouXian', '上犹县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360725', 'ChongYiXian', '崇义县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360726', 'AnYuanXian', '安远县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360727', 'LongNanXian', '龙南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360728', 'DingNanXian', '定南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360729', 'QuanNanXian', '全南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360730', 'NingDuXian', '宁都县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360731', 'YuDuXian', '于都县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360732', 'XingGuoXian', '兴国县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360733', 'HuiChangXian', '会昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360734', 'XunWuXian', '寻乌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360735', 'ShiChengXian', '石城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360781', 'RuiJinShi', '瑞金市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3607DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '360800', 'JiAn', '吉安');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360802', 'JiZhouQu', '吉州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360803', 'QingYuanQu', '青原区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360821', 'JiAnXian', '吉安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360822', 'JiShuiXian', '吉水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360823', 'XiaJiangXian', '峡江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360824', 'XinGanXian', '新干县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360825', 'YongFengXian', '永丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360826', 'TaiHeXian', '泰和县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360827', 'SuiChuanXian', '遂川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360828', 'WanAnXian', '万安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360829', 'AnFuXian', '安福县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360830', 'YongXinXian', '永新县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360881', 'JingGangShanShi', '井冈山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3608DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '360900', 'YiChun', '宜春');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360902', 'YuanZhouQu', '袁州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360921', 'FengXinXian', '奉新县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360922', 'WanZaiXian', '万载县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360923', 'ShangGaoXian', '上高县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360924', 'YiFengXian', '宜丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360925', 'JingAnXian', '靖安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360926', 'TongGuXian', '铜鼓县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360981', 'FengChengShi', '丰城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360982', 'ZhangShuShi', '樟树市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '360983', 'GaoAnShi', '高安市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3609DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '361000', 'FuZhou', '抚州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361002', 'LinChuanQu', '临川区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361021', 'NanChengXian', '南城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361022', 'LiChuanXian', '黎川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361023', 'NanFengXian', '南丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361024', 'ChongRenXian', '崇仁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361025', 'LeAnXian', '乐安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361026', 'YiHuangXian', '宜黄县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361027', 'JinXiXian', '金溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361028', 'ZiXiXian', '资溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361029', 'DongXiangXian', '东乡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361030', 'GuangChangXian', '广昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3610DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '361100', 'ShangRao', '上饶');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361102', 'XinZhouQu', '信州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361103', 'GuangFengQu', '广丰区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361121', 'ShangRaoXian', '上饶县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361123', 'YuShanXian', '玉山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361124', 'QianShanXian', '铅山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361125', 'HengFengXian', '横峰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361126', 'YiYangXian', '弋阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361127', 'YuGanXian', '余干县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361128', 'PoYangXian', '鄱阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361129', 'WanNianXian', '万年县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361130', 'WuYuanXian', '婺源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '361181', 'DeXingShi', '德兴市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3611DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '36CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '36CCDD', 'QiTa', '其他');

/*****************************江西 结束*****************************/

/*****************************山东 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '370000', 'ShanDong', '山东', '鲁');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '370100', 'JiNan', '济南');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370102', 'LiXiaQu', '历下区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370103', 'ShiZhongQu', '市中区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370104', 'HuaiYinQu', '槐荫区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370105', 'TianQiaoQu', '天桥区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370112', 'LiChengQu', '历城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370113', 'ChangQingQu', '长清区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370124', 'PingYinXian', '平阴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370125', 'JiYangXian', '济阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370126', 'ShangHeXian', '商河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370181', 'ZhangQiuShi', '章丘市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3701DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '370200', 'QingDao', '青岛');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370202', 'ShiNanQu', '市南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370203', 'ShiBeiQu', '市北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370211', 'HuangDaoQu', '黄岛区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370212', 'LaoShanQu', '崂山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370213', 'LiCangQu', '李沧区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370214', 'ChengYangQu', '城阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370281', 'JiaoZhouShi', '胶州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370282', 'JiMoShi', '即墨市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370283', 'PingDuShi', '平度市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370285', 'LaiXiShi', '莱西市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3702DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '370300', 'ZiBo', '淄博');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370302', 'ZiChuanQu', '淄川区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370303', 'ZhangDianQu', '张店区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370304', 'BoShanQu', '博山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370305', 'LinZiQu', '临淄区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370306', 'ZhouCunQu', '周村区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370321', 'HuanTaiXian', '桓台县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370322', 'GaoQingXian', '高青县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370323', 'YiYuanXian', '沂源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3703DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '370400', 'ZaoZhuang', '枣庄');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370402', 'ShiZhongQu', '市中区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370403', 'XueChengQu', '薛城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370404', 'YiChengQu', '峄城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370405', 'TaiErZhuangQu', '台儿庄区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370406', 'ShanTingQu', '山亭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370481', 'TengZhouShi', '滕州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3704DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '370500', 'DongYing', '东营');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370502', 'DongYingQu', '东营区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370503', 'HeKouQu', '河口区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370521', 'KenLiXian', '垦利县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370522', 'LiJinXian', '利津县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370523', 'GuangRaoXian', '广饶县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3705DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '370600', 'YanTai', '烟台');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370602', 'ZhiFuQu', '芝罘区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370611', 'FuShanQu', '福山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370612', 'MouPingQu', '牟平区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370613', 'LaiShanQu', '莱山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370634', 'ChangDaoXian', '长岛县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370681', 'LongKouShi', '龙口市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370682', 'LaiYangShi', '莱阳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370683', 'LaiZhouShi', '莱州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370684', 'PengLaiShi', '蓬莱市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370685', 'ZhaoYuanShi', '招远市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370686', 'QiXiaShi', '栖霞市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370687', 'HaiYangShi', '海阳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3706DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '370700', 'WeiFang', '潍坊');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370702', 'WeiChengQu', '潍城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370703', 'HanTingQu', '寒亭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370704', 'FangZiQu', '坊子区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370705', 'KuiWenQu', '奎文区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370724', 'LinQuXian', '临朐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370725', 'ChangLeXian', '昌乐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370781', 'QingZhouShi', '青州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370782', 'ZhuChengShi', '诸城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370783', 'ShouGuangShi', '寿光市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370784', 'AnQiuShi', '安丘市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370785', 'GaoMiShi', '高密市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370786', 'ChangYiShi', '昌邑市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3707DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '370800', 'JiNing', '济宁');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370811', 'RenChengQu', '任城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370812', 'YanZhouQu', '兖州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370826', 'WeiShanXian', '微山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370827', 'YuTaiXian', '鱼台县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370828', 'JinXiangXian', '金乡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370829', 'JiaXiangXian', '嘉祥县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370830', 'WenShangXian', '汶上县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370831', 'SiShuiXian', '泗水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370832', 'LiangShanXian', '梁山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370881', 'QuFuShi', '曲阜市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370883', 'ZouChengShi', '邹城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3708DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '370900', 'TaiAn', '泰安');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370902', 'TaiShanQu', '泰山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370911', 'DaiYueQu', '岱岳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370921', 'NingYangXian', '宁阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370923', 'DongPingXian', '东平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370982', 'XinTaiShi', '新泰市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '370983', 'FeiChengShi', '肥城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3709DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '371000', 'WeiHai', '威海');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371002', 'HuanCuiQu', '环翠区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371003', 'WenDengQu', '文登区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371082', 'RongChengShi', '荣成市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371083', 'RuShanShi', '乳山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3710DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '371100', 'RiZhao', '日照');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371102', 'DongGangQu', '东港区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371103', 'LanShanQu', '岚山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371121', 'WuLianXian', '五莲县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371122', 'JuXian', '莒县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3711DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '371200', 'LaiWu', '莱芜');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371202', 'LaiChengQu', '莱城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371203', 'GangChengQu', '钢城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3712DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '371300', 'LinYi', '临沂');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371302', 'LanShanQu', '兰山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371311', 'LuoZhuangQu', '罗庄区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371312', 'HeDongQu', '河东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371321', 'YiNanXian', '沂南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371322', 'TanChengXian', '郯城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371323', 'YiShuiXian', '沂水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371324', 'LanLingXian', '兰陵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371325', 'FeiXian', '费县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371326', 'PingYiXian', '平邑县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371327', 'JuNanXian', '莒南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371328', 'MengYinXian', '蒙阴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371329', 'LinShuXian', '临沭县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3713DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '371400', 'DeZhou', '德州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371402', 'DeChengQu', '德城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371403', 'LingChengQu', '陵城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371422', 'NingJinXian', '宁津县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371423', 'QingYunXian', '庆云县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371424', 'LinYiXian', '临邑县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371425', 'QiHeXian', '齐河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371426', 'PingYuanXian', '平原县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371427', 'XiaJinXian', '夏津县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371428', 'WuChengXian', '武城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371481', 'LeLingShi', '乐陵市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371482', 'YuChengShi', '禹城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3714DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '371500', 'LiaoCheng', '聊城');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371502', 'DongChangFuQu', '东昌府区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371521', 'YangGuXian', '阳谷县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371522', 'XinXian', '莘县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371523', 'ChiPingXian', '茌平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371524', 'DongAXian', '东阿县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371525', 'GuanXian', '冠县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371526', 'GaoTangXian', '高唐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371581', 'LinQingShi', '临清市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3715DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '371600', 'BinZhou', '滨州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371602', 'BinChengQu', '滨城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371603', 'ZhanHuaQu', '沾化区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371621', 'HuiMinXian', '惠民县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371622', 'YangXinXian', '阳信县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371623', 'WuDiXian', '无棣县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371625', 'BoXingXian', '博兴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371626', 'ZouPingXian', '邹平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3716DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '371700', 'HeZe', '菏泽');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371702', 'MuDanQu', '牡丹区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371721', 'CaoXian', '曹县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371722', 'ShanXian', '单县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371723', 'ChengWuXian', '成武县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371724', 'JuYeXian', '巨野县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371725', 'YunChengXian', '郓城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371726', 'JuanChengXian', '鄄城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371727', 'DingTaoXian', '定陶县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '371728', 'DongMingXian', '东明县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '3717DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '37CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '37CCDD', 'QiTa', '其他');

/*****************************山东 结束*****************************/

/*****************************河南 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '410000', 'HeNan', '河南', '豫');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '410100', 'ZhengZhou', '郑州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410102', 'ZhongYuanQu', '中原区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410103', 'ErQiQu', '二七区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410104', 'GuanChengHuiZuQu', '管城回族区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410105', 'JinShuiQu', '金水区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410106', 'ShangJieQu', '上街区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410108', 'HuiJiQu', '惠济区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410122', 'ZhongMouXian', '中牟县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410181', 'GongYiShi', '巩义市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410182', 'YingYangShi', '荥阳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410183', 'XinMiShi', '新密市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410184', 'XinZhengShi', '新郑市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410185', 'DengFengShi', '登封市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4101DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '410200', 'KaiFeng', '开封');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410202', 'LongTingQu', '龙亭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410203', 'ShunHeHuiZuQu', '顺河回族区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410204', 'GuLouQu', '鼓楼区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410205', 'YuWangTaiQu', '禹王台区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410211', 'JinMingQu', '金明区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410212', 'XiangFuQu', '祥符区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410221', 'QiXian', '杞县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410222', 'TongXuXian', '通许县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410223', 'WeiShiXian', '尉氏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410225', 'LanKaoXian', '兰考县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4102DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '410300', 'LuoYang', '洛阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410302', 'LaoChengQu', '老城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410303', 'XiGongQu', '西工区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410304', 'ChanHeHuiZuQu', '瀍河回族区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410305', 'JianXiQu', '涧西区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410306', 'JiLiQu', '吉利区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410311', 'LuoLongQu', '洛龙区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410322', 'MengJinXian', '孟津县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410323', 'XinAnXian', '新安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410324', 'LuanChuanXian', '栾川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410325', 'SongXian', '嵩县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410326', 'RuYangXian', '汝阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410327', 'YiYangXian', '宜阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410328', 'LuoNingXian', '洛宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410329', 'YiChuanXian', '伊川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410381', 'YanShiShi', '偃师市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4103DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '410400', 'PingDingShan', '平顶山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410402', 'XinHuaQu', '新华区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410403', 'WeiDongQu', '卫东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410404', 'ShiLongQu', '石龙区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410411', 'ZhanHeQu', '湛河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410421', 'BaoFengXian', '宝丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410422', 'YeXian', '叶县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410423', 'LuShanXian', '鲁山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410425', 'JiaXian', '郏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410481', 'WuGangShi', '舞钢市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410482', 'RuZhouShi', '汝州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4104DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '410500', 'AnYang', '安阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410502', 'WenFengQu', '文峰区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410503', 'BeiGuanQu', '北关区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410505', 'YinDuQu', '殷都区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410506', 'LongAnQu', '龙安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410522', 'AnYangXian', '安阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410523', 'TangYinXian', '汤阴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410526', 'HuaXian', '滑县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410527', 'NeiHuangXian', '内黄县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410581', 'LinZhouShi', '林州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4105DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '410600', 'HeBi', '鹤壁');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410602', 'HeShanQu', '鹤山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410603', 'ShanChengQu', '山城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410611', 'QiBinQu', '淇滨区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410621', 'JunXian', '浚县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410622', 'QiXian', '淇县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4106DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '410700', 'XinXiang', '新乡');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410702', 'HongQiQu', '红旗区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410703', 'WeiBinQu', '卫滨区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410704', 'FengQuanQu', '凤泉区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410711', 'MuYeQu', '牧野区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410721', 'XinXiangXian', '新乡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410724', 'HuoJiaXian', '获嘉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410725', 'YuanYangXian', '原阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410726', 'YanJinXian', '延津县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410727', 'FengQiuXian', '封丘县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410728', 'ChangYuanXian', '长垣县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410781', 'WeiHuiShi', '卫辉市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410782', 'HuiXianShi', '辉县市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4107DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '410800', 'JiaoZuo', '焦作');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410802', 'JieFangQu', '解放区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410803', 'ZhongZhanQu', '中站区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410804', 'MaCunQu', '马村区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410811', 'ShanYangQu', '山阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410821', 'XiuWuXian', '修武县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410822', 'BoAiXian', '博爱县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410823', 'WuZhiXian', '武陟县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410825', 'WenXian', '温县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410882', 'QinYangShi', '沁阳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410883', 'MengZhouShi', '孟州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4108DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '410900', 'PuYang', '濮阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410902', 'HuaLongQu', '华龙区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410922', 'QingFengXian', '清丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410923', 'NanLeXian', '南乐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410926', 'FanXian', '范县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410927', 'TaiQianXian', '台前县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '410928', 'PuYangXian', '濮阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4109DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '411000', 'XuChang', '许昌');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411002', 'WeiDuQu', '魏都区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411023', 'XuChangXian', '许昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411024', 'YanLingXian', '鄢陵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411025', 'XiangChengXian', '襄城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411081', 'YuZhouShi', '禹州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411082', 'ChangGeShi', '长葛市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4110DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '411100', 'LuoHe', '漯河');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411102', 'YuanHuiQu', '源汇区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411103', 'YanChengQu', '郾城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411104', 'ZhaoLingQu', '召陵区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411121', 'WuYangXian', '舞阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411122', 'LinYingXian', '临颍县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4111DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '411200', 'SanMenXia', '三门峡');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411202', 'HuBinQu', '湖滨区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411221', 'MianChiXian', '渑池县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411222', 'ShanXian', '陕县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411224', 'LuShiXian', '卢氏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411281', 'YiMaShi', '义马市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411282', 'LingBaoShi', '灵宝市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4112DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '411300', 'NanYang', '南阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411302', 'WanChengQu', '宛城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411303', 'WoLongQu', '卧龙区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411321', 'NanZhaoXian', '南召县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411322', 'FangChengXian', '方城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411323', 'XiXiaXian', '西峡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411324', 'ZhenPingXian', '镇平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411325', 'NeiXiangXian', '内乡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411326', 'XiChuanXian', '淅川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411327', 'SheQiXian', '社旗县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411328', 'TangHeXian', '唐河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411329', 'XinYeXian', '新野县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411330', 'TongBaiXian', '桐柏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411381', 'DengZhouShi', '邓州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4113DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '411400', 'ShangQiu', '商丘');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411402', 'LiangYuanQu', '梁园区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411403', 'SuiYangQu', '睢阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411421', 'MinQuanXian', '民权县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411422', 'SuiXian', '睢县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411423', 'NingLingXian', '宁陵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411424', 'ZheChengXian', '柘城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411425', 'YuChengXian', '虞城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411426', 'XiaYiXian', '夏邑县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411481', 'YongChengShi', '永城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4114DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '411500', 'XinYang', '信阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411502', 'ShiHeQu', '浉河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411503', 'PingQiaoQu', '平桥区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411521', 'LuoShanXian', '罗山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411522', 'GuangShanXian', '光山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411523', 'XinXian', '新县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411524', 'ShangChengXian', '商城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411525', 'GuShiXian', '固始县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411526', 'HuangChuanXian', '潢川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411527', 'HuaiBinXian', '淮滨县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411528', 'XiXian', '息县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4115DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '411600', 'ZhouKou', '周口');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411602', 'ChuanHuiQu', '川汇区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411621', 'FuGouXian', '扶沟县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411622', 'XiHuaXian', '西华县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411623', 'ShangShuiXian', '商水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411624', 'ShenQiuXian', '沈丘县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411625', 'DanChengXian', '郸城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411626', 'HuaiYangXian', '淮阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411627', 'TaiKangXian', '太康县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411628', 'LuYiXian', '鹿邑县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411681', 'XiangChengShi', '项城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4116DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '411700', 'ZhuMaDian', '驻马店');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411702', 'YiChengQu', '驿城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411721', 'XiPingXian', '西平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411722', 'ShangCaiXian', '上蔡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411723', 'PingYuXian', '平舆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411724', 'ZhengYangXian', '正阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411725', 'QueShanXian', '确山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411726', 'BiYangXian', '泌阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411727', 'RuNanXian', '汝南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411728', 'SuiPingXian', '遂平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '411729', 'XinCaiXian', '新蔡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4117DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '419001', 'JiYuan', '济源');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '419001DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '41CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '41CCDD', 'QiTa', '其他');

/*****************************河南 结束*****************************/

/*****************************湖北 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '420000', 'HuBei', '湖北', '鄂');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '420100', 'WuHan', '武汉');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420102', 'JiangAnQu', '江岸区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420103', 'JiangHanQu', '江汉区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420104', 'QiaoKouQu', '硚口区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420105', 'HanYangQu', '汉阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420106', 'WuChangQu', '武昌区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420107', 'QingShanQu', '青山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420111', 'HongShanQu', '洪山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420112', 'DongXiHuQu', '东西湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420113', 'HanNanQu', '汉南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420114', 'CaiDianQu', '蔡甸区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420115', 'JiangXiaQu', '江夏区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420116', 'HuangPiQu', '黄陂区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420117', 'XinZhouQu', '新洲区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4201DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '420200', 'HuangShi', '黄石');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420202', 'HuangShiGangQu', '黄石港区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420203', 'XiSaiShanQu', '西塞山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420204', 'XiaLuQu', '下陆区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420205', 'TieShanQu', '铁山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420222', 'YangXinXian', '阳新县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420281', 'DaYeShi', '大冶市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4202DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '420300', 'ShiYan', '十堰');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420302', 'MaoJianQu', '茅箭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420303', 'ZhangWanQu', '张湾区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420304', 'YunYangQu', '郧阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420322', 'YunXiXian', '郧西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420323', 'ZhuShanXian', '竹山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420324', 'ZhuXiXian', '竹溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420325', 'FangXian', '房县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420381', 'DanJiangKouShi', '丹江口市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4203DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '420500', 'YiChang', '宜昌');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420502', 'XiLingQu', '西陵区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420503', 'WuJiaGangQu', '伍家岗区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420504', 'DianJunQu', '点军区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420505', 'YaoTingQu', '猇亭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420506', 'YiLingQu', '夷陵区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420525', 'YuanAnXian', '远安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420526', 'XingShanXian', '兴山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420527', 'ZiGuiXian', '秭归县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420528', 'ChangYangTuJiaZuZiZhiXian', '长阳土家族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420529', 'WuFengTuJiaZuZiZhiXian', '五峰土家族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420581', 'YiDuShi', '宜都市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420582', 'DangYangShi', '当阳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420583', 'ZhiJiangShi', '枝江市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4205DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '420600', 'XiangYang', '襄阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420602', 'XiangChengQu', '襄城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420606', 'FanChengQu', '樊城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420607', 'XiangZhouQu', '襄州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420624', 'NanZhangXian', '南漳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420625', 'GuChengXian', '谷城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420626', 'BaoKangXian', '保康县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420682', 'LaoHeKouShi', '老河口市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420683', 'ZaoYangShi', '枣阳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420684', 'YiChengShi', '宜城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4206DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '420700', 'EZhou', '鄂州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420702', 'LiangZiHuQu', '梁子湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420703', 'HuaRongQu', '华容区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420704', 'EChengQu', '鄂城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4207DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '420800', 'JingMen', '荆门');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420802', 'DongBaoQu', '东宝区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420804', 'DuoDaoQu', '掇刀区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420821', 'JingShanXian', '京山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420822', 'ShaYangXian', '沙洋县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420881', 'ZhongXiangShi', '钟祥市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4208DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '420900', 'XiaoGan', '孝感');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420902', 'XiaoNanQu', '孝南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420921', 'XiaoChangXian', '孝昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420922', 'DaWuXian', '大悟县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420923', 'YunMengXian', '云梦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420981', 'YingChengShi', '应城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420982', 'AnLuShi', '安陆市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '420984', 'HanChuanShi', '汉川市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4209DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '421000', 'JingZhou', '荆州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421002', 'ShaShiQu', '沙市区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421003', 'JingZhouQu', '荆州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421022', 'GongAnXian', '公安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421023', 'JianLiXian', '监利县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421024', 'JiangLingXian', '江陵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421081', 'ShiShouShi', '石首市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421083', 'HongHuShi', '洪湖市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421087', 'SongZiShi', '松滋市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4210DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '421100', 'HuangGang', '黄冈');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421102', 'HuangZhouQu', '黄州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421121', 'TuanFengXian', '团风县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421122', 'HongAnXian', '红安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421123', 'LuoTianXian', '罗田县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421124', 'YingShanXian', '英山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421125', 'XiShuiXian', '浠水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421126', 'QiChunXian', '蕲春县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421127', 'HuangMeiXian', '黄梅县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421181', 'MaChengShi', '麻城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421182', 'WuXueShi', '武穴市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4211DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '421200', 'XianNing', '咸宁');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421202', 'XianAnQu', '咸安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421221', 'JiaYuXian', '嘉鱼县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421222', 'TongChengXian', '通城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421223', 'ChongYangXian', '崇阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421224', 'TongShanXian', '通山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421281', 'ChiBiShi', '赤壁市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4212DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '421300', 'SuiZhou', '随州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421303', 'CengDuQu', '曾都区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421321', 'SuiXian', '随县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '421381', 'GuangShuiShi', '广水市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4213DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '422800', 'EnShi', '恩施');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '422801', 'EnShiShi', '恩施市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '422802', 'LiChuanShi', '利川市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '422822', 'JianShiXian', '建始县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '422823', 'BaDongXian', '巴东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '422825', 'XuanEnXian', '宣恩县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '422826', 'XianFengXian', '咸丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '422827', 'LaiFengXian', '来凤县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '422828', 'HeFengXian', '鹤峰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4228DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '429004', 'XianTao', '仙桃');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '429004DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '429005', 'QianJiang', '潜江');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '429005DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '429006', 'TianMen', '天门');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '429006DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '429021', 'ShenNongJiaLinQu', '神农架林区');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '429021DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '42CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '42CCDD', 'QiTa', '其他');

/*****************************湖北 结束*****************************/

/*****************************湖南 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '430000', 'HuNan', '湖南', '湘');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '430100', 'ChangSha', '长沙');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430102', 'FuRongQu', '芙蓉区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430103', 'TianXinQu', '天心区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430104', 'YueLuQu', '岳麓区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430105', 'KaiFuQu', '开福区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430111', 'YuHuaQu', '雨花区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430112', 'WangChengQu', '望城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430121', 'ChangShaXian', '长沙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430124', 'NingXiangXian', '宁乡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430181', 'LiuYangShi', '浏阳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4301DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '430200', 'ZhuZhou', '株洲');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430202', 'HeTangQu', '荷塘区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430203', 'LuSongQu', '芦淞区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430204', 'ShiFengQu', '石峰区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430211', 'TianYuanQu', '天元区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430221', 'ZhuZhouXian', '株洲县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430223', 'YouXian', '攸县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430224', 'ChaLingXian', '茶陵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430225', 'YanLingXian', '炎陵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430281', 'LiLingShi', '醴陵市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4302DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '430300', 'XiangTan', '湘潭');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430302', 'YuHuQu', '雨湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430304', 'YueTangQu', '岳塘区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430321', 'XiangTanXian', '湘潭县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430381', 'XiangXiangShi', '湘乡市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430382', 'ShaoShanShi', '韶山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4303DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '430400', 'HengYang', '衡阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430405', 'ZhuHuiQu', '珠晖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430406', 'YanFengQu', '雁峰区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430407', 'ShiGuQu', '石鼓区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430408', 'ZhengXiangQu', '蒸湘区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430412', 'NanYueQu', '南岳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430421', 'HengYangXian', '衡阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430422', 'HengNanXian', '衡南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430423', 'HengShanXian', '衡山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430424', 'HengDongXian', '衡东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430426', 'QiDongXian', '祁东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430481', 'LeiYangShi', '耒阳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430482', 'ChangNingShi', '常宁市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4304DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '430500', 'ShaoYang', '邵阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430502', 'ShuangQingQu', '双清区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430503', 'DaXiangQu', '大祥区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430511', 'BeiTaQu', '北塔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430521', 'ShaoDongXian', '邵东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430522', 'XinShaoXian', '新邵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430523', 'ShaoYangXian', '邵阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430524', 'LongHuiXian', '隆回县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430525', 'DongKouXian', '洞口县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430527', 'SuiNingXian', '绥宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430528', 'XinNingXian', '新宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430529', 'ChengBuMiaoZuZiZhiXian', '城步苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430581', 'WuGangShi', '武冈市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4305DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '430600', 'YueYang', '岳阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430602', 'YueYangLouQu', '岳阳楼区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430603', 'YunXiQu', '云溪区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430611', 'JunShanQu', '君山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430621', 'YueYangXian', '岳阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430623', 'HuaRongXian', '华容县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430624', 'XiangYinXian', '湘阴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430626', 'PingJiangXian', '平江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430681', 'MiLuoShi', '汨罗市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430682', 'LinXiangShi', '临湘市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4306DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '430700', 'ChangDe', '常德');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430702', 'WuLingQu', '武陵区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430703', 'DingChengQu', '鼎城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430721', 'AnXiangXian', '安乡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430722', 'HanShouXian', '汉寿县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430723', 'LiXian', '澧县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430724', 'LinLiXian', '临澧县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430725', 'TaoYuanXian', '桃源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430726', 'ShiMenXian', '石门县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430781', 'JinShiShi', '津市市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4307DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '430800', 'ZhangJiaJie', '张家界');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430802', 'YongDingQu', '永定区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430811', 'WuLingYuanQu', '武陵源区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430821', 'CiLiXian', '慈利县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430822', 'SangZhiXian', '桑植县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4308DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '430900', 'YiYang', '益阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430902', 'ZiYangQu', '资阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430903', 'HeShanQu', '赫山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430921', 'NanXian', '南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430922', 'TaoJiangXian', '桃江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430923', 'AnHuaXian', '安化县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '430981', 'YuanJiangShi', '沅江市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4309DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '431000', 'ChenZhou', '郴州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431002', 'BeiHuQu', '北湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431003', 'SuXianQu', '苏仙区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431021', 'GuiYangXian', '桂阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431022', 'YiZhangXian', '宜章县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431023', 'YongXingXian', '永兴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431024', 'JiaHeXian', '嘉禾县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431025', 'LinWuXian', '临武县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431026', 'RuChengXian', '汝城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431027', 'GuiDongXian', '桂东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431028', 'AnRenXian', '安仁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431081', 'ZiXingShi', '资兴市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4310DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '431100', 'YongZhou', '永州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431102', 'LingLingQu', '零陵区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431103', 'LengShuiTanQu', '冷水滩区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431121', 'QiYangXian', '祁阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431122', 'DongAnXian', '东安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431123', 'ShuangPaiXian', '双牌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431124', 'DaoXian', '道县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431125', 'JiangYongXian', '江永县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431126', 'NingYuanXian', '宁远县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431127', 'LanShanXian', '蓝山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431128', 'XinTianXian', '新田县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431129', 'JiangHuaYaoZuZiZhiXian', '江华瑶族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4311DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '431200', 'HuaiHua', '怀化');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431202', 'HeChengQu', '鹤城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431221', 'ZhongFangXian', '中方县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431222', 'YuanLingXian', '沅陵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431223', 'ChenXiXian', '辰溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431224', 'XuPuXian', '溆浦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431225', 'HuiTongXian', '会同县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431226', 'MaYangMiaoZuZiZhiXian', '麻阳苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431227', 'XinHuangDongZuZiZhiXian', '新晃侗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431228', 'ZhiJiangDongZuZiZhiXian', '芷江侗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431229', 'JingZhouMiaoZuDongZuZiZhiXian', '靖州苗族侗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431230', 'TongDaoDongZuZiZhiXian', '通道侗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431281', 'HongJiangShi', '洪江市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4312DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '431300', 'LouDi', '娄底');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431302', 'LouXingQu', '娄星区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431321', 'ShuangFengXian', '双峰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431322', 'XinHuaXian', '新化县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431381', 'LengShuiJiangShi', '冷水江市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '431382', 'LianYuanShi', '涟源市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4313DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '433100', 'XiangXi', '湘西');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '433101', 'JiShouShi', '吉首市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '433122', 'LuXiXian', '泸溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '433123', 'FengHuangXian', '凤凰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '433124', 'HuaYuanXian', '花垣县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '433125', 'BaoJingXian', '保靖县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '433126', 'GuZhangXian', '古丈县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '433127', 'YongShunXian', '永顺县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '433130', 'LongShanXian', '龙山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4331DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '43CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '43CCDD', 'QiTa', '其他');

/*****************************湖南 结束*****************************/

/*****************************广东 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '440000', 'GuangDong', '广东', '粤');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '440100', 'GuangZhou', '广州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440103', 'LiWanQu', '荔湾区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440104', 'YueXiuQu', '越秀区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440105', 'HaiZhuQu', '海珠区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440106', 'TianHeQu', '天河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440111', 'BaiYunQu', '白云区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440112', 'HuangPuQu', '黄埔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440113', 'FanYuQu', '番禺区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440114', 'HuaDuQu', '花都区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440115', 'NanShaQu', '南沙区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440117', 'CongHuaQu', '从化区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440118', 'ZengChengQu', '增城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4401DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '440200', 'ShaoGuan', '韶关');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440203', 'WuJiangQu', '武江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440204', 'ZhenJiangQu', '浈江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440205', 'QuJiangQu', '曲江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440222', 'ShiXingXian', '始兴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440224', 'RenHuaXian', '仁化县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440229', 'WengYuanXian', '翁源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440232', 'RuYuanYaoZuZiZhiXian', '乳源瑶族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440233', 'XinFengXian', '新丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440281', 'LeChangShi', '乐昌市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440282', 'NanXiongShi', '南雄市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4402DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '440300', 'ShenZhen', '深圳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440303', 'LuoHuQu', '罗湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440304', 'FuTianQu', '福田区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440305', 'NanShanQu', '南山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440306', 'BaoAnQu', '宝安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440307', 'LongGangQu', '龙岗区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440308', 'YanTianQu', '盐田区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4403DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '440400', 'ZhuHai', '珠海');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440402', 'XiangZhouQu', '香洲区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440403', 'DouMenQu', '斗门区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440404', 'JinWanQu', '金湾区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4404DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '440500', 'ShanTou', '汕头');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440507', 'LongHuQu', '龙湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440511', 'JinPingQu', '金平区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440512', 'HaoJiangQu', '濠江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440513', 'ChaoYangQu', '潮阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440514', 'ChaoNanQu', '潮南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440515', 'ChengHaiQu', '澄海区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440523', 'NanAoXian', '南澳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4405DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '440600', 'FoShan', '佛山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440604', 'ChanChengQu', '禅城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440605', 'NanHaiQu', '南海区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440606', 'ShunDeQu', '顺德区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440607', 'SanShuiQu', '三水区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440608', 'GaoMingQu', '高明区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4406DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '440700', 'JiangMen', '江门');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440703', 'PengJiangQu', '蓬江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440704', 'JiangHaiQu', '江海区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440705', 'XinHuiQu', '新会区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440781', 'TaiShanShi', '台山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440783', 'KaiPingShi', '开平市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440784', 'HeShanShi', '鹤山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440785', 'EnPingShi', '恩平市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4407DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '440800', 'ZhanJiang', '湛江');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440802', 'ChiKanQu', '赤坎区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440803', 'XiaShanQu', '霞山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440804', 'PoTouQu', '坡头区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440811', 'MaZhangQu', '麻章区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440823', 'SuiXiXian', '遂溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440825', 'XuWenXian', '徐闻县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440881', 'LianJiangShi', '廉江市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440882', 'LeiZhouShi', '雷州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440883', 'WuChuanShi', '吴川市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4408DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '440900', 'MaoMing', '茂名');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440902', 'MaoNanQu', '茂南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440904', 'DianBaiQu', '电白区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440981', 'GaoZhouShi', '高州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440982', 'HuaZhouShi', '化州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '440983', 'XinYiShi', '信宜市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4409DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '441200', 'ZhaoQing', '肇庆');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441202', 'DuanZhouQu', '端州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441203', 'DingHuQu', '鼎湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441204', 'GaoYaoQu', '高要区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441223', 'GuangNingXian', '广宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441224', 'HuaiJiXian', '怀集县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441225', 'FengKaiXian', '封开县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441226', 'DeQingXian', '德庆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441284', 'SiHuiShi', '四会市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4412DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '441300', 'HuiZhou', '惠州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441302', 'HuiChengQu', '惠城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441303', 'HuiYangQu', '惠阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441322', 'BoLuoXian', '博罗县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441323', 'HuiDongXian', '惠东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441324', 'LongMenXian', '龙门县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4413DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '441400', 'MeiZhou', '梅州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441402', 'MeiJiangQu', '梅江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441403', 'MeiXianQu', '梅县区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441422', 'DaBuXian', '大埔县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441423', 'FengShunXian', '丰顺县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441424', 'WuHuaXian', '五华县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441426', 'PingYuanXian', '平远县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441427', 'JiaoLingXian', '蕉岭县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441481', 'XingNingShi', '兴宁市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4414DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '441500', 'ShanWei', '汕尾');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441502', 'ChengQu', '城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441521', 'HaiFengXian', '海丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441523', 'LuHeXian', '陆河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441581', 'LuFengShi', '陆丰市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4415DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '441600', 'HeYuan', '河源');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441602', 'YuanChengQu', '源城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441621', 'ZiJinXian', '紫金县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441622', 'LongChuanXian', '龙川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441623', 'LianPingXian', '连平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441624', 'HePingXian', '和平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441625', 'DongYuanXian', '东源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4416DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '441700', 'YangJiang', '阳江');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441702', 'JiangChengQu', '江城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441704', 'YangDongQu', '阳东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441721', 'YangXiXian', '阳西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441781', 'YangChunShi', '阳春市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4417DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '441800', 'QingYuan', '清远');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441802', 'QingChengQu', '清城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441803', 'QingXinQu', '清新区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441821', 'FoGangXian', '佛冈县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441823', 'YangShanXian', '阳山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441825', 'LianShanZhuangZuYaoZuZiZhiXian', '连山壮族瑶族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441826', 'LianNanYaoZuZiZhiXian', '连南瑶族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441881', 'YingDeShi', '英德市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '441882', 'LianZhouShi', '连州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4418DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '441900', 'DongGuan', '东莞');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4419DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '442000', 'ZhongShan', '中山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4420DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '445100', 'ChaoZhou', '潮州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '445102', 'XiangQiaoQu', '湘桥区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '445103', 'ChaoAnQu', '潮安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '445122', 'RaoPingXian', '饶平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4451DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '445200', 'JieYang', '揭阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '445202', 'RongChengQu', '榕城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '445203', 'JieDongQu', '揭东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '445222', 'JieXiXian', '揭西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '445224', 'HuiLaiXian', '惠来县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '445281', 'PuNingShi', '普宁市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4452DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '445300', 'YunFu', '云浮');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '445302', 'YunChengQu', '云城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '445303', 'YunAnQu', '云安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '445321', 'XinXingXian', '新兴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '445322', 'YuNanXian', '郁南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '445381', 'LuoDingShi', '罗定市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4453DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '44CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '44CCDD', 'QiTa', '其他');

/*****************************广东 结束*****************************/

/*****************************广西 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '450000', 'GuangXi', '广西', '桂');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '450100', 'NanNing', '南宁');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450102', 'XingNingQu', '兴宁区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450103', 'QingXiuQu', '青秀区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450105', 'JiangNanQu', '江南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450107', 'XiXiangTangQu', '西乡塘区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450108', 'LiangQingQu', '良庆区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450109', 'YongNingQu', '邕宁区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450110', 'WuMingQu', '武鸣区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450123', 'LongAnXian', '隆安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450124', 'MaShanXian', '马山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450125', 'ShangLinXian', '上林县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450126', 'BinYangXian', '宾阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450127', 'HengXian', '横县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4501DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '450200', 'LiuZhou', '柳州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450202', 'ChengZhongQu', '城中区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450203', 'YuFengQu', '鱼峰区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450204', 'LiuNanQu', '柳南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450205', 'LiuBeiQu', '柳北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450221', 'LiuJiangXian', '柳江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450222', 'LiuChengXian', '柳城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450223', 'LuZhaiXian', '鹿寨县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450224', 'RongAnXian', '融安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450225', 'RongShuiMiaoZuZiZhiXian', '融水苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450226', 'SanJiangDongZuZiZhiXian', '三江侗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4502DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '450300', 'GuiLin', '桂林');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450302', 'XiuFengQu', '秀峰区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450303', 'DieCaiQu', '叠彩区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450304', 'XiangShanQu', '象山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450305', 'QiXingQu', '七星区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450311', 'YanShanQu', '雁山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450312', 'LinGuiQu', '临桂区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450321', 'YangShuoXian', '阳朔县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450323', 'LingChuanXian', '灵川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450324', 'QuanZhouXian', '全州县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450325', 'XingAnXian', '兴安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450326', 'YongFuXian', '永福县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450327', 'GuanYangXian', '灌阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450328', 'LongShengGeZuZiZhiXian', '龙胜各族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450329', 'ZiYuanXian', '资源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450330', 'PingLeXian', '平乐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450331', 'LiPuXian', '荔浦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450332', 'GongChengYaoZuZiZhiXian', '恭城瑶族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4503DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '450400', 'WuZhou', '梧州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450403', 'WanXiuQu', '万秀区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450405', 'ChangZhouQu', '长洲区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450406', 'LongWeiQu', '龙圩区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450421', 'CangWuXian', '苍梧县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450422', 'TengXian', '藤县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450423', 'MengShanXian', '蒙山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450481', 'CenXiShi', '岑溪市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4504DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '450500', 'BeiHai', '北海');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450502', 'HaiChengQu', '海城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450503', 'YinHaiQu', '银海区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450512', 'TieShanGangQu', '铁山港区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450521', 'HePuXian', '合浦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4505DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '450600', 'FangChengGang', '防城港');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450602', 'GangKouQu', '港口区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450603', 'FangChengQu', '防城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450621', 'ShangSiXian', '上思县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450681', 'DongXingShi', '东兴市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4506DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '450700', 'QinZhou', '钦州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450702', 'QinNanQu', '钦南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450703', 'QinBeiQu', '钦北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450721', 'LingShanXian', '灵山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450722', 'PuBeiXian', '浦北县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4507DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '450800', 'GuiGang', '贵港');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450802', 'GangBeiQu', '港北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450803', 'GangNanQu', '港南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450804', 'TanTangQu', '覃塘区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450821', 'PingNanXian', '平南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450881', 'GuiPingShi', '桂平市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4508DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '450900', 'YuLin', '玉林');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450902', 'YuZhouQu', '玉州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450903', 'FuMianQu', '福绵区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450921', 'RongXian', '容县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450922', 'LuChuanXian', '陆川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450923', 'BoBaiXian', '博白县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450924', 'XingYeXian', '兴业县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '450981', 'BeiLiuShi', '北流市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4509DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '451000', 'BaiSe', '百色');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451002', 'YouJiangQu', '右江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451021', 'TianYangXian', '田阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451022', 'TianDongXian', '田东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451023', 'PingGuoXian', '平果县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451024', 'DeBaoXian', '德保县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451026', 'NaPoXian', '那坡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451027', 'LingYunXian', '凌云县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451028', 'LeYeXian', '乐业县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451029', 'TianLinXian', '田林县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451030', 'XiLinXian', '西林县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451031', 'LongLinGeZuZiZhiXian', '隆林各族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451081', 'JingXiShi', '靖西市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4510DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '451100', 'HeZhou', '贺州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451102', 'BaBuQu', '八步区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451121', 'ZhaoPingXian', '昭平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451122', 'ZhongShanXian', '钟山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451123', 'FuChuanYaoZuZiZhiXian', '富川瑶族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4511DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '451200', 'HeChi', '河池');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451202', 'JinChengJiangQu', '金城江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451221', 'NanDanXian', '南丹县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451222', 'TianEXian', '天峨县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451223', 'FengShanXian', '凤山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451224', 'DongLanXian', '东兰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451225', 'LuoChengMuLaoZuZiZhiXian', '罗城仫佬族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451226', 'HuanJiangMaoNanZuZiZhiXian', '环江毛南族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451227', 'BaMaYaoZuZiZhiXian', '巴马瑶族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451228', 'DuAnYaoZuZiZhiXian', '都安瑶族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451229', 'DaHuaYaoZuZiZhiXian', '大化瑶族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451281', 'YiZhouShi', '宜州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4512DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '451300', 'LaiBin', '来宾');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451302', 'XingBinQu', '兴宾区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451321', 'XinChengXian', '忻城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451322', 'XiangZhouXian', '象州县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451323', 'WuXuanXian', '武宣县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451324', 'JinXiuYaoZuZiZhiXian', '金秀瑶族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451381', 'HeShanShi', '合山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4513DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '451400', 'ChongZuo', '崇左');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451402', 'JiangZhouQu', '江州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451421', 'FuSuiXian', '扶绥县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451422', 'NingMingXian', '宁明县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451423', 'LongZhouXian', '龙州县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451424', 'DaXinXian', '大新县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451425', 'TianDengXian', '天等县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '451481', 'PingXiangShi', '凭祥市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4514DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '45CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '45CCDD', 'QiTa', '其他');

/*****************************广西 结束*****************************/

/*****************************海南 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '460000', 'HaiNan', '海南', '琼');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '460100', 'HaiKou', '海口');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '460105', 'XiuYingQu', '秀英区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '460106', 'LongHuaQu', '龙华区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '460107', 'QiongShanQu', '琼山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '460108', 'MeiLanQu', '美兰区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4601DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '460200', 'SanYa', '三亚');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '460202', 'HaiTangQu', '海棠区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '460203', 'JiYangQu', '吉阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '460204', 'TianYaQu', '天涯区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '460205', 'YaZhouQu', '崖州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4602DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '460300', 'SanSha', '三沙');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '460321', 'XiShaQunDao', '西沙群岛');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '460322', 'NanShaQunDao', '南沙群岛');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '460323', 'ZhongShaQunDaoDeDaoJiaoJiQiHaiYu', '中沙群岛的岛礁及其海域');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '4603DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469001', 'WuZhiShan', '五指山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469001DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469002', 'QiongHai', '琼海');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469002DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469003', 'DanZhou', '儋州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469003DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469005', 'WenChang', '文昌');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469005DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469006', 'WanNing', '万宁');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469006DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469007', 'DongFang', '东方');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469007DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469021', 'DingAnXian', '定安县');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469021DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469022', 'TunChangXian', '屯昌县');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469022DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469023', 'ChengMaiXian', '澄迈县');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469023DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469024', 'LinGaoXian', '临高县');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469024DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469025', 'BaiSha', '白沙');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469025DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469026', 'ChangJiang', '昌江');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469026DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469027', 'LeDong', '乐东');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469027DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469028', 'LingShui', '陵水');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469028DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469029', 'BaoTing', '保亭');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469029DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '469030', 'QiongZhong', '琼中');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '469030DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '46CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '46CCDD', 'QiTa', '其他');

/*****************************海南 结束*****************************/

/*****************************重庆 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '500000', 'ChongQing', '重庆', '渝');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '500100', 'ChongQing', '重庆');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500101', 'WanZhouQu', '万州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500102', 'FuLingQu', '涪陵区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500103', 'YuZhongQu', '渝中区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500104', 'DaDuKouQu', '大渡口区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500105', 'JiangBeiQu', '江北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500106', 'ShaPingBaQu', '沙坪坝区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500107', 'JiuLongPoQu', '九龙坡区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500108', 'NanAnQu', '南岸区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500109', 'BeiBeiQu', '北碚区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500110', 'QiJiangQu', '綦江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500111', 'DaZuQu', '大足区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500112', 'YuBeiQu', '渝北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500113', 'BaNanQu', '巴南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500114', 'QianJiangQu', '黔江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500115', 'ChangShouQu', '长寿区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500116', 'JiangJinQu', '江津区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500117', 'HeChuanQu', '合川区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500118', 'YongChuanQu', '永川区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500119', 'NanChuanQu', '南川区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500120', 'BiShanQu', '璧山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500151', 'TongLiangQu', '铜梁区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500152', 'TongNanQu', '潼南区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500153', 'RongChangQu', '荣昌区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500228', 'LiangPingXian', '梁平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500229', 'ChengKouXian', '城口县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500230', 'FengDuXian', '丰都县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500231', 'DianJiangXian', '垫江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500232', 'WuLongXian', '武隆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500233', 'ZhongXian', '忠县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500234', 'KaiXian', '开县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500235', 'YunYangXian', '云阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500236', 'FengJieXian', '奉节县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500237', 'WuShanXian', '巫山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500238', 'WuXiXian', '巫溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500240', 'ShiZhuTuJiaZuZiZhiXian', '石柱土家族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500241', 'XiuShanTuJiaZuMiaoZuZiZhiXian', '秀山土家族苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500242', 'YouYangTuJiaZuMiaoZuZiZhiXian', '酉阳土家族苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '500243', 'PengShuiMiaoZuTuJiaZuZiZhiXian', '彭水苗族土家族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5001DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '50CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '50CCDD', 'QiTa', '其他');

/*****************************重庆 结束*****************************/

/*****************************四川 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '510000', 'SiChuan', '四川', '川');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '510100', 'ChengDu', '成都');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510104', 'JinJiangQu', '锦江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510105', 'QingYangQu', '青羊区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510106', 'JinNiuQu', '金牛区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510107', 'WuHouQu', '武侯区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510108', 'ChengHuaQu', '成华区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510112', 'LongQuanYiQu', '龙泉驿区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510113', 'QingBaiJiangQu', '青白江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510114', 'XinDuQu', '新都区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510115', 'WenJiangQu', '温江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510121', 'JinTangXian', '金堂县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510122', 'ShuangLiuXian', '双流县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510124', 'PiXian', '郫县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510129', 'DaYiXian', '大邑县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510131', 'PuJiangXian', '蒲江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510132', 'XinJinXian', '新津县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510181', 'DuJiangYanShi', '都江堰市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510182', 'PengZhouShi', '彭州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510183', 'QiongLaiShi', '邛崃市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510184', 'ChongZhouShi', '崇州市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5101DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '510300', 'ZiGong', '自贡');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510302', 'ZiLiuJingQu', '自流井区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510303', 'GongJingQu', '贡井区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510304', 'DaAnQu', '大安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510311', 'YanTanQu', '沿滩区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510321', 'RongXian', '荣县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510322', 'FuShunXian', '富顺县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5103DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '510400', 'PanZhiHua', '攀枝花');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510402', 'DongQu', '东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510403', 'XiQu', '西区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510411', 'RenHeQu', '仁和区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510421', 'MiYiXian', '米易县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510422', 'YanBianXian', '盐边县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5104DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '510500', 'LuZhou', '泸州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510502', 'JiangYangQu', '江阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510503', 'NaXiQu', '纳溪区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510504', 'LongMaTanQu', '龙马潭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510521', 'LuXian', '泸县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510522', 'HeJiangXian', '合江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510524', 'XuYongXian', '叙永县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510525', 'GuLinXian', '古蔺县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5105DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '510600', 'DeYang', '德阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510603', 'JingYangQu', '旌阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510623', 'ZhongJiangXian', '中江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510626', 'LuoJiangXian', '罗江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510681', 'GuangHanShi', '广汉市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510682', 'ShenFangShi', '什邡市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510683', 'MianZhuShi', '绵竹市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5106DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '510700', 'MianYang', '绵阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510703', 'FuChengQu', '涪城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510704', 'YouXianQu', '游仙区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510722', 'SanTaiXian', '三台县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510723', 'YanTingXian', '盐亭县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510724', 'AnXian', '安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510725', 'ZiTongXian', '梓潼县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510726', 'BeiChuanQiangZuZiZhiXian', '北川羌族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510727', 'PingWuXian', '平武县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510781', 'JiangYouShi', '江油市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5107DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '510800', 'GuangYuan', '广元');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510802', 'LiZhouQu', '利州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510811', 'ZhaoHuaQu', '昭化区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510812', 'ChaoTianQu', '朝天区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510821', 'WangCangXian', '旺苍县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510822', 'QingChuanXian', '青川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510823', 'JianGeXian', '剑阁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510824', 'CangXiXian', '苍溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5108DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '510900', 'SuiNing', '遂宁');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510903', 'ChuanShanQu', '船山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510904', 'AnJuQu', '安居区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510921', 'PengXiXian', '蓬溪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510922', 'SheHongXian', '射洪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '510923', 'DaYingXian', '大英县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5109DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '511000', 'NeiJiang', '内江');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511002', 'ShiZhongQu', '市中区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511011', 'DongXingQu', '东兴区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511024', 'WeiYuanXian', '威远县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511025', 'ZiZhongXian', '资中县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511028', 'LongChangXian', '隆昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5110DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '511100', 'LeShan', '乐山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511102', 'ShiZhongQu', '市中区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511111', 'ShaWanQu', '沙湾区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511112', 'WuTongQiaoQu', '五通桥区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511113', 'JinKouHeQu', '金口河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511123', 'JianWeiXian', '犍为县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511124', 'JingYanXian', '井研县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511126', 'JiaJiangXian', '夹江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511129', 'MuChuanXian', '沐川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511132', 'EBianYiZuZiZhiXian', '峨边彝族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511133', 'MaBianYiZuZiZhiXian', '马边彝族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511181', 'EMeiShanShi', '峨眉山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5111DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '511300', 'NanChong', '南充');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511302', 'ShunQingQu', '顺庆区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511303', 'GaoPingQu', '高坪区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511304', 'JiaLingQu', '嘉陵区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511321', 'NanBuXian', '南部县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511322', 'YingShanXian', '营山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511323', 'PengAnXian', '蓬安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511324', 'YiLongXian', '仪陇县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511325', 'XiChongXian', '西充县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511381', 'LangZhongShi', '阆中市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5113DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '511400', 'MeiShan', '眉山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511402', 'DongPoQu', '东坡区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511403', 'PengShanQu', '彭山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511421', 'RenShouXian', '仁寿县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511423', 'HongYaXian', '洪雅县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511424', 'DanLengXian', '丹棱县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511425', 'QingShenXian', '青神县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5114DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '511500', 'YiBin', '宜宾');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511502', 'CuiPingQu', '翠屏区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511503', 'NanXiQu', '南溪区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511521', 'YiBinXian', '宜宾县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511523', 'JiangAnXian', '江安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511524', 'ChangNingXian', '长宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511525', 'GaoXian', '高县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511526', 'GongXian', '珙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511527', 'YunLianXian', '筠连县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511528', 'XingWenXian', '兴文县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511529', 'PingShanXian', '屏山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5115DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '511600', 'GuangAn', '广安');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511602', 'GuangAnQu', '广安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511603', 'QianFengQu', '前锋区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511621', 'YueChiXian', '岳池县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511622', 'WuShengXian', '武胜县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511623', 'LinShuiXian', '邻水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511681', 'HuaYingShi', '华蓥市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5116DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '511700', 'DaZhou', '达州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511702', 'TongChuanQu', '通川区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511703', 'DaChuanQu', '达川区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511722', 'XuanHanXian', '宣汉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511723', 'KaiJiangXian', '开江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511724', 'DaZhuXian', '大竹县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511725', 'QuXian', '渠县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511781', 'WanYuanShi', '万源市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5117DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '511800', 'YaAn', '雅安');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511802', 'YuChengQu', '雨城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511803', 'MingShanQu', '名山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511822', 'YingJingXian', '荥经县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511823', 'HanYuanXian', '汉源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511824', 'ShiMianXian', '石棉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511825', 'TianQuanXian', '天全县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511826', 'LuShanXian', '芦山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511827', 'BaoXingXian', '宝兴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5118DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '511900', 'BaZhong', '巴中');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511902', 'BaZhouQu', '巴州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511903', 'EnYangQu', '恩阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511921', 'TongJiangXian', '通江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511922', 'NanJiangXian', '南江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '511923', 'PingChangXian', '平昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5119DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '512000', 'ZiYang', '资阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '512002', 'YanJiangQu', '雁江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '512021', 'AnYueXian', '安岳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '512022', 'LeZhiXian', '乐至县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '512081', 'JianYangShi', '简阳市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5120DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '513200', 'ABa', '阿坝');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513221', 'WenChuanXian', '汶川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513222', 'LiXian', '理县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513223', 'MaoXian', '茂县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513224', 'SongPanXian', '松潘县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513225', 'JiuZhaiGouXian', '九寨沟县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513226', 'JinChuanXian', '金川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513227', 'XiaoJinXian', '小金县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513228', 'HeiShuiXian', '黑水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513229', 'MaErKangXian', '马尔康县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513230', 'RangTangXian', '壤塘县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513231', 'ABaXian', '阿坝县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513232', 'RuoErGaiXian', '若尔盖县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513233', 'HongYuanXian', '红原县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5132DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '513300', 'GanZi', '甘孜');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513301', 'KangDingShi', '康定市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513322', 'LuDingXian', '泸定县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513323', 'DanBaXian', '丹巴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513324', 'JiuLongXian', '九龙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513325', 'YaJiangXian', '雅江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513326', 'DaoFuXian', '道孚县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513327', 'LuHuoXian', '炉霍县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513328', 'GanZiXian', '甘孜县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513329', 'XinLongXian', '新龙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513330', 'DeGeXian', '德格县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513331', 'BaiYuXian', '白玉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513332', 'ShiQuXian', '石渠县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513333', 'SeDaXian', '色达县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513334', 'LiTangXian', '理塘县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513335', 'BaTangXian', '巴塘县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513336', 'XiangChengXian', '乡城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513337', 'DaoChengXian', '稻城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513338', 'DeRongXian', '得荣县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5133DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '513400', 'LiangShan', '凉山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513401', 'XiChangShi', '西昌市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513422', 'MuLiZangZuZiZhiXian', '木里藏族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513423', 'YanYuanXian', '盐源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513424', 'DeChangXian', '德昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513425', 'HuiLiXian', '会理县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513426', 'HuiDongXian', '会东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513427', 'NingNanXian', '宁南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513428', 'PuGeXian', '普格县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513429', 'BuTuoXian', '布拖县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513430', 'JinYangXian', '金阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513431', 'ZhaoJueXian', '昭觉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513432', 'XiDeXian', '喜德县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513433', 'MianNingXian', '冕宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513434', 'YueXiXian', '越西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513435', 'GanLuoXian', '甘洛县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513436', 'MeiGuXian', '美姑县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '513437', 'LeiBoXian', '雷波县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5134DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '51CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '51CCDD', 'QiTa', '其他');

/*****************************四川 结束*****************************/

/*****************************贵州 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '520000', 'GuiZhou', '贵州', '贵');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '520100', 'GuiYang', '贵阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520102', 'NanMingQu', '南明区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520103', 'YunYanQu', '云岩区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520111', 'HuaXiQu', '花溪区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520112', 'WuDangQu', '乌当区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520113', 'BaiYunQu', '白云区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520115', 'GuanShanHuQu', '观山湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520121', 'KaiYangXian', '开阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520122', 'XiFengXian', '息烽县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520123', 'XiuWenXian', '修文县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520181', 'QingZhenShi', '清镇市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5201DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '520200', 'LiuPanShui', '六盘水');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520201', 'ZhongShanQu', '钟山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520203', 'LiuZhiTeQu', '六枝特区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520221', 'ShuiChengXian', '水城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520222', 'PanXian', '盘县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5202DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '520300', 'ZunYi', '遵义');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520302', 'HongHuaGangQu', '红花岗区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520303', 'HuiChuanQu', '汇川区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520321', 'ZunYiXian', '遵义县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520322', 'TongZiXian', '桐梓县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520323', 'SuiYangXian', '绥阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520324', 'ZhengAnXian', '正安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520325', 'DaoZhenGeLaoZuMiaoZuZiZhiXian', '道真仡佬族苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520326', 'WuChuanGeLaoZuMiaoZuZiZhiXian', '务川仡佬族苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520327', 'FengGangXian', '凤冈县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520328', 'MeiTanXian', '湄潭县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520329', 'YuQingXian', '余庆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520330', 'XiShuiXian', '习水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520381', 'ChiShuiShi', '赤水市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520382', 'RenHuaiShi', '仁怀市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5203DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '520400', 'AnShun', '安顺');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520402', 'XiXiuQu', '西秀区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520403', 'PingBaQu', '平坝区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520422', 'PuDingXian', '普定县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520423', 'ZhenNingBuYiZuMiaoZuZiZhiXian', '镇宁布依族苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520424', 'GuanLingBuYiZuMiaoZuZiZhiXian', '关岭布依族苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520425', 'ZiYunMiaoZuBuYiZuZiZhiXian', '紫云苗族布依族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5204DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '520500', 'BiJie', '毕节');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520502', 'QiXingGuanQu', '七星关区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520521', 'DaFangXian', '大方县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520522', 'QianXiXian', '黔西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520523', 'JinShaXian', '金沙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520524', 'ZhiJinXian', '织金县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520525', 'NaYongXian', '纳雍县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520526', 'WeiNingYiZuHuiZuMiaoZuZiZhiXian', '威宁彝族回族苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520527', 'HeZhangXian', '赫章县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5205DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '520600', 'TongRen', '铜仁');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520602', 'BiJiangQu', '碧江区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520603', 'WanShanQu', '万山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520621', 'JiangKouXian', '江口县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520622', 'YuPingDongZuZiZhiXian', '玉屏侗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520623', 'ShiQianXian', '石阡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520624', 'SiNanXian', '思南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520625', 'YinJiangTuJiaZuMiaoZuZiZhiXian', '印江土家族苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520626', 'DeJiangXian', '德江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520627', 'YanHeTuJiaZuZiZhiXian', '沿河土家族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '520628', 'SongTaoMiaoZuZiZhiXian', '松桃苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5206DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '522300', 'QianXiNan', '黔西南');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522301', 'XingYiShi', '兴义市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522322', 'XingRenXian', '兴仁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522323', 'PuAnXian', '普安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522324', 'QingLongXian', '晴隆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522325', 'ZhenFengXian', '贞丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522326', 'WangMoXian', '望谟县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522327', 'CeHengXian', '册亨县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522328', 'AnLongXian', '安龙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5223DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '522600', 'QianDongNan', '黔东南');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522601', 'KaiLiShi', '凯里市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522622', 'HuangPingXian', '黄平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522623', 'ShiBingXian', '施秉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522624', 'SanSuiXian', '三穗县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522625', 'ZhenYuanXian', '镇远县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522626', 'CenGongXian', '岑巩县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522627', 'TianZhuXian', '天柱县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522628', 'JinPingXian', '锦屏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522629', 'JianHeXian', '剑河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522630', 'TaiJiangXian', '台江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522631', 'LiPingXian', '黎平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522632', 'RongJiangXian', '榕江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522633', 'CongJiangXian', '从江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522634', 'LeiShanXian', '雷山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522635', 'MaJiangXian', '麻江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522636', 'DanZhaiXian', '丹寨县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5226DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '522700', 'QianNan', '黔南');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522701', 'DuYunShi', '都匀市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522702', 'FuQuanShi', '福泉市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522722', 'LiBoXian', '荔波县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522723', 'GuiDingXian', '贵定县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522725', 'WengAnXian', '瓮安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522726', 'DuShanXian', '独山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522727', 'PingTangXian', '平塘县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522728', 'LuoDianXian', '罗甸县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522729', 'ChangShunXian', '长顺县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522730', 'LongLiXian', '龙里县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522731', 'HuiShuiXian', '惠水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '522732', 'SanDuShuiZuZiZhiXian', '三都水族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5227DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '52CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '52CCDD', 'QiTa', '其他');

/*****************************贵州 结束*****************************/

/*****************************云南 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '530000', 'YunNan', '云南', '云');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '530100', 'KunMing', '昆明');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530102', 'WuHuaQu', '五华区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530103', 'PanLongQu', '盘龙区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530111', 'GuanDuQu', '官渡区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530112', 'XiShanQu', '西山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530113', 'DongChuanQu', '东川区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530114', 'ChengGongQu', '呈贡区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530122', 'JinNingXian', '晋宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530124', 'FuMinXian', '富民县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530125', 'YiLiangXian', '宜良县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530126', 'ShiLinYiZuZiZhiXian', '石林彝族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530127', 'SongMingXian', '嵩明县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530128', 'LuQuanYiZuMiaoZuZiZhiXian', '禄劝彝族苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530129', 'XunDianHuiZuYiZuZiZhiXian', '寻甸回族彝族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530181', 'AnNingShi', '安宁市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5301DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '530300', 'QuJing', '曲靖');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530302', 'QiLinQu', '麒麟区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530321', 'MaLongXian', '马龙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530322', 'LuLiangXian', '陆良县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530323', 'ShiZongXian', '师宗县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530324', 'LuoPingXian', '罗平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530325', 'FuYuanXian', '富源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530326', 'HuiZeXian', '会泽县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530328', 'ZhanYiXian', '沾益县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530381', 'XuanWeiShi', '宣威市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5303DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '530400', 'YuXi', '玉溪');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530402', 'HongTaQu', '红塔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530421', 'JiangChuanXian', '江川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530422', 'ChengJiangXian', '澄江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530423', 'TongHaiXian', '通海县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530424', 'HuaNingXian', '华宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530425', 'YiMenXian', '易门县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530426', 'EShanYiZuZiZhiXian', '峨山彝族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530427', 'XinPingYiZuDaiZuZiZhiXian', '新平彝族傣族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530428', 'YuanJiangHaNiZuYiZuDaiZuZiZhiXian', '元江哈尼族彝族傣族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5304DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '530500', 'BaoShan', '保山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530502', 'LongYangQu', '隆阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530521', 'ShiDianXian', '施甸县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530523', 'LongLingXian', '龙陵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530524', 'ChangNingXian', '昌宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530581', 'TengChongShi', '腾冲市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5305DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '530600', 'ZhaoTong', '昭通');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530602', 'ZhaoYangQu', '昭阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530621', 'LuDianXian', '鲁甸县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530622', 'QiaoJiaXian', '巧家县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530623', 'YanJinXian', '盐津县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530624', 'DaGuanXian', '大关县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530625', 'YongShanXian', '永善县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530626', 'SuiJiangXian', '绥江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530627', 'ZhenXiongXian', '镇雄县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530628', 'YiLiangXian', '彝良县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530629', 'WeiXinXian', '威信县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530630', 'ShuiFuXian', '水富县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5306DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '530700', 'LiJiang', '丽江');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530702', 'GuChengQu', '古城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530721', 'YuLongNaXiZuZiZhiXian', '玉龙纳西族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530722', 'YongShengXian', '永胜县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530723', 'HuaPingXian', '华坪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530724', 'NingLangYiZuZiZhiXian', '宁蒗彝族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5307DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '530800', 'PuEr', '普洱');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530802', 'SiMaoQu', '思茅区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530821', 'NingErHaNiZuYiZuZiZhiXian', '宁洱哈尼族彝族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530822', 'MoJiangHaNiZuZiZhiXian', '墨江哈尼族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530823', 'JingDongYiZuZiZhiXian', '景东彝族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530824', 'JingGuDaiZuYiZuZiZhiXian', '景谷傣族彝族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530825', 'ZhenYuanYiZuHaNiZuLaHuZuZiZhiXian', '镇沅彝族哈尼族拉祜族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530826', 'JiangChengHaNiZuYiZuZiZhiXian', '江城哈尼族彝族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530827', 'MengLianDaiZuLaHuZuWaZuZiZhiXian', '孟连傣族拉祜族佤族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530828', 'LanCangLaHuZuZiZhiXian', '澜沧拉祜族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530829', 'XiMengWaZuZiZhiXian', '西盟佤族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5308DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '530900', 'LinCang', '临沧');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530902', 'LinXiangQu', '临翔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530921', 'FengQingXian', '凤庆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530922', 'YunXian', '云县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530923', 'YongDeXian', '永德县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530924', 'ZhenKangXian', '镇康县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530925', 'ShuangJiangLaHuZuWaZuBuLangZuDaiZuZiZhiXian', '双江拉祜族佤族布朗族傣族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530926', 'GengMaDaiZuWaZuZiZhiXian', '耿马傣族佤族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '530927', 'CangYuanWaZuZiZhiXian', '沧源佤族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5309DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '532300', 'ChuXiong', '楚雄');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532301', 'ChuXiongShi', '楚雄市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532322', 'ShuangBaiXian', '双柏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532323', 'MouDingXian', '牟定县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532324', 'NanHuaXian', '南华县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532325', 'YaoAnXian', '姚安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532326', 'DaYaoXian', '大姚县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532327', 'YongRenXian', '永仁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532328', 'YuanMouXian', '元谋县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532329', 'WuDingXian', '武定县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532331', 'LuFengXian', '禄丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5323DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '532500', 'HongHe', '红河');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532501', 'GeJiuShi', '个旧市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532502', 'KaiYuanShi', '开远市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532503', 'MengZiShi', '蒙自市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532504', 'MiLeShi', '弥勒市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532523', 'PingBianMiaoZuZiZhiXian', '屏边苗族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532524', 'JianShuiXian', '建水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532525', 'ShiPingXian', '石屏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532527', 'LuXiXian', '泸西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532528', 'YuanYangXian', '元阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532529', 'HongHeXian', '红河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532530', 'JinPingMiaoZuYaoZuDaiZuZiZhiXian', '金平苗族瑶族傣族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532531', 'LvChunXian', '绿春县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532532', 'HeKouYaoZuZiZhiXian', '河口瑶族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5325DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '532600', 'WenShan', '文山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532601', 'WenShanShi', '文山市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532622', 'YanShanXian', '砚山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532623', 'XiChouXian', '西畴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532624', 'MaLiPoXian', '麻栗坡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532625', 'MaGuanXian', '马关县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532626', 'QiuBeiXian', '丘北县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532627', 'GuangNanXian', '广南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532628', 'FuNingXian', '富宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5326DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '532800', 'XiShuangBanNa', '西双版纳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532801', 'JingHongShi', '景洪市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532822', 'MengHaiXian', '勐海县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532823', 'MengLaXian', '勐腊县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5328DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '532900', 'DaLi', '大理');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532901', 'DaLiShi', '大理市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532922', 'YangBiYiZuZiZhiXian', '漾濞彝族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532923', 'XiangYunXian', '祥云县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532924', 'BinChuanXian', '宾川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532925', 'MiDuXian', '弥渡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532926', 'NanJianYiZuZiZhiXian', '南涧彝族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532927', 'WeiShanYiZuHuiZuZiZhiXian', '巍山彝族回族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532928', 'YongPingXian', '永平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532929', 'YunLongXian', '云龙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532930', 'ErYuanXian', '洱源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532931', 'JianChuanXian', '剑川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '532932', 'HeQingXian', '鹤庆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5329DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '533100', 'DeHong', '德宏');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '533102', 'RuiLiShi', '瑞丽市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '533103', 'MangShi', '芒市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '533122', 'LiangHeXian', '梁河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '533123', 'YingJiangXian', '盈江县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '533124', 'LongChuanXian', '陇川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5331DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '533300', 'NuJiang', '怒江');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '533321', 'LuShuiXian', '泸水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '533323', 'FuGongXian', '福贡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '533324', 'GongShanDuLongZuNuZuZiZhiXian', '贡山独龙族怒族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '533325', 'LanPingBaiZuPuMiZuZiZhiXian', '兰坪白族普米族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5333DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '533400', 'DiQing', '迪庆');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '533401', 'XiangGeLiLaShi', '香格里拉市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '533422', 'DeQinXian', '德钦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '533423', 'WeiXiLiSuZuZiZhiXian', '维西傈僳族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5334DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '53CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '53CCDD', 'QiTa', '其他');

/*****************************云南 结束*****************************/

/*****************************西藏 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '540000', 'XiZang', '西藏', '藏');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '540100', 'LaSa', '拉萨');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540102', 'ChengGuanQu', '城关区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540121', 'LinZhouXian', '林周县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540122', 'DangXiongXian', '当雄县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540123', 'NiMuXian', '尼木县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540124', 'QuShuiXian', '曲水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540125', 'DuiLongDeQingXian', '堆龙德庆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540126', 'DaZiXian', '达孜县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540127', 'MoZhuGongKaXian', '墨竹工卡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5401DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '540200', 'RiKaZe', '日喀则');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540202', 'SangZhuZiQu', '桑珠孜区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540221', 'NanMuLinXian', '南木林县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540222', 'JiangZiXian', '江孜县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540223', 'DingRiXian', '定日县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540224', 'SaJiaXian', '萨迦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540225', 'LaZiXian', '拉孜县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540226', 'AngRenXian', '昂仁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540227', 'XieTongMenXian', '谢通门县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540228', 'BaiLangXian', '白朗县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540229', 'RenBuXian', '仁布县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540230', 'KangMaXian', '康马县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540231', 'DingJieXian', '定结县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540232', 'ZhongBaXian', '仲巴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540233', 'YaDongXian', '亚东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540234', 'JiLongXian', '吉隆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540235', 'NieLaMuXian', '聂拉木县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540236', 'SaGaXian', '萨嘎县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540237', 'GangBaXian', '岗巴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5402DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '540300', 'ChangDu', '昌都');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540302', 'KaRuoQu', '卡若区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540321', 'JiangDaXian', '江达县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540322', 'GongJueXian', '贡觉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540323', 'LeiWuQiXian', '类乌齐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540324', 'DingQingXian', '丁青县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540325', 'ChaYaXian', '察雅县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540326', 'BaSuXian', '八宿县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540327', 'ZuoGongXian', '左贡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540328', 'MangKangXian', '芒康县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540329', 'LuoLongXian', '洛隆县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540330', 'BianBaXian', '边坝县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5403DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '540400', 'LinZhi', '林芝');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540402', 'BaYiQu', '巴宜区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540421', 'GongBuJiangDaXian', '工布江达县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540422', 'MiLinXian', '米林县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540423', 'MoTuoXian', '墨脱县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540424', 'BoMiXian', '波密县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540425', 'ChaYuXian', '察隅县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '540426', 'LangXian', '朗县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5404DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '542200', 'ShanNan', '山南');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542221', 'NaiDongXian', '乃东县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542222', 'ZhaNangXian', '扎囊县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542223', 'GongGaXian', '贡嘎县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542224', 'SangRiXian', '桑日县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542225', 'QiongJieXian', '琼结县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542226', 'QuSongXian', '曲松县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542227', 'CuoMeiXian', '措美县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542228', 'LuoZhaXian', '洛扎县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542229', 'JiaChaXian', '加查县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542231', 'LongZiXian', '隆子县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542232', 'CuoNaXian', '错那县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542233', 'LangQiaZiXian', '浪卡子县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5422DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '542400', 'NaQu', '那曲');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542421', 'NaQuXian', '那曲县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542422', 'JiaLiXian', '嘉黎县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542423', 'BiRuXian', '比如县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542424', 'NieRongXian', '聂荣县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542425', 'AnDuoXian', '安多县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542426', 'ShenZhaXian', '申扎县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542427', 'SuoXian', '索县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542428', 'BanGeXian', '班戈县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542429', 'BaQingXian', '巴青县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542430', 'NiMaXian', '尼玛县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542431', 'ShuangHuXian', '双湖县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5424DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '542500', 'ALi', '阿里');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542521', 'PuLanXian', '普兰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542522', 'ZhaDaXian', '札达县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542523', 'GaErXian', '噶尔县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542524', 'RiTuXian', '日土县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542525', 'GeJiXian', '革吉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542526', 'GaiZeXian', '改则县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '542527', 'CuoQinXian', '措勤县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '5425DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '54CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '54CCDD', 'QiTa', '其他');

/*****************************西藏 结束*****************************/

/*****************************陕西 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '610000', 'ShanXi', '陕西', '陕');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '610100', 'XiAn', '西安');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610102', 'XinChengQu', '新城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610103', 'BeiLinQu', '碑林区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610104', 'LianHuQu', '莲湖区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610111', 'BaQiaoQu', '灞桥区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610112', 'WeiYangQu', '未央区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610113', 'YanTaQu', '雁塔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610114', 'YanLiangQu', '阎良区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610115', 'LinTongQu', '临潼区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610116', 'ChangAnQu', '长安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610117', 'GaoLingQu', '高陵区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610122', 'LanTianXian', '蓝田县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610124', 'ZhouZhiXian', '周至县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610125', 'HuXian', '户县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6101DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '610200', 'TongChuan', '铜川');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610202', 'WangYiQu', '王益区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610203', 'YinTaiQu', '印台区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610204', 'YaoZhouQu', '耀州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610222', 'YiJunXian', '宜君县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6102DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '610300', 'BaoJi', '宝鸡');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610302', 'WeiBinQu', '渭滨区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610303', 'JinTaiQu', '金台区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610304', 'ChenCangQu', '陈仓区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610322', 'FengXiangXian', '凤翔县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610323', 'QiShanXian', '岐山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610324', 'FuFengXian', '扶风县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610326', 'MeiXian', '眉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610327', 'LongXian', '陇县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610328', 'QianYangXian', '千阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610329', 'LinYouXian', '麟游县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610330', 'FengXian', '凤县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610331', 'TaiBaiXian', '太白县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6103DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '610400', 'XianYang', '咸阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610402', 'QinDuQu', '秦都区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610403', 'YangLingQu', '杨陵区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610404', 'WeiChengQu', '渭城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610422', 'SanYuanXian', '三原县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610423', 'JingYangXian', '泾阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610424', 'QianXian', '乾县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610425', 'LiQuanXian', '礼泉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610426', 'YongShouXian', '永寿县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610427', 'BinXian', '彬县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610428', 'ChangWuXian', '长武县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610429', 'XunYiXian', '旬邑县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610430', 'ChunHuaXian', '淳化县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610431', 'WuGongXian', '武功县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610481', 'XingPingShi', '兴平市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6104DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '610500', 'WeiNan', '渭南');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610502', 'LinWeiQu', '临渭区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610521', 'HuaXian', '华县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610522', 'TongGuanXian', '潼关县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610523', 'DaLiXian', '大荔县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610524', 'HeYangXian', '合阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610525', 'ChengChengXian', '澄城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610526', 'PuChengXian', '蒲城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610527', 'BaiShuiXian', '白水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610528', 'FuPingXian', '富平县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610581', 'HanChengShi', '韩城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610582', 'HuaYinShi', '华阴市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6105DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '610600', 'YanAn', '延安');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610602', 'BaoTaQu', '宝塔区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610621', 'YanChangXian', '延长县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610622', 'YanChuanXian', '延川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610623', 'ZiChangXian', '子长县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610624', 'AnSaiXian', '安塞县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610625', 'ZhiDanXian', '志丹县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610626', 'WuQiXian', '吴起县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610627', 'GanQuanXian', '甘泉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610628', 'FuXian', '富县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610629', 'LuoChuanXian', '洛川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610630', 'YiChuanXian', '宜川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610631', 'HuangLongXian', '黄龙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610632', 'HuangLingXian', '黄陵县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6106DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '610700', 'HanZhong', '汉中');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610702', 'HanTaiQu', '汉台区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610721', 'NanZhengXian', '南郑县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610722', 'ChengGuXian', '城固县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610723', 'YangXian', '洋县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610724', 'XiXiangXian', '西乡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610725', 'MianXian', '勉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610726', 'NingQiangXian', '宁强县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610727', 'LveYangXian', '略阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610728', 'ZhenBaXian', '镇巴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610729', 'LiuBaXian', '留坝县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610730', 'FoPingXian', '佛坪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6107DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '610800', 'YuLin', '榆林');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610802', 'YuYangQu', '榆阳区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610821', 'ShenMuXian', '神木县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610822', 'FuGuXian', '府谷县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610823', 'HengShanXian', '横山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610824', 'JingBianXian', '靖边县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610825', 'DingBianXian', '定边县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610826', 'SuiDeXian', '绥德县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610827', 'MiZhiXian', '米脂县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610828', 'JiaXian', '佳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610829', 'WuBaoXian', '吴堡县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610830', 'QingJianXian', '清涧县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610831', 'ZiZhouXian', '子洲县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6108DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '610900', 'AnKang', '安康');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610902', 'HanBinQu', '汉滨区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610921', 'HanYinXian', '汉阴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610922', 'ShiQuanXian', '石泉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610923', 'NingShanXian', '宁陕县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610924', 'ZiYangXian', '紫阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610925', 'LanGaoXian', '岚皋县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610926', 'PingLiXian', '平利县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610927', 'ZhenPingXian', '镇坪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610928', 'XunYangXian', '旬阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '610929', 'BaiHeXian', '白河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6109DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '611000', 'ShangLuo', '商洛');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '611002', 'ShangZhouQu', '商州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '611021', 'LuoNanXian', '洛南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '611022', 'DanFengXian', '丹凤县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '611023', 'ShangNanXian', '商南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '611024', 'ShanYangXian', '山阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '611025', 'ZhenAnXian', '镇安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '611026', 'ZhaShuiXian', '柞水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6110DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '61CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '61CCDD', 'QiTa', '其他');

/*****************************陕西 结束*****************************/

/*****************************甘肃 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '620000', 'GanSu', '甘肃', '甘');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '620100', 'LanZhou', '兰州');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620102', 'ChengGuanQu', '城关区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620103', 'QiLiHeQu', '七里河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620104', 'XiGuQu', '西固区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620105', 'AnNingQu', '安宁区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620111', 'HongGuQu', '红古区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620121', 'YongDengXian', '永登县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620122', 'GaoLanXian', '皋兰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620123', 'YuZhongXian', '榆中县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6201DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '620200', 'JiaYuGuan', '嘉峪关');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6202DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '620300', 'JinChang', '金昌');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620302', 'JinChuanQu', '金川区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620321', 'YongChangXian', '永昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6203DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '620400', 'BaiYin', '白银');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620402', 'BaiYinQu', '白银区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620403', 'PingChuanQu', '平川区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620421', 'JingYuanXian', '靖远县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620422', 'HuiNingXian', '会宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620423', 'JingTaiXian', '景泰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6204DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '620500', 'TianShui', '天水');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620502', 'QinZhouQu', '秦州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620503', 'MaiJiQu', '麦积区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620521', 'QingShuiXian', '清水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620522', 'QinAnXian', '秦安县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620523', 'GanGuXian', '甘谷县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620524', 'WuShanXian', '武山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620525', 'ZhangJiaChuanHuiZuZiZhiXian', '张家川回族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6205DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '620600', 'WuWei', '武威');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620602', 'LiangZhouQu', '凉州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620621', 'MinQinXian', '民勤县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620622', 'GuLangXian', '古浪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620623', 'TianZhuZangZuZiZhiXian', '天祝藏族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6206DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '620700', 'ZhangYe', '张掖');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620702', 'GanZhouQu', '甘州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620721', 'SuNanYuGuZuZiZhiXian', '肃南裕固族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620722', 'MinYueXian', '民乐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620723', 'LinZeXian', '临泽县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620724', 'GaoTaiXian', '高台县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620725', 'ShanDanXian', '山丹县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6207DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '620800', 'PingLiang', '平凉');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620802', 'KongTongQu', '崆峒区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620821', 'JingChuanXian', '泾川县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620822', 'LingTaiXian', '灵台县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620823', 'ChongXinXian', '崇信县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620824', 'HuaTingXian', '华亭县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620825', 'ZhuangLangXian', '庄浪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620826', 'JingNingXian', '静宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6208DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '620900', 'JiuQuan', '酒泉');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620902', 'SuZhouQu', '肃州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620921', 'JinTaXian', '金塔县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620922', 'GuaZhouXian', '瓜州县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620923', 'SuBeiMengGuZuZiZhiXian', '肃北蒙古族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620924', 'AKeSaiHaSaKeZuZiZhiXian', '阿克塞哈萨克族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620981', 'YuMenShi', '玉门市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '620982', 'DunHuangShi', '敦煌市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6209DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '621000', 'QingYang', '庆阳');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621002', 'XiFengQu', '西峰区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621021', 'QingChengXian', '庆城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621022', 'HuanXian', '环县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621023', 'HuaChiXian', '华池县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621024', 'HeShuiXian', '合水县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621025', 'ZhengNingXian', '正宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621026', 'NingXian', '宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621027', 'ZhenYuanXian', '镇原县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6210DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '621100', 'DingXi', '定西');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621102', 'AnDingQu', '安定区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621121', 'TongWeiXian', '通渭县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621122', 'LongXiXian', '陇西县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621123', 'WeiYuanXian', '渭源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621124', 'LinTaoXian', '临洮县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621125', 'ZhangXian', '漳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621126', 'MinXian', '岷县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6211DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '621200', 'LongNan', '陇南');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621202', 'WuDuQu', '武都区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621221', 'ChengXian', '成县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621222', 'WenXian', '文县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621223', 'DangChangXian', '宕昌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621224', 'KangXian', '康县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621225', 'XiHeXian', '西和县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621226', 'LiXian', '礼县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621227', 'HuiXian', '徽县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '621228', 'LiangDangXian', '两当县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6212DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '622900', 'LinXia', '临夏');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '622901', 'LinXiaShi', '临夏市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '622921', 'LinXiaXian', '临夏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '622922', 'KangLeXian', '康乐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '622923', 'YongJingXian', '永靖县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '622924', 'GuangHeXian', '广河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '622925', 'HeZhengXian', '和政县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '622926', 'DongXiangZuZiZhiXian', '东乡族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '622927', 'JiShiShanBaoAnZuDongXiangZuSaLaZuZiZhiXian', '积石山保安族东乡族撒拉族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6229DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '623000', 'GanNan', '甘南');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '623001', 'HeZuoShi', '合作市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '623021', 'LinTanXian', '临潭县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '623022', 'ZhuoNiXian', '卓尼县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '623023', 'ZhouQuXian', '舟曲县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '623024', 'DieBuXian', '迭部县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '623025', 'MaQuXian', '玛曲县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '623026', 'LuQuXian', '碌曲县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '623027', 'XiaHeXian', '夏河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6230DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '62CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '62CCDD', 'QiTa', '其他');

/*****************************甘肃 结束*****************************/

/*****************************青海 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '630000', 'QingHai', '青海', '青');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '630100', 'XiNing', '西宁');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '630102', 'ChengDongQu', '城东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '630103', 'ChengZhongQu', '城中区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '630104', 'ChengXiQu', '城西区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '630105', 'ChengBeiQu', '城北区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '630121', 'DaTongHuiZuTuZuZiZhiXian', '大通回族土族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '630122', 'HuangZhongXian', '湟中县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '630123', 'HuangYuanXian', '湟源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6301DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '630200', 'HaiDong', '海东');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '630202', 'LeDuQu', '乐都区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '630203', 'PingAnQu', '平安区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '630222', 'MinHeHuiZuTuZuZiZhiXian', '民和回族土族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '630223', 'HuZhuTuZuZiZhiXian', '互助土族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '630224', 'HuaLongHuiZuZiZhiXian', '化隆回族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '630225', 'XunHuaSaLaZuZiZhiXian', '循化撒拉族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6302DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '632200', 'HaiBei', '海北');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632221', 'MenYuanHuiZuZiZhiXian', '门源回族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632222', 'QiLianXian', '祁连县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632223', 'HaiYanXian', '海晏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632224', 'GangChaXian', '刚察县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6322DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '632300', 'HuangNan', '黄南');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632321', 'TongRenXian', '同仁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632322', 'JianZhaXian', '尖扎县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632323', 'ZeKuXian', '泽库县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632324', 'HeNanMengGuZuZiZhiXian', '河南蒙古族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6323DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '632500', 'HaiNan', '海南');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632521', 'GongHeXian', '共和县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632522', 'TongDeXian', '同德县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632523', 'GuiDeXian', '贵德县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632524', 'XingHaiXian', '兴海县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632525', 'GuiNanXian', '贵南县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6325DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '632600', 'GuoLuo', '果洛');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632621', 'MaQinXian', '玛沁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632622', 'BanMaXian', '班玛县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632623', 'GanDeXian', '甘德县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632624', 'DaRiXian', '达日县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632625', 'JiuZhiXian', '久治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632626', 'MaDuoXian', '玛多县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6326DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '632700', 'YuShu', '玉树');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632701', 'YuShuShi', '玉树市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632722', 'ZaDuoXian', '杂多县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632723', 'ChengDuoXian', '称多县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632724', 'ZhiDuoXian', '治多县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632725', 'NangQianXian', '囊谦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632726', 'QuMaLaiXian', '曲麻莱县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6327DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '632800', 'HaiXi', '海西');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632801', 'GeErMuShi', '格尔木市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632802', 'DeLingHaShi', '德令哈市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632821', 'WuLanXian', '乌兰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632822', 'DuLanXian', '都兰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '632823', 'TianJunXian', '天峻县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6328DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '63CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '63CCDD', 'QiTa', '其他');

/*****************************青海 结束*****************************/

/*****************************宁夏 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '640000', 'NingXia', '宁夏', '宁');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '640100', 'YinChuan', '银川');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640104', 'XingQingQu', '兴庆区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640105', 'XiXiaQu', '西夏区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640106', 'JinFengQu', '金凤区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640121', 'YongNingXian', '永宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640122', 'HeLanXian', '贺兰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640181', 'LingWuShi', '灵武市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6401DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '640200', 'ShiZuiShan', '石嘴山');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640202', 'DaWuKouQu', '大武口区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640205', 'HuiNongQu', '惠农区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640221', 'PingLuoXian', '平罗县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6402DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '640300', 'WuZhong', '吴忠');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640302', 'LiTongQu', '利通区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640303', 'HongSiBaoQu', '红寺堡区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640323', 'YanChiXian', '盐池县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640324', 'TongXinXian', '同心县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640381', 'QingTongXiaShi', '青铜峡市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6403DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '640400', 'GuYuan', '固原');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640402', 'YuanZhouQu', '原州区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640422', 'XiJiXian', '西吉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640423', 'LongDeXian', '隆德县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640424', 'JingYuanXian', '泾源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640425', 'PengYangXian', '彭阳县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6404DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '640500', 'ZhongWei', '中卫');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640502', 'ShaPoTouQu', '沙坡头区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640521', 'ZhongNingXian', '中宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '640522', 'HaiYuanXian', '海原县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6405DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '64CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '64CCDD', 'QiTa', '其他');

/*****************************宁夏 结束*****************************/

/*****************************新疆 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '650000', 'XinJiang', '新疆', '新');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '650100', 'WuLuMuQi', '乌鲁木齐');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650102', 'TianShanQu', '天山区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650103', 'ShaYiBaKeQu', '沙依巴克区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650104', 'XinShiQu', '新市区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650105', 'ShuiMoGouQu', '水磨沟区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650106', 'TouTunHeQu', '头屯河区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650107', 'DaBanChengQu', '达坂城区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650109', 'MiDongQu', '米东区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650121', 'WuLuMuQiXian', '乌鲁木齐县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6501DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '650200', 'KeLaMaYi', '克拉玛依');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650202', 'DuShanZiQu', '独山子区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650203', 'KeLaMaYiQu', '克拉玛依区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650204', 'BaiJianTanQu', '白碱滩区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650205', 'WuErHeQu', '乌尔禾区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6502DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '650400', 'TuLuFan', '吐鲁番');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650402', 'GaoChangQu', '高昌区');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650421', 'ShanShanXian', '鄯善县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '650422', 'TuoKeXunXian', '托克逊县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6504DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '652200', 'HaMi', '哈密');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652201', 'HaMiShi', '哈密市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652222', 'BaLiKunHaSaKeZiZhiXian', '巴里坤哈萨克自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652223', 'YiWuXian', '伊吾县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6522DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '652300', 'ChangJi', '昌吉');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652301', 'ChangJiShi', '昌吉市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652302', 'FuKangShi', '阜康市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652323', 'HuTuBiXian', '呼图壁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652324', 'MaNaSiXian', '玛纳斯县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652325', 'QiTaiXian', '奇台县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652327', 'JiMuSaErXian', '吉木萨尔县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652328', 'MuLeiHaSaKeZiZhiXian', '木垒哈萨克自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6523DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '652700', 'BoErTaLa', '博尔塔拉');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652701', 'BoLeShi', '博乐市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652702', 'ALaShanKouShi', '阿拉山口市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652722', 'JingHeXian', '精河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652723', 'WenQuanXian', '温泉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6527DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '652800', 'BaYinGuoLeng', '巴音郭楞');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652801', 'KuErLeShi', '库尔勒市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652822', 'LunTaiXian', '轮台县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652823', 'YuLiXian', '尉犁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652824', 'RuoQiangXian', '若羌县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652825', 'QieMoXian', '且末县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652826', 'YanQiHuiZuZiZhiXian', '焉耆回族自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652827', 'HeJingXian', '和静县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652828', 'HeShuoXian', '和硕县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652829', 'BoHuXian', '博湖县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6528DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '652900', 'AKeSu', '阿克苏');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652901', 'AKeSuShi', '阿克苏市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652922', 'WenSuXian', '温宿县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652923', 'KuCheXian', '库车县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652924', 'ShaYaXian', '沙雅县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652925', 'XinHeXian', '新和县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652926', 'BaiChengXian', '拜城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652927', 'WuShenXian', '乌什县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652928', 'AWaTiXian', '阿瓦提县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '652929', 'KePingXian', '柯坪县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6529DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '653000', 'KeZiLeSu', '克孜勒苏');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653001', 'ATuShenShi', '阿图什市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653022', 'AKeTaoXian', '阿克陶县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653023', 'AHeQiXian', '阿合奇县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653024', 'WuQiaXian', '乌恰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6530DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '653100', 'KaShi', '喀什');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653101', 'KaShiShi', '喀什市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653121', 'ShuFuXian', '疏附县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653122', 'ShuLeXian', '疏勒县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653123', 'YingJiShaXian', '英吉沙县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653124', 'ZePuXian', '泽普县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653125', 'ShaCheXian', '莎车县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653126', 'YeChengXian', '叶城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653127', 'MaiGaiTiXian', '麦盖提县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653128', 'YuePuHuXian', '岳普湖县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653129', 'JiaShiXian', '伽师县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653130', 'BaChuXian', '巴楚县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653131', 'TaShenKuErGanTaJiKeZiZhiXian', '塔什库尔干塔吉克自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6531DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '653200', 'HeTian', '和田');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653201', 'HeTianShi', '和田市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653221', 'HeTianXian', '和田县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653222', 'MoYuXian', '墨玉县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653223', 'PiShanXian', '皮山县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653224', 'LuoPuXian', '洛浦县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653225', 'CeLeXian', '策勒县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653226', 'YuTianXian', '于田县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '653227', 'MinFengXian', '民丰县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6532DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '654000', 'YiLi', '伊犁');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654002', 'YiNingShi', '伊宁市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654003', 'KuiTunShi', '奎屯市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654004', 'HuoErGuoSiShi', '霍尔果斯市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654021', 'YiNingXian', '伊宁县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654022', 'ChaBuChaErXiBoZiZhiXian', '察布查尔锡伯自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654023', 'HuoChengXian', '霍城县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654024', 'GongLiuXian', '巩留县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654025', 'XinYuanXian', '新源县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654026', 'ZhaoSuXian', '昭苏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654027', 'TeKeSiXian', '特克斯县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654028', 'NiLeKeXian', '尼勒克县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6540DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '654200', 'TaCheng', '塔城');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654201', 'TaChengShi', '塔城市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654202', 'WuSuShi', '乌苏市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654221', 'EMinXian', '额敏县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654223', 'ShaWanXian', '沙湾县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654224', 'TuoLiXian', '托里县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654225', 'YuMinXian', '裕民县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654226', 'HeBuKeSaiErMengGuZiZhiXian', '和布克赛尔蒙古自治县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6542DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '654300', 'ALeTai', '阿勒泰');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654301', 'ALeTaiShi', '阿勒泰市');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654321', 'BuErJinXian', '布尔津县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654322', 'FuYunXian', '富蕴县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654323', 'FuHaiXian', '福海县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654324', 'HaBaHeXian', '哈巴河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654325', 'QingHeXian', '青河县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '654326', 'JiMuNaiXian', '吉木乃县');
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '6543DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '659001', 'ShiHeZi', '石河子');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '659001DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '659002', 'ALaEr', '阿拉尔');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '659002DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '659003', 'TuMuShuKe', '图木舒克');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '659003DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '659004', 'WuJiaQu', '五家渠');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '659004DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '65CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '65CCDD', 'QiTa', '其他');

/*****************************新疆 结束*****************************/

/*****************************台湾 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '710000', 'TaiWan', '台湾', '台');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '710100', 'TaiWan', '台湾');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '7101DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '71CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '71CCDD', 'QiTa', '其他');

/*****************************台湾 结束*****************************/

/*****************************香港 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '810000', 'XiangGang', '香港', '港');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '810100', 'XiangGang', '香港');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '8101DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '81CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '81CCDD', 'QiTa', '其他');

/*****************************香港 结束*****************************/

/*****************************澳门 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, '820000', 'AoMen', '澳门', '澳');
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '820100', 'AoMen', '澳门');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '8201DD', 'QiTa', '其他');

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, '82CCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, '82CCDD', 'QiTa', '其他');

/*****************************澳门 结束*****************************/

/*****************************其他 开始*****************************/
INSERT INTO sys_province(CountryId, Code, NameEN, NameCN, ShortNameCN) VALUES (@CountryId, 'CNPPPP', 'QiTa', '其他', NULL);
SET @ProvinceId = LAST_INSERT_ID();

INSERT INTO sys_city(ProvinceId, Code, NameEN, NameCN) VALUES (@ProvinceId, 'CNCCCC', 'QiTa', '其他');
SET @CityId = LAST_INSERT_ID();
INSERT INTO sys_district(CityId, Code, NameEN, NameCN) VALUES (@CityId, 'CNCCDD', 'QiTa', '其他');

/*****************************其他 结束*****************************/


