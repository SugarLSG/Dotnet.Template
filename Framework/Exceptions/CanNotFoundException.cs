﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("访问的资源不存在或已被删除")]
    public sealed class CanNotFoundException : APPBaseException
    {
        /// <summary>
        /// message: (0007) 访问的资源不存在或已被删除
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public CanNotFoundException(params object[] args)
        {
            Code = "0007";
            Args = args;
        }
    }
}
