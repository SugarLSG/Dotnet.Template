﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("签名验证失败")]
    public sealed class SignVerifyFailureException : APPBaseException
    {
        /// <summary>
        /// message: (1003) 签名验证失败
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public SignVerifyFailureException(params object[] args)
        {
            Code = "1003";
            Args = args;
        }
    }
}
