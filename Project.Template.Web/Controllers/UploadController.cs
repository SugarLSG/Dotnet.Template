﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Project.Template.Web.Controllers
{
    [AllowAnonymous]
    public class UploadController : Controller
    {
        public ActionResult All(HttpPostedFileBase fileData, string param1, string param2)
        {
            // do sth.

            //throw new Exception("上传文件测试异常");

            return new UploadFileResult(new
            {
                Guid = Guid.NewGuid().ToString("N"),
                Param1 = param1,
                Param2 = param2,
                CreatedTime = DateTime.Now
            });
        }
    }
}