﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("无效的凭证Id")]
    public sealed class InvalidAppIdException : APPBaseException
    {
        /// <summary>
        /// message: (1001) 无效的凭证Id
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public InvalidAppIdException(params object[] args)
        {
            Code = "1001";
            Args = args;
        }
    }
}
