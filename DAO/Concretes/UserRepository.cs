﻿using APP.Core.Enums;
using APP.Core.Models;
using APP.Core.Repositories;
using DAO.Abstracts;
using Framework.Models.User;
using Framework.Role;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Concretes
{
    public class UserRepository : IUserRepository
    {
        private IBaseRepository BaseRepository { get; set; }


        public PagingResult<SearchResult> Search(SearchCondition condition)
        {
            var sql = new StringBuilder(@"
SELECT UserId,
       Account,
       Name,
       Type,
       Status,
       CreatedTime
  FROM sys_user u
 WHERE     1 = 1");

            var parameters = new Dictionary<string, object>();
            if (condition.Account.HasValue())
            {
                sql.Append(" AND INSTR(Account, :account) > 0");
                parameters.Add("account", condition.Account);
            }
            if (condition.Name.HasValue())
            {
                sql.Append(" AND INSTR(Name, :name) > 0");
                parameters.Add("name", condition.Name);
            }
            if (condition.Type.HasValue)
            {
                sql.Append(" AND Type = :type");
                parameters.Add("type", condition.Type.Value);
            }
            if (condition.Status.HasValue)
            {
                sql.Append(" AND Status = :status");
                parameters.Add("status", condition.Status.Value);
            }
            if (condition.Role.HasValue)
            {
                sql.Append(" AND EXISTS (SELECT 1 FROM sys_user_role ur WHERE ur.UserId = u.UserId AND ur.RoleCode = :rolecode)");
                parameters.Add("rolecode", condition.Role.Value.ToString());
            }

            return BaseRepository.ExecutePagingList<SearchResult>(
                sql.ToString(),
                new Dictionary<string, Sorted> { { "CreatedTime", Sorted.Desc } },
                parameters,
                condition.FirstIndex,
                condition.Size
            );
        }

        public int UpdateUserRoles(int userId, IEnumerable<RoleCode> roles)
        {
            if (roles == null || !roles.Any())
                return 0;

            var listSqls = new List<string>();
            foreach (var role in roles)
                listSqls.Add(string.Format("(:userid, '{0}')", role.ToString()));

            return BaseRepository.ExecuteUpdate(string.Format(@"
DELETE FROM sys_user_role WHERE UserId = :userid;
INSERT INTO sys_user_role(UserId, RoleCode) VALUES {0};", string.Join(", ", listSqls)),
                new Dictionary<string, object> { { "userid", userId } }
            );
        }
    }
}
