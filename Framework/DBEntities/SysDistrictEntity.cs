﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Attributes;
using APP.Core.Extensions.Nhibernate;
using Newtonsoft.Json;
using Framework.Enums;
using NHibernate.Mapping.Attributes;
using NHibernate.Type;
using System;
using System.ComponentModel;

namespace Framework.DBEntity
{
    /// <summary>
    /// 地区
    /// </summary>
    [Class(0, Table = "sys_district")]
    [Cache(1, Usage = CacheUsage.ReadWrite)]
    public class SysDistrictEntity
    {
        public const string TABLE_NAME = "sys_district";
        public const string COL_DISTRICTID = "DistrictId";
        public const string COL_CITYID = "CityId";
        public const string COL_CODE = "Code";
        public const string COL_NAMEEN = "NameEN";
        public const string COL_NAMECN = "NameCN";
        public const string COL_CREATEDTIME = "CreatedTime";
        public const string COL_MODIFIEDTIME = "ModifiedTime";

        /// <summary>
        /// 地区Id
        /// </summary>
        [Description("地区Id")]
        [Id(0, Name = "DistrictId", Column = "DistrictId")]
        [Key(1)]
        [Generator(2, Class = "native")]
        public virtual int? DistrictId { get; set; }

        /// <summary>
        /// 城市Id
        /// </summary>
        [Description("城市Id")]
        [Property(Column = "CityId", NotNull = true)]
        public virtual int CityId { get; set; }

        private SysCityEntity _city = null;
        /// <summary>
        /// 外键 CityId 对应 sys_city 类实例;
        /// </summary>
        [IgnoreConvertTypeScalar]
        [JsonIgnore]
        public virtual SysCityEntity City { get { return _city ?? (_city = NHibernateExtension.GetEntityById<SysCityEntity>(CityId)); } }

        /// <summary>
        /// 编码
        /// </summary>
        [Description("编码")]
        [Property(Column = "Code", NotNull = true, Length = 8)]
        public virtual string Code { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        [Description("英文名")]
        [Property(Column = "NameEN", NotNull = true, Length = 256)]
        public virtual string NameEN { get; set; }

        /// <summary>
        /// 中文名
        /// </summary>
        [Description("中文名")]
        [Property(Column = "NameCN", NotNull = true, Length = 256)]
        public virtual string NameCN { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Property(Column = "CreatedTime", NotNull = true)]
        public virtual DateTime CreatedTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        [Property(Column = "ModifiedTime", NotNull = true)]
        public virtual DateTime ModifiedTime { get; set; }
    }
}
