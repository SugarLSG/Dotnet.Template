﻿namespace Project.Template.Web.Models.Geetest
{
    public class GeetestValidateInfo
    {
        public bool Online { get; set; }

        public string Challenge { get; set; }

        public string Validate { get; set; }

        public string Seccode { get; set; }
    }
}