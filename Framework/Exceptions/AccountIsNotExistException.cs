﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using APP.Core.Exceptions;
using System.ComponentModel;

namespace Framework.Exceptions
{
    [Description("账号不存在")]
    public sealed class AccountIsNotExistException : APPBaseException
    {
        /// <summary>
        /// message: (2002) 账号不存在
        /// </summary>
        /// <param name="args">请传入对应 message 所需参数</param>
        public AccountIsNotExistException(params object[] args)
        {
            Code = "2002";
            Args = args;
        }
    }
}
