﻿var Login = function () {
    "use strict";
    var runBoxToShow = function () {
        var el = $('.box-login');
        if (getParameterByName('box').length) {
            switch (getParameterByName('box')) {
                case "register":
                    el = $('.box-register');
                    break;
                case "forgot":
                    el = $('.box-forgot');
                    break;
                default:
                    el = $('.box-login');
                    break;
            }
        }
        el.show().addClass("animated flipInX").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            $(this).removeClass("animated flipInX");
        });
    };
    var runLoginButtons = function () {
        $('.forgot').on('click', function () {
            $('.box-login').removeClass("animated flipInX").addClass("animated bounceOutRight").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                $(this).hide().removeClass("animated bounceOutRight");

            });
            $('.box-forgot').show().addClass("animated bounceInLeft").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                $(this).show().removeClass("animated bounceInLeft");

            });
        });
        $('.register').on('click', function () {
            $('.box-login').removeClass("animated flipInX").addClass("animated bounceOutRight").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                $(this).hide().removeClass("animated bounceOutRight");

            });
            $('.box-register').show().addClass("animated bounceInLeft").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                $(this).show().removeClass("animated bounceInLeft");

            });

        });
        $('.go-back').click(function () {
            var boxToShow;
            if ($('.box-register').is(":visible")) {
                boxToShow = $('.box-register');
            } else {
                boxToShow = $('.box-forgot');
            }
            boxToShow.addClass("animated bounceOutLeft").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                boxToShow.hide().removeClass("animated bounceOutLeft");

            });
            $('.box-login').show().addClass("animated bounceInRight").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                $(this).show().removeClass("animated bounceInRight");

            });
        });
    };
    //function to return the querystring parameter with a given name.
    var getParameterByName = function (name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    return {
        //main function to initiate template pages
        init: function () {
            runBoxToShow();
            runLoginButtons();
        }
    };
}();

$(function () {
    Main.init();
    Login.init();

    var COOKIENAME_USERACCOUNT = 'USER.ACCOUNT',
        COOKIE_EXPIREDMINUTE = 7 * 24 * 60;

    //读取cokkies
    var userAccount = cookie.get(COOKIENAME_USERACCOUNT);
    if (userAccount) {
        $('#Account').val(userAccount.base64Decrypt()).valid();
        $('#Password').focus();
    } else {
        $('#Account').focus();
    }

    // 登录表单提交
    $('#form_login').appSubmit({
        onBeforeSend: function (requestData) {
            $('#btnLogin').attr({ disabled: true });
        },
        onSuccess: function (responseData) {
            if (responseData.result) {
                if ($('#remember').is(':checked')) {
                    cookie.set(COOKIENAME_USERACCOUNT, $('#Account').val().base64Encrypt(), COOKIE_EXPIREDMINUTE);
                } else {
                    cookie.remove(COOKIENAME_USERACCOUNT);
                }
                location.href = responseData.data;
            } else {
                alert(responseData.data && responseData.data.length ? responseData.data[0] : responseData.message);
                $('#btnLogin').removeAttr('disabled');
            }
        },
        onError: function () {
            $('#btnLogin').removeAttr('disabled');
        }
    });
});
