﻿using APP.Core.Caches;
using APP.Core.Enums;
using APP.Core.Utilities;
using Framework.Extensions;
using Framework.Utilities;
using System;
using System.IO;
using Topshelf;

namespace Project.Template.Task
{
    class Program
    {
        static void Main(string[] args)
        {
            // 检查 APP.Core 版本
            CoreConfig.CheckCoreVersion(CoreConfig.CoreVersion_1_10_94);

            // 生成配置
            ConfigExtension.GenerateTaskSpringConfig();

            // shared cache
            SharedCacheUtility.Configure(ConfigurationUtility.GetString(
                CommonSetting.APPSETTINGS_SHARED_CACHE_ENGINE).ToEnumNullable<CacheEngine>() ?? CacheEngine.MemoryCache,
                errorHandle: new Func<bool>(() =>
                {
                    // 发送邮件间隔1小时（以上）
                    if ((DateTime.Now - SharedCacheUtility.LastErrorHandleTime).TotalHours > 1)
                    {
                        try
                        {
                            BusinessMailUtility.SendSystemMailToManager(
                                "Redis异常通知",
                                string.Format(
                                    "{0} Redis连接异常（{1}），已切换至内存缓存，请尽快处理",
                                    CommonSetting.PROJECT_TASK_CODE,
                                    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                                )
                            );
                        }
                        catch { }

                        return true;
                    }

                    return false;
                })
            );

            // log
            LogUtility.Configure(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configs/log4net.xml"));

            // 附加任务
            HostFactory.Run(x =>
            {
                x.Service<QuartzServer>();

                var description = ConfigurationUtility.GetSectionString("taskSettings", "description");
                var displayName = ConfigurationUtility.GetSectionString("taskSettings", "displayName");
                var serviceName = ConfigurationUtility.GetSectionString("taskSettings", "serviceName");
                var systemEnvironment = CoreConfig.SystemEnvironment;
                if (systemEnvironment != SystemEnvironment.PRO)
                {
                    description += ("_" + systemEnvironment);
                    displayName += ("_" + systemEnvironment);
                    serviceName += ("_" + systemEnvironment);
                }
                x.SetDescription(description);
                x.SetDisplayName(displayName);
                x.SetServiceName(serviceName);

                x.EnablePauseAndContinue();
            });
        }
    }
}
