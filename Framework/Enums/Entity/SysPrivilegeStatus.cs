﻿// ========================================
// 本文件由模版生成，请勿修改！
// ========================================

using System.ComponentModel;

namespace Framework.Enums
{
    /// <summary>
    /// 状态
    /// ENUM[int]
    /// -1-Disabled-已禁用
    /// 1-Enabled-已启用
    /// </summary>
    public enum SysPrivilegeStatus
    {
        /// <summary>
        /// 已禁用
        /// </summary>
        [Description("已禁用")]
        Disabled = -1,
        /// <summary>
        /// 已启用
        /// </summary>
        [Description("已启用")]
        Enabled = 1,
    }
}
