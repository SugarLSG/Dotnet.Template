﻿using APP.Core.AOP.Attributes;
using APP.Core.Repositories;
using Framework.DBEntity;
using Framework.Enums;
using Service.Abstracts;
using System.Collections.Generic;
using System.Linq;

namespace Service.Concretes
{
    public class AreaService : IAreaService
    {
        private IBaseRepository BaseRepository { get; set; }


        [Cache]
        public IEnumerable<SysCountryEntity> AllCountries()
        {
            var countryList = BaseRepository.GetEntityList<SysCountryEntity>();
            return countryList.OrderBy(i => i.NameCN)
                .Where(i => i.NameCN != CommonSetting.NAME_OTHER_CN)
                .Concat(
                    countryList.Where(i => i.NameCN == CommonSetting.NAME_OTHER_CN)
                );
        }

        [Cache]
        public IEnumerable<SysProvinceEntity> AllProvinces()
        {
            var provinceList = BaseRepository.GetEntityList<SysProvinceEntity>();
            return provinceList.OrderBy(i => i.NameCN)
                .Where(i => i.NameCN != CommonSetting.NAME_OTHER_CN)
                .Concat(
                    provinceList.Where(i => i.NameCN == CommonSetting.NAME_OTHER_CN)
                );
        }

        [Cache]
        public IEnumerable<SysCityEntity> AllCities()
        {
            var cityList = BaseRepository.GetEntityList<SysCityEntity>();
            return cityList.OrderBy(i => i.NameCN)
                .Where(i => i.NameCN != CommonSetting.NAME_OTHER_CN)
                .Concat(
                    cityList.Where(i => i.NameCN == CommonSetting.NAME_OTHER_CN)
                );
        }

        [Cache]
        public IEnumerable<SysDistrictEntity> AllDistricts()
        {
            var districtList = BaseRepository.GetEntityList<SysDistrictEntity>();
            return districtList.OrderBy(i => i.NameCN)
                .Where(i => i.NameCN != CommonSetting.NAME_OTHER_CN)
                .Concat(
                    districtList.Where(i => i.NameCN == CommonSetting.NAME_OTHER_CN)
                );
        }


        [Cache]
        public object AllAreas()
        {
            return new
            {
                Continents = EnumExtension.GetEnumList<SysCountryContinentCode>().Select(i => new
                {
                    Code = i.ToString(),
                    Name = i.ToMessage()
                }),
                Countries = AllCountries().Select(i => new
                {
                    CountryId = i.CountryId,
                    Continent = i.ContinentCode.ToString(),
                    ISO = i.ISOCode3,
                    NameEN = i.NameEN,
                    NameCN = i.NameCN
                }),
                Provinces = AllProvinces().Select(i => new
                {
                    ProvinceId = i.ProvinceId,
                    CountryId = i.CountryId,
                    Code = i.Code,
                    NameEN = i.NameEN,
                    NameCN = i.NameCN,
                    ShortName = i.ShortNameCN
                }),
                Cities = AllCities().Select(i => new
                {
                    CityId = i.CityId,
                    ProvinceId = i.ProvinceId,
                    Code = i.Code,
                    NameEN = i.NameEN,
                    NameCN = i.NameCN
                }),
                Districts = AllDistricts().Select(i => new
                {
                    DistrictId = i.DistrictId,
                    CityId = i.CityId,
                    Code = i.Code,
                    NameEN = i.NameEN,
                    NameCN = i.NameCN
                })
            };
        }
    }
}
